/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        bodyColor: '#081c36',
        grayOverlay: '#00000080',
        error: '#ff424e',
        main: '#000000',
        whiteAB: '#ABABAB',
        yellowFFF4: '#fff432',
        gray4B: '#4B5264',
        gray8F: '#8f9090',
      },
    },
  },
  plugins: [],
};
