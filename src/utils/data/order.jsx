import * as Types from "../../constants/actionType";
export const orderStatuses = [
  {
    status: Types.WAITING_FOR_PROGRESSING,
    title: "Chờ xử lý",
    color: "#f6c23e",
  },
  {
    status: Types.PACKING,
    title: "Đang chuẩn bị hàng",
    color: "#f6c23e",
  },
  {
    status: Types.OUT_OF_STOCK,
    title: "Hết hàng",
    color: "#e74a3b",
  },
  {
    status: Types.USER_CANCELLED,
    title: "Shop huỷ",
    color: "#e74a3b",
  },
  {
    status: Types.CUSTOMER_CANCELLED,
    title: "Khách đã hủy",
    color: "#e74a3b",
  },
  {
    status: Types.SHIPPING,
    title: "Đang giao hàng",
    color: "#f6c23e",
  },
  {
    status: Types.DELIVERY_ERROR,
    title: "Lỗi giao hàng",
    color: "#e74a3b",
  },
  {
    status: Types.CUSTOMER_RETURNING,
    title: "Chờ trả hàng",
    color: "#5a5c69",
  },
  {
    status: Types.CUSTOMER_HAS_RETURNS,
    title: "Đã trả hàng",
    color: "#5a5c69",
  },
  {
    status: Types.WAIT_FOR_PAYMENT,
    title: "Đợi thanh toán",
    color: "#f6c23e",
  },
  {
    status: Types.COMPLETED,
    title: "Đã hoàn thành",
    color: "#1cc88a",
  },
  {
    status: Types.RECEIVED_PRODUCT,
    title: "Đã nhận hàng",
    color: "#1cc88a",
  },
];
export const paymentStatuses = [
  {
    status: Types.PAYMENT_TYPE_CASH,
    title: "Tiền mặt",
    color: "#f6c23e",
  },
  {
    status: Types.PAYMENT_TYPE_SWIPE,
    title: "Quẹt",
    color: "#5a5c69",
  },
  {
    status: Types.PAYMENT_TYPE_COD,
    title: "COD",
    color: "#e74a3b",
  },
  {
    status: Types.PAYMENT_TYPE_TRANSFER,
    title: "Chuyển khoản",
    color: "#1179da",
  },
  {
    status: Types.PAYMENT_TYPE_ELECTRONIC_WALLET,
    title: "Ví điện tử",
    color: "#1cc88a",
  },
];
