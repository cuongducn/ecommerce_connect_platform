import moment from "moment";

export const formatDateStr = (date, format = "YYYY-MM-DD HH:mm:ss") => {
  return moment(date).format(format);
};

export const formatDateObj = (date) => {
  return moment(date).toDate();
};

export function handleRealTime(date) {
  const newDate = new Date(date);
  const nowDate = new Date();

  const currentDate = nowDate.getTime() - newDate.getTime();
  const minutes = Math.floor((currentDate / 1000 / 60) % 60);
  const hours = Math.floor((currentDate / 1000 / 60 / 60) % 24);
  const days = Math.floor(currentDate / 1000 / 60 / 60 / 24);
  const time =
    days > 0
      ? days + " d"
      : hours > 0
      ? hours + " h"
      : minutes > 0
      ? minutes + " m"
      : "Just now";
  return time;
}
