import { uploadImage } from "../actions/upload";

export const handleImageUploadBefore = (
  files,
  information,
  core,
  uploadHandler
) => {
  const formImage = new FormData();
  formImage.append("image", files[0]);

  uploadImage(formImage).then((image_url) => {
    if (image_url) {
      const response = {
        result: [
          {
            url: image_url,
            name: files[0].name,
            size: files[0].size,
          },
        ],
      };
      uploadHandler(response);
    } else {
      uploadHandler(null);
    }
  });
};
