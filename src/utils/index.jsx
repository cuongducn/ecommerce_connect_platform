import { regexs } from './regexs';
import slugify from 'slugify';
import { actionsLocalStorage } from './actionsLocalStorage';
export const formatNumber = (str, separator = '.') => {
  if (str === undefined || str === null) return str;
  const strFormat = str
    .toString()
    .replace(/[A-Za-z`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/g, '');
  if (Number(strFormat) >= 1000) {
    return strFormat
      .split('')
      .reverse()
      .reduce((prev, next, index) => {
        return (index % 3 ? next : next + separator) + prev;
      });
  } else if (Number(strFormat) > 0 && Number(strFormat) < 1000) {
    return Number(strFormat);
  } else {
    return '';
  }
};

export const combineAddress = (addressDetail, wards, district, province) => {
  return `${
    addressDetail && (wards || district || province)
      ? `${addressDetail}, `
      : addressDetail
      ? `${addressDetail}`
      : ''
  }${wards && (district || province) ? `${wards}, ` : wards ? `${wards}` : ''}${
    district && province ? `${district}, ` : district ? `${district}` : ''
  }${province ? `${province}` : ''}`;
};

export const isEmail = (email) => {
  const regexEmail = regexs.email;
  const isCheckedEmail = regexEmail.test(email);
  return isCheckedEmail;
};
export const isNumberString = (numberString) => {
  return numberString;
};

export const removeAscent = (str) => {
  if (str === null || str === undefined) return str;
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  return str;
};

export const createSlug = (str, id) => {
  let slug;

  slug = slugify(id ? str + ` ${id}` : str, {
    replacement: '-',
    remove: undefined,
    lower: true,
    strict: false,
    locale: 'vi',
    trim: true,
  });

  return slug;
};

export const getIdSlug = (str) => {
  return str?.split('-').slice(-1)[0];
};

export const getSearchParams = (key, data) => {
  const searchParams = window.location.search;
  let newSearchParams = searchParams;
  if (key) {
    if (searchParams?.includes(key?.toString())) {
      newSearchParams = searchParams
        ?.split('&')
        ?.reduce((prevKey, currentKey, index) => {
          const newKey = currentKey.includes(key?.toString())
            ? `${key}=${data}`
            : currentKey;
          return (
            prevKey +
            `${
              index === searchParams?.split('&')?.length - 1
                ? newKey
                : `${newKey}&`
            }`
          );
        }, '');
    }
  }
  return newSearchParams ? newSearchParams?.replace(/\?/g, '') : '';
};
export const setLocationBeforeLogin = () => {
  const path = `${window.location.pathname}${
    window.location.search ? window.location.search : ''
  }`;
  actionsLocalStorage.setItem('locationBefore', path);
};
export const getLocationBeforeLogin = () => {
  const path = actionsLocalStorage.getItem('locationBefore');
  return path;
};

export const randomString = (length) => {
  var result = '';
  var characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};
export const getQueryParams = (name) => {
  return new URLSearchParams(window ? window.location.search : {}).get(name);
};

export const isRoleAdmin = () => {
  const pathName = window.location.pathname;
  const notIncludesPath = [
    '/admin/khong-tim-thay-trang',
    '/admin/loi-he-thong',
  ];
  return pathName.includes('/admin') && !notIncludesPath.includes(pathName);
};

export const isRoleCus = () => {
  const pathName = window.location.pathname;
  const notIncludesPath = [
    '/admin/khong-tim-thay-trang',
    '/admin/loi-he-thong',
  ];
  return pathName.includes('/c/') && !notIncludesPath.includes(pathName);
};
