import * as Types from "../constants/actionType";
export const setPathName = () => {
  const pathName = window.location.pathname;
  const seachName = window.location.search;
  localStorage.setItem("pathAppointment", `${pathName}${seachName}`);
};

export const getPathName = () => {
  return localStorage.getItem("pathAppointment") || "";
};

export const removePathName = () => {
  return localStorage.removeItem("pathAppointment");
};
export const getParamsPathName = () => {
  const pathName = getPathName();
  let type = "";
  let clinicId = "";
  if (pathName) {
    type = pathName.includes("/phong-kham")
      ? Types.TYPE_MEDICAL_PRIVATE_CLINIC
      : pathName.includes("/kham-tai-nha")
      ? Types.TYPE_MEDICAL_AT_HOME
      : pathName.includes("/kham-online")
      ? Types.TYPE_MEDICAL_ONLINE
      : "";
    if (type === Types.TYPE_MEDICAL_PRIVATE_CLINIC) {
      const searchParams =
        pathName.split("?")?.length > 0 ? pathName.split("?")[1] : "";
      if (searchParams) {
        const params = new URLSearchParams(searchParams);
        clinicId = params.get("clinicId");
      }
    }
    return {
      type: type,
      clinicId: clinicId,
    };
  }
  return {
    type: "",
    clinicId: "",
  };
};

export const stepAppointment = {
  getPathName,
  setPathName,
  removePathName,
  getParamsPathName,
};
