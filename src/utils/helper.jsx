import { v4 } from 'uuid';
import Medicine from '../assets/medicine.png';
import MedicalRecord from '../assets/medical-record.png';
import Hospital from '../assets/hospital.png';
import MedicalExaminationHome from '../assets/medical_examination_home.png';
import MedicalExaminationOnline from '../assets/medical_examination_online.png';
import IconUser from '../components/icon/user/IconUser';
import IconUserPatient from '../components/icon/user/IconUserPatient';
import IconCalendar from '../components/icon/calendar/IconCalendar';
import IconWallet from '../components/icon/wallet/IconWallet';
import IconBuble from '../components/icon/buble/IconBuble';
import IconCameraVideo from '../components/icon/cameraVideo/IconCameraVideo';
import * as Types from '../constants/actionType';
export const navbarMenu = [
  {
    id: v4(),
    name: 'ALL ITEMS',
    url: '/p/for-him',
    // childs: [
    //   {
    //     id: v4(),
    //     name: 'Khám tại phòng khám',
    //     url: '/tim-phong-kham',
    //     type: Types.TYPE_MEDICAL_PRIVATE_CLINIC,
    //   },
    //   {
    //     id: v4(),
    //     name: 'Khám tại nhà',
    //     url: '/kham-tai-nha/dat-hen',
    //     type: Types.TYPE_MEDICAL_AT_HOME,
    //   },
    //   {
    //     id: v4(),
    //     name: 'Khám online',
    //     url: '/kham-online/dat-hen',
    //     type: Types.TYPE_MEDICAL_ONLINE,
    //   },
    // ],
  },
  {
    id: v4(),
    name: 'BEST',
    url: '/mua-thuoc',
    // childs: [],
  },
  {
    id: v4(),
    name: 'ƯU ĐÃI',
    url: '/ho-so-suc-khoe',
    // childs: [],
  },
  {
    id: v4(),
    name: 'BLOG',
    url: '/blog',
    // childs: [],
  },
];

export const subNavbarMenu = [
  {
    id: v4(),
    name: 'Tìm kiếm',
    url: '/ho-so-suc-khoe',
    childs: [],
  },
  {
    id: v4(),
    name: 'Giỏ hàng',
    url: '/ho-so-suc-khoe',
    childs: [],
  },
];

export const getQueryParams = (name) => {
  return new URLSearchParams(window ? window.location.search : {}).get(name);
};

export const choice = [
  {
    id: v4(),
    name: 'Khám tại phòng khám',
    title: 'Đặt khám ưu tiên tại phòng khám',
    icon: Hospital,
    url: '/tim-phong-kham',
    type: Types.TYPE_MEDICAL_PRIVATE_CLINIC,
  },
  {
    id: v4(),
    name: 'Khám tại nhà',
    title: 'Đặt khám tại nhà uy tín và nhanh chóng',
    icon: MedicalExaminationHome,
    url: '/kham-tai-nha/dat-hen',
    type: Types.TYPE_MEDICAL_AT_HOME,
  },
  {
    id: v4(),
    name: 'Khám online',
    title: 'Khám từ xa với bác sĩ qua video call',
    icon: MedicalExaminationOnline,
    url: '/kham-online/dat-hen',
    type: Types.TYPE_MEDICAL_ONLINE,
  },
  {
    id: v4(),
    name: 'Tư vấn miễn phí',
    title: 'Tư vấn miễn phí với bác sĩ qua chat',
    icon: MedicalExaminationOnline,
    url: '/tu-van-mien-phi',
    type: -1,
  },
  {
    id: v4(),
    name: 'Hồ sơ sức khỏe',
    title: 'Xem và lưu trữ kết quả khám bệnh',
    icon: MedicalRecord,
    url: '/ho-so-suc-khoe',
    type: -1,
  },
  {
    id: v4(),
    name: 'Mua thuốc',
    title: 'Mua thuốc và đặt ship thuốc nhanh chóng',
    icon: Medicine,
    url: '/mua-thuoc',
    type: -1,
  },
];

export const genders = [
  {
    label: 'Nam',
    value: 1,
  },
  {
    label: 'Nữ',
    value: 2,
  },
  {
    label: 'Khác',
    value: 0,
  },
];

export const departments = [
  {
    label: 'Cấp cứu',
    value: 1,
  },
  {
    label: 'Hồi sức tích cực',
    value: 2,
  },
  {
    label: 'Lao hô hấp',
    value: 3,
  },
  {
    label: 'Nội tổng hợp',
    value: 4,
  },
  {
    label: 'Hô hấp',
    value: 5,
  },
  {
    label: 'Bệnh phổi mãn tính',
    value: 6,
  },
  {
    label: 'Nhi',
    value: 7,
  },
];

export const sidebarMenu = [
  {
    id: v4(),
    name: 'Thông tin cá nhân',
    icon: <IconUser className="w-6 h-6"></IconUser>,
    url: '/c/profile',
    childs: [],
  },
  {
    id: v4(),
    name: 'Đơn hàng',
    icon: <IconCalendar className="w-6 h-6"></IconCalendar>,
    url: '/c/order',
    childs: [],
  },
  {
    id: v4(),
    name: 'Chat với support',
    icon: <IconBuble className="w-6 h-6"></IconBuble>,
    url: '/c/chat-support',
    childs: [],
  },
  {
    id: v4(),
    name: 'Đăng xuất',
    icon: <IconBuble className="w-6 h-6"></IconBuble>,
    url: '/c/logout',
    childs: [],
  },
  // {
  //   id: v4(),
  //   name: 'Ví khách hàng',
  //   icon: <IconWallet className="w-6 h-6"></IconWallet>,
  //   url: '/vi-khach-hang',
  //   childs: [
  //     {
  //       id: v4(),
  //       name: 'Thông tin',
  //       url: '/vi-khach-hang/thong-tin',
  //     },
  //     {
  //       id: v4(),
  //       name: 'Lịch sử số dư',
  //       url: '/vi-khach-hang/lich-su-so-du',
  //     },
  //     {
  //       id: v4(),
  //       name: 'Lịch sử nạp rút',
  //       url: '/vi-khach-hang/lich-su-nap-rut',
  //     },
  //   ],
  // },
  // {
  //   id: v4(),
  //   name: 'Lịch khám',
  //   icon: <IconCalendar className="w-6 h-6"></IconCalendar>,
  //   url: '/lich-kham',
  //   childs: [],
  // },
  // {
  //   id: v4(),
  //   name: 'Hồ sơ sức khỏe',
  //   icon: <IconUserPatient className="w-6 h-6"></IconUserPatient>,
  //   url: '/ho-so-suc-khoe',
  //   childs: [],
  // },
];
