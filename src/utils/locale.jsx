import enGB from 'date-fns/locale/en-GB';

const Calendar = {
  sunday: 'Su',
  monday: 'Mo',
  tuesday: 'Tu',
  wednesday: 'We',
  thursday: 'Th',
  friday: 'Fr',
  saturday: 'Sa',
  ok: 'OK',
  today: 'Today',
  yesterday: 'Yesterday',
  hours: 'Hours',
  minutes: 'Minutes',
  seconds: 'Seconds',
  formattedMonthPattern: 'MMM yyyy',
  formattedDayPattern: 'dd MMM yyyy',
  dateLocale: enGB,
};

export const locale = {
  common: {
    loading: 'Đang tải...',
    emptyMessage: 'Không có kết quả',
  },
  Plaintext: {
    unfilled: 'Không được điền',
    notSelected: 'Không được lựa chọn',
    notUploaded: 'không được tải lên',
  },
  Pagination: {
    more: 'Hơn nữa',
    prev: 'Trước đõ',
    next: 'Tiếp theo',
    first: 'Đầu',
    last: 'Cuối',
    limit: '{0} / trang',
    total: 'Tổng số hàng: {0}',
    skip: 'Đi tới{0}',
  },
  Calendar,
  DatePicker: {
    ...Calendar,
  },
  DateRangePicker: {
    ...Calendar,
    last7Days: 'Last 7 Days',
  },
  Picker: {
    noResultsText: 'Không có kết quả',
    placeholder: 'Lựa chọn',
    searchPlaceholder: 'Tìm kiếm',
    checkAll: 'Tất cả',
  },
  InputPicker: {
    newItem: 'Mục mới',
    createOption: 'Tạo tùy chọn "{0}"',
  },
  Uploader: {
    inited: 'Mặc định',
    progress: 'Đang tải lên',
    error: 'Lỗi',
    complete: 'Đã hoàn thành',
    emptyFile: 'Trống rỗng',
    upload: 'Tải lên',
  },
  CloseButton: {
    closeLabel: 'Đóng',
  },
  Breadcrumb: {
    expandText: 'Mở đường dẫn',
  },
  Toggle: {
    on: 'Mở',
    off: 'Đóng',
  },
};
