const getItem = (key) => {
  return localStorage.getItem(key);
};
const setItem = (key, value) => {
  JSON.stringify(localStorage.setItem(key, value));
};
const removeItem = (key) => {
  localStorage.removeItem(key);
};
const clearItems = () => {
  localStorage.clear();
};

export const actionsLocalStorage = {
  getItem,
  setItem,
  removeItem,
  clearItems,
};
