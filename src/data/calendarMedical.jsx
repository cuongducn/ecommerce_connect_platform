import { callApi } from "../api";

export const getListAppointmentsMedical = (params) => {
  return callApi(
    `/customer/book_calendars${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
export const getAppointmentMedical = (idApoinment) => {
  return callApi(`/customer/book_calendars/${idApoinment}`, "get", null);
};
export const addAppointmentMedical = (data) => {
  return callApi(`/customer/book_calendars`, "post", data);
};
export const cancelAppointmentMedical = (idApoinment) => {
  return callApi(
    `/customer/book_calendars/${idApoinment}/cancel_book_calendar`,
    "put",
    null
  );
};
export const paymentAppointmentMedical = (id) => {
  return callApi(
    `/customer/book_calendars/${id}/payment_book_calendar`,
    "post",
    null
  );
};
