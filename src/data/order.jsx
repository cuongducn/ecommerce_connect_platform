import { callApi } from '../api';

export const getListOrders = (params) => {
  return callApi(`/customer/orders${params ? `?${params}` : ''}`, 'get', null);
};
export const getDetailOrder = (idOrder) => {
  return callApi(`/customer/orders/${idOrder}`, 'get', null);
};

export const getOrders = (params) => {
  return callApi(`/customer/orders${params ? `?${params}` : ''}`, 'get', null);
};

export const getOrder = (order_cde) => {
  return callApi(`/customer/orders/${order_cde}`, 'get', null);
};

export const addOrder = (data) => {
  return callApi(`/customer/orders`, 'post', data);
};
