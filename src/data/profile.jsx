import { callApi } from "../api";

export const getProfile = () => {
  return callApi("/customer/profile", "get", null);
};
export const updateProfile = (form) => {
  return callApi("/customer/profile", "put", form);
};
