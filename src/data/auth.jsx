import { callApi } from '../api';

export const loginSocial = (from) => {
  return callApi('/customer/redirect/' + from, 'get', null);
};
export const login = (form) => {
  return callApi('/customer/login', 'post', form);
};
export const register = (form) => {
  return callApi('/customer/register', 'post', form);
};
export const checkExistAccount = (form) => {
  return callApi('/customer/check_exists', 'post', form);
};
export const resetPassword = (form) => {
  return callApi('/customer/reset_password', 'post', form);
};
export const checkExistAccountAdmin = (form) => {
  return callApi('/admin/check_exists', 'post', form);
};
