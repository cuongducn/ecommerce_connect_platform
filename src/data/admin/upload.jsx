import { callApi, upload } from "../../api";

const formData = new FormData();

export const uploadMedia = (file, store_code) => {
  formData.set("image", file);
  if (store_code)
    return callApi(`/store/${store_code}/images`, "post", formData);

  return upload(`/image-upload`, "post", formData);
};
export const uploadVideo = (file, store_code) => {
  formData.set("video", file);
  if (store_code) return callApi(`/store/${store_code}/videos`, "post", file);

  return upload(`/video-upload`, "post", formData);
};
