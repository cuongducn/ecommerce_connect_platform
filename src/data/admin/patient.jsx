import { callApi } from "../api";

export const getAllPatients = (queryString) => {
  return callApi(
    `/admin/customers${queryString ? `?${queryString}` : ""}`,
    "get",
    null
  );
};
