import { callApiAdmin } from "../../api";

export const listBlogs = (data) => {
  return callApiAdmin(`/admin/blogs`, "get", null, data);
};

export const detailBlogs = (id) => {
  return callApiAdmin(`/admin/blogs/${id}`, "get");
};

export const updateBlogs = ({ id, ...data }) => {
  return callApiAdmin(`/admin/blogs/${id}`, "put", data);
};

export const createBlogs = (data) => {
  return callApiAdmin(`/admin/blogs`, "post", data);
};
export const deleteBlogs = (blogs_ids) => {
  return callApiAdmin(`/admin/blogs`, "delete", blogs_ids);
};
