import { callApi } from "../api";

export const getProfile = () => {
  return callApi("/admin/profile", "get", null);
};
export const updateProfile = (form) => {
  return callApi("/admin/profile", "put", form);
};
