import { callApiAdmin } from '../../api';

export const listCategory = (data) => {
  return callApiAdmin(`/admin/categories`, 'get', null, data);
};

export const detailCategory = (id) => {
  return callApiAdmin(`/admin/categories/${id}`, 'get');
};

export const UpdateCategory = ({ id, ...data }) => {
  return callApiAdmin(`/admin/categories/${id}`, 'put', data);
};

export const createCategory = (data) => {
  return callApiAdmin(`/admin/categories`, 'post', data);
};

export const deleteCategory = (category_ids) => {
  return callApiAdmin(`/admin/categories`, 'delete', category_ids);
};

export const getCategeryDetail = (idCategory) => {
  return callApiAdmin(`/admin/categories/${idCategory}`, 'get', null);
};
export const updateCategory = (idCategory, data) => {
  return callApiAdmin(`/admin/categories/${idCategory}`, 'put', data);
};

export const listCategories = (queryString) => {
  return callApiAdmin(
    `/admin/categories${queryString ? `?${queryString}` : ''}`,
    'get',
    null
  );
};
export const listCatogory = (data) => {
  return callApiAdmin(`/admin/categories`, 'get', null, data);
};

export const detailCatogory = (id) => {
  return callApiAdmin(`/admin/products/${id}`, 'get');
};

export const UpdateCatogory = ({ id, ...data }) => {
  return callApiAdmin(`/admin/products/${id}`, 'put', data);
};

export const createCatogory = (data) => {
  return callApiAdmin(`/admin/products`, 'post', data);
};

export const deleteCatogory = (products_ids) => {
  return callApiAdmin(`/products`, 'delete', products_ids);
};

export const getCategoryDetail = (idCategory) => {
  return callApiAdmin(`/admin/categories/${idCategory}`, 'get', null);
};
