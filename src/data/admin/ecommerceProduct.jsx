import { callApiAdmin } from '../../api';

export const getListEcommerceProducts = (params) => {
  return callApiAdmin(
    `/admin/ecommerce/db/products${params ? `?${params}` : ''}`,
    'get',
    null
  );
};

export const updateEcommerceProduct = (id, data) => {
  return callApiAdmin(`/admin/ecommerce/connect/${id}`, 'put', data);
};

export const deleteEcommerceProduct = (data) => {
  return callApiAdmin(`/admin/ecommerce/connect`, 'delete', data);
};
