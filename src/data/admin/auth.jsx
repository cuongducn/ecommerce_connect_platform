import { callApi } from '../../api';

export const postLogin = (form) => {
  return callApi('/admin/login', 'post', form);
};
export const resetPassword = (form) => {
  return callApi('/admin/reset_password', 'post', form);
};
export const changePassword = (form) => {
  return callApi('/admin/change_password', 'post', form);
};
