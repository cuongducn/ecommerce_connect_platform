import { callApi } from "../api";

export const getListDepartments = (queryString) => {
  return callApi(
    `/admin/medical_departments${queryString ? `?${queryString}` : ""}`,
    "get",
    null
  );
};
export const getDepartmentDetail = (idDepartment) => {
  return callApi(`/admin/medical_departments/${idDepartment}`, "get", null);
};
export const addDepartment = (data) => {
  return callApi(`/admin/medical_departments`, "post", data);
};
export const updateDepartment = (idDepartment, data) => {
  return callApi(`/admin/medical_departments/${idDepartment}`, "put", data);
};
export const deleteDepartment = (idDepartment) => {
  return callApi(`/admin/medical_departments/${idDepartment}`, "delete", null);
};
