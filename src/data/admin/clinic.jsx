import { callApi } from "../api";

export const getClinics = (queryString) => {
  return callApi(
    `/admin/clinics${queryString ? `?${queryString}` : ""}`,
    "get",
    null
  );
};
export const getClinicDetail = (idClinic) => {
  return callApi(`/admin/clinics/${idClinic}`, "get", null);
};
export const updateClinic = (idClinic, data) => {
  return callApi(`/admin/clinics/${idClinic}`, "put", data);
};

export const getPacketMedicals = (queryString) => {
  return callApi(
    `/admin/packet_medicals${queryString ? `?${queryString}` : ""}`,
    "get",
    null
  );
};
export const getPacketMedical = (idPacket) => {
  return callApi(`/admin/packet_medicals/${idPacket}`, "get", null);
};
export const addPacketMedical = (data) => {
  return callApi(`/admin/packet_medicals`, "post", data);
};
export const updatePacketMedical = (idPacket, data) => {
  return callApi(`/admin/packet_medicals/${idPacket}`, "put", data);
};
export const deletePacketMedical = (idPacket) => {
  return callApi(`/admin/packet_medicals/${idPacket}`, "delete", null);
};
