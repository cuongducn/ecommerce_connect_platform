import { callApi } from "../api";

export const getSetup = () => {
  return callApi("/admin/admin_configs", "get", null);
};
export const updateSetup = (form) => {
  return callApi("/admin/admin_configs", "post", form);
};
