import { callApiAdmin } from "../../api";

export const getListOrders = (params) => {
  return callApiAdmin(
    `/admin/orders${params ? `?${params}` : ""}`,
    "get",
    null
  );
};

export const getOneOrder = (id) => {
  return callApiAdmin(`/admin/orders/${id}`, "get", null);
};

export const updateStatusOrder = (id, data) => {
  return callApiAdmin(`/admin/orders/${id}/order_status`, "put", data);
};

export const updateOrder = (id, data) => {
  return callApiAdmin(`/admin/orders/${id}`, "put", data);
};

export const deleteOrder = (data) => {
  return callApiAdmin(`/admin/orders`, "delete", data);
};

export const exportExcel = () => {
  return callApiAdmin(`/admin/orders/export_excel`, "get", null);
};

export const exportBills = (id) => {
  return callApiAdmin(`/admin/orders/${id}/export_bills`, "get", null);
};
