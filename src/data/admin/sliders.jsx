import { callApiAdmin } from "../../api";

export const listSliders = (data) => {
  return callApiAdmin(`/admin/sliders`, "get", null, data);
};

export const detailSliders = (id) => {
  return callApiAdmin(`/admin/sliders/${id}`, "get");
};

export const updateSliders = ({ id, ...data }) => {
  return callApiAdmin(`/admin/sliders/${id}`, "put", data);
};

export const createSliders = (data) => {
  return callApiAdmin(`/admin/sliders`, "post", data);
};

export const deleteSliders = (sliders_ids) => {
  return callApiAdmin(`/admin/sliders`, "delete", sliders_ids);
};
