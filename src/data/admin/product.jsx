import { callApiAdmin } from "../../api";

export const getListProducts = (params) => {
  return callApiAdmin(
    `/admin/products${params ? `?${params}` : ""}`,
    "get",
    null
  );
};

export const getOneProduct = (id) => {
  return callApiAdmin(`/admin/products/${id}`, "get", null);
};

export const addProduct = (data) => {
  return callApiAdmin(`/admin/products`, "post", data);
};

export const updateProduct = (id, data) => {
  return callApiAdmin(`/admin/products/${id}`, "put", data);
};

export const deleteProduct = (data) => {
  return callApiAdmin(`/admin/products`, "delete", data);
};

export const updateStatusProduct = (id, data) => {
  return callApiAdmin(`/admin/products/status_product/${id}`, "put", data);
};
