import { callApi } from "../api";

export const getDoctors = (queryString) => {
  return callApi(
    `/admin/info_doctors${queryString ? `?${queryString}` : ""}`,
    "get",
    null
  );
};
export const updateDoctor = (idDoctor, data) => {
  return callApi(`/admin/info_doctors/${idDoctor}`, "put", data);
};
