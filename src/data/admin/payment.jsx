import { callApi } from "../api";

export const getListHistoryPayment = (queryString) => {
  return callApi(
    `/admin/history_medicals${queryString ? `?${queryString}` : ""}`,
    "get",
    null
  );
};
export const confirmPayment = (id, data) => {
  return callApi(`/admin/history_medicals/${id}/confirm_payment`, "put", data);
};
