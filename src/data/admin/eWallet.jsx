import { callApi } from "../api";

export const getListDepositWithdrawPatient = (queryString) => {
  return callApi(
    `/admin/deposit_and_withdrawals${queryString ? `?${queryString}` : ""}`,
    "get",
    null
  );
};
export const getDetailDepositWithdrawPatient = (id) => {
  return callApi(`/admin/deposit_and_withdrawals/${id}`, "get", null);
};
export const confirmDepositWithdrawPatient = (id, data) => {
  return callApi(`/admin/deposit_and_withdrawals/${id}`, "put", data);
};

export const getListDepositWithdrawDoctor = (queryString) => {
  return callApi(
    `/admin/deposit_and_withdrawal_doctors${
      queryString ? `?${queryString}` : ""
    }`,
    "get",
    null
  );
};
export const getDetailDepositWithdrawDoctor = (id) => {
  return callApi(`/admin/deposit_and_withdrawal_doctors/${id}`, "get", null);
};
export const confirmDepositWithdrawDoctor = (id, data) => {
  return callApi(`/admin/deposit_and_withdrawal_doctors/${id}`, "put", data);
};

//Lịch sử số dư
export const getListHistoryBalance = (queryString) => {
  return callApi(
    `/admin/e_wallet_histories${queryString ? `?${queryString}` : ""}`,
    "get",
    null
  );
};
