import { callApi } from "../api";

export const getContacts = () => {
  return callApi("/admin/admin_contacts", "get", null);
};
export const updateContacts = (form) => {
  return callApi("/admin/admin_contacts", "post", form);
};
