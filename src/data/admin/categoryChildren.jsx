import { callApiAdmin } from '../../api';

export const listChildren = (data) => {
  return callApiAdmin(`/admin/category_children`, 'get', null, data);
};

export const detailChildren = (id) => {
  return callApiAdmin(`/admin/category_children/${id}`, 'get');
};

export const UpdateChildren = ({ id, ...data }) => {
  return callApiAdmin(`/admin/category_children/${id}`, 'put', data);
};

export const createChildren = (data) => {
  return callApiAdmin(`/admin/category_children`, 'post', data);
};

export const deleteChildren = (category_children_ids) => {
  return callApiAdmin(
    `/admin/category_children`,
    'delete',
    category_children_ids
  );
};
