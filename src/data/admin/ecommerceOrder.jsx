import { callApiAdmin } from '../../api';

export const getListEcommerceConnects = (params) => {
  return callApiAdmin(
    `/admin/ecommerce/connect/list${params ? `?${params}` : ''}`,
    'get',
    null
  );
};

export const updateEcommerceConnect = (id, data) => {
  return callApiAdmin(`/admin/ecommerce/connect/${id}`, 'put', data);
};

export const deleteEcommerceConnect = (data) => {
  return callApiAdmin(`/admin/ecommerce/connect`, 'delete', data);
};

export const updateStatusEcommerceConnect = (id, data) => {
  return callApiAdmin(
    `/admin/ecommerce/connect/status_product/${id}`,
    'put',
    data
  );
};
