import { callApi } from "../api";

export const getHistoryVideoCall = (params) => {
  return callApi(
    `/customer/history_video_calls${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
