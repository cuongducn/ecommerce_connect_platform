import { callApi } from '../api';

export const getProducts = (params) => {
  return callApi(
    `/customer/products${params ? `?${params}` : ''}`,
    'get',
    null
  );
};
export const getProduct = (idProduct) => {
  return callApi(`/customer/products/${idProduct}`, 'get', null);
};
