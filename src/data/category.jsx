import { callApi } from '../api';

export const category = (id) => {
  return callApi(`/customer/categories/${id}`, 'get');
};

export const categories = (queryString) => {
  return callApi(
    `/customer/categories${queryString ? `?${queryString}` : ''}`,
    'get',
    null
  );
};
