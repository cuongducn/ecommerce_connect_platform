import { callApi } from "../api";

export const getListHistoryMedical = (params) => {
  return callApi(
    `/customer/history_medicals${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
export const getDetailHistoryMedical = (idPacket) => {
  return callApi(`/customer/history_medicals/${idPacket}`, "get", null);
};
