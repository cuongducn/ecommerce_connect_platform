import { callApi } from "../api";

export const getListMessages = (idDoctor, params) => {
  return callApi(
    `/customer/messages/${idDoctor}${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
export const getChatConversation = (params) => {
  return callApi(
    `/customer/chat_conversations${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
export const sendMessage = (idDoctor, data) => {
  return callApi(`/customer/messages/${idDoctor}`, "post", data);
};
