import { callApi } from "../api";

export const getClinics = (params) => {
  return callApi(`/customer/clinics${params ? `?${params}` : ""}`, "get", null);
};
export const getClinic = (idClinic) => {
  return callApi(`/customer/clinics/${idClinic}`, "get", null);
};
export const getCalendarsClinic = (idClinic, params) => {
  return callApi(
    `/customer/clinic_calendars/${idClinic}${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
export const getDoctorsClinic = (idClinic, params) => {
  return callApi(
    `/customer/doctor_in_clinic/${idClinic}${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
