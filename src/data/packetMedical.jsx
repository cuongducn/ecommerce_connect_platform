import { callApi } from "../api";

export const getPacketsMedical = (params) => {
  return callApi(
    `/customer/packet_medicals${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
export const getPacketMedical = (idPacket) => {
  return callApi(`/customer/packet_medicals/${idPacket}`, "get", null);
};
