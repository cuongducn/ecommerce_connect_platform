import { callApi } from "../api";

export const getListDependPerson = (params) => {
  return callApi(
    `/customer/customer_depends${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
export const getDependPerson = (idDependPerson) => {
  return callApi(`/customer/customer_depends/${idDependPerson}`, "get", null);
};
export const addDependPerson = (form) => {
  return callApi(`/customer/customer_depends`, "post", form);
};
export const updateDependPerson = (idDependPerson, form) => {
  return callApi(`/customer/customer_depends/${idDependPerson}`, "put", form);
};
export const deleteDependPerson = (idDependPerson) => {
  return callApi(
    `/customer/customer_depends/${idDependPerson}`,
    "delete",
    null
  );
};
