import { callApi } from "../api";

export const getBadges = () => {
  return callApi("/customer/badges", "get", null);
};
