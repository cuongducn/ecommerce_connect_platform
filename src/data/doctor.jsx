import { callApi } from "../api";

export const getDoctors = (params) => {
  return callApi(`/customer/doctors${params ? `?${params}` : ""}`, "get", null);
};
export const getCalendarsInClinic = (params) => {
  return callApi(
    `/customer/calendar_doctor_clinic_works${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
export const getCalendarsIndividual = (params) => {
  return callApi(
    `/customer/calendar_doctor_personal_works${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
