import { callApi } from "../api";

export const getProvince = () => {
  return callApi("/place/vn/province/1", "get", null);
};
export const getDistrict = (idProvince) => {
  return callApi(`/place/vn/district/${idProvince}`, "get", null);
};
export const getWards = (idDistrict) => {
  return callApi(`/place/vn/wards/${idDistrict}`, "get", null);
};
