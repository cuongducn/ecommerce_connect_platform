import { callApi } from '../api';

export const getBlogs = (params) => {
  return callApi(`/customer/blogs${params ? `?${params}` : ''}`, 'get', null);
};
export const getDetailBlog = (slug) => {
  return callApi(`/customer/blogs/${slug}`, 'get', null);
};
