import { callApi } from '../api';

export const getHome = () => {
  return callApi(`/customer/home`, 'get', null);
};
