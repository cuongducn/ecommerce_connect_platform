import { callApi } from "../api";

export const getEWallet = () => {
  return callApi(`/customer/e_wallet`, "get", null);
};
export const getEWalletHistory = (params) => {
  return callApi(
    `/customer/e_wallet_histories${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
export const getListDepositWithdrawalsHistory = (params) => {
  return callApi(
    `/customer/e_wallet/deposit_and_withdrawals${params ? `?${params}` : ""}`,
    "get",
    null
  );
};
export const getDetailDepositWithdrawalsHistory = (idHistory) => {
  return callApi(
    `/customer/e_wallet/deposit_and_withdrawals/${idHistory}`,
    "get",
    null
  );
};
export const cancelDepositWithdrawals = (idHistory, data) => {
  return callApi(
    `/customer/e_wallet/deposit_and_withdrawals/${idHistory}`,
    "put",
    data
  );
};
export const requireCancelDepositWithdrawals = (data) => {
  return callApi(`/customer/e_wallet/deposit_and_withdrawals`, "post", data);
};
