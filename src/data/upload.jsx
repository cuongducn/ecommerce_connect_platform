import { upload } from "../api";

export const uploadImage = (form) => {
  return upload("/image-upload", "post", form);
};
