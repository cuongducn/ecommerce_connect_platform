import axios from "axios";
import { actionsLocalStorage } from "../utils/actionsLocalStorage";
import * as apiType from "../constants/apiType";
import history from "../history";
import { setLocationBeforeLogin } from "../utils";
const exceptPrefix = [
  "/login",
  "/register",
  "/reset_password",
  "/check_exists",
  "/send_email_otp",
  "/send_email_otp",
  "/send_otp",
];
const checkEndPoint = (endpoint) => {
  for (const prefix of exceptPrefix) {
    if (endpoint.includes(prefix)) {
      return true;
    }
  }
  return false;
};

export const callApi = (endPoint, method, body) => {
  if (checkEndPoint(endPoint) === false) {
    axios.interceptors.request.use(
      (config) => {
        const token = actionsLocalStorage.getItem("token");
        if (token) {
          config.headers["customer-token"] = token;
        }
        return config;
      },
      (error) => {
        return Promise.reject(error);
      }
    );
    axios.interceptors.response.use(
      (response) => {
        return response;
      },
      (error) => {
        if (error.response.data.code === 404) {
          window.location.replace("/khong-tim-thay-trang");
        } else if (error.response.data.code === 401) {
          setLocationBeforeLogin();
          localStorage.removeItem("token");
          history.push("/c/login");
        } else if (error.response.data.code === 500) {
          window.location.replace("/loi-he-thong");
        }
        return Promise.reject(error);
      }
    );
  }
  return axios({
    method,
    url: `${apiType.API_URL_HOST}${endPoint}`,
    data: body,
    headers: { "Content-Type": "application/json" },
  });
};

export const upload = (endPoint, method, body) => {
  axios.interceptors.request.use(
    (config) => {
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      if (error.response.data.code === 404) {
        window.location.replace("/khong-tim-thay-trang");
      } else if (error.response.data.code === 401) {
        // window.location.replace("/dang-nhap");
      } else if (error.response.data.code === 500) {
        window.location.replace("/loi-he-thong");
      }
      return Promise.reject(error);
    }
  );

  return axios({
    method,
    url: `${apiType.API_URL_DATA}${endPoint}`,
    data: body,
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
};

export const callApiAdmin = (endPoint, method, body) => {
  if (checkEndPoint(endPoint) === false) {
    axios.interceptors.request.use(
      (config) => {
        const token = actionsLocalStorage.getItem("admin-token");
        if (token) {
          config.headers["admin-token"] = token;
        }
        return config;
      },
      (error) => {
        return Promise.reject(error);
      }
    );
    axios.interceptors.response.use(
      (response) => {
        return response;
      },
      (error) => {
        if (error.response.data.code === 404) {
          // window.location.replace('admin/khong-tim-thay-trang');
        } else if (error.response.data.code === 401) {
          setLocationBeforeLogin();
          localStorage.removeItem("admin-token");
          history.push("/admin/login");
        } else if (error.response.data.code === 500) {
          window.location.replace("admin/loi-he-thong");
        }
        return Promise.reject(error);
      }
    );
  }
  return axios({
    method,
    url: `${apiType.API_URL_HOST}${endPoint}`,
    data: body,
    headers: { "Content-Type": "application/json" },
  });
};
