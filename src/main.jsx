import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { CustomProvider } from 'rsuite';
import { locale } from './utils/locale';
import rootReducers from './reducers';
import 'react-toastify/dist/ReactToastify.css';
import 'rsuite/dist/rsuite.min.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './styles/index.scss';

export const store = createStore(rootReducers, applyMiddleware(thunk));

ReactDOM.createRoot(document.getElementById('root')).render(
  <BrowserRouter>
    <Provider store={store}>
      <CustomProvider locale={locale}>
        <App />
      </CustomProvider>
      <ToastContainer></ToastContainer>
    </Provider>
  </BrowserRouter>
);
