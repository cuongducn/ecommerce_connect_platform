import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as blogActions from '../../actions/blog';
import { useEffect } from 'react';
import { Button } from 'antd';
import { useParams } from 'react-router-dom';

const DetailBlog = () => {
  const dispatch = useDispatch();
  const params = useParams();

  const { blog } = useSelector((state) => state.cusBlogsReducers);
  useEffect(() => {
    dispatch(blogActions.detailBlogs(params.slug));
  }, [dispatch]);

  return (
    <div className="px-[5vw] pb-[50px]">
      <div>
        <h1 className=" text-[1.4rem] uppercase mt-2 sm:mt-5 py-[20px]">
          {blog.title}
        </h1>
        <div
          className="landscape relative mb-[50px]"
          style={{ backgroundImage: `url(${blog.image})` }}
        ></div>
      </div>
      {/* <div>{blog.summary}</div> */}
      <div dangerouslySetInnerHTML={{ __html: blog.summary }} />
    </div>
  );
};

export default DetailBlog;
