import React from 'react';
import Slider from 'react-slick';
import styled from 'styled-components';
import IconArrowRight from '../../components/icon/arrow/IconArrowRight';
import IconArrowLeft from '../../components/icon/arrow/IconArrowLeft';
import { Button, Modal } from 'antd';
import { useState } from 'react';
import * as productActions from '../../actions/product';
import { Tabs } from 'antd';
import { useParams } from 'react-router-dom';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import ProductCard from '../../components/product/ProductCard';
import { formatNumber } from '../../utils';
import { useNavigate } from 'react-router-dom';
import { alert } from '../../utils/alerts';
import { actionsLocalStorage } from '../../utils/actionsLocalStorage';
import * as cartAction from '../../actions/cart';

const ProductDetailStyles = styled.div`
  .btn-custom_submit:hover {
    color: white;
    border: 1px solid black;
    font-weight: 500;
  }

  .ant-tabs-nav-list {
    justify-content: space-between;
    width: 100%;
  }

  .snap-start {
    scroll-snap-align: start;
  }
`;

const ProductDetail = () => {
  const navigate = useNavigate();
  const { slug } = useParams();
  const dispatch = useDispatch();
  const [isShowTableSize, setIsShowTableSize] = useState(false);
  const { product } = useSelector((state) => state.productReducers);
  const { totalItem } = useSelector((state) => state.cartReducers);
  const [priceShow, setPriceShow] = useState(0);
  const [property, setProperty] = useState({});
  const [idxProperty, setIdxProperty] = useState(0);
  const [idxSubProperty, setIdxSubProperty] = useState(0);
  const [subProperty, setSubProperty] = useState({});
  const [productStore, setProductStore] = useState({
    product: product,
    pProperty: null,
    pSubProperty: null,
  });

  useEffect(() => {
    dispatch(productActions.getProduct(slug));
  }, [dispatch]);

  useEffect(() => {
    setProductStore((prevState) => ({
      ...prevState,
      product: product,
    }));
    setPriceShow(product.price);
  }, [product]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const slideSubImgSettings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    vertical: true,
    verticalSwiping: true,
    draggable: true,
    arrows: false,
  };
  const slideMainImgSettings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    draggable: true,

    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };
  const productRelateSettings = {
    dots: true,
    // infinite: products?.data?.length > 4,
    speed: 800,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,

    beforeChange: (oldIndex, newIndex) => {
      // setIndexActive(newIndex);
    },
    customPaging: (i) => {
      return (
        <div className="pt-5">
          <div
          // className={`w-5 h-1 rounded-full ${
          //   i === indexActive ? 'bg-main' : 'bg-[rgba(38,166,154,0.3)]'
          // }`}
          ></div>
        </div>
      );
    },
    responsive: [
      {
        breakpoint: 768,
        settings: {
          // infinite: products?.data?.length > 2,
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 500,
        settings: {
          // infinite: products?.data?.length > 1,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const tabItems = [
    {
      key: '1',
      label: `Mô tả`,
      children: (
        <div
          style={{ overflow: 'auto', width: '100%', height: '100%' }}
          className="max-w-full product-detail_description"
          dangerouslySetInnerHTML={{ __html: product.description }}
        />
      ),
    },
    {
      key: '2',
      label: `Giao hàng và thanh toán`,
      children: (
        <div className="px-2 sm:px-4 ">
          <h3 className="capitalize font-semibold">chính sách giao hàng</h3>
          <table className="table-auto border-collapse border border-slate-500 mt-2">
            <thead>
              <tr>
                <th className="border uppercase text-xs p-2">khu vực</th>
                <th className="border uppercase text-xs p-2">phí giao hàng</th>
                <th className="border uppercase text-xs p-2">
                  thời gian vận chuyển
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="border p-2">
                  Nội thành Hà Nội &amp; TP. Hồ Chí Minh
                </td>
                <td className="border p-2">Đồng giá 30.000Đ</td>
                <td className="border p-2">1-2 ngày làm việc.</td>
              </tr>
              <tr>
                <td className="border p-2">
                  Ngoại thành Hà Nội &amp; TP. Hồ Chí Minh
                </td>
                <td className="border p-2">Đồng giá 30.000Đ</td>
                <td className="border p-2">3-7 ngày làm việc.</td>
              </tr>
              <tr>
                <td className="border p-2">Các tỉnh thành khác</td>
                <td className="border p-2">Đồng giá 30.000Đ </td>
                <td className="border p-2">5-7 ngày làm việc.</td>
              </tr>
            </tbody>
          </table>
          <h3 className="capitalize font-semibold mt-4">
            chính sách thanh toán
          </h3>
          <p>
            Khách hàng mua hàng tại ASAHAA có thể thanh toán bằng 3 hình thức
            sau:
          </p>
          <ul className="list-decimal px-4 py-2">
            <li className="text-xs p-2">
              Trả tiền khi nhận hàng (COD): khi nhận được hàng, người nhận hàng
              sẽ thanh toán trực tiếp cho người giao hàng. Khoản thanh toán bao
              gồm tiền hàng và phí giao hàng cho vận chuyển.
            </li>
            <li className="text-xs p-2">Thanh toán qua ví ShopeePay.</li>
            <li className="text-xs p-2">
              Thanh toán bằng thẻ ATM nội địa/thẻ thanh toán quốc tế Visa,
              MasterCard.
            </li>
          </ul>
        </div>
      ),
    },
    {
      key: '3',
      label: `Tips`,
      children: (
        <div className="px-4 sm:px-2 ">
          <h3 className="capitalize font-semibold ">bảo quản</h3>
          <p>*Lưu ý</p>
          <ul className="list-decimal px-4 py-2">
            <li className="text-xs p-2">
              Không để quần áo ở nơi ẩm và nên giặt ngay sau khi sử dụng để
              tránh ẩm mốc
            </li>
            <li className="text-xs p-2">
              Không giặt chung áo trắng với quần áo màu
            </li>
            <li className="text-xs p-2">
              Không nên giặt trong nước nóng quá 40 độ để tránh bị giãn và mất
              form
            </li>
            <li className="text-xs p-2">
              Không đổ trực tiếp bột giặt lên quần áo khi giặt để tránh bị phai
              và loang màu
            </li>
            <li className="text-xs p-2">
              Không ngâm trong xà phòng quá lâu để tránh bạc màu
            </li>
          </ul>
          <p>*Mẹo giữ quần áo lâu mới</p>
          <ul className="list-decimal px-4 py-2">
            <li className="text-xs p-2">
              Nên giặt áo bằng nước lạnh hoặc nước hơi ấm, nước nóng sẽ làm vải
              áo giãn ra
            </li>
            <li className="text-xs p-2">
              Phơi áo dưới nắng nhẹ, tránh nắng gắt để áo không bị bạc màu
            </li>
          </ul>
        </div>
      ),
    },
  ];

  //Custom Prev, Next
  function NextArrow(props) {
    const { onClick } = props;
    return (
      <div
        onClick={onClick}
        className="absolute right-4 flex items-center justify-center text-white border-gray-600 rounded-full w-11 h-11 top-1/2 bg-transparent cursor-pointer -translate-y-1/2 z-10 hover:border-gray-200 transition-all"
      >
        <IconArrowRight className="w-11 h-11"></IconArrowRight>
      </div>
    );
  }

  function PrevArrow(props) {
    const { onClick } = props;
    return (
      <div
        onClick={onClick}
        className="absolute left-4 flex items-center justify-center text-white border-gray-600 rounded-full w-11 h-11 top-1/2 bg-transparent cursor-pointer -translate-y-1/2 z-10 hover:border-gray-200 transition-all"
      >
        <IconArrowLeft className="w-11 h-11"></IconArrowLeft>
      </div>
    );
  }

  const addToCart = () => {
    let newProduct = {};

    const newListProperty = (productStore.pProperty = {
      ...productStore.pProperty,
      product_sub_property: productStore.pSubProperty,
    });

    if (productStore?.product?.properties !== null) {
      const newProperty = {
        id: productStore?.product?.properties?.id,
        product_id: productStore?.product?.properties?.product_id,
        property_name: productStore?.product?.properties?.property_name,
        sub_property_name: productStore?.product?.properties?.sub_property_name,
        list_properties: newListProperty,
      };
      newProduct = {
        ...productStore.product,
        properties: newProperty,
        quantity: 1,
      };
    } else {
      newProduct = {
        ...productStore.product,
        properties: null,
        quantity: 1,
      };
    }

    delete newProduct.product_relate;
    delete newProduct.like;
    delete newProduct.updated_at;
    delete newProduct.quantity_in_stock;
    if (
      productStore?.product?.properties !== null &&
      productStore?.product?.properties?.property_name !== null &&
      productStore.pProperty === null
    ) {
      alert.error(
        `Vui lòng chọn ${
          productStore?.product?.properties?.property_name || 'Màu'
        } sản phẩm`
      );
    }
    if (
      productStore?.product?.properties !== null &&
      productStore?.product?.properties?.sub_property_name !== null &&
      productStore.pSubProperty === null
    ) {
      alert.error(
        `Vui lòng chọn ${
          productStore?.product?.properties?.sub_property_name || 'kích thước'
        }  sản phẩm`
      );
    }
    const cart = JSON.parse(actionsLocalStorage.getItem('cart')) || {
      cartItems: [],
      totalMoney: 0,
      totalQuantity: 0,
      totalItem: 0,
    };
    const existingItemIndex = cart.cartItems.findIndex(
      (item) => item.id === newProduct?.id
    );
    if (existingItemIndex !== -1) {
      cart.cartItems[existingItemIndex].quantity += 1;
    } else {
      cart.cartItems.push(newProduct);
    }
    cart.totalItem = 0;
    cart.totalQuantity = 0;
    cart.cartItems.forEach((item) => {
      cart.totalItem += 1;
      if (
        item?.properties !== null &&
        item?.properties?.property_name !== null
      ) {
        if (item?.properties?.sub_property_name !== null) {
          cart.totalQuantity += item.quantity;
          cart.totalMoney =
            item?.properties?.list_properties?.product_sub_property?.price *
            cart.totalQuantity;
        } else {
          cart.totalQuantity += item.quantity;
          cart.totalMoney = item?.properties?.price * cart.totalQuantity;
        }
      }
    });
    // cartAction.addItemCart(newProduct)

    actionsLocalStorage.setItem('cart', JSON.stringify(cart));
    dispatch(cartAction.getCart());
  };

  const handleChooseColor = (e, p, idx) => {
    setProperty(p);
    setIdxProperty(idx);

    setProductStore((prevState) => ({
      ...prevState,
      pProperty: p,
    }));
  };
  const handleChoosePrice = (p, idx) => {
    setPriceShow(p.price);
    setSubProperty(p);
    setProductStore((prevState) => ({
      ...prevState,
      pSubProperty: p,
    }));
  };
  return (
    <ProductDetailStyles>
      <div className="grid grid-cols-12 gap-4 mx-[6vw] my-[10px] max-h-[120vh]">
        <div className="col-span-8 ">
          <div className="grid grid-cols-12 ">
            <div className="col-span-3 mr-[10px]">
              <Slider {...slideSubImgSettings}>
                {product?.image_link?.map((image, idx) => (
                  <div
                    key={idx}
                    className="snap-start p-sub_image"
                    style={{ width: '100%', display: 'inline-block' }}
                  >
                    {/* <img
                        className="w-full object-cover"
                        src={image.image}
                        alt=""
                      /> */}
                    <div
                      className="relative portrait bg-primary-100 bg-blend-multiply hover:bg-blend-normal"
                      style={{ backgroundImage: `url(${image})` }}
                    ></div>
                  </div>
                ))}
              </Slider>
            </div>
            <div className="col-span-9  ">
              <Slider {...slideMainImgSettings}>
                {product?.image_link?.map((image, idx) => (
                  // <div key={idx} className="ml-[-10px]">
                  //   <div>
                  //     <img
                  //       className="w-full  object-cover"
                  //       src={image.image}
                  //       alt=""
                  //     />
                  //   </div>
                  // </div>
                  <div
                    key={idx}
                    className=""
                    style={{ width: '100%', display: 'inline-block' }}
                  >
                    {/* <img
                      className="w-full object-cover"
                      src={image.image}
                      alt=""
                    /> */}
                    <div
                      className="relative portrait"
                      style={{ backgroundImage: `url(${image})` }}
                    ></div>
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </div>
        <div className="col-span-4">
          <h1 className="uppercase text-2xl">{product.name}</h1>
          <div className="flex items-center sm:my-4 gap-10 sm:w-full">
            <p className="text-3xl font-semibold">
              {formatNumber(priceShow, ',')}
            </p>
          </div>
          <div className="flex justify-between flex-col text-sm border border-dashed border-green-600 rounded my-4 p-4">
            <h5 className="font-bold text-md uppercase text-green-600 ">
              ƯU ĐÃI ĐỘC QUYỀN :
            </h5>
            <ul className="list-disc pl-4">
              <li className="text-sm">
                Freeship toàn quốc cho đơn hàng từ 599K.
              </li>
              <li className="text-sm">Đổi hàng trong vòng 30 ngày.</li>
              <li className="text-sm">
                Ưu đãi hạng thành viên giảm 15%, 20%, 25%.
              </li>
            </ul>
          </div>
          {product.properties != null ? (
            <>
              <div className="py-2">
                <h4 className="text-center justify-center items-center sm:justify-start capitalize mb-2 flex gap-2">
                  chọn{' '}
                  {product?.properties !== null
                    ? product?.properties?.property_name
                    : ''}{' '}
                  : {property.name}
                  <p className="font-semibold"></p>
                </h4>
                <div className="flex justify-center items-center sm:justify-start flex-wrap gap-4">
                  {product?.properties?.list_properties?.map((p, idx) => (
                    <div className="basis-1/5" key={idx}>
                      <button
                        className="w-full transition-all border disabled:grayscale
                    border-secondary focus:ring-2 focus:ring-gray-500"
                      >
                        <span
                          onClick={(e) => handleChooseColor(e, p, idx)}
                          className="square border"
                          value={p.name}
                          // style={{ backgroundImage: `url(${p.image_link})` }}
                          style={{
                            backgroundImage: `url(${p.image_url})`,
                          }}
                        ></span>
                      </button>
                    </div>
                  ))}
                </div>
              </div>

              <div className="py-2">
                <div className="flex justify-between items-center">
                  <h4 className="text-center capitalize sm:text-left">
                    chọn{' '}
                    {product?.properties !== null &&
                      product?.properties?.sub_property_name}
                  </h4>
                  <h5
                    className="border-b border-primary-500 flex items-center gap-2 cursor-pointer"
                    onClick={() => setIsShowTableSize(!isShowTableSize)}
                  >
                    Bảng size{' '}
                    <svg width="20" height="20" viewBox="0 0 256 256">
                      <path
                        fill="currentColor"
                        d="M232 76.7L179.3 24a16.1 16.1 0 0 0-22.6 0L90.3 90.3l-36 36L24 156.7a15.9 15.9 0 0 0 0 22.6L76.7 232a15.9 15.9 0 0 0 22.6 0L232 99.3a15.9 15.9 0 0 0 0-22.6ZM220.7 88L88 220.7L35.3 168L60 143.3l26.3 26.4a8.2 8.2 0 0 0 11.4 0a8.1 8.1 0 0 0 0-11.4L71.3 132L96 107.3l26.3 26.4a8.2 8.2 0 0 0 11.4 0a8.1 8.1 0 0 0 0-11.4L107.3 96L132 71.3l26.3 26.4a8.2 8.2 0 0 0 11.4 0a8.1 8.1 0 0 0 0-11.4L143.3 60L168 35.3L220.7 88Z"
                      ></path>
                    </svg>
                  </h5>
                </div>
                <div className="flex justify-center sm:justify-between items-center gap-2">
                  <ul className="flex items-center justify-center sm:justify-start gap-5 py-2">
                    {product?.properties?.list_properties[idxProperty][
                      'product_sub_property'
                    ] &&
                      product?.properties?.list_properties[idxProperty][
                        'product_sub_property'
                      ]?.map((p, idxC) => {
                        return (
                          <li key={p.id}>
                            <button
                              className="w-10 h-10 grid place-content-center border bg-white border-transparent focus:outline-none focus:ring-2 focus:ring-gray-500"
                              onClick={() => handleChoosePrice(p, idxC)}
                            >
                              <span>{p.name}</span>
                            </button>
                          </li>
                        );
                      })}
                  </ul>
                </div>
              </div>
            </>
          ) : null}

          <div className="py-5 mb-5">
            <Button
              className="w-[100%] bg-[black] text-[white] btn-custom_submit"
              size="large"
              onClick={() => addToCart()}
            >
              THÊM VÀO GIỎ
            </Button>
          </div>
          <div>
            <Tabs defaultActiveKey="1" items={tabItems} size="middle" />
          </div>
        </div>
      </div>
      <div className="p-4 border-t sm:p-16">
        <h3 className="uppercase text-sm sm:text-xl mb-4">
          CÓ THỂ BẠN SẼ THÍCH
        </h3>
        <div>
          {product?.product_relate?.length > 0 && (
            <Slider {...productRelateSettings}>
              {product?.product_relate?.map((product) => (
                <a
                  // onClick={() => navigate(`/product/${product.slug}`)}
                  href={`/product/${product.slug}`}
                  key={product.id}
                >
                  <ProductCard product={product} key={product.id} />
                </a>
              ))}
            </Slider>
          )}
        </div>
      </div>
      <Modal
        title="Bảng size"
        open={isShowTableSize}
        onCancel={() => setIsShowTableSize(!isShowTableSize)}
        closable={true}
        closeIcon={false}
        centered={true}
        maskClosable={true}
        footer={null}
        width={'70vw'}
      >
        <div className="flex w-[100%]">
          <img
            src="https://ssstutter.com/img/HER.jpg"
            alt=""
            className="w-[50%]"
          />
          <img
            src="https://ssstutter.com/img/HIM.jpg"
            alt=""
            className="w-[50%]"
          />
        </div>
      </Modal>
    </ProductDetailStyles>
  );
};

// export default ProductDetail;

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addItemCart: (product) => dispatch(cartAction.addItemCart(product)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
