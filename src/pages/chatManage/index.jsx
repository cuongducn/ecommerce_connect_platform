import React, { useCallback, useState } from "react";
import { Avatar, Input, InputGroup } from "rsuite";
import styled from "styled-components";
import SearchIcon from "@rsuite/icons/Search";
import { debounce } from "lodash";
import * as chatAction from "../../actions/chat";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import IconSearch from "../../components/icon/search/IconSearch";
import UserIcon from "@rsuite/icons/legacy/User";
import { handleRealTime } from "../../utils/date";
import IconEmoij from "../../components/icon/emoij/IconEmoij";
import Emoij from "../../components/emoij/Emoij";
import IconSend from "../../components/icon/send/IconSend";
import UploadListImage from "../../components/form/UploadListImage";
import IconImage from "../../components/icon/image/IconImage";
import InfiniteScroll from "react-infinite-scroll-component";
import moment from "moment";
import * as Types from "../../constants/actionType";
import { useRef } from "react";
import { io } from "socket.io-client";
import lodash from "lodash";

const host = "http://39socket.karavui.com:6441";
const ChatManageStyles = styled.div`
  .chat__manage {
    box-shadow: 2px 3px 15px 5px rgba(0, 0, 0, 0.05);
  }
`;

const ChatManage = () => {
  const dispatch = useDispatch();
  const { chatConversation, listMessages, message } = useSelector(
    (state) => state.chatReducers
  );

  const [searchInput, setSearchInput] = useState("");
  const [params, setParams] = useState({
    page: 1,
    limit: 10,
    search: "",
  });
  const [rooms, setRooms] = useState([]);
  const [inputMessage, setInputMessage] = useState("");
  const [images, setImages] = useState([]);
  const [messages, setMessages] = useState([]);
  const [hasMore, setHasMore] = useState(true);
  const [roomSelected, setRoomSelected] = useState({});

  const socketRef = useRef();
  const listMessagesRef = useRef();

  //Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        return {
          ...prevParams,
          search: valueSearch,
          page: 1,
        };
      });
    }, 500),
    []
  );
  const handleSearch = (value) => {
    setSearchInput(value);
    debounceCallBack(value);
  };
  // Handle Selected Room Chat
  const handleSelectRoomChat = (room) => {
    setRoomSelected(room);
    setMessages([]);
    dispatch(chatAction.getListMessages(room?.doctor?.id, `limit=10&page=1`));
  };

  const handleEnterInputMessage = (e) => {
    if (inputMessage === "" || e.keyCode !== 13) return;
    handleSendMessage();
  };
  const handleFetchData = () => {
    if (messages.length >= listMessages.total) {
      setHasMore(false);
      return;
    }
    setParams((params) => {
      return {
        ...params,
        page: params.page + 1,
      };
    });
  };
  const handleSelectEmojiAddComment = (item) => {
    setInputMessage((prevValue) => prevValue + " " + item.native);
  };
  const handleSendMessage = () => {
    const values = {
      content: inputMessage,
      images: images,
    };
    dispatch(chatAction.sendMessage(roomSelected?.doctor?.id, values));
  };
  const showEndMessage = () => {
    return (
      <div className="flex flex-col items-center justify-center w-full py-4 gap-y-1">
        <div className="w-[60px] h-[60px] rounded-full">
          {roomSelected?.doctor?.avatar_image ? (
            <img
              src={roomSelected?.doctor?.avatar_image}
              alt={roomSelected?.doctor?.name}
              className="object-cover w-full h-full rounded-full"
            />
          ) : (
            <Avatar className="w-full h-full" circle>
              <UserIcon />
            </Avatar>
          )}
        </div>
        <div className="font-medium">{roomSelected?.doctor?.name}</div>
      </div>
    );
  };

  const queryParams = (page, limit, search) => {
    return `page=${page}${limit ? `&limit=${limit}` : ""}${
      search ? `&search=${search}` : ""
    }`;
  };

  //Sau khi gửi tin nhắn
  useEffect(() => {
    if (Object.entries(message).length > 0) {
      setInputMessage("");
      setImages([]);

      dispatch({
        type: Types.SEND_MESSAGE,
        data: {},
      });
    }
  }, [dispatch, message]);

  //Xử lý hiển thị thời gian
  const handleShowTime = (time) => {
    if (time) {
      const total = moment(moment()).diff(time);
      const days = Math.floor(total / 1000 / 60 / 60 / 24);
      const dayNowInWeek = moment().isoWeekday();
      const daySendedInWeek = moment(time).isoWeekday();

      let formatDate = "";

      if (days <= 7) {
        formatDate =
          daySendedInWeek === dayNowInWeek
            ? moment(time).format("HH:mm")
            : daySendedInWeek === 1
            ? `Thứ 2 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 2
            ? `Thứ 3 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 3
            ? `Thứ 4 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 4
            ? `Thứ 5 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 5
            ? `Thứ 6 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 6
            ? `Thứ 7 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 7
            ? `Chủ nhật ${moment(time).format("HH:mm")}`
            : "";
      } else {
        formatDate = `${moment(time).format("DD/MM/YYYY HH:mm")}`;
      }
      return formatDate;
    }
    return "";
  };

  //Lắng nghe server
  useEffect(() => {
    socketRef.current = io(host);
    if (Object.entries(roomSelected)?.length > 0) {
      socketRef.current.on(
        `chat:message_from_user_to_customer:${roomSelected?.doctor?.id}:${roomSelected?.customer.id}`,
        (messages) => {
          if (messages?.id) {
            setMessages((oldMsgs) => [messages, ...oldMsgs]);
            const query = queryParams(1, params.limit, params.search);
            dispatch(chatAction.getChatConversation(query));
          }
        }
      ); // mỗi khi có tin nhắn thì mess sẽ được render thêm
    }

    return () => {
      socketRef.current.disconnect();
    };
  }, [dispatch, roomSelected]);

  //Set height list message
  useEffect(() => {
    const boxMessage = document.querySelector(".list-messages");
    const boxMessageContent = document.querySelector(".list-messages__content");
    const menuChat = document.querySelector(".menu__chat");
    if (boxMessage && menuChat && boxMessageContent) {
      boxMessage.style.height = `${556 - menuChat.offsetHeight}px`;
      boxMessageContent.style.height = `${556 - menuChat.offsetHeight}px`;
    }
  });

  //Lấy dữ liệu all tin nhắn trong store lần đầu
  useEffect(() => {
    if (
      Object.entries(listMessages).length > 0 &&
      !lodash.isEqual(listMessagesRef.current, listMessages)
    ) {
      setMessages((prevMessages) => [...prevMessages, ...listMessages?.data]);
      listMessagesRef.current = listMessages;
    }
  }, [dispatch, listMessages, messages.length]);

  //Lấy dữ liệu messages từ be
  useEffect(() => {
    const query = queryParams(params.page, params.limit, params.search);
    dispatch(chatAction.getChatConversation(query));
  }, [dispatch, params.limit, params.page, params.search]);

  useEffect(() => {
    if (Object.entries(chatConversation).length === 0) return;
    setRooms(chatConversation?.data);
  }, [chatConversation]);

  useEffect(() => {
    dispatch({
      type: Types.HOME_SHOW_CHAT_POPUP,
      data: {
        show: false,
        data: {},
      },
    });
  }, [dispatch]);

  return (
    <ChatManageStyles>
      <div className="flex flex-col gap-y-8">
        <div>
          <h4 className="mb-4 text-xl font-medium">Chat với bác sĩ</h4>
          <div className="flex rounded-[10px] chat__manage">
            <div className=" w-[33%] flex-shrink-0 border-r border-r-gray-100">
              <div className="px-[10px] py-8">
                <div className="flex items-center px-3 py-2 rounded-full gap-x-2">
                  <InputGroup inside>
                    <Input
                      placeholder="Tìm kiếm..."
                      value={searchInput}
                      onChange={handleSearch}
                    />
                    <InputGroup.Button>
                      <SearchIcon />
                    </InputGroup.Button>
                  </InputGroup>
                </div>
              </div>
              <div className="px-[10px] h-[500px] overflow-y-auto">
                {Object.entries(chatConversation).length !== 0 ? (
                  <div className="flex flex-col mb-6">
                    {rooms.length > 0 ? (
                      rooms.map((room) => (
                        <div
                          className={`flex p-2 rounded-[10px] gap-x-[10px] cursor-pointer hover:bg-[rgba(0,0,0,0.03)] ${
                            roomSelected.user_id === room.user_id
                              ? `bg-[rgba(0,0,0,0.05)]`
                              : ""
                          } transition-all items-center`}
                          key={room.user_id}
                          onClick={() => handleSelectRoomChat(room)}
                        >
                          <div className="flex-shrink-0 w-16 h-16 rounded-full">
                            {room?.doctor?.avatar_image ? (
                              <img
                                src={room?.doctor?.avatar_image}
                                alt={room?.doctor?.name}
                                className="object-cover w-full h-full rounded-full"
                              />
                            ) : (
                              <Avatar size="lg" circle>
                                <UserIcon />
                              </Avatar>
                            )}
                          </div>
                          <div className="w-full">
                            <div className="flex items-center justify-between w-full">
                              <h4
                                className={`${
                                  room.cus_seen ? "" : "font-semibold"
                                }`}
                              >
                                {room?.doctor?.name}
                              </h4>
                              <span className="text-xs">
                                {room.created_at !== null
                                  ? handleRealTime(room.updated_at)
                                  : null}
                              </span>
                            </div>
                            <div
                              className={`text-sm ${
                                room.cus_seen
                                  ? "text-gray-400"
                                  : "text-blue-500"
                              } room__messageEnd`}
                            >
                              {room.last_mess === null ? "" : room.last_mess}
                            </div>
                          </div>
                        </div>
                      ))
                    ) : (
                      <div className="flex flex-col items-center text-gray-400 gap-y-3">
                        <IconSearch className="w-6 h-6"></IconSearch>
                        <p>Chưa có cuộc hội thoại nào !</p>
                      </div>
                    )}
                  </div>
                ) : (
                  <div className="flex justify-center my-10">
                    <span className="w-10 h-10 border-4 border-white rounded-full border-t-transparent animate-spin"></span>
                  </div>
                )}
              </div>
            </div>
            <div className="w-full">
              {Object.entries(listMessages).length === 0 ? (
                <div className="flex items-center justify-center w-full h-full text-gray-400 gap-y-3">
                  <div className="flex flex-col items-center gap-y-3">
                    <IconSearch className="w-8 h-8"></IconSearch>
                    <p className="text-lg">Chưa có cuộc hội thoại nào !</p>
                  </div>
                </div>
              ) : (
                <div className="flex flex-col h-full">
                  <div className="flex items-center w-full p-4 shadow-md gap-x-3">
                    <div className="w-10 h-10 rounded-full">
                      {roomSelected?.doctor?.avatar_image ? (
                        <img
                          className="w-full h-full rounded-full"
                          src={roomSelected?.doctor?.avatar_image}
                          alt={roomSelected?.doctor?.name}
                        />
                      ) : (
                        <Avatar className="w-full h-full" circle>
                          <UserIcon />
                        </Avatar>
                      )}
                    </div>
                    <h4>{roomSelected?.doctor?.name}</h4>
                  </div>
                  <div className="flex flex-col justify-end h-[500px] list-messages">
                    <div>
                      {messages.length > 0 ? (
                        <InfiniteScroll
                          dataLength={messages.length}
                          next={handleFetchData}
                          className="flex flex-col-reverse px-2 mt-5 overflow-y-auto gap-y-1 list-messages__content"
                          hasMore={hasMore}
                          inverse={true}
                          height={500}
                          endMessage={showEndMessage()}
                        >
                          {messages.map((message, index) => (
                            <div
                              key={message.id}
                              className={`flex ${
                                !message.is_user_send ? "justify-end" : ""
                              }`}
                            >
                              {!message.is_user_send ? (
                                <div className="flex flex-col items-end w-full">
                                  {message.content !== "" &&
                                    message.content !== null && (
                                      <div
                                        className={`px-4 py-[6px] bg-gray-100 rounded-3xl inline-block relative max-w-[68%] group
                                  `}
                                      >
                                        {message.content}
                                        <div className="absolute top-1/2 -translate-y-1/2 z-10 flex items-center py-[6px] px-2 text-xs bg-gray-300 rounded-md right-[calc(100%+1px)] whitespace-nowrap group-hover:visible invisible opacity-0 group-hover:opacity-100 transition-all">
                                          {handleShowTime(message.created_at)}
                                        </div>
                                      </div>
                                    )}
                                  {message?.images_json !== null &&
                                    message?.images_json?.length > 0 && (
                                      <div className="relative flex justify-end my-1">
                                        <div
                                          className={`relative grid  gap-3 group ${
                                            message.images_json?.length === 1
                                              ? "grid-cols-1"
                                              : "grid-cols-2"
                                          }`}
                                        >
                                          <div className="absolute top-1/2 -translate-y-1/2 z-10 flex items-center py-[6px] px-2 text-xs bg-gray-300 rounded-md right-[calc(100%+1px)] whitespace-nowrap group-hover:visible invisible opacity-0 group-hover:opacity-100 transition-all">
                                            {handleShowTime(message.created_at)}
                                          </div>
                                          {message.images_json?.map(
                                            (url, index) => (
                                              <div
                                                key={index}
                                                className="w-[105px] h-[105px] flex items-center justify-center rounded-lg border border-gray-200"
                                              >
                                                <img
                                                  src={url}
                                                  alt={url}
                                                  loading="lazy"
                                                  className="object-contain h-full"
                                                />
                                              </div>
                                            )
                                          )}
                                        </div>
                                      </div>
                                    )}
                                </div>
                              ) : (
                                <div className="flex flex-row items-end gap-x-2">
                                  <div className="w-10 h-10 rounded-full">
                                    {index > 0 &&
                                    index < messages.length &&
                                    messages[index].is_user_send ===
                                      messages[index - 1]
                                        .is_user_send ? null : (
                                      <>
                                        {roomSelected.doctor?.avatar_image ? (
                                          <img
                                            src={`${roomSelected.doctor?.avatar_image}`}
                                            alt={roomSelected.doctor?.name}
                                            className="object-cover w-full h-full rounded-full"
                                          />
                                        ) : (
                                          <Avatar
                                            className="w-full h-full"
                                            circle
                                          >
                                            <UserIcon />
                                          </Avatar>
                                        )}
                                      </>
                                    )}
                                  </div>
                                  <div>
                                    {message.content !== "" &&
                                      message.content !== null && (
                                        <div
                                          className={`px-4 py-[6px] bg-gray-100 rounded-3xl inline-block max-w-[68%] relative group
                                  `}
                                        >
                                          {message.content}
                                          <div className="absolute top-1/2 -translate-y-1/2 z-10 flex items-center py-[6px] px-2 text-xs bg-gray-300 rounded-md right-[calc(100%+1px)] whitespace-nowrap group-hover:visible invisible opacity-0 group-hover:opacity-100 transition-all">
                                            {handleShowTime(message.created_at)}
                                          </div>
                                        </div>
                                      )}

                                    {message?.images_json?.length > 0 && (
                                      <div className="relative flex my-1">
                                        <div
                                          className={`relative grid  gap-3 group ${
                                            message.images_json?.length === 1
                                              ? "grid-cols-1"
                                              : "grid-cols-2"
                                          }`}
                                        >
                                          <div className="absolute top-1/2 -translate-y-1/2 z-10 flex items-center py-[6px] px-2 text-xs bg-gray-300 rounded-md right-[calc(100%+1px)] whitespace-nowrap group-hover:visible invisible opacity-0 group-hover:opacity-100 transition-all">
                                            {handleShowTime(message.created_at)}
                                          </div>
                                          {message?.images_json?.map(
                                            (url, index) => (
                                              <div
                                                key={index}
                                                className="w-[105px] h-[105px] flex items-center justify-center rounded-lg border border-gray-200"
                                              >
                                                <img
                                                  src={url}
                                                  alt={url}
                                                  loading="lazy"
                                                  className="object-contain h-full"
                                                />
                                              </div>
                                            )
                                          )}
                                        </div>
                                      </div>
                                    )}
                                  </div>
                                </div>
                              )}
                            </div>
                          ))}
                        </InfiniteScroll>
                      ) : null}
                      {messages.length === 0 &&
                        Object.entries(listMessages).length > 0 && (
                          <div className="text-sm font-medium text-center">
                            Bạn đã kết nối với {roomSelected.doctor?.name}
                          </div>
                        )}
                    </div>
                  </div>
                  <div className="flex w-full p-2 gap-x-2 menu__chat">
                    <div className="flex flex-col w-full h-full bg-gray-100 rounded-2xl">
                      <div
                        className={`${
                          images.length > 0 ? "block" : "hidden"
                        } p-2`}
                      >
                        <UploadListImage
                          className="uploadListImage"
                          style={{
                            width: "60px",
                            height: "60px",
                          }}
                          images={[]}
                          setFiles={setImages}
                        ></UploadListImage>
                      </div>
                      <div className="flex items-center h-full gap-x-2">
                        <input
                          type="text"
                          placeholder="Aa"
                          className="px-3 py-[6px] bg-transparent w-full inputSearch flex flex-wrap h-full"
                          value={inputMessage}
                          onChange={(e) => setInputMessage(e.target.value)}
                          onKeyDown={handleEnterInputMessage}
                        ></input>
                        <div className="relative flex px-2 transition-all rounded-full cursor-pointer hover:text-blue-500 group">
                          <IconEmoij className="w-6 h-6"></IconEmoij>
                          <div className="absolute right-0 z-50 invisible opacity-0 bottom-full emoji-modal group-hover:opacity-100 group-hover:visible">
                            <Emoij
                              handleSelectEmoji={handleSelectEmojiAddComment}
                            ></Emoij>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-col justify-end">
                      <div className="flex">
                        <div
                          className="flex items-center justify-center p-2 transition-all rounded-full cursor-pointer hover:bg-gray-100 hover:text-blue-500"
                          onClick={() => {
                            document
                              .querySelector(".uploadListImage input")
                              .click();
                          }}
                        >
                          <IconImage className="w-6 h-6"></IconImage>
                        </div>
                        <div
                          className={`flex items-center justify-center p-2 transition-all rounded-full cursor-pointer hover:bg-gray-100 hover:text-blue-500 ${
                            inputMessage === ""
                              ? "pointer-events-none select-none opacity-70"
                              : ""
                          }`}
                          onClick={handleSendMessage}
                        >
                          <IconSend className="w-6 h-6"></IconSend>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </ChatManageStyles>
  );
};

export default ChatManage;
