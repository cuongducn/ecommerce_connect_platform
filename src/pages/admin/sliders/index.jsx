import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
import {
  Button,
  Card,
  Col,
  Form,
  Image,
  Input,
  Modal,
  Row,
  Select,
  Switch,
  Table,
} from "antd";
import { useForm } from "antd/es/form/Form";
import { debounce } from "lodash";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSearchParams } from "react-router-dom";
import * as slidersAction from "../../../actions/admin/sliders";
import { fileType } from "../../../constants/ConstantModel";
import { HiOutlineCamera } from "react-icons/hi";
import dayjs from "dayjs";

const Sliders = () => {
  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      width: 50,
      align: "center",
      render: (a, b, index) => <span>{index + 1}</span>,
    },
    {
      title: "Tên",
      dataIndex: "name",
      align: "center",
      render: (text, row) => (
        <a
          className="text-blue-700 hover:cursor-pointer"
          onClick={() => {
            setId(row.id);
          }}
        >
          {text}
        </a>
      ),
    },
    {
      title: "Image",
      dataIndex: "image",
      key: "image",
      render: (image) => (
        <img src={image} alt="Product" style={{ width: "100px" }} />
      ),
    },
    {
      title: "Ngày tạo",
      dataIndex: "created_at",

      align: "center",
      // render: (text) => <div>{dayjs(text).format('DD/MM/YYYY') || ''} </div>,
      render: (text) => <div>{text} </div>,
    },
    {
      title: "hiển thị",
      dataIndex: "is_show",
      align: "center",
      render: (text, row) => {
        return (
          <Switch
            style={{ border: "1px solid #808080" }}
            checked={text} // Trạng thái switch, true nếu text === 1, ngược lại false
            onChange={(checked) => {
              onStatus(row.id, checked);
            }}
          />
        );
      },
    },

    {
      title: "Thao tác",
      dataIndex: "delete",

      width: 150,
      align: "center",

      render: (val, row) => (
        <Row className="flex justify-center items-center">
          <Col className="mr-1">
            <Button
              onClick={() => {
                setId(row.id);
                setMode("update");
                showModal();
              }}
              shape="circle"
              className="flex justify-center items-center border-white hover:border-none"
            >
              <EditOutlined />
            </Button>
          </Col>
          <Col>
            <Button
              shape="circle"
              className="flex justify-center items-center border-white hover:border-none"
              onClick={() => {
                setId(row.id);
                showModalDelete();
              }}
            >
              <DeleteOutlined />
            </Button>
          </Col>
        </Row>
      ),
    },
  ];

  const [mode, setMode] = useState("add");

  const [isModalDelete, setIsModalDelete] = useState(false);
  const [modalCreate, setModalCreate] = useState(false);
  const [id, setId] = useState(0);
  const [loading] = useState(false);
  const dispatch = useDispatch();
  const [searchParams, setSearchParams] = useSearchParams();
  const [form] = useForm();
  const [imageUrl, setImageUrl] = useState("");
  const [imageFile, setImageFile] = useState(null);

  const handleCancel = () => {
    setModalCreate(false);
    setIsModalDelete(false);
  };

  const showModal = () => {
    setModalCreate(true);
  };
  const showModalDelete = () => {
    setIsModalDelete(true);
  };

  const { data: sliders } = useSelector(
    (state) => state.slidersReducers.sliders.listSliders
  );
  const { data: imgUrl } = useSelector(
    (state) => state.slidersReducers.sliders.uploadImage
  );

  console.log(imgUrl);

  const onCreateSliders = (val) => {
    slidersAction.createSliders(
      {
        ...val,
        image: imgUrl[fileType.IMAGE_SLIDERS],
      },
      () => {
        form.resetFields();
        setModalCreate(false);
      }
    )(dispatch);
  };

  const [filter, setFilter] = useState({
    page: 1,
    limit: 10,
  });
  const onStatus = (id, val) => {
    const newStatus = val ? 1 : 0;
    slidersAction.updateSliders({
      id,
      is_show: newStatus,
    })(dispatch);
  };

  const onUpdateSliders = (val) => {
    slidersAction.updateSliders(
      {
        id,
        ...val,
        image: imgUrl[fileType.IMAGE_SLIDERS],
      },
      () => setModalCreate(false)
    )(dispatch);
  };
  const onDelete = () => {
    slidersAction.deleteSliders(
      {
        sliders_ids: [id],
      },
      () => setIsModalDelete(false)
    )(dispatch);
  };

  const searchParamsObject = Object.fromEntries(searchParams.entries());

  const onSearch = debounce((val) => {
    const values = {
      ...searchParamsObject,
      ...val,
    };
    setFilter(values);
  }, 1000);

  const { data: SlidersDetails } = useSelector(
    (state) => state.slidersReducers.sliders.detailSliders
  );

  useEffect(() => {
    if (id) {
      slidersAction.detailSliders(id)(dispatch);
    }
  }, [id]);

  useEffect(() => {
    slidersAction.listSliders(filter)(dispatch);
  }, [filter]);

  useEffect(() => {
    if (mode === "update") form.setFieldsValue(SlidersDetails);
  }, [mode, SlidersDetails]);

  const onUpload = (e) => {
    const file = e.target.files[0];
    setImageFile(file);
    const reader = new FileReader();
    reader.onload = (event) => {
      setImageUrl(event.target.result);
    };
    reader.readAsDataURL(file);
  };

  useEffect(() => {
    if (imageFile) {
      slidersAction.uploadImgSliders(
        fileType.IMAGE_SLIDERS,
        imageFile
      )(dispatch);
    }
  }, [imageFile, dispatch]);

  return (
    <div className="mx-6 my-6 w-full">
      <Card className="w-full">
        <div>
          <div className="grid grid-cols-2">
            <div className="text-[#00897b] font-sans text-xl col-span-1">
              Danh sách Sliders
            </div>
            <div className="col-span-1 flex justify-end">
              <Button
                onClick={() => {
                  setMode("add");
                  showModal(true);
                }}
                size="large"
                className="bg-[#00897b] text-white mr-3 "
                icon={<PlusOutlined className="justify-center" />}
              >
                Thêm Sliders
              </Button>
            </div>
          </div>
        </div>
        <Form
          name="basic"
          autoComplete="off"
          labelAlign="left"
          layout="vertical"
          onValuesChange={(c, v) => onSearch(v)}
          initialValues={{
            search: searchParams.get("search") || "",
            date_range: [moment(), moment()],
          }}
        >
          <div className="grid gap-2 grid-cols-2 mt-8 ">
            <div className="gird-col-1 w-[500px]">
              <Form.Item name="search">
                <Input
                  className="h-[40px] "
                  placeholder="Tìm kiếm tên thành viên, số điện thoại,.. "
                  prefix={<EditOutlined />}
                />
              </Form.Item>
            </div>
          </div>
        </Form>
        <div>
          <Table
            size="small"
            columns={columns}
            dataSource={sliders.data || []}
            pagination={{
              current: filter.current_page,
              pageSize: filter.per_page,
              total: sliders.total,
              onChange: (page, pageSize) => {
                setFilter({
                  page: page,
                  limit: pageSize,
                });
                setSearchParams({
                  ...searchParams,
                  page: page,
                  limit: pageSize,
                });
              },
            }}
          />
        </div>
      </Card>

      <Modal
        onCancel={handleCancel}
        open={modalCreate}
        title={mode === "add" ? "Thêm sliders" : " Cập nhật sliders"}
        footer={[
          <Button
            style={{
              hover: { color: "#FFFFFF" },
              border: "1px solid #00897b",
              textColor: "#00897b",
            }}
            key="back"
            type="text"
            onClick={handleCancel}
          >
            Hủy
          </Button>,
          <Button
            style={{
              backgroundColor: "#00897b",
              color: "#ffffff",
            }}
            loading={loading}
            form="form-add-blogs"
            htmlType="submit"
            key="submit"
          >
            Lưu
          </Button>,
        ]}
      >
        <Form
          form={form}
          layout="vertical"
          name="form-add-blogs"
          onFinish={(val) => {
            if (mode === "add") onCreateSliders(val);
            else if (mode === "update") onUpdateSliders(val);
          }}
          initialValues={mode === "update" ? SlidersDetails : {}}
        >
          <Form.Item label="Tên" name="name">
            <Input />
          </Form.Item>
          <Form.Item label="hình ảnh">
            <div className="relative overflow-visible w-[100px] h-[100px]">
              <Image
                className="rounded-[10px]"
                width={"100%"}
                height={"100%"}
                placeholder={
                  <Image
                    preview={false}
                    width={"100%"}
                    height={"100%"}
                    src={imageUrl}
                  />
                }
                src={sliders.image || imgUrl[fileType.IMAGE_SLIDERS]}
              />
              <div>
                <label
                  htmlFor="image_sliders"
                  className=" absolute cursor-pointer 
      z-30  bg-gray-700/50 rounded-full flex items-center justify-center text-white 
       p-2 mb-3 w-10 h-10 bottom-0 right-0 translate-x-1/3 translate-y-1/3"
                >
                  <input
                    onChange={onUpload}
                    type="file"
                    className="hidden"
                    id="image_sliders"
                  />{" "}
                  <HiOutlineCamera className="text-[18px] translate-y-[2px]" />
                </label>
              </div>
            </div>
          </Form.Item>
          <Form.Item
            label="hiển thị"
            name="is_show"
            valuePropName="checked"
            getValueFromEvent={(value) => (value ? 1 : 0)}
          >
            <Switch style={{ border: "1px solid #808080" }} />
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        onCancel={handleCancel}
        open={isModalDelete}
        title="Xóa sliders"
        footer={[
          <Button
            style={{
              hover: { color: "#FFFFFF" },
              border: "1px solid #696CFF",
              textColor: "#696CFF",
            }}
            key="back"
            type="text"
            onClick={handleCancel}
          >
            Hủy
          </Button>,
          <Button
            style={{
              backgroundColor: "#00897b",
            }}
            // loading={loadingDelete}
            onClick={() => onDelete()}
            htmlType="submit"
            key="submit"
            type="primary"
          >
            Xóa
          </Button>,
        ]}
      >
        <span>Bạn có chắc chắn muốn xóa Sliders này không ?</span>
      </Modal>
    </div>
  );
};

export default Sliders;
