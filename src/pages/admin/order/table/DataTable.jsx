import {
  Table,
  Pagination,
  InputGroup,
  Input,
  Stack,
  InputPicker,
  IconButton,
  DateRangePicker,
  Checkbox,
  Button,
} from 'rsuite';
import { useCallback, useEffect, useRef, useState } from 'react';
import { Search, Edit, Trash } from '@rsuite/icons';
import { useDispatch, useSelector } from 'react-redux';
import * as orderAction from '../../../../actions/admin/order';
import { formatNumber } from '../../../../utils';
import styled from 'styled-components';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { debounce } from 'lodash';
import * as Types from '../../../../constants/actionType';
import ModalCustom from '../../../../components/admin/modal';
import { formatDateObj, formatDateStr } from '../../../../utils/date';
import { orderStatuses, paymentStatuses } from '../../../../utils/data/order';
import CheckCell from '../../../../components/admin/table/CheckCell';
const { Column, HeaderCell, Cell } = Table;
const rowKey = 'id';

const DataTableStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding-top: 20px;
  border-radius: 10px;
  .header {
    display: flex;
    justify-content: flex-end;
    margin-bottom: 20px;
    padding: 0 20px;
    .input__search-content {
      width: 250px;
    }
  }
  @media only screen and (max-width: 768px) {
    .header {
      .input__search-content {
        width: 190px;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .header {
      flex-direction: column;
      row-gap: 10px;
      padding: 0 20px;
      .rs-stack-item {
        width: 100%;
        overflow: visible !important;
        .input__search-content {
          width: 100%;
        }
        .rs-picker {
          margin-right: 0 !important;
          width: 100% !important;
        }
      }
    }
  }
`;
const DataTable = () => {
  const dispatch = useDispatch();
  const { listOrders } = useSelector((state) => state.orderAdminReducers);
  const { data } = listOrders;

  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get('page')) || 1,
    limit: Number(searchParams.get('limit')) || 20,
    order_status: Number(searchParams.get('order_status')) || '',
    payment_status: Number(searchParams.get('payment_status')) || '',
    search: searchParams.get('search') || '',
    date_from: searchParams.get('date_from') || '',
    date_to: searchParams.get('date_to') || '',
    searchInput: searchParams.get('search') || '',
  });
  const [openModalDeleteListOrder, setOpenModalDeleteListOrder] =
    useState(false);
  const listOrderStatusRef = useRef(orderStatuses);
  const listPaymentStatusRef = useRef(paymentStatuses);
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const navigate = useNavigate();
  const statusOrderRef = useRef(
    orderStatuses.map((orderStatus) => ({
      label: orderStatus.title,
      value: orderStatus.status,
    }))
  );
  const statusPaymentRef = useRef(
    paymentStatuses.map((paymentStatus) => ({
      label: paymentStatus.title,
      value: paymentStatus.status,
    }))
  );

  // Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        const queries = getParams(
          1,
          prevParams.limit,
          prevParams.order_status,
          prevParams.payment_status,
          valueSearch,
          prevParams.date_from,
          prevParams.date_to
        );
        navigate(`/admin/orders?${queries}`);
        return {
          ...prevParams,
          page: 1,
          search: valueSearch,
          searchInput: valueSearch,
        };
      });
    }, 500),
    []
  );
  const handleSearchProducts = (e) => {
    const value = e;
    setParams({
      ...params,
      searchInput: value,
    });
    debounceCallBack(value);
  };

  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === 'string') {
          x = x.charCodeAt();
        }
        if (typeof y === 'string') {
          y = y.charCodeAt();
        }
        if (sortData.type === 'asc') {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    let dataIndex = [];
    if (data?.length > 0) {
      dataIndex = data.map((item, index) => {
        item.index =
          (Number(listOrders.current_page) - 1) * Number(listOrders.per_page) +
          index +
          1;
        return item;
      });
    }
    return dataIndex;
  };
  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };

  // Handle change params
  const handleChangePage = (pageChange) => {
    const queries = getParams(
      pageChange,
      params.limit,
      params.order_status,
      params.payment_status,
      params.search,
      params.date_from,
      params.date_to
    );
    navigate(`/admin/orders?${queries}`);
    setParams({
      ...params,
      page: pageChange,
    });
  };

  //Handle Change Status
  const handleChangeOrderStatus = (value) => {
    const queries = getParams(
      1,
      params.limit,
      value,
      params.payment_status,
      params.search,
      params.date_from,
      params.date_to
    );

    navigate(`/admin/orders?${queries}`);
    setParams({
      ...params,
      page: 1,
      order_status: value !== '' && value !== null ? value : '',
    });
  };

  //Handle Change Payment
  const handleChangePaymentStatus = (value) => {
    const queries = getParams(
      1,
      params.limit,
      params.order_status,
      value,
      params.search,
      params.date_from,
      params.date_to
    );

    navigate(`/admin/orders?${queries}`);
    setParams({
      ...params,
      page: 1,
      payment_status: value !== '' && value !== null ? value : '',
    });
  };

  //Handle Check Box
  const [checkedKeys, setCheckedKeys] = useState([]);

  let checked = false;
  let indeterminate = false;

  if (checkedKeys.length === data?.length) {
    checked = true;
  } else if (checkedKeys.length === 0) {
    checked = false;
  } else if (checkedKeys.length > 0 && checkedKeys.length < data?.length) {
    indeterminate = true;
  }

  const handleCheckAll = (value, checked) => {
    const keys = checked ? data.map((item) => item.id) : [];
    setCheckedKeys(keys);
  };
  const handleCheck = (value, checked) => {
    const keys = checked
      ? [...checkedKeys, value]
      : checkedKeys.filter((item) => item !== value);
    setCheckedKeys(keys);
  };

  //Handle Actions
  const ActionCell = ({ rowData, dataKey, ...props }) => {
    const [open, setOpen] = useState(false);
    const handleClose = () => setOpen(false);

    const handleDeletePacketMedical = () => {
      const data = {
        order_ids: [rowData[dataKey]],
      };

      const queryString = getParams(
        params.page,
        params.limit,
        params.order_status,
        params.payment_status,
        params.search,
        params.date_from,
        params.date_to
      );
      dispatch(orderAction.deleteOrder(data, queryString));
    };
    return (
      <Cell {...props} style={{ padding: 5 }} className="link-group">
        <div className="btn__actions">
          <IconButton
            appearance="subtle"
            onClick={() =>
              navigate(
                `/admin/orders/update/${rowData['order_code']}?${getParams(
                  params.page,
                  params.limit,
                  params.order_status,
                  params.payment_status,
                  params.search,
                  params.date_from,
                  params.date_to
                )}`
              )
            }
            icon={<Edit />}
          />
          <IconButton
            appearance="subtle"
            onClick={() => setOpen(true)}
            icon={<Trash />}
          />
          <ModalCustom
            size="sm"
            title="Thông báo"
            open={open}
            nameAction="Xóa"
            handleClose={handleClose}
            handleAction={handleDeletePacketMedical}
          >
            Bạn có chắc chắn muốn xóa đơn hàng này không?
          </ModalCustom>
        </div>
      </Cell>
    );
  };

  const handleRemoveListOrder = () => {
    const data = {
      order_ids: checkedKeys,
    };

    const queryString = getParams(
      params.page,
      params.limit,
      params.order_status,
      params.payment_status,
      params.search,
      params.date_from,
      params.date_to
    );
    dispatch(
      orderAction.deleteOrder(data, queryString, () => {
        setCheckedKeys([]);
        setOpenModalDeleteListOrder(false);
      })
    );
  };

  const getParams = (
    page,
    limit,
    order_status,
    payment_status,
    search,
    date_from,
    date_to
  ) => {
    let params = '';
    if (page) {
      params += `page=${page}`;
    }
    if (limit) {
      params += `&limit=${limit}`;
    }
    if (order_status !== '' && order_status !== null) {
      params += `&order_status=${order_status}`;
    }
    if (payment_status !== '' && payment_status !== null) {
      params += `&payment_status=${payment_status}`;
    }
    if (search !== '') {
      params += `&search=${search}`;
    }
    if (date_from !== '') {
      params += `&date_from=${date_from}`;
    }
    if (date_to !== '') {
      params += `&date_to=${date_to}`;
    }

    return params;
  };

  //Handle Date
  const handleSelectedDate = (values) => {
    if (values) {
      setParams({
        ...params,
        page: 1,
        date_from: formatDateStr(values[0], 'YYYY-MM-DD'),
        date_to: formatDateStr(values[1], 'YYYY-MM-DD'),
      });
    } else {
      setParams({
        ...params,
        date_from: '',
        date_to: '',
      });
    }
    const queries = getParams(
      1,
      params.limit,
      params.order_status,
      params.payment_status,
      params.search,
      values ? formatDateStr(values[0], 'YYYY-MM-DD') : '',
      values ? formatDateStr(values[1], 'YYYY-MM-DD') : ''
    );
    navigate(`/admin/orders?${queries}`);
  };

  //Hanle Export
  const handleExport = () => {
    dispatch(orderAction.exportExcel());
  };

  // Call API Get listOrders
  useEffect(() => {
    const queryString = getParams(
      params.page,
      params.limit,
      params.order_status,
      params.payment_status,
      params.search,
      params.date_from,
      params.date_to
    );
    dispatch(orderAction.getListOrders(queryString));
  }, [
    dispatch,
    params.page,
    params.limit,
    params.search,
    params.order_status,
    params.payment_status,
    params.date_from,
    params.date_to,
  ]);

  return (
    <DataTableStyles>
      <Stack justifyContent="flex-end">
        <button
          onClick={handleExport}
          type="button"
          className="text-white bg-gradient-to-r from-green-400 via-green-500 to-green-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-green-300 dark:focus:ring-green-800 shadow-lg shadow-green-500/50 dark:shadow-lg dark:shadow-green-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center flex mr-5 mb-5 gap-x-2"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5m-13.5-9L12 3m0 0l4.5 4.5M12 3v13.5"
            />
          </svg>
          Export
        </button>
      </Stack>
      <Stack className="header" justifyContent="space-between" spacing={20}>
        <Stack spacing={20}>
          <InputGroup inside className="input__search-content">
            <Input
              placeholder="Tìm kiếm đơn hàng..."
              onChange={handleSearchProducts}
              value={params.searchInput}
            />
            <InputGroup.Addon>
              <Search />
            </InputGroup.Addon>
          </InputGroup>

          <DateRangePicker
            value={
              params.date_from && params.date_to
                ? [
                    formatDateObj(params.date_from),
                    formatDateObj(params.date_to),
                  ]
                : []
            }
            format="yyyy-MM-dd"
            ranges={[]}
            style={{ width: 230 }}
            onChange={handleSelectedDate}
          />
        </Stack>
        <Stack spacing={20}>
          <InputPicker
            value={params.payment_status}
            data={statusPaymentRef.current}
            style={{ width: 230 }}
            placeholder="Trạng thái thanh toán: Tất cả"
            onChange={handleChangePaymentStatus}
          />
          <InputPicker
            value={params.order_status}
            data={statusOrderRef.current}
            style={{ width: 220 }}
            placeholder="Trạng thái đơn hàng: Tất cả"
            onChange={handleChangeOrderStatus}
          />
        </Stack>
      </Stack>
      {checkedKeys?.length > 0 && (
        <Stack
          justifyContent="flex-start"
          style={{
            marginLeft: '20px',
            marginBottom: '20px',
          }}
        >
          <Button
            color="red"
            appearance="primary"
            startIcon={<Trash />}
            style={{
              backgroundColor: '#d50101f5',
            }}
            onClick={() => setOpenModalDeleteListOrder(true)}
          >
            Xóa {checkedKeys?.length} sản phẩm
          </Button>
          <ModalCustom
            size="sm"
            title="Thông báo"
            open={openModalDeleteListOrder}
            nameAction="Xóa"
            handleClose={() => setOpenModalDeleteListOrder(false)}
            handleAction={handleRemoveListOrder}
          >
            Bạn có chắc chắn muốn xóa {checkedKeys.length} đơn hàng này không?
          </ModalCustom>
        </Stack>
      )}
      <Table
        autoHeight
        rowKey={rowKey}
        hover={false}
        data={getData()}
        sortColumn={sortData.column}
        sortType={sortData.type}
        onSortColumn={handleSortColumn}
        loading={sortData.loading}
      >
        <Column width={50} align="center">
          <HeaderCell style={{ padding: 0 }}>
            <div style={{ lineHeight: '40px' }}>
              <Checkbox
                inline
                checked={checked}
                indeterminate={indeterminate}
                onChange={handleCheckAll}
              />
            </div>
          </HeaderCell>
          <CheckCell
            dataKey="id"
            checkedKeys={checkedKeys}
            onChange={handleCheck}
          />
        </Column>
        <Column align="center" width={50}>
          <HeaderCell>STT</HeaderCell>
          <Cell dataKey="index" />
        </Column>
        <Column width={180}>
          <HeaderCell>Mã đơn</HeaderCell>
          <Cell>
            {(rowData) => (
              <span
                className="text-blue-500 cursor-pointer hover:text-blue-600"
                onClick={() =>
                  navigate(
                    `/admin/orders/detail/${rowData.order_code}?${getParams(
                      params.page,
                      params.limit,
                      params.order_status,
                      params.payment_status,
                      params.search,
                      params.date_from,
                      params.date_to
                    )}`
                  )
                }
              >
                {rowData.order_code}
              </span>
            )}
          </Cell>
        </Column>
        <Column flexGrow={1}>
          <HeaderCell>Tên người nhận</HeaderCell>
          <Cell>{(rowData) => rowData.customer_name}</Cell>
        </Column>
        <Column width={150}>
          <HeaderCell>Tổng tiền</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData.total_final
                ? `${formatNumber(rowData.total_final)}đ`
                : '0đ'
            }
          </Cell>
        </Column>
        <Column width={150}>
          <HeaderCell>Thời gian tạo đơn</HeaderCell>
          <Cell>{(rowData) => rowData.created_at}</Cell>
        </Column>
        <Column width={150}>
          <HeaderCell>Trạng thái thanh toán</HeaderCell>
          <Cell>
            {(rowData) => (
              <span
                style={{
                  color: listPaymentStatusRef.current?.filter(
                    (orderPayment) =>
                      orderPayment.status === rowData.payment_status
                  )?.[0]?.color,
                }}
              >
                {
                  listPaymentStatusRef.current?.filter(
                    (orderPayment) =>
                      orderPayment.status === rowData.payment_status
                  )?.[0]?.title
                }
              </span>
            )}
          </Cell>
        </Column>
        <Column width={150}>
          <HeaderCell>Trạng thái đơn hàng</HeaderCell>
          <Cell>
            {(rowData) => (
              <span
                style={{
                  color: listOrderStatusRef.current?.filter(
                    (orderStatus) => orderStatus.status === rowData.order_status
                  )?.[0]?.color,
                }}
              >
                {
                  listOrderStatusRef.current?.filter(
                    (orderStatus) => orderStatus.status === rowData.order_status
                  )?.[0]?.title
                }
              </span>
            )}
          </Cell>
        </Column>
        <Column width={140} align="center">
          <HeaderCell>Hành động</HeaderCell>
          <ActionCell dataKey="id" />
        </Column>
      </Table>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          size="xs"
          layout={['total', '-', 'pager', 'skip']}
          total={listOrders.total}
          limitOptions={[10, 30, 50]}
          limit={params.limit}
          activePage={params.page}
          onChangePage={handleChangePage}
        />
      </div>
    </DataTableStyles>
  );
};

export default DataTable;
