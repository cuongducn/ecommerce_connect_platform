import { Breadcrumb } from 'rsuite';
import styled from 'styled-components';
import Breadcrumbs from '../../../components/admin/breadcrumb';
import Copyright from '../../../components/admin/copyright/Copyright';
import DataTable from './table/DataTable';

const OrderStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const Product = () => {
  return (
    <OrderStyles>
      <Breadcrumbs
        title="Đơn hàng"
        breadcrumbItems={[<Breadcrumb.Item active>Đơn hàng</Breadcrumb.Item>]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </OrderStyles>
  );
};
export default Product;
