import { Col, Form, Grid, InputGroup, InputNumber, Row, Schema } from "rsuite";

import Input from "../../../../../components/admin/form/Input";
import { useEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { formatNumber, getQueryParams } from "../../../../../utils";

import { useDispatch, useSelector } from "react-redux";
import * as orderAction from "../../../../../actions/admin/order";
import * as placeAction from "../../../../../actions/place";
import InputSelect from "../../../../../components/admin/form/InputSelect";
import NoImageProduct from "../../../../../assets/default_product.jpg";
import { v4 } from "uuid";

const dataShipperType = [
  {
    label: "Giao hàng tiết kiệm",
    value: 0,
  },
  {
    label: "Giao hàng nhanh",
    value: 1,
  },
  {
    label: "Viettel Post",
    value: 2,
  },
];

export default function Content() {
  const params = useParams();
  const idOrder = params.order_code;

  const dispatch = useDispatch();
  const { order } = useSelector((state) => state.orderAdminReducers);
  const { province, district, wards } = useSelector(
    (state) => state.placeReducers
  );

  const formRef = useRef();
  const navigate = useNavigate();
  const [formValue, setFormValue] = useState({
    order_code: "",
    shipper_type: "",
    total_shipping_fee: "",
    discount: "",
    time_line_product: [],
    customer_name: "",
    customer_country: "",
    customer_province: "",
    customer_district: "",
    customer_wards: "",
    customer_address_detail: "",
    customer_email: "",
    customer_phone: "",
    customer_note: "",
  });
  const [values, setValues] = useState([]);
  const handleMinus = (index) => {
    const newValues = values.reduce((prevValues, currentValues, indexValue) => {
      const newValue =
        index === indexValue ? parseInt(values[index], 10) - 1 : currentValues;
      return [...prevValues, newValue];
    }, []);

    setValues(newValues);
  };
  const handlePlus = (index) => {
    const newValues = values.reduce((prevValues, currentValues, indexValue) => {
      const newValue =
        index === indexValue ? parseInt(values[index], 10) + 1 : currentValues;
      return [...prevValues, newValue];
    }, []);
    setValues(newValues);
  };
  const handleChange = (value, index) => {
    const newValues = values.reduce((prevValues, currentValues, indexValue) => {
      const newValue = index === indexValue ? Number(value) : currentValues;
      return [...prevValues, newValue];
    }, []);
    setValues(newValues);
  };

  const handleFilterShipperType = (shipperType) => {
    const dataReturn = dataShipperType.filter(
      (dataType) => dataType.value === shipperType
    );
    if (dataReturn.length > 0) {
      return dataReturn;
    }
    return [];
  };

  // Handle Change Value Form
  const handleChangeValueForm = (values, e) => {
    if (e.target) {
      const name = e.target.name;
      const value = e.target.value;
      if (name === "total_shipping_fee") {
        setFormValue({
          ...values,
          [name]: value ? formatNumber(value) : "",
        });
      } else {
        setFormValue({
          ...values,
        });
      }
    } else {
      const name = e.name;
      let valuesPlace = {};
      if (name === "customer_province") {
        if (values[name]) {
          dispatch(placeAction.getDistrict(values[name]));
        }
        valuesPlace = {
          customer_district: "",
          customer_wards: "",
        };
      } else if (name === "customer_district") {
        if (values[name]) {
          dispatch(placeAction.getWards(values[name]));
        }
        valuesPlace = {
          customer_wards: "",
        };
      }
      setFormValue({
        ...values,
        ...valuesPlace,
      });
    }
  };

  const handleFilterPlace = (value, data) => {
    let newData;
    data.forEach((element) => {
      if (element.id !== value) return;
      newData = {
        value: element.id,
        label: element.name,
      };
    });

    return newData;
  };
  const handleOptionPlace = (data) => {
    const newData = data.reduce((prevData, currentData) => {
      return [
        ...prevData,
        {
          value: currentData.id,
          label: currentData.name,
        },
      ];
    }, []);

    return newData;
  };
  const handleUpdateOrder = () => {
    if (!formRef.current.check()) {
      return;
    }

    console.log(handleChangeQuantityProduct());
    const newDataForm = {
      ...handleChangeQuantityProduct(),
      ...formValue,
      total_shipping_fee: formValue.total_shipping_fee
        ? formValue?.total_shipping_fee?.toString()?.replace(/\,/g, "")
        : 0,
    };
    const page = getQueryParams("page") || 1;
    const limit = getQueryParams("limit") || 20;
    const order_status = getQueryParams("order_status") || "";
    const payment_status = getQueryParams("payment_status") || "";
    const search = getQueryParams("search") || "";
    const date_from = getQueryParams("date_from") || "";
    const date_to = getQueryParams("date_to") || "";

    const params = getParams(
      page,
      limit,
      order_status,
      payment_status,
      search,
      date_from,
      date_to
    );

    dispatch(
      orderAction.updateOrder(idOrder, newDataForm, params, () => {
        navigate(`/admin/orders?${params}`);
      })
    );
  };
  const handleChangeQuantityProduct = () => {
    const newOrder = JSON.parse(JSON.stringify(order));

    order.time_line_product?.forEach((element, index) => {
      newOrder.time_line_product[index].quantity = values?.[index];
    });

    return newOrder;
  };

  const getParams = (
    page,
    limit,
    order_status,
    payment_status,
    search,
    date_from,
    date_to
  ) => {
    let params = "";
    if (page) {
      params += `page=${page}`;
    }
    if (limit) {
      params += `&limit=${limit}`;
    }
    if (order_status !== "" && order_status !== null) {
      params += `&order_status=${order_status}`;
    }
    if (payment_status !== "" && payment_status !== null) {
      params += `&payment_status=${payment_status}`;
    }
    if (search !== "") {
      params += `&search=${search}`;
    }
    if (date_from !== "") {
      params += `&date_from=${date_from}`;
    }
    if (date_to !== "") {
      params += `&date_to=${date_to}`;
    }

    return params;
  };

  //Get Data Detail
  useEffect(() => {
    if (Object.entries(order).length === 0 || !idOrder) return;

    setFormValue((prevForm) => ({
      ...prevForm,
      order_code: order.order_code,
      shipper_type: order.shipper_type,
    }));
    setValues(() => Array(order.time_line_product?.length).fill(0));
  }, [idOrder, order]);

  // Call API Get Detail order
  useEffect(() => {
    if (!idOrder) {
      return;
    }
    dispatch(orderAction.getOneOrder(idOrder));
    dispatch(placeAction.getProvince());
  }, [dispatch, idOrder]);
  return (
    <div>
      <Form
        fluid
        ref={formRef}
        onChange={handleChangeValueForm}
        formValue={formValue}
      >
        <div className="flex flex-col  gap-y-5">
          <div className="grid grid-cols-2 gap-x-5 p-4 rounded-xl bg-[#fff]">
            <Input
              name="order_code"
              label="Mã đơn hàng"
              placeholder="Nhập mã đơn hàng..."
              readOnly
            />
            <InputSelect
              name="shipper_type"
              label="Đơn vị vận chuyển"
              placeholder="Chọn đơn vị giao vận"
              className="h-[35.78px] text-sm"
              options={dataShipperType}
              value={handleFilterShipperType(formValue.shipper_type)}
            />
            <Input
              typeInput="number"
              min={0}
              max={100}
              name="discount"
              label="Phần trăm giảm giá"
              placeholder="Nhập % giảm giá..."
            />{" "}
            <Input
              name="total_shipping_fee"
              label="Tổng phí vận chuyển"
              placeholder="Nhập phí vận chuyển..."
            />
            <Input
              name="customer_name"
              label="Tên khách hàng"
              placeholder="Nhập tên khách hàng..."
            />
            <Input
              name="customer_phone"
              label="SĐT khách hàng"
              placeholder="Nhập SĐT khách hàng..."
            />
            <Input
              name="customer_email"
              label="Email khách hàng"
              placeholder="Nhập email khách hàng..."
            />
            <Input
              name="customer_note"
              label="Ghi chú đơn hàng"
              placeholder="Nhập ghi chú đơn hàng..."
            />
            <InputSelect
              name="customer_province"
              label="Tỉnh/Thành phố"
              placeholder="VD: Thành phố Hà Nội"
              className="h-[35.78px] text-sm"
              options={handleOptionPlace(province)}
              value={handleFilterPlace(formValue.customer_province, province)}
            />
            <InputSelect
              name="customer_district"
              label="Quận/Huyện"
              placeholder="VD: Quận Hoàn Kiếm"
              className="h-[35.78px] text-sm"
              options={handleOptionPlace(district)}
              value={handleFilterPlace(formValue.customer_district, district)}
            />
            <InputSelect
              name="customer_wards"
              label="Xã/Phường"
              placeholder="VD: Phường Yên Lãng"
              className="h-[35.78px] text-sm"
              options={handleOptionPlace(wards)}
              value={handleFilterPlace(formValue.customer_wards, wards)}
            />
            <Input
              name="customer_address_detail"
              label="Địa chỉ"
              placeholder="VD: Số 3, đường Lạc Long Quân"
            />
          </div>
          {order.time_line_product?.length > 0 ? (
            <div className="p-4 rounded-xl bg-[#fff]">
              <h4 className="mb-4 font-semibold">
                {order.time_line_product?.length} sản phẩm
              </h4>
              <Grid fluid className="flex flex-col gap-y-5">
                {order.time_line_product?.map((product, index) => (
                  <Row key={v4()}>
                    <Col xs={1}>{index}</Col>
                    <Col xs={3}>
                      <div className="w-[80px] h-[80px]">
                        {product.image_link ? (
                          <img
                            className="w-full h-full rounded-md"
                            src={product.image_url}
                            alt="product_image"
                          />
                        ) : (
                          <img
                            className="w-full h-full rounded-md"
                            src={NoImageProduct}
                            alt="product_image"
                          />
                        )}
                      </div>
                    </Col>
                    <Col xs={12}>
                      <div>
                        <div>{product.name}</div>
                        <div className="text-gray-500 text-sm">
                          <span>Phân loại:</span>{" "}
                          <span>
                            {product.properties?.list_properties?.name}{" "}
                            {
                              product.properties?.list_properties
                                ?.product_sub_property?.name
                            }
                          </span>
                        </div>
                      </div>
                    </Col>
                    <Col xs={4}>
                      {" "}
                      <div className="w-[140px]">
                        <InputGroup>
                          <InputGroup.Button onClick={() => handleMinus(index)}>
                            -
                          </InputGroup.Button>
                          <InputNumber
                            className={"custom-input-number"}
                            value={values?.[index] || 0}
                            onChange={(value) => handleChange(value, index)}
                          />
                          <InputGroup.Button onClick={() => handlePlus(index)}>
                            +
                          </InputGroup.Button>
                        </InputGroup>
                      </div>
                    </Col>
                    <Col xs={4}>
                      {product.properties?.list_properties?.product_sub_property
                        ?.price
                        ? formatNumber(
                            product.properties?.list_properties
                              ?.product_sub_property?.price
                          )
                        : formatNumber(
                            product.properties?.list_properties?.price
                          )}
                      đ
                    </Col>
                  </Row>
                ))}
              </Grid>
            </div>
          ) : null}

          <div className="p-4 rounded-xl bg-[#fff]">
            <div className="flex gap-x-5">
              <button
                type="button"
                className="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center flex"
                onClick={handleUpdateOrder}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-5 h-5"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                  />
                </svg>
                Cập nhật đơn hàng
              </button>
              <button
                type="button"
                className="text-gray-900 bg-gradient-to-r from-lime-200 via-lime-400 to-lime-500 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-lime-300 dark:focus:ring-lime-800 shadow-lg shadow-lime-500/50 dark:shadow-lg dark:shadow-lime-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center flex"
                onClick={() => navigate(-1)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-5 h-5"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19.5 12h-15m0 0l6.75 6.75M4.5 12l6.75-6.75"
                  />
                </svg>
                Quay lại
              </button>
            </div>
          </div>
        </div>
      </Form>
    </div>
  );
}
