import { Breadcrumb } from "rsuite";
import styled from "styled-components";
import Breadcrumbs from "../../../../../components/admin/breadcrumb";
import Copyright from "../../../../../components/admin/copyright/Copyright";
import Content from "./Content";
import NavLink from "../../../../../components/admin/navLink/NavLink";

const OrderUpdateStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const OrderUpdate = () => {
  return (
    <OrderUpdateStyles>
      <Breadcrumbs
        title={"Cập nhật đơn hàng"}
        breadcrumbItems={[
          <Breadcrumb.Item to="/admin/orders" as={NavLink}>
            Đơn hàng
          </Breadcrumb.Item>,
          <Breadcrumb.Item active>Cập nhật</Breadcrumb.Item>,
        ]}
      >
        <Content></Content>
        <Copyright></Copyright>
      </Breadcrumbs>
    </OrderUpdateStyles>
  );
};
export default OrderUpdate;
