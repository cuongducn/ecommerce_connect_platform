import { Breadcrumb } from "rsuite";
import styled from "styled-components";
import Breadcrumbs from "../../../../../components/admin/breadcrumb";
import Copyright from "../../../../../components/admin/copyright/Copyright";
import Content from "./Content";
import NavLink from "../../../../../components/admin/navLink/NavLink";

const OrderStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const OrderDetail = () => {
  return (
    <OrderStyles>
      <Breadcrumbs
        title="Đơn hàng"
        breadcrumbItems={[
          <Breadcrumb.Item to="/admin/orders" as={NavLink}>
            Đơn hàng
          </Breadcrumb.Item>,
          <Breadcrumb.Item active>Chi tiết</Breadcrumb.Item>,
        ]}
      >
        <Content></Content>
        <Copyright></Copyright>
      </Breadcrumbs>
    </OrderStyles>
  );
};
export default OrderDetail;
