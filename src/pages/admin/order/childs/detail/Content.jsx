import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import * as orderAction from "../../../../../actions/admin/order";
import { useEffect, useRef } from "react";
import {
  orderStatuses,
  paymentStatuses,
} from "../../../../../utils/data/order";
import NoImage from "../../../../../assets/no_img.png";
import { formatNumber } from "../../../../../utils";

export default function Content() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { order } = useSelector((state) => state.orderAdminReducers);
  const params = useParams();
  const orderCode = params.order_code;

  const listOrderStatusRef = useRef(orderStatuses);
  const listPaymentStatusRef = useRef(paymentStatuses);

  const handlePrint = () => {
    dispatch(orderAction.exportBills(orderCode));
  };
  const handleChangeStatus = (value, type) => {
    var data = {};
    if (type === "order_status") {
      data = {
        order_status: value,
      };
    } else {
      data = {
        payment_status: value,
      };
    }
    dispatch(orderAction.updateStatusOrder(orderCode, data));
  };

  useEffect(() => {
    if (!orderCode) return;
    dispatch(orderAction.getOneOrder(orderCode));
  }, [dispatch, orderCode]);
  return (
    <div>
      <div className="flex gap-x-5 justify-end">
        <button
          type="button"
          className="text-gray-900 bg-gradient-to-r from-lime-200 via-lime-400 to-lime-500 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-lime-300 dark:focus:ring-lime-800 shadow-lg shadow-lime-500/50 dark:shadow-lg dark:shadow-lime-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center flex gap-x-1"
          onClick={() => navigate(-1)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-5 h-5"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M19.5 12h-15m0 0l6.75 6.75M4.5 12l6.75-6.75"
            />
          </svg>
          Quay lại
        </button>
        <button
          type="button"
          className="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center flex gap-x-1"
          onClick={handlePrint}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-5 h-5"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M6.72 13.829c-.24.03-.48.062-.72.096m.72-.096a42.415 42.415 0 0110.56 0m-10.56 0L6.34 18m10.94-4.171c.24.03.48.062.72.096m-.72-.096L17.66 18m0 0l.229 2.523a1.125 1.125 0 01-1.12 1.227H7.231c-.662 0-1.18-.568-1.12-1.227L6.34 18m11.318 0h1.091A2.25 2.25 0 0021 15.75V9.456c0-1.081-.768-2.015-1.837-2.175a48.055 48.055 0 00-1.913-.247M6.34 18H5.25A2.25 2.25 0 013 15.75V9.456c0-1.081.768-2.015 1.837-2.175a48.041 48.041 0 011.913-.247m10.5 0a48.536 48.536 0 00-10.5 0m10.5 0V3.375c0-.621-.504-1.125-1.125-1.125h-8.25c-.621 0-1.125.504-1.125 1.125v3.659M18 10.5h.008v.008H18V10.5zm-3 0h.008v.008H15V10.5z"
            />
          </svg>
          In hóa đơn
        </button>
      </div>
      <div className="flex gap-x-5 mt-6">
        <div className="w-[300px] flex flex-col gap-y-5">
          <div>
            <div className="bg-[rgb(230,232,233)] py-2 px-5 font-semibold">
              Trạng thái đơn hàng
            </div>
            <div className="h-[400px] overflow-auto gap-y-1 flex flex-col">
              {listOrderStatusRef.current?.map((orderStatus, index) => (
                <div
                  className="py-2 px-5 cursor-pointer"
                  key={index}
                  onClick={() =>
                    handleChangeStatus(orderStatus.status, "order_status")
                  }
                  style={{
                    backgroundColor:
                      order.order_status === orderStatus.status
                        ? "rgb(0, 137, 123)"
                        : "rgb(240,240,240)",
                    color:
                      order.order_status === orderStatus.status
                        ? "#fff"
                        : "#081c36",
                  }}
                >
                  {orderStatus.title}
                </div>
              ))}
            </div>
          </div>
          <div>
            <div className="bg-[rgb(230,232,233)] py-2 px-5 font-semibold">
              Trạng thái thanh toán
            </div>
            <div className="h-[400px] overflow-auto gap-y-1 flex flex-col">
              {listPaymentStatusRef.current?.map((paymentStatus, index) => (
                <div
                  className="bg-[rgb(240,240,240)] py-2 px-5 cursor-pointer"
                  key={index}
                  onClick={() =>
                    handleChangeStatus(paymentStatus.status, "payment_status")
                  }
                  style={{
                    backgroundColor:
                      order.payment_status === paymentStatus.status
                        ? "rgb(0, 137, 123)"
                        : "rgb(240,240,240)",
                    color:
                      order.payment_status === paymentStatus.status
                        ? "#fff"
                        : "#081c36",
                  }}
                >
                  {paymentStatus.title}
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className="flex-1 flex flex-col gap-y-5">
          <div className="rounded-md bg-[#fff] p-4 ">
            <div className="mb-5 font-semibold text-lg">
              Mã đơn: #{orderCode} | {order.time_line_product?.length} sản phẩm
            </div>
            <div className="flex flex-col gap-y-4 h-[217px] overflow-y-auto">
              {order.time_line_product?.length > 0 &&
                order.time_line_product?.map((product, index) => (
                  <div
                    key={index}
                    className={`flex gap-x-3 ${
                      index !== order.time_line_product?.length - 1
                        ? "border-b border-gray-100"
                        : ""
                    }`}
                  >
                    <div className="">
                      <img
                        className="w-[100px] h-[100px]"
                        src={
                          product.image_link?.length > 0
                            ? product.image_link[0]
                            : NoImage
                        }
                        alt={product.name}
                      />
                    </div>
                    <div className="flex-1">
                      <div className="font-medium mb-2">{product.name}</div>
                      <div className="w-full flex justify-between items-center">
                        <span>Tổng số lượng</span>
                        <span>x{product.quantity}</span>
                      </div>
                      <div className="w-full flex justify-between items-center">
                        <span>Giá sản phẩm</span>
                        <span>{formatNumber(product.price)}đ</span>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
          <div className="rounded-md bg-[#fff] p-4 ">
            <div className="mb-5 font-semibold text-lg">
              Thông tin khách hàng
            </div>
            <div className="flex flex-col gap-y-2">
              {" "}
              <div>Khách hàng: {order.name_sender}</div>
              <div>SĐT khách hàng: {order.phone_number}</div>
              <div>Người nhận: {order.customer_name}</div>
              <div>SĐT người nhận: {order.customer_phone}</div>
              <div>
                Địa chỉ nhận:{" "}
                {`${
                  order.customer_address_detail
                    ? `${order.customer_address_detail},`
                    : ""
                }${
                  order.customer_wards_name
                    ? `${order.customer_wards_name},`
                    : ""
                }${
                  order.customer_district_name
                    ? `${order.customer_district_name},`
                    : ""
                }${
                  order.customer_province_name
                    ? `${order.customer_province_name}`
                    : ""
                }`}
              </div>
              <div>Email: {order.customer_email}</div>
              <div>Thời gian: {order.created_at}</div>
              <div>
                Phương thức thanh toán:{" "}
                {
                  paymentStatuses.filter(
                    (paymentStatus) =>
                      paymentStatus.status === order.payment_status
                  )?.[0]?.title
                }
              </div>
              <div>Ghi chú: {order.note}</div>
            </div>
          </div>
        </div>
        <div className="w-[300px]">
          <div className="bg-[#fff] p-4 rounded-md">
            <div className="font-semibold mb-4">Tổng tiền</div>
            <div className="flex flex-col gap-y-2">
              <div className="flex justify-between">
                <span>Phí vận chuyển</span>
                <span>
                  {order.total_shipping_fee
                    ? formatNumber(order.total_shipping_fee)
                    : 0}
                  đ
                </span>
              </div>
              <div className="flex justify-between">
                <span>Giảm giá</span>
                <span>
                  {order.discount ? formatNumber(order.discount) : 0}%
                </span>
              </div>
              <div className="flex justify-between ">
                <span>Thành tiền</span>
                <span className="font-semibold text-xl">
                  {order.total_final ? formatNumber(order.total_final) : 0}đ
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
