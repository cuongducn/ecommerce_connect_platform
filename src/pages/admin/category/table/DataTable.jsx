import {
  Table,
  Pagination,
  InputGroup,
  Input,
  Stack,
  InputPicker,
  IconButton,
} from 'rsuite';
import { useCallback, useEffect, useState } from 'react';
import ImageCell from '../../../../components/admin/table/ImageCell';
import NameCell from '../../../../components/admin/table/NameCell';
import { Search, Check, Close, Edit, Trash } from '@rsuite/icons';
import { useDispatch, useSelector } from 'react-redux';
import * as category from '../../../../actions/admin/category';
import { combineAddress, formatNumber } from '../../../../utils';
import styled from 'styled-components';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { debounce } from 'lodash';
import ToggleLoading from '../../../../components/admin/button/ToggleLoading';
import * as Types from '../../../../constants/actionType';
import * as categoryAction from '../../../../actions/admin/category';

const { Column, HeaderCell, Cell } = Table;
const rowKey = 'id';
const statusCensorship = [
  {
    label: 'Chưa được duyệt',
    value: Types.STATUS_PROGRESSING,
  },
  {
    label: 'Đã được duyệt',
    value: Types.STATUS_COMPLETED,
  },
  {
    label: 'Đã hủy',
    value: Types.STATUS_CANCELLED,
  },
];
const DataTableStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding-top: 20px;
  border-radius: 10px;
  .header {
    display: flex;
    justify-content: flex-end;
    margin-bottom: 20px;
    padding: 0 20px;
    .input__search-content {
      width: 300px;
    }
  }
  @media only screen and (max-width: 768px) {
    .header {
      .input__search-content {
        width: 190px;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .header {
      flex-direction: column;
      row-gap: 10px;
      padding: 0 20px;
      .rs-stack-item {
        width: 100%;
        overflow: visible !important;
        .input__search-content {
          width: 100%;
        }
        .rs-picker {
          margin-right: 0 !important;
          width: 100% !important;
        }
      }
    }
  }
`;
const DataTable = () => {
  const dispatch = useDispatch();
  const { isLoadingCategory } = useSelector((state) => state.loadingReducers);
  const { data: categories } = useSelector(
    (state) => state.categoryAdminReducers.categoryAdmin.categories
  );
  const { data } = categories;
  const [categorySelected, setCategorySelected] = useState({});
  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get('page')) || 1,
    limit: 20,
    status: Number(searchParams.get('status')) || '',
    search: searchParams.get('search') || '',
    searchInput: searchParams.get('search') || '',
  });
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const navigate = useNavigate();

  // Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        navigate(
          `/categories?page=1${valueSearch ? `&search=${valueSearch}` : ''}${
            prevParams.status !== '' ? `&status=${prevParams.status}` : ''
          }`
        );
        return {
          ...prevParams,
          page: 1,
          search: valueSearch,
          searchInput: valueSearch,
        };
      });
    }, 500),
    []
  );
  const handleSearchCategories = (e) => {
    const value = e;
    setParams({
      ...params,
      searchInput: value,
    });
    debounceCallBack(value);
  };
  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data?.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === 'string') {
          x = x.charCodeAt();
        }
        if (typeof y === 'string') {
          y = y.charCodeAt();
        }
        if (sortData.type === 'asc') {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    let dataIndex = [];
    if (data?.length > 0) {
      dataIndex = data.map((item, index) => {
        item.index =
          (Number(categories.current_page) - 1) * Number(categories.per_page) +
          index +
          1;
        return item;
      });
    }
    return dataIndex;
  };
  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };
  // Handle change params
  const handleChangePage = (pageChange) => {
    navigate(
      `/categories?page=${pageChange}${
        params.search ? `&search=${params.search}` : ''
      }${params.status !== '' ? `&status=${params.status}` : ''}`
    );
    setParams({
      ...params,
      page: pageChange,
    });
  };

  //Handle Consorship clinic
  const handleConsorshipClinic = (idClinic, isChecked) => {
    const data = {
      status: isChecked ? Types.STATUS_COMPLETED : Types.STATUS_CANCELLED,
    };

    dispatch(clinic.updateClinic(idClinic, data));
  };

  //Handle Actions
  const ActionCell = ({ rowData, dataKey, ...props }) => {
    const [open, setOpen] = useState(false);
    const handleClose = () => setOpen(false);

    const handleDeletePacketMedical = () => {
      const data = {
        product_ids: [rowData[dataKey]],
      };
      console.log(rowData[dataKey]);
      const queryString = getParams(
        params.page,
        params.limit,
        params.isShowHome,
        params.search,
        params.date_from,
        params.date_to
      );
      categoryAction.deleteCategory(rowData[dataKey], () =>
        setIsModalDelete(false)
      )(dispatch);
    };
    return (
      <Cell {...props} style={{ padding: 5 }} className="link-group">
        <div className="btn__actions">
          <IconButton
            appearance="subtle"
            onClick={() =>
              navigate(
                `/admin/products/update/${rowData[dataKey]}?${getParams(
                  params.page,
                  params.limit,
                  params.isShowHome,
                  params.search,
                  params.date_from,
                  params.date_to
                )}`
              )
            }
            icon={<Edit />}
          />
          <IconButton
            appearance="subtle"
            onClick={() => setOpen(true)}
            icon={<Trash />}
          />
        </div>

        <ModalCustom
          size="sm"
          title="Thông báo"
          open={open}
          nameAction="Xóa"
          handleClose={handleClose}
          // handleAction={handleDeletePacketMedical}
        >
          Bạn có chắc chắn muốn xóa sản phẩm này không?
        </ModalCustom>
      </Cell>
    );
  };

  //Handle Change Status
  const handleChangeStatus = (value) => {
    navigate(
      `/phong-kham?page=1${params.search ? `&search=${params.search}` : ''}${
        value !== null ? `&status=${value}` : ''
      }`
    );
    setParams({
      ...params,
      page: 1,
      status:
        value === Types.STATUS_COMPLETED
          ? Types.STATUS_COMPLETED
          : value === Types.STATUS_CANCELLED
          ? Types.STATUS_CANCELLED
          : value === Types.STATUS_PROGRESSING
          ? Types.STATUS_PROGRESSING
          : '',
    });
  };

  // Call API Get Clinics
  useEffect(() => {
    // dispatch(
    //   clinic.getClinics(
    //     `page=${params.page}&limit=${params.limit}&search=${params.search}&status=${params.status}`
    //   )
    // );
    dispatch(
      category.categories(
        `page=${params.page}&limit=${params.limit}&search=${params.search}&status=${params.status}`
      )
    );
  }, [dispatch, params.page, params.limit, params.search, params.status]);
  return (
    <DataTableStyles>
      <Stack className="header" justifyContent="space-between">
        <InputGroup inside className="input__search-content">
          <Input
            placeholder="Tìm kiếm danh mục..."
            onChange={handleSearchCategories}
            value={params.searchInput}
          />
          <InputGroup.Addon>
            <Search />
          </InputGroup.Addon>
        </InputGroup>
        <InputPicker
          // data={statusCensorship}
          style={{ width: 170 }}
          placeholder="Trạng thái: Tất cả"
          onChange={handleChangeStatus}
        />
      </Stack>
      <Table
        autoHeight
        wordWrap="break-word"
        rowKey={rowKey}
        hover={false}
        data={getData()}
        sortColumn={sortData.column}
        sortType={sortData.type}
        onSortColumn={handleSortColumn}
        loading={sortData.loading}
      >
        <Column align="center" width={50}>
          <HeaderCell>STT</HeaderCell>
          <Cell dataKey="index" />
        </Column>
        <Column align="center" width={200}>
          <HeaderCell>Ảnh danh mục</HeaderCell>
          <ImageCell
            dataKey="image_url"
            styleImage={{
              width: '40px',
              height: '40px',
            }}
          />
        </Column>
        <Column width={150} sortable>
          <HeaderCell>Tên danh mục</HeaderCell>
          <Cell dataKey="name" />
        </Column>
        <Column width={120}>
          <HeaderCell>Trạng thái</HeaderCell>
          <Cell>
            {(rowData) => (
              <>
                {rowData.status === Types.STATUS_PROGRESSING ? (
                  <div className="btn__actions" style={{ marginTop: -9 }}>
                    <IconButton
                      color="green"
                      appearance="link"
                      // onClick={() =>
                      //   handleConsorshipClinic(rowData[rowKey], true)
                      // }
                      icon={<Check />}
                    />
                    <IconButton
                      color="red"
                      appearance="link"
                      // onClick={() =>
                      //   handleConsorshipClinic(rowData[rowKey], false)
                      // }
                      icon={<Close />}
                    />
                  </div>
                ) : (
                  <ToggleLoading
                    isLoading={isLoadingCategory}
                    status={rowData['is_show_home']}
                    data={rowData}
                    // dataSelected={categorySelected}
                    // setDataSelected={setCategorySelected}
                    // handleAction={handleConsorshipClinic}
                  />
                )}
              </>
            )}
          </Cell>
        </Column>
        <Column width={140} align="center">
          <HeaderCell>Hành động</HeaderCell>
          <ActionCell dataKey="id" />
        </Column>
      </Table>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          size="xs"
          layout={['total', '-', 'pager', 'skip']}
          total={categories.total}
          limitOptions={[10, 30, 50]}
          limit={params.limit}
          activePage={params.page}
          onChangePage={handleChangePage}
        />
      </div>
    </DataTableStyles>
  );
};

export default DataTable;
