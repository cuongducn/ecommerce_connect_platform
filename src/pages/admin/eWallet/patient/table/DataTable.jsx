import React from "react";
import {
  Avatar,
  Button,
  IconButton,
  Input,
  InputGroup,
  InputPicker,
  Modal,
  Pagination,
  Stack,
  Table,
} from "rsuite";
import styled from "styled-components";
import { Search, Check, Close } from "@rsuite/icons";
import * as Types from "../../../../constants/actionType";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useState } from "react";
import { useCallback } from "react";
import { debounce } from "lodash";
import { useEffect } from "react";
import * as eWalletAction from "../../../../actions/eWallet";
import RowExpandedInformation from "./RowExpandedInformation";
import ExpandCell from "../../../../components/table/ExpandCell";
import NameCell from "../../../../components/table/NameCell";
import { formatNumber } from "../../../../utils";

const { Column, HeaderCell, Cell } = Table;
const rowKey = "id";
const statusCensorship = [
  {
    label: "Chờ xét duyệt",
    value: Types.STATUS_PROGRESSING,
  },
  {
    label: "Đã duyệt",
    value: Types.STATUS_COMPLETED,
  },
  {
    label: "Đã hủy",
    value: Types.STATUS_CANCELLED,
  },
];
const DataTableStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding-top: 20px;
  border-radius: 10px;
  .header {
    display: flex;
    justify-content: flex-start;
    margin-bottom: 20px;
    padding-left: 20px;
    .input__search-content {
      width: 300px;
    }
  }
  @media only screen and (max-width: 768px) {
    .header {
      .input__search-content {
        width: 170px;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .header {
      flex-direction: column;
      row-gap: 10px;
      padding: 0 20px;
      .rs-stack-item {
        width: 100%;
        overflow: visible !important;
        .input__search-content {
          width: 100%;
        }
        .rs-picker {
          margin-right: 0 !important;
          width: 100% !important;
        }
      }
    }
  }
`;

const DataTable = () => {
  const dispatch = useDispatch();
  const { listDepositWithdrawPatient } = useSelector(
    (state) => state.eWalletReducers
  );
  const { data } = listDepositWithdrawPatient;

  const [expandedRowKeys, setExpandedRowKeys] = useState([]);
  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get("page")) || 1,
    limit: 20,
    status: Number(searchParams.get("status")) || "",
    search: searchParams.get("search") || "",
    searchInput: searchParams.get("search") || "",
  });
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const navigate = useNavigate();
  const [openModalDelete, setOpenModalDelete] = useState();
  const [note, setNote] = useState("");
  const [depositWithdrawSelected, setDepositWithdrawSelected] = useState();
  const [statusSelected, setStatusSelected] = useState();

  const handleCloseModal = () => {
    setDepositWithdrawSelected(null);
    setOpenModalDelete(false);
    setNote("");
    setStatusSelected("");
  };
  const handleConfirm = () => {
    const data = {
      note,
      is_withdrawal:
        depositWithdrawSelected?.is_withdrawal == true ? true : false,
      status: statusSelected,
    };
    dispatch(
      eWalletAction.confirmDepositWithdrawPatient(
        depositWithdrawSelected?.id,
        data,
        `page=${params.page}&limit=${params.limit}&search=${params.search}&status=${params.status}`,
        () => {
          handleCloseModal();
        }
      )
    );
  };
  const handleChange = (value) => {
    setNote(value);
  };

  // Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        navigate(
          `/quan-ly-nap-rut/benh-nhan?page=1${
            valueSearch ? `&search=${valueSearch}` : ""
          }${prevParams.status !== "" ? `&status=${prevParams.status}` : ""}`
        );
        return {
          ...prevParams,
          page: 1,
          search: valueSearch,
          searchInput: valueSearch,
        };
      });
    }, 500),
    []
  );
  const handleSearchDoctors = (e) => {
    const value = e;
    setParams({
      ...params,
      searchInput: value,
    });
    debounceCallBack(value);
  };
  const handleChangePage = (pageChange) => {
    navigate(
      `/quan-ly-nap-rut/benh-nhan?page=${pageChange}${
        params.search ? `&search=${params.search}` : ""
      }${params.status !== "" ? `&status=${params.status}` : ""}`
    );
    setParams({
      ...params,
      page: pageChange,
    });
  };

  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === "string") {
          x = x.charCodeAt();
        }
        if (typeof y === "string") {
          y = y.charCodeAt();
        }
        if (sortData.type === "asc") {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    return data;
  };

  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };

  // Handle Open Expand
  const handleExpanded = (rowData) => {
    let open = false;
    const nextExpandedRowKeys = [];

    expandedRowKeys.forEach((key) => {
      if (key === rowData[rowKey]) {
        open = true;
      } else {
        nextExpandedRowKeys.push(key);
      }
    });

    if (!open) {
      nextExpandedRowKeys.push(rowData[rowKey]);
    }

    setExpandedRowKeys(nextExpandedRowKeys);
  };

  //Handle Change Status
  const handleChangeStatus = (value) => {
    navigate(
      `/quan-ly-nap-rut/benh-nhan?page=1${
        params.search ? `&search=${params.search}` : ""
      }${value !== null ? `&status=${value}` : ""}`
    );

    setParams({
      ...params,
      page: 1,
      status:
        value === Types.STATUS_PROGRESSING
          ? Types.STATUS_PROGRESSING
          : value === Types.STATUS_COMPLETED
          ? Types.STATUS_COMPLETED
          : value === Types.STATUS_CANCELLED
          ? Types.STATUS_CANCELLED
          : "",
    });
  };

  //Xét duyệt yêu cầu rút nạp ví
  const handleUpdateStatus = (data, status) => {
    setDepositWithdrawSelected(data);
    setOpenModalDelete(true);
    setStatusSelected(status);
  };

  // Call API Get History Payment
  useEffect(() => {
    dispatch(
      eWalletAction.getListDepositWithdrawPatient(
        `page=${params.page}&limit=${params.limit}&search=${params.search}&status=${params.status}`
      )
    );
  }, [dispatch, params.page, params.limit, params.search, params.status]);
  return (
    <DataTableStyles>
      <Stack className="header" justifyContent="space-between">
        <InputGroup inside className="input__search-content">
          <Input
            placeholder="Tìm kiếm thông tin nạp rút..."
            onChange={handleSearchDoctors}
            value={params.searchInput}
          />
          <InputGroup.Addon>
            <Search />
          </InputGroup.Addon>
        </InputGroup>
        <InputPicker
          data={statusCensorship}
          style={{ width: 170, marginRight: 20 }}
          placeholder="Trạng thái: Tất cả"
          onChange={handleChangeStatus}
        />
      </Stack>
      <Table
        autoHeight
        rowKey={rowKey}
        hover={false}
        expandedRowKeys={expandedRowKeys}
        renderRowExpanded={RowExpandedInformation}
        rowExpandedHeight={400}
        data={getData()}
        sortColumn={sortData.column}
        sortType={sortData.type}
        onSortColumn={handleSortColumn}
        loading={sortData.loading}
      >
        <Column align="center" width={50}>
          <HeaderCell>#</HeaderCell>
          <ExpandCell
            dataKey="id"
            expandedRowKeys={expandedRowKeys}
            onChange={handleExpanded}
          />
        </Column>
        <Column width={160}>
          <HeaderCell>Bệnh nhân</HeaderCell>
          <Cell>{(rowData) => rowData["customer"]?.name}</Cell>
        </Column>
        <Column width={120}>
          <HeaderCell>Yêu cầu</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData["is_withdrawal"] === true ? "Rút tiền" : "Nạp tiền"
            }
          </Cell>
        </Column>
        <Column width={120}>
          <HeaderCell>Giá tiền</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData["money"] ? `${formatNumber(rowData["money"])} ₫` : ""
            }
          </Cell>
        </Column>

        <Column flexGrow={1}>
          <HeaderCell>Nội dung chuyển</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData["content_transfer"] ? rowData["content_transfer"] : ""
            }
          </Cell>
        </Column>
        <Column width={215}>
          <HeaderCell>Thời gian yêu cầu</HeaderCell>
          <Cell>
            {(rowData) => (rowData["created_at"] ? rowData["created_at"] : "")}
          </Cell>
        </Column>
        <Column width={215}>
          <HeaderCell>Lý do xét duyệt</HeaderCell>
          <Cell>{(rowData) => (rowData["note"] ? rowData["note"] : "")}</Cell>
        </Column>

        <Column width={180}>
          <HeaderCell>Xét duyệt</HeaderCell>
          <Cell>
            {(rowData) => (
              <>
                {rowData.status === Types.STATUS_PROGRESSING ? (
                  <div className="btn__actions" style={{ marginTop: -7 }}>
                    <IconButton
                      color="green"
                      appearance="link"
                      onClick={() =>
                        handleUpdateStatus(rowData, Types.STATUS_COMPLETED)
                      }
                      icon={<Check />}
                    />
                    <IconButton
                      color="red"
                      appearance="link"
                      onClick={() =>
                        handleUpdateStatus(rowData, Types.STATUS_CANCELLED)
                      }
                      icon={<Close />}
                    />
                  </div>
                ) : rowData.status === Types.STATUS_COMPLETED ? (
                  <div style={{ color: "#22a12a" }}>Đã duyệt</div>
                ) : rowData.status === Types.STATUS_CANCELLED ? (
                  <div style={{ color: "#b81c07" }}>Đã hủy</div>
                ) : (
                  ""
                )}
              </>
            )}
          </Cell>
        </Column>
      </Table>
      <Modal size="xs" open={openModalDelete} onClose={handleCloseModal}>
        <Modal.Header>
          <Modal.Title>
            <div
              className="text-xl font-medium text-center"
              style={{
                fontWeight: "500",
              }}
            >
              Xác nhận giao dịch
            </div>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <p className="mx-auto text-center text-gray4B">
              Bạn có muốn{" "}
              {statusSelected == Types.STATUS_COMPLETED
                ? " cho phép "
                : " hủy "}{" "}
              giao dịch
              {depositWithdrawSelected?.is_withdrawal == true
                ? " rút tiền "
                : " nạp tiền "}
              này không?
            </p>
            <div className="my-3">
              <Input
                placeholder="Nhập lý do"
                value={note}
                name="note"
                onChange={handleChange}
              ></Input>
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              {statusSelected == Types.STATUS_COMPLETED ? (
                <Button
                  appearance="primary"
                  color="green"
                  onClick={handleConfirm}
                >
                  Duyệt
                </Button>
              ) : (
                <Button
                  appearance="primary"
                  color="red"
                  onClick={handleConfirm}
                >
                  Hủy
                </Button>
              )}
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          size="xs"
          layout={["total", "-", "pager", "skip"]}
          total={listDepositWithdrawPatient.total}
          limitOptions={[10, 30, 50]}
          limit={params.limit}
          activePage={params.page}
          onChangePage={handleChangePage}
        />
      </div>
    </DataTableStyles>
  );
};

export default DataTable;
