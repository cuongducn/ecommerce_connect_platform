import React from "react";
import { Col, Grid, Row } from "rsuite";
import styled from "styled-components";

const RowExpandedInformationStyles = styled.div`
  h4 {
    margin-bottom: 15px;
  }
  .information__left {
    display: flex;
    flex-direction: column;
    row-gap: 5px;
  }
  .information__left {
    .information__title {
      font-weight: 500;
    }
  }
  .information__right {
    span {
      font-weight: 500;
    }
  }
`;

const RowExpandedInformation = (rowData) => {
  return (
    <RowExpandedInformationStyles>
      <Grid fluid>
        <Row className="rowExpanded">
          <Col>
            <div className="information__left">
              <div>
                <span className="information__title">Tên tài khoản: </span>
                <span>{rowData.bank_account_name_transfer}</span>
              </div>
              <div>
                <span className="information__title">Số tài khoản: </span>
                <span>{rowData.bank_account_number_transfer}</span>
              </div>
              <div>
                <span className="information__title">Tên ngân hàng: </span>
                <span>{rowData.bank_name_transfer}</span>
              </div>
            </div>
          </Col>
        </Row>
      </Grid>
    </RowExpandedInformationStyles>
  );
};

export default RowExpandedInformation;
