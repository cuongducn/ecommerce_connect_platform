import { Breadcrumb } from 'rsuite';
import styled from 'styled-components';
import Breadcrumbs from '../../../../components/admin/breadcrumb';
import Copyright from '../../../../components/admin/copyright/Copyright';
import DataTable from './table/DataTable';

const PatientStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const DepositWithdrawPatient = () => {
  return (
    <PatientStyles>
      <Breadcrumbs
        title="Nạp rút bệnh nhân"
        breadcrumbItems={[
          <Breadcrumb.Item active>Nạp rút bệnh nhân</Breadcrumb.Item>,
        ]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </PatientStyles>
  );
};
export default DepositWithdrawPatient;
