import { Breadcrumb } from 'rsuite';
import styled from 'styled-components';
import Breadcrumbs from '../../../../components/admin/breadcrumb';
import Copyright from '../../../../components/admin/copyright/Copyright';
import DataTable from './table/DataTable';

const DoctorStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const DepositWithdrawDoctor = () => {
  return (
    <DoctorStyles>
      <Breadcrumbs
        title="Nạp rút bác sĩ"
        breadcrumbItems={[
          <Breadcrumb.Item active>Nạp rút bác sĩ</Breadcrumb.Item>,
        ]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </DoctorStyles>
  );
};
export default DepositWithdrawDoctor;
