import { Breadcrumb } from 'rsuite';
import styled from 'styled-components';
import Breadcrumbs from '../../../../components/admin/breadcrumb';
import Copyright from '../../../../components/admin/copyright/Copyright';
import DataTable from './table/DataTable';

const HistoryBalanceStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const HistoryBalance = () => {
  return (
    <HistoryBalanceStyles>
      <Breadcrumbs
        title="Lịch sử số dư"
        breadcrumbItems={[
          <Breadcrumb.Item active>Lịch sử số dư</Breadcrumb.Item>,
        ]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </HistoryBalanceStyles>
  );
};
export default HistoryBalance;
