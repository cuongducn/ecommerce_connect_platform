import React from "react";
import {
  Input,
  InputGroup,
  InputPicker,
  Pagination,
  Stack,
  Table,
} from "rsuite";
import styled from "styled-components";
import { Search } from "@rsuite/icons";
import * as Types from "../../../../constants/actionType";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useState } from "react";
import { useCallback } from "react";
import { debounce } from "lodash";
import { useEffect } from "react";
import * as eWalletAction from "../../../../actions/eWallet";
import RowExpandedInformation from "./RowExpandedInformation";
import { formatNumber } from "../../../../utils";

const { Column, HeaderCell, Cell } = Table;
const rowKey = "id";

const DataTableStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding-top: 20px;
  border-radius: 10px;
  .header {
    display: flex;
    justify-content: flex-start;
    margin-bottom: 20px;
    padding-left: 20px;
    .input__search-content {
      width: 300px;
    }
  }
  @media only screen and (max-width: 768px) {
    .header {
      .input__search-content {
        width: 170px;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .header {
      flex-direction: column;
      row-gap: 10px;
      padding: 0 20px;
      .rs-stack-item {
        width: 100%;
        overflow: visible !important;
        .input__search-content {
          width: 100%;
        }
        .rs-picker {
          margin-right: 0 !important;
          width: 100% !important;
        }
      }
    }
  }
`;

const DataTable = () => {
  const dispatch = useDispatch();
  const { listHistoryBalance } = useSelector((state) => state.eWalletReducers);
  const { data } = listHistoryBalance;

  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get("page")) || 1,
    limit: 20,
    status: Number(searchParams.get("status")) || "",
    search: searchParams.get("search") || "",
    searchInput: searchParams.get("search") || "",
  });
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const navigate = useNavigate();

  // Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        navigate(
          `/quan-ly-nap-rut/lich-su-so-du?page=1${
            valueSearch ? `&search=${valueSearch}` : ""
          }${prevParams.status !== "" ? `&status=${prevParams.status}` : ""}`
        );
        return {
          ...prevParams,
          page: 1,
          search: valueSearch,
          searchInput: valueSearch,
        };
      });
    }, 500),
    []
  );
  const handleSearchDoctors = (e) => {
    const value = e;
    setParams({
      ...params,
      searchInput: value,
    });
    debounceCallBack(value);
  };
  const handleChangePage = (pageChange) => {
    navigate(
      `/quan-ly-nap-rut/lich-su-so-du?page=${pageChange}${
        params.search ? `&search=${params.search}` : ""
      }${params.status !== "" ? `&status=${params.status}` : ""}`
    );
    setParams({
      ...params,
      page: pageChange,
    });
  };

  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === "string") {
          x = x.charCodeAt();
        }
        if (typeof y === "string") {
          y = y.charCodeAt();
        }
        if (sortData.type === "asc") {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    let dataIndex = [];
    if (data?.length > 0) {
      dataIndex = data.map((item, index) => {
        item.index =
          (Number(listHistoryBalance.current_page) - 1) *
            Number(listHistoryBalance.per_page) +
          index +
          1;
        return item;
      });
    }
    return dataIndex;
  };

  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };

  // Call API Get History Payment
  useEffect(() => {
    dispatch(
      eWalletAction.getListHistoryBalance(
        `page=${params.page}&limit=${params.limit}&search=${params.search}&status=${params.status}`
      )
    );
  }, [dispatch, params.page, params.limit, params.search, params.status]);
  return (
    <DataTableStyles>
      <Stack className="header" justifyContent="space-between">
        <InputGroup inside className="input__search-content">
          <Input
            placeholder="Tìm kiếm lịch sử số dư..."
            onChange={handleSearchDoctors}
            value={params.searchInput}
          />
          <InputGroup.Addon>
            <Search />
          </InputGroup.Addon>
        </InputGroup>
      </Stack>
      <Table
        autoHeight
        rowKey={rowKey}
        hover={false}
        renderRowExpanded={RowExpandedInformation}
        rowExpandedHeight={400}
        data={getData()}
        sortColumn={sortData.column}
        sortType={sortData.type}
        onSortColumn={handleSortColumn}
        loading={sortData.loading}
      >
        <Column align="center" width={50}>
          <HeaderCell>STT</HeaderCell>
          <Cell dataKey="index" />
        </Column>
        <Column width={140}>
          <HeaderCell>Nguồn tiền</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData["type_money_from"] === Types.TYPE_FROM_PATIENT
                ? "Bệnh nhân"
                : rowData["type_money_from"] === Types.TYPE_FROM_SYSTEM
                ? "Hệ thống"
                : rowData["type_money_from"] === Types.TYPE_FROM_OUTSIDE
                ? "Bên ngoài"
                : ""
            }
          </Cell>
        </Column>
        <Column width={140}>
          <HeaderCell>Số tiền gốc</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData["balance_origin"]
                ? `${formatNumber(rowData["balance_origin"])} đ`
                : "0 đ"
            }
          </Cell>
        </Column>
        <Column width={140}>
          <HeaderCell>Số tiền thay đổi</HeaderCell>
          <Cell>
            {(rowData) => (
              <span
                style={{
                  color: rowData["take_out_money"] ? "red" : "green",
                }}
              >
                {rowData["money_change"]
                  ? `${rowData["take_out_money"] ? "-" : "+"}${formatNumber(
                      rowData["money_change"]
                    )} đ`
                  : "0 đ"}
              </span>
            )}
          </Cell>
        </Column>
        <Column width={140}>
          <HeaderCell>Số tiền đã thay đổi</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData["account_balance_changed"]
                ? `${formatNumber(rowData["account_balance_changed"])} đ`
                : "0 đ"
            }
          </Cell>
        </Column>
        <Column flexGrow={1}>
          <HeaderCell>Miêu tả</HeaderCell>
          <Cell>{(rowData) => rowData["description"]}</Cell>
        </Column>
        <Column width={215}>
          <HeaderCell>Thời gian thực hiện</HeaderCell>
          <Cell>
            {(rowData) => (rowData["created_at"] ? rowData["created_at"] : "")}
          </Cell>
        </Column>
      </Table>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          size="xs"
          layout={["total", "-", "pager", "skip"]}
          total={listHistoryBalance.total}
          limitOptions={[10, 30, 50]}
          limit={params.limit}
          activePage={params.page}
          onChangePage={handleChangePage}
        />
      </div>
    </DataTableStyles>
  );
};

export default DataTable;
