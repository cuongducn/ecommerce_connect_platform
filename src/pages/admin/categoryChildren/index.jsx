import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
import {
  Button,
  Card,
  Col,
  Form,
  Image,
  Input,
  Modal,
  Row,
  Select,
  Switch,
  Table,
} from "antd";
import { useForm } from "antd/es/form/Form";
import { debounce, uniqueId } from "lodash";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSearchParams } from "react-router-dom";
import * as childrenAction from "../../../actions/admin/categoryChildren";
import { fileType } from "../../../constants/ConstantModel";
import { HiOutlineCamera } from "react-icons/hi";
import * as category from "../../../actions/admin/category";

const CategoryChildren = () => {
  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      key: "stt",
      width: 50,
      align: "center",
      render: (a, b, index) => <span>{index + 1}</span>,
    },
    {
      title: "Tên",
      dataIndex: "name",
      align: "center",
      render: (text, row) => (
        <a
          className="text-blue-700 hover:cursor-pointer"
          onClick={() => {
            setId(row.id);
          }}
        >
          {text}
        </a>
      ),
    },
    {
      title: "Ngày tạo",
      dataIndex: "created_at",

      align: "center",
      render: (text) => <div>{text}</div>,
      // render: (text) => <div>{dayjs(text).format('DD/MM/YYYY')}</div>,
    },
    {
      title: "hiển thị",
      dataIndex: "is_show_home",
      align: "center",
      render: (text, row) => {
        return (
          <Switch
            style={{ border: "1px solid #808080" }}
            checked={text} // Trạng thái switch, true nếu text === 1, ngược lại false
            onChange={(checked) => {
              onStatus(row.id, checked);
            }}
          />
        );
      },
    },

    {
      title: "Thao tác",
      dataIndex: "delete",

      width: 150,
      align: "center",

      render: (val, row) => (
        <Row className="flex justify-center items-center">
          <Col className="mr-1">
            <Button
              onClick={() => {
                setId(row.id);
                setMode("update");
                showModal();
              }}
              shape="circle"
              className="flex justify-center items-center border-white hover:border-none"
            >
              <EditOutlined />
            </Button>
          </Col>
          <Col>
            <Button
              shape="circle"
              className="flex justify-center items-center border-white hover:border-none"
              onClick={() => {
                setId(row.id);
                showModalDelete();
              }}
            >
              <DeleteOutlined />
            </Button>
          </Col>
        </Row>
      ),
    },
  ];

  const [mode, setMode] = useState("add");

  const [isModalDelete, setIsModalDelete] = useState(false);
  const [modalCreate, setModalCreate] = useState(false);
  const [id, setId] = useState(0);
  const [loading] = useState(false);
  const dispatch = useDispatch();
  const [searchParams, setSearchParams] = useSearchParams();
  const [form] = useForm();
  const [imageUrl, setImageUrl] = useState("");
  const [file, setFile] = useState(null);

  const handleCancel = () => {
    setModalCreate(false);
    setIsModalDelete(false);
  };

  const showModal = () => {
    setModalCreate(true);
  };
  const showModalDelete = () => {
    setIsModalDelete(true);
  };

  const { data: categoryChildren } = useSelector(
    (state) => state.childrenReducers.categoryChildren.listChildren
  );
  const { data: imgUrl } = useSelector(
    (state) => state.blogsReducers.blogs.uploadImage
  );

  const onCreateChiders = (val) => {
    childrenAction.createChildren(val, () => {
      form.resetFields();
      setModalCreate(false);
    })(dispatch);
  };

  const [filter, setFilter] = useState({
    page: 1,
    limit: 10,
  });

  const onStatus = (id, val) => {
    const newStatus = val ? 1 : 0;
    childrenAction.updateChildren({
      id,
      is_show_home: newStatus,
    })(dispatch);
  };

  const onUpdateChiders = (val) => {
    childrenAction.updateChildren(
      {
        id,
        ...val,
        image_url: imgUrl[fileType.IMAGE_CHILDREN],
      },
      () => setModalCreate(false)
    )(dispatch);
  };
  const onDelete = () => {
    childrenAction.deleteChildren(
      {
        category_children_ids: [id],
      },
      () => setIsModalDelete(false)
    )(dispatch);
  };

  const searchParamsObject = Object.fromEntries(searchParams.entries());

  const onSearch = debounce((val) => {
    const values = {
      ...searchParamsObject,
      ...val,
    };
    setFilter(values);
  }, 1000);

  const { data: ChildrenDetail } = useSelector(
    (state) => state.childrenReducers.categoryChildren.detailChildren
  );
  const {
    data: { data: categories = [] },
  } = useSelector((state) => state.categorysReducers.categories.listCategories);

  useEffect(() => {
    category.listCategories()(dispatch);
  }, []);

  useEffect(() => {
    if (id) {
      childrenAction.detailChildren(id)(dispatch);
    }
  }, [id]);

  useEffect(() => {
    childrenAction.listChildren(filter)(dispatch);
  }, [filter]);

  useEffect(() => {
    if (mode === "update") form.setFieldsValue(ChildrenDetail);
  }, [mode, ChildrenDetail]);

  const onAvatarUpload = (e) => {
    const file = e.target.files[0];
    setFile(file);
    const reader = new FileReader();
    reader.onload = (event) => {
      setImageUrl(event.target.result);
    };
    reader.readAsDataURL(file);
  };

  useEffect(() => {
    if (file) {
      childrenAction.uploadImgChildren(fileType.IMAGE_CHILDREN, file)(dispatch);
    }
  }, [file, dispatch]);

  return (
    <div className="mx-6 my-6 w-full">
      <Card className="w-full">
        <div>
          <div className="grid grid-cols-2">
            <div className="text-[#00897b] font-sans text-xl col-span-1">
              Danh sách danh mục con
            </div>
            <div className="col-span-1 flex justify-end">
              <Button
                onClick={() => {
                  setMode("add");
                  showModal(true);
                }}
                size="large"
                className="bg-[#00897b] text-white mr-3 "
                icon={<PlusOutlined className="justify-center" />}
              >
                Thêm Danh mục con
              </Button>
            </div>
          </div>
        </div>
        <Form
          name="basic"
          autoComplete="off"
          labelAlign="left"
          layout="vertical"
          onValuesChange={(c, v) => onSearch(v)}
          initialValues={{
            search: searchParams.get("search") || "",
            date_range: [moment(), moment()],
          }}
        >
          <div className="grid gap-2 mt-8 ">
            <div className="gird-col-1 w-[400px]">
              <Form.Item name="search">
                <Input
                  className="h-[40px] "
                  placeholder="Tìm kiếm tên danh mục con,.. "
                  prefix={<EditOutlined />}
                />
              </Form.Item>
            </div>
          </div>
        </Form>
        <div>
          <Table
            size="small"
            columns={columns}
            rowKey="id"
            dataSource={categoryChildren.data || []}
            pagination={{
              current: filter.current_page,
              pageSize: filter.per_page,
              total: categoryChildren.total,
              onChange: (page, pageSize) => {
                setFilter({
                  page: page,
                  limit: pageSize,
                });
                setSearchParams({
                  ...searchParams,
                  page: page,
                  limit: pageSize,
                });
              },
            }}
          />
        </div>
      </Card>

      <Modal
        onCancel={handleCancel}
        open={modalCreate}
        title={mode === "add" ? "Thêm Danh mục con" : " Cập nhật Danh mục con"}
        footer={[
          <Button key="back" type="text" onClick={handleCancel}>
            Hủy bỏ
          </Button>,
          <Button
            style={{
              backgroundColor: "#00897b",
              color: "#ffffff",
            }}
            loading={loading}
            form="form-add-blogs"
            htmlType="submit"
            key="submit"
          >
            Lưu
          </Button>,
        ]}
      >
        <Form
          form={form}
          layout="vertical"
          name="form-add-blogs"
          onFinish={(val) => {
            if (mode === "add") onCreateChiders(val);
            else if (mode === "update") onUpdateChiders(val);
          }}
          initialValues={mode === "update" ? ChildrenDetail : {}}
        >
          <Form.Item label="Danh mục" name="category_id">
            <Select placeholder="chọn danh mục">
              {categories.map((item, index) => (
                <Select.Option key={`categories-opt-${index}`} value={item.id}>
                  {item?.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Tên" name="name">
            <Input />
          </Form.Item>
          <Form.Item label="hình ảnh">
            <div className="relative overflow-visible w-[100px] h-[100px]">
              <Image
                className="rounded-[10px]"
                width={"100%"}
                height={"100%"}
                placeholder={
                  <Image
                    preview={false}
                    width={"100%"}
                    height={"100%"}
                    src={imageUrl}
                  />
                }
                src={
                  categoryChildren.image_children ||
                  imgUrl[fileType.IMAGE_CHILDREN]
                }
                // src={
                //   ChildrenDetail.image_url || imgUrl[fileType.IMAGE_CHILDREN]
                // }
              />
              <div>
                <label
                  htmlFor="image_children"
                  className=" absolute cursor-pointer 
      z-30  bg-gray-700/50 rounded-full flex items-center justify-center text-white 
       p-2 mb-3 w-10 h-10 bottom-0 right-0 translate-x-1/3 translate-y-1/3"
                >
                  <input
                    onChange={onAvatarUpload}
                    type="file"
                    className="hidden"
                    id="image_children"
                  />{" "}
                  <HiOutlineCamera className="text-[18px] translate-y-[2px]" />
                </label>
              </div>
            </div>
          </Form.Item>
          <Form.Item label="Hiển thị " name="is_show_home">
            <Switch style={{ border: "1px solid #808080" }} />
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        onCancel={handleCancel}
        open={isModalDelete}
        title="Xóa blogs"
        footer={[
          <Button key="back" type="text" onClick={handleCancel}>
            Hủy
          </Button>,
          <Button
            style={{
              backgroundColor: "#00897b",
            }}
            // loading={loadingDelete}
            onClick={() => onDelete()}
            htmlType="submit"
            key="submit"
            type="primary"
          >
            Xóa
          </Button>,
        ]}
      >
        <span>Bạn có chắc chắn muốn xóa danh mục con này không ?</span>
      </Modal>
    </div>
  );
};

export default CategoryChildren;
