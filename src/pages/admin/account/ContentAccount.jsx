import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, Schema, Stack } from "rsuite";
import StackItem from "rsuite/esm/Stack/StackItem";
import styled from "styled-components";
import Input from "../../components/form/Input";
import * as profileActions from "../../actions/profile";
import * as auth from "../../actions/auth";
import * as Types from "../../constants/actionType";

const ContentAccountStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding: 20px;
  border-radius: 10px;
  .account__header {
    margin-bottom: 20px;
    .account__title {
      font-size: 18px;
      font-weight: 500;
    }
  }
  .account__form {
    display: flex;
    flex-direction: column;
    row-gap: 20px;
  }

  @media only screen and (max-width: 576px) {
    .account__header {
      flex-direction: column;
      row-gap: 10px;
      align-items: flex-start !important;
    }
  }
`;

const { StringType } = Schema.Types;
const model = Schema.Model({
  old_password: StringType()
    .isRequired("Chưa nhập mật khẩu hiện tại.")
    .minLength(6, "Mật khẩu phải tối thiểu 6 ký tự."),
  new_password: StringType()
    .isRequired("Chưa nhập mật khẩu mới.")
    .minLength(6, "Mật khẩu phải tối thiểu 6 ký tự."),
});

const ContentAccount = () => {
  const dispatch = useDispatch();
  const { information } = useSelector((state) => state.profileReducers);
  const { isSuccessed } = useSelector((state) => state.authReducers);
  const { isLoadingAccount } = useSelector((state) => state.loadingReducers);

  const formRef = useRef();
  const [formValue, setFormValue] = useState({
    phone_number: "",
    old_password: "",
    new_password: "",
  });

  // Handle Change Value Form
  const handleChangeValueForm = (values) => {
    setFormValue({
      ...values,
    });
  };

  //Handle Reset Value Form
  const handleRessetValueForm = () => {
    setFormValue((prevFormValue) => {
      return {
        ...prevFormValue,
        old_password: "",
        new_password: "",
      };
    });
  };

  // Handle Update
  const handleChangePassword = async () => {
    if (!formRef.current.check()) {
      return;
    }
    const newFormValue = {
      old_password: formValue.old_password,
      new_password: formValue.new_password,
    };
    dispatch(auth.changePassword(newFormValue));
  };

  //Reset form when update success
  useEffect(() => {
    if (isSuccessed)
      setFormValue((prevFormValue) => {
        return {
          ...prevFormValue,
          old_password: "",
          new_password: "",
        };
      });
    dispatch({ type: Types.CHANGE_PASSWORD, data: false });
  }, [dispatch, isSuccessed]);

  //Get Data Profile
  useEffect(() => {
    dispatch(profileActions.getProfile());
  }, [dispatch]);
  useEffect(() => {
    if (Object.keys(information).length > 0) {
      setFormValue((prevForm) => {
        return { ...prevForm, phone_number: information.phone_number };
      });
    }
  }, [information]);
  return (
    <ContentAccountStyles>
      <div className="account__content">
        <Form
          fluid
          checkTrigger="change"
          ref={formRef}
          onChange={handleChangeValueForm}
          formValue={formValue}
          model={model}
        >
          <Stack justifyContent="space-between" className="account__header">
            <div className="account__title">Thông tin</div>
            <Stack spacing={15}>
              <Button
                appearance="primary"
                type="submit"
                style={{
                  backgroundColor: "#26a69a",
                }}
                onClick={handleChangePassword}
                loading={isLoadingAccount}
              >
                Cập nhập
              </Button>

              <Button appearance="default" onClick={handleRessetValueForm}>
                Làm mới
              </Button>
            </Stack>
          </Stack>
          <div className="account__form">
            <Stack spacing={30} justifyContent="center">
              <StackItem grow={1}>
                <Input
                  name="phone_number"
                  disabled
                  label="SĐT"
                  placeholder="Nhập SĐT..."
                />
              </StackItem>
            </Stack>
            <Stack spacing={30} justifyContent="center">
              <StackItem grow={1}>
                <Input
                  name="old_password"
                  typeInput="password"
                  label="Mật khẩu"
                  placeholder="Nhập mật khẩu..."
                />
              </StackItem>
            </Stack>
            <Stack spacing={30} justifyContent="center">
              <StackItem grow={1}>
                <Input
                  name="new_password"
                  typeInput="password"
                  label="Mật khẩu mới"
                  placeholder="Nhập mật khẩu mới..."
                />
              </StackItem>
            </Stack>
          </div>
        </Form>
      </div>
    </ContentAccountStyles>
  );
};

export default ContentAccount;
