import React from "react";
import { Breadcrumb } from "rsuite";
import styled from "styled-components";
import Breadcrumbs from "../../components/breadcrumb";
import Copyright from "../../components/copyright/Copyright";
import ContentAccount from "./ContentAccount";
const AccountStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;
const Account = () => {
  return (
    <AccountStyles>
      <Breadcrumbs
        title="Thông tin tài khoản"
        breadcrumbItems={[<Breadcrumb.Item active>Tài khoản</Breadcrumb.Item>]}
      >
        <ContentAccount></ContentAccount>
        <Copyright></Copyright>
      </Breadcrumbs>
    </AccountStyles>
  );
};

export default Account;
