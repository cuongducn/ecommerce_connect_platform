import { Table, Pagination, InputGroup, Input, Stack } from "rsuite";
import { useCallback, useEffect, useState } from "react";
import ImageCell from "../../../components/table/ImageCell";
import NameCell from "../../../components/table/NameCell";
import { Search } from "@rsuite/icons";
import { useDispatch, useSelector } from "react-redux";
import * as patientAction from "../../../actions/patient";
import styled from "styled-components";
import { useNavigate, useSearchParams } from "react-router-dom";
import { debounce } from "lodash";
import { combineAddress, formatNumber } from "../../../utils";
const { Column, HeaderCell, Cell } = Table;
const rowKey = "id";

const DataTableStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding-top: 20px;
  border-radius: 10px;
  .header {
    display: flex;
    justify-content: flex-start;
    margin-bottom: 20px;
    padding-left: 20px;
    .input__search-content {
      width: 300px;
    }
  }
  @media only screen and (max-width: 768px) {
    .header {
      .input__search-content {
        width: 170px;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .header {
      flex-direction: column;
      row-gap: 10px;
      padding: 0 20px;
      .rs-stack-item {
        width: 100%;
        overflow: visible !important;
        .input__search-content {
          width: 100%;
        }
        .rs-picker {
          margin-right: 0 !important;
          width: 100% !important;
        }
      }
    }
  }
`;
const DataTable = () => {
  const dispatch = useDispatch();
  const { allPatients } = useSelector((state) => state.patientReducers);
  const { data } = allPatients;

  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get("page")) || 1,
    limit: 20,
    search: searchParams.get("search") || "",
    searchInput: searchParams.get("search") || "",
  });
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const navigate = useNavigate();

  // Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        navigate(
          `/benh-nhan?page=1${valueSearch ? `&search=${valueSearch}` : ""}`
        );
        return {
          ...prevParams,
          page: 1,
          search: valueSearch,
          searchInput: valueSearch,
        };
      });
    }, 500),
    []
  );
  const handleSearchPatients = (e) => {
    const value = e;
    setParams({
      ...params,
      searchInput: value,
    });
    debounceCallBack(value);
  };

  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === "string") {
          x = x.charCodeAt();
        }
        if (typeof y === "string") {
          y = y.charCodeAt();
        }
        if (sortData.type === "asc") {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    let dataIndex = [];
    if (data?.length > 0) {
      dataIndex = data.map((item, index) => {
        item.index =
          (Number(allPatients.current_page) - 1) *
            Number(allPatients.per_page) +
          index +
          1;
        return item;
      });
    }
    return dataIndex;
  };
  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };

  const handleChangePage = (pageChange) => {
    navigate(
      `/benh-nhan?page=${pageChange}${
        params.search ? `&search=${params.search}` : ""
      }`
    );
    setParams({
      ...params,
      page: pageChange,
    });
  };

  // Call API Get Doctors
  useEffect(() => {
    dispatch(
      patientAction.getAllPatients(
        `page=${params.page}&limit=${params.limit}&search=${params.search}`
      )
    );
  }, [dispatch, params.page, params.limit, params.search]);
  return (
    <DataTableStyles>
      <Stack className="header" justifyContent="space-between">
        <InputGroup inside className="input__search-content">
          <Input
            placeholder="Tìm kiếm bệnh nhân..."
            onChange={handleSearchPatients}
            value={params.searchInput}
          />
          <InputGroup.Addon>
            <Search />
          </InputGroup.Addon>
        </InputGroup>
      </Stack>
      <Table
        autoHeight
        rowKey={rowKey}
        hover={false}
        data={getData()}
        sortColumn={sortData.column}
        sortType={sortData.type}
        onSortColumn={handleSortColumn}
        loading={sortData.loading}
      >
        <Column align="center" width={50}>
          <HeaderCell>STT</HeaderCell>
          <Cell dataKey="index" />
        </Column>
        <Column align="center" width={70}>
          <HeaderCell>Hình ảnh</HeaderCell>
          <ImageCell
            dataKey="avatar_image"
            styleImage={{
              width: "40px",
              height: "40px",
            }}
          />
        </Column>
        <Column width={170} sortable>
          <HeaderCell>Tên</HeaderCell>
          <NameCell
            dataKey="name"
            dataPopup={{
              name: "Tên",
              username: "Tên tài khoản",
              sex: "Giới tính",
              date_of_birth: "Ngày sinh",
            }}
            isSex={[2]}
          />
        </Column>
        <Column width={170}>
          <HeaderCell>Email</HeaderCell>
          <Cell>{(rowData) => rowData.email}</Cell>
        </Column>
        <Column width={170}>
          <HeaderCell>Số diện thoại</HeaderCell>
          <Cell>{(rowData) => rowData.phone_number}</Cell>
        </Column>
        <Column flexGrow={1}>
          <HeaderCell>Địa chỉ</HeaderCell>
          <Cell>
            {(rowData) => (
              <span
                title={combineAddress(
                  rowData.address_detail,
                  rowData.wards_name,
                  rowData.district_name,
                  rowData.province_name
                )}
              >
                {combineAddress(
                  rowData.address_detail,
                  rowData.wards_name,
                  rowData.district_name,
                  rowData.province_name
                )}
              </span>
            )}
          </Cell>
        </Column>
        <Column width={170}>
          <HeaderCell>Tên tài khoản</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData.bank_account_name ? rowData.bank_account_name : ""
            }
          </Cell>
        </Column>
        <Column width={170}>
          <HeaderCell>Số tài khoản</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData.bank_account_number ? rowData.bank_account_number : ""
            }
          </Cell>
        </Column>
        <Column width={170}>
          <HeaderCell>Tên ngân hàng</HeaderCell>
          <Cell>
            {(rowData) => (rowData.bank_name ? rowData.bank_name : "")}
          </Cell>
        </Column>
      </Table>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          size="xs"
          layout={["total", "-", "pager", "skip"]}
          total={allPatients.total}
          limitOptions={[10, 30, 50]}
          limit={params.limit}
          activePage={params.page}
          onChangePage={handleChangePage}
        />
      </div>
    </DataTableStyles>
  );
};

export default DataTable;
