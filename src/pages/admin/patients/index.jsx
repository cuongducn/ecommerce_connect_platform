import { Breadcrumb } from 'rsuite';
import styled from 'styled-components';
import Breadcrumbs from '../../../../components/admin/breadcrumb';
import Copyright from '../../../../components/admin/copyright/Copyright';
import DataTable from './table/DataTable';

const PatientsStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const Patients = () => {
  return (
    <PatientsStyles>
      <Breadcrumbs
        title="Danh sách bệnh nhân"
        breadcrumbItems={[<Breadcrumb.Item active>Bệnh nhân</Breadcrumb.Item>]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </PatientsStyles>
  );
};
export default Patients;
