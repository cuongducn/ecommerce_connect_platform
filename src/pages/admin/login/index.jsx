import React from 'react';
import {
  Button,
  ButtonToolbar,
  Col,
  Container,
  Content,
  Form,
  Grid,
  Header,
  Navbar,
  Panel,
  Row,
  Schema,
} from 'rsuite';
import styled from 'styled-components';
import { regexs } from '../../../utils/regexs';
import { useDispatch, useSelector } from 'react-redux';
import * as auth from '../../../actions/admin/auth';
import { useNavigate } from 'react-router-dom';
import TextField from '../../../components/admin/form/Input';
import { Link } from 'react-router-dom';

const LoginStyles = styled.div`
  .login__navbar {
    background-color: #00897b;
    .logo {
      color: white;
    }
  }
  .login__content {
    box-shadow: 1px 2px 7px 2px rgba(0, 0, 0, 0.2);
    border-radius: 10px;
    overflow: hidden;
    .login__bgDoctor {
      width: 100%;
      height: 100%;
    }
  }
  .login__forgotPassword {
    font-size: 12px;
    position: absolute;
    top: -24px;
    right: -8px;
  }
  .login__btn {
    background-color: #00897b;
  }
`;

const { StringType } = Schema.Types;
const model = Schema.Model({
  phone_number: StringType()
    .isRequired('Trường SĐT bắt buộc phải nhập.')
    .addRule((value) => {
      const phoneRegex = regexs.phone;
      let isValidPhone = phoneRegex.test(value);
      if (isValidPhone) {
        return true;
      }
      return false;
    }, 'SĐT không đúng định dạng.'),
  password: StringType()
    .isRequired('Trường mật khẩu bắt buộc phải nhập.')
    .minLength(6, 'Mật khẩu phải tối thiểu 6 ký tự.'),
});

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isLoadingLogin } = useSelector((state) => state.loadingReducers);

  const formRef = React.useRef();
  const [formValue, setFormValue] = React.useState({
    phone_number: '',
    password: '',
  });

  // Handle Login
  const handleLoginForm = () => {
    if (!formRef.current.check()) {
      return;
    }
    dispatch(
      auth.loginAdmin(formValue, () => {
        navigate('/admin');
      })
    );
  };
  return (
    <LoginStyles className="login">
      <Container>
        <Header>
          <Navbar appearance="inverse" className="login__navbar">
            <Navbar.Brand>
              <Link to={'/'}>
                <span className="logo fw-semibold">Asahaa</span>
              </Link>
            </Navbar.Brand>
          </Navbar>
        </Header>
        <Content className="mt-5">
          <Grid>
            <Row
              style={{
                display: 'flex',
                justifyContent: 'center',
              }}
            >
              <Col xs={22} sm={16} md={12} className="login__content">
                <Panel header={<h3>Đăng nhập</h3>} className="login-main">
                  <Form
                    fluid
                    ref={formRef}
                    onChange={setFormValue}
                    formValue={formValue}
                    model={model}
                  >
                    <TextField
                      name="phone_number"
                      label="SĐT"
                      placeholder="Nhập số điện thoại..."
                    />
                    <TextField
                      name="password"
                      label="Mật khẩu"
                      placeholder="Nhập mật khẩu..."
                      typeInput="password"
                    />
                    <Form.Group>
                      <ButtonToolbar
                        style={{
                          position: 'relative',
                        }}
                      >
                        <Button
                          appearance="primary"
                          className="login__btn"
                          type="submit"
                          onClick={handleLoginForm}
                          loading={isLoadingLogin}
                        >
                          Đăng nhập
                        </Button>
                        <Button
                          appearance="link"
                          className="login__forgotPassword"
                        >
                          Quên mật khẩu?
                        </Button>
                      </ButtonToolbar>
                    </Form.Group>
                  </Form>
                </Panel>
              </Col>
            </Row>
          </Grid>
        </Content>
      </Container>
    </LoginStyles>
  );
};

export default Login;
