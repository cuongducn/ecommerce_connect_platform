import React from 'react';
import ErrorPage from '../../../../components/admin/error/Error';
import { IconButton } from 'rsuite';
import ArrowLeftLine from '@rsuite/icons/ArrowLeftLine';
import Link from '../../../../components/admin/navLink/Link';

const Error500 = () => (
  <ErrorPage code={500}>
    <p className="error-page-title">
      Rất tiếc... Bạn vừa tìm thấy một trang lỗi
    </p>
    <p className="error-page-subtitle text-muted ">
      Chúng tôi xin lỗi nhưng máy chủ của chúng tôi đã gặp lỗi nội bộ
    </p>
    <IconButton
      icon={<ArrowLeftLine />}
      appearance="primary"
      as={Link}
      href="/"
    >
      Đưa tôi về Home
    </IconButton>
  </ErrorPage>
);

export default Error500;
