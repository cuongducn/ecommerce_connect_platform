import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, Stack } from "rsuite";
import StackItem from "rsuite/esm/Stack/StackItem";
import styled from "styled-components";
import Input from "../../components/form/Input";
import * as setupActions from "../../actions/setup";
import { formatNumber } from "../../utils";

const ContentSetupStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding: 20px;
  border-radius: 10px;
  .setup__header {
    margin-bottom: 20px;
    .setup__title {
      font-size: 18px;
      font-weight: 500;
    }
  }
  .setup__form {
    display: flex;
    flex-direction: column;
    row-gap: 20px;
  }
  @media only screen and (max-width: 768px) {
    padding: 20px 10px !important;
    .setup__main {
      flex-direction: column;
      gap: 15px !important;
      .setup__item {
        width: 100%;
        margin-right: 0 !important;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .setup__header {
      flex-direction: column;
      row-gap: 10px;
      align-items: flex-start !important;
    }
  }
`;

const ContentSetup = () => {
  const dispatch = useDispatch();
  const { information } = useSelector((state) => state.setupReducers);
  const { isLoadingSetup } = useSelector((state) => state.loadingReducers);

  const [formValue, setFormValue] = useState({
    percent_discount: 0,
    fee_call_minutes: 0,
    free_minutes: 0,
    free_amount_call_patient: 0,
  });

  // Handle Change Value Form
  const handleChangeValueForm = (values, e) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormValue({
      ...values,
      [name]: name === "fee_call_minutes" ? formatNumber(value) : value,
    });
  };

  //Handle Reset Value Form
  const handleRessetValueForm = () => {
    setFormValue({
      percent_discount: 0,
      fee_call_minutes: 0,
      free_minutes: 0,
      free_amount_call_patient: 0,
      time_countdown_cus_unpaid: 0,
    });
  };

  // Handle Update
  const handleUpdateSetup = async () => {
    const newFormValue = {
      percent_discount:
        formValue.percent_discount === "" ? 0 : formValue.percent_discount,
      fee_call_minutes:
        formValue.fee_call_minutes === "" ? 0 : formValue.fee_call_minutes,
      free_minutes: formValue.free_minutes === "" ? 0 : formValue.free_minutes,
      time_countdown_cus_unpaid: !formValue.time_countdown_cus_unpaid
        ? 0
        : formValue.time_countdown_cus_unpaid,
      free_amount_call_patient:
        formValue.free_amount_call_patient === ""
          ? 0
          : formValue.free_amount_call_patient,
    };
    dispatch(setupActions.updateSetup(newFormValue));
  };

  //Get Data Setup
  useEffect(() => {
    dispatch(setupActions.getSetup());
  }, [dispatch]);
  useEffect(() => {
    if (information && Object.keys(information).length > 0) {
      setFormValue({
        percent_discount: information.percent_discount,
        fee_call_minutes: information.fee_call_minutes
          ? formatNumber(information.fee_call_minutes)
          : 0,
        free_minutes: information.free_minutes,
        free_amount_call_patient: information.free_amount_call_patient,
        time_countdown_cus_unpaid: information.time_countdown_cus_unpaid,
      });
    }
  }, [information]);
  return (
    <ContentSetupStyles>
      <div className="contact__content">
        <Form
          fluid
          checkTrigger="change"
          onChange={handleChangeValueForm}
          formValue={formValue}
        >
          <Stack justifyContent="space-between" className="setup__header">
            <div className="setup__title">Nhập thông tin</div>
            <Stack spacing={15}>
              <Button
                appearance="primary"
                type="submit"
                style={{
                  backgroundColor: "#26a69a",
                }}
                onClick={handleUpdateSetup}
                loading={isLoadingSetup}
              >
                Cập nhập
              </Button>

              <Button appearance="default" onClick={handleRessetValueForm}>
                Làm mới
              </Button>
            </Stack>
          </Stack>
          <div className="setup__form">
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="setup__main"
            >
              <StackItem grow={1} className="setup__item">
                <Input
                  typeInput="number"
                  min={0}
                  name="free_minutes"
                  label="Số phút miễn phí"
                  placeholder="Nhập số phút..."
                />
              </StackItem>
              <StackItem grow={1} className="setup__item">
                <Input
                  name="fee_call_minutes"
                  label="Phí gọi theo phút"
                  placeholder="Nhập phí gọi..."
                />
              </StackItem>
            </Stack>
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="setup__main"
            >
              <StackItem grow={1} className="setup__item">
                <Input
                  typeInput="number"
                  min={0}
                  name="free_amount_call_patient"
                  label="Số lượng miễn phí cuộc gọi"
                  placeholder="Nhập số cuộc gọi miễn phí..."
                />
              </StackItem>
              <StackItem grow={1} className="setup__item">
                <Input
                  typeInput="number"
                  min={0}
                  name="percent_discount"
                  label="Phần trăm giảm giá"
                  placeholder="Nhập phần trăm giảm giá..."
                />
              </StackItem>
            </Stack>
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="setup__main"
            >
              <StackItem grow={1} className="setup__item">
                <Input
                  typeInput="number"
                  min={0}
                  name="time_countdown_cus_unpaid"
                  label="Thời gian chờ thanh toán"
                  placeholder="Nhập thời gian chờ thanh toán..."
                />
              </StackItem>
            </Stack>
          </div>
        </Form>
      </div>
    </ContentSetupStyles>
  );
};

export default ContentSetup;
