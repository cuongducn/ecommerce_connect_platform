import React from "react";
import { Breadcrumb } from "rsuite";
import styled from "styled-components";
import Breadcrumbs from "../../components/breadcrumb";
import Copyright from "../../components/copyright/Copyright";
import ContentSetup from "./ContentSetup";
const SetupStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;
const Setup = () => {
  return (
    <SetupStyles>
      <Breadcrumbs
        title="Thông tin thiết lập"
        breadcrumbItems={[<Breadcrumb.Item active>Thiết lập</Breadcrumb.Item>]}
      >
        <ContentSetup></ContentSetup>
        <Copyright></Copyright>
      </Breadcrumbs>
    </SetupStyles>
  );
};

export default Setup;
