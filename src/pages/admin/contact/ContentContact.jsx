import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form, Schema, Stack } from "rsuite";
import StackItem from "rsuite/esm/Stack/StackItem";
import styled from "styled-components";
import Input from "../../components/form/Input";
import * as contactActions from "../../actions/contact";
import { regexs } from "../../utils/regexs";
import Textarea from "../../components/form/Textarea";

const ContentContactStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding: 20px;
  border-radius: 10px;
  .contact__header {
    margin-bottom: 20px;
    .contact__title {
      font-size: 18px;
      font-weight: 500;
    }
  }
  .contact__form {
    display: flex;
    flex-direction: column;
    row-gap: 20px;
  }
  @media only screen and (max-width: 768px) {
    padding: 20px 10px !important;
    .contact__main {
      flex-direction: column;
      gap: 15px !important;
      .contact__item {
        width: 100%;
        margin-right: 0 !important;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .contact__header {
      flex-direction: column;
      row-gap: 10px;
      align-items: flex-start !important;
    }
  }
`;

const { StringType } = Schema.Types;
const model = Schema.Model({
  phone_number: StringType()
    .isRequired("Trường SĐT bắt buộc phải nhập.")
    .addRule((value) => {
      const phoneRegex = regexs.phone;
      let isValidPhone = phoneRegex.test(value);
      if (isValidPhone) {
        return true;
      }
      return false;
    }, "SĐT không đúng định dạng"),
});

const ContentContact = () => {
  const dispatch = useDispatch();
  const { information } = useSelector((state) => state.contactReducers);
  const { isLoadingContact } = useSelector((state) => state.loadingReducers);

  const formRef = useRef();
  const [formValue, setFormValue] = useState({
    email: "",
    phone_number: "",
    address: "",
    hotline: "",
    facebook: "",
    zalo: "",
    city_info: "",
    policy: "",
    bank_name: "",
    bank_account_name: "",
    bank_account_number: "",
    content: "",
  });

  // Handle Change Value Form
  const handleChangeValueForm = (values) => {
    setFormValue({
      ...values,
    });
  };

  //Handle Reset Value Form
  const handleRessetValueForm = () => {
    setFormValue({
      email: "",
      phone_number: "",
      address: "",
      hotline: "",
      facebook: "",
      zalo: "",
      city_info: "",
      policy: "",
      bank_name: "",
      bank_account_name: "",
      bank_account_number: "",
      content: "",
    });
  };

  // Handle Update
  const handleUpdateContact = async () => {
    if (!formRef.current.check()) {
      return;
    }

    dispatch(contactActions.updateContacts(formValue));
  };

  //Get Data Contact
  useEffect(() => {
    dispatch(contactActions.getContacts());
  }, [dispatch]);
  useEffect(() => {
    if (Object.keys(information).length > 0) {
      setFormValue({
        email: information.email,
        phone_number: information.phone_number,
        address: information.address,
        hotline: information.hotline,
        facebook: information.facebook,
        zalo: information.zalo,
        city_info: information.city_info,
        policy: information.policy,
        bank_name: information.bank_name,
        bank_account_name: information.bank_account_name,
        bank_account_number: information.bank_account_number,
        content: information.content,
      });
    }
  }, [information]);
  return (
    <ContentContactStyles>
      <div className="contact__content">
        <Form
          fluid
          checkTrigger="change"
          ref={formRef}
          onChange={handleChangeValueForm}
          formValue={formValue}
          model={model}
        >
          <Stack justifyContent="space-between" className="contact__header">
            <div className="contact__title">Nhập thông tin</div>
            <Stack spacing={15}>
              <Button
                appearance="primary"
                type="submit"
                style={{
                  backgroundColor: "#26a69a",
                }}
                onClick={handleUpdateContact}
                loading={isLoadingContact}
              >
                Cập nhập
              </Button>

              <Button appearance="default" onClick={handleRessetValueForm}>
                Làm mới
              </Button>
            </Stack>
          </Stack>
          <div className="contact__form">
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="contact__main"
            >
              <StackItem grow={1} className="contact__item">
                <Input name="email" label="Email" placeholder="Nhập email..." />
              </StackItem>
              <StackItem grow={1} className="contact__item">
                <Input
                  name="phone_number"
                  label="SĐT"
                  placeholder="Nhập SĐT..."
                />
              </StackItem>
            </Stack>
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="contact__main"
            >
              <StackItem grow={1} className="contact__item">
                <Input
                  name="address"
                  label="Địa chỉ"
                  placeholder="Nhập địa chỉ..."
                />
              </StackItem>
              <StackItem grow={1} className="contact__item">
                <Input
                  name="hotline"
                  label="Hotline"
                  placeholder="Nhập hotline..."
                />
              </StackItem>
            </Stack>
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="contact__main"
            >
              <StackItem grow={1} className="contact__item">
                <Input
                  name="bank_name"
                  label="Tên ngân hàng"
                  placeholder="Nhập tên ngân hàng..."
                />
              </StackItem>
              <StackItem grow={1} className="contact__item">
                <Input
                  name="city_info"
                  label="Thành phố"
                  placeholder="Nhập thành phố..."
                />
              </StackItem>
            </Stack>
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="contact__main"
            >
              <StackItem grow={1} className="contact__item">
                <Input
                  name="bank_account_name"
                  label="Tên tài khoản"
                  placeholder="Nhập tên tài khoản..."
                />
              </StackItem>
              <StackItem grow={1} className="contact__item">
                <Input
                  name="facebook"
                  label="Tên facebook"
                  placeholder="Nhập liên kết facebook..."
                />
              </StackItem>
            </Stack>
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="contact__main"
            >
              <StackItem grow={1} className="contact__item">
                <Input
                  name="bank_account_number"
                  label="Số tài khoản ngân hàng"
                  placeholder="Nhập số tài khoản..."
                />
              </StackItem>
              <StackItem grow={1} className="contact__item">
                <Input
                  name="zalo"
                  label="Tên zalo"
                  placeholder="Nhập liên kết zalo..."
                />
              </StackItem>
            </Stack>
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="contact__main"
            >
              <StackItem grow={1} className="contact__item">
                <Textarea
                  name="policy"
                  rows="1"
                  label="Chính sách"
                  placeholder="Nhập chính sách..."
                />
              </StackItem>
              <StackItem grow={1} className="contact__item">
                <Textarea
                  name="content"
                  rows="1"
                  label="Miêu tả"
                  placeholder="Nhập miêu tả..."
                />
              </StackItem>
            </Stack>
          </div>
        </Form>
      </div>
    </ContentContactStyles>
  );
};

export default ContentContact;
