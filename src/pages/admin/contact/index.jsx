import React from "react";
import { Breadcrumb } from "rsuite";
import styled from "styled-components";
import Breadcrumbs from "../../components/breadcrumb";
import Copyright from "../../components/copyright/Copyright";
import ContentContact from "./ContentContact";
const ContactStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;
const Contact = () => {
  return (
    <ContactStyles>
      <Breadcrumbs
        title="Thông tin liên hệ"
        breadcrumbItems={[<Breadcrumb.Item active>Liên hệ</Breadcrumb.Item>]}
      >
        <ContentContact></ContentContact>
        <Copyright></Copyright>
      </Breadcrumbs>
    </ContactStyles>
  );
};

export default Contact;
