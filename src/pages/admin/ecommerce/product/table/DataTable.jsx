import {
  Table,
  Pagination,
  InputGroup,
  Input,
  Stack,
  InputPicker,
  Checkbox,
  IconButton,
  Button,
  CheckTreePicker,
  DateRangePicker,
  Modal,
} from 'rsuite';
import { useCallback, useEffect, useState } from 'react';
import ImageCell from '../../../../../components/admin/table/ImageCell';
import { Search, Check, Close, Edit, Trash } from '@rsuite/icons';
import { useDispatch, useSelector } from 'react-redux';
import ToggleLoading from '../../../../../components/admin/button/ToggleLoading';
import * as productAction from '../../../../../actions/admin/product';
import * as categoryAction from '../../../../../actions/category';
import * as ecommerceProductAction from '../../../../../actions/admin/ecommerceProduct';
import * as ecommerceConnectAction from '../../../../../actions/admin/ecommerceConnect';
import { formatNumber } from '../../../../../utils';
import styled from 'styled-components';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { debounce } from 'lodash';
import * as Types from '../../../../../constants/actionType';
import CheckCell from '../../../../../components/admin/table/CheckCell';
import ModalCustom from '../../../../../components/admin/modal';
import { formatDateObj, formatDateStr } from '../../../../../utils/date';
import IconLazada from '../../../../../components/icon/lazada/iconLazada';
import IconTiki from '../../../../../components/icon/tiki/iconTiki';
import IconShopee from '../../../../../components/icon/shopee/iconShopee';
import IconTiktok from '../../../../../components/icon/tiktok/iconTiktok';
import CheckOutlineIcon from '@rsuite/icons/CheckOutline';
import CloseOutlineIcon from '@rsuite/icons/CloseOutline';
const { Column, HeaderCell, Cell } = Table;
const rowKey = 'id';

const DataTableStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding-top: 20px;
  border-radius: 10px;
  .header {
    display: flex;
    justify-content: flex-end;
    margin-bottom: 20px;
    padding: 0 20px;
    .input__search-content {
      width: 250px;
    }
  }

  .icon-ecommerce_table {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  @media only screen and (max-width: 768px) {
    .header {
      .input__search-content {
        width: 190px;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .header {
      flex-direction: column;
      row-gap: 10px;
      padding: 0 20px;
      .rs-stack-item {
        width: 100%;
        overflow: visible !important;
        .input__search-content {
          width: 100%;
        }
        .rs-picker {
          margin-right: 0 !important;
          width: 100% !important;
        }
      }
    }
  }
`;
const DataTable = () => {
  const dispatch = useDispatch();
  const { isLoadingProductAdmin } = useSelector(
    (state) => state.loadingReducers
  );
  const { listEcommerceConnect } = useSelector(
    (state) => state.ecommerceConnectReducers
  );
  const { listEcommerceProduct } = useSelector(
    (state) => state.ecommerceProductReducers
  );
  const { data } = listEcommerceProduct.data;

  console.log(listEcommerceProduct);

  const [openModalDeleteListProduct, setOpenModalDeleteListProduct] =
    useState(false);

  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get('page')) || 1,
    limit: Number(searchParams.get('limit')) || 20,
    status: Number(searchParams.get('status')) || '',
    search: searchParams.get('search') || '',
    date_from: searchParams.get('date_from') || '',
    date_to: searchParams.get('date_to') || '',
    search: searchParams.get('search') || '',
    shop_ids: searchParams.get('shop_ids') || '',
  });
  const [ecommerceConnectSelected, setEcommerceConnectSelected] = useState([]);
  const [openFormConnect, setOpenFormConnect] = useState(false);
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const navigate = useNavigate();

  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === 'string') {
          x = x.charCodeAt();
        }
        if (typeof y === 'string') {
          y = y.charCodeAt();
        }
        if (sortData.type === 'asc') {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    let dataIndex = [];
    if (data?.length > 0) {
      dataIndex = data.map((item, index) => {
        item.index =
          (Number(listEcommerceProduct.current_page) - 1) *
            Number(listEcommerceProduct.per_page) +
          index +
          1;
        return item;
      });
    }
    return dataIndex;
  };
  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };

  // Handle change params
  const handleChangePage = (pageChange) => {
    const queries = getParams(
      pageChange,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      params.category_ids,
      params.category_children_ids
    );
    navigate(`/admin/products?${queries}`);
    setParams({
      ...params,
      page: pageChange,
    });
  };

  //Handle Check Box
  const [checkedKeys, setCheckedKeys] = useState([]);

  let checked = false;
  let indeterminate = false;

  if (checkedKeys.length === data?.length) {
    checked = true;
  } else if (checkedKeys.length === 0) {
    checked = false;
  } else if (checkedKeys.length > 0 && checkedKeys.length < data?.length) {
    indeterminate = true;
  }

  const handleCheckAll = (value, checked) => {
    const keys = checked ? data.map((item) => item.id) : [];
    setCheckedKeys(keys);
  };
  const handleCheck = (value, checked) => {
    const keys = checked
      ? [...checkedKeys, value]
      : checkedKeys.filter((item) => item !== value);
    setCheckedKeys(keys);
  };

  //Handle Actions
  const ActionCell = ({ rowData, dataKey, ...props }) => {
    const [open, setOpen] = useState(false);
    const handleClose = () => setOpen(false);

    const handleDeletePacketMedical = () => {
      const data = {
        product_ids: [rowData[dataKey]],
      };

      const queryString = getParams(
        params.page,
        params.limit,
        params.status,
        params.search,
        params.date_from,
        params.date_to,
        params.category_ids,
        params.category_children_ids
      );
      dispatch(productAction.deleteProduct(data, queryString));
    };

    return (
      <Cell {...props} style={{ padding: 5 }} className="link-group">
        <div className="btn__actions">
          <IconButton
            appearance="subtle"
            onClick={() =>
              navigate(
                `/admin/products/update/${rowData[dataKey]}?${getParams(
                  params.page,
                  params.limit,
                  params.status,
                  params.search,
                  params.date_from,
                  params.date_to,
                  params.category_ids,
                  params.category_children_ids
                )}`
              )
            }
            icon={<Edit />}
          />
          <IconButton
            appearance="subtle"
            onClick={() => setOpen(true)}
            icon={<Trash />}
          />
        </div>

        <ModalCustom
          size="sm"
          title="Thông báo"
          open={open}
          nameAction="Xóa"
          handleClose={handleClose}
          handleAction={handleDeletePacketMedical}
        >
          Bạn có chắc chắn muốn xóa sản phẩm này không?
        </ModalCustom>
      </Cell>
    );
  };

  const handleRemoveListProduct = () => {
    const data = {
      product_ids: checkedKeys,
    };

    const queryString = getParams(
      params.page,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      params.category_ids,
      params.category_children_ids
    );
    dispatch(
      productAction.deleteProduct(data, queryString, () => {
        setCheckedKeys([]);
        setOpenModalDeleteListProduct(false);
      })
    );
  };
  const getParams = (
    page,
    limit,
    status,
    search,
    date_from,
    date_to,
    shop_ids
  ) => {
    let params = '';
    if (page) {
      params += `page=${page}`;
    }
    if (limit) {
      params += `&limit=${limit}`;
    }
    if (status !== '' && status !== null) {
      params += `&status=${status}`;
    }
    if (search !== '') {
      params += `&search=${search}`;
    }
    if (date_from !== '') {
      params += `&date_from=${date_from}`;
    }
    if (date_to !== '') {
      params += `&date_to=${date_to}`;
    }
    if (shop_ids !== '') {
      params += `&shop_ids=${shop_ids}`;
    }
    return params;
  };

  //Handle ecommerceProduct
  const handleShowListEcommerceProducts = () => {
    if (Object.entries(listEcommerceConnect?.data)?.length > 0) {
      const newListCategories = [];
      for (var category of listEcommerceConnect.data) {
        let newCategory = {};
        newCategory.label = category.shop_name;
        newCategory.value = category.shop_id;

        newCategory.children = [];

        newListCategories.push(newCategory);
      }

      return newListCategories;
    }

    return [];
  };
  const handleSelectedEcommerceProducts = (values) => {
    const new_shop_ids = [];

    listEcommerceConnect?.data.forEach((ecommerceConnect) => {
      if (values?.includes(ecommerceConnect.shop_id)) {
        new_shop_ids.push(ecommerceConnect.shop_id);
      }
    });

    const queries = getParams(
      1,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      new_shop_ids.join(',')
    );
    navigate(`/admin/ecommerce-product/tiki?${queries}`);
    setParams({
      ...params,
      page: 1,
      shop_ids: new_shop_ids.join(','),
    });

    setEcommerceConnectSelected(values);
  };

  useEffect(() => {
    const newShopIds = searchParams.get('shop_ids') || '';
    const newShopIdSelected = newShopIds?.split(',') || [];

    setEcommerceConnectSelected(
      newShopIdSelected
        .filter((element) => element !== '')
        ?.map((e) => Number(e))
    );
  }, []);

  // Call API Get listProducts
  useEffect(() => {
    const queryString = getParams(
      params.page,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      params.shop_ids
    );
    dispatch(ecommerceProductAction.getListEcommerceProducts(queryString));
  }, [
    dispatch,
    params.page,
    params.limit,
    params.search,
    params.status,
    params.date_from,
    params.date_to,
    params.shop_ids,
  ]);

  const handleSearchProducts = (e) => {
    const value = e;
    setParams({
      ...params,
      search: value,
    });
    debounceCallBack(value);
  };

  // Call API Get Category
  useEffect(() => {
    dispatch(ecommerceProductAction.getListEcommerceProducts());
    dispatch(ecommerceConnectAction.getListEcommerceConnects());
  }, [dispatch]);

  return (
    <DataTableStyles>
      <Stack
        style={{
          margin: '20px',
        }}
      >
        {' '}
        <InputGroup inside className="input__search-content">
          <Input
            placeholder="Tìm kiếm sản phẩm..."
            onChange={handleSearchProducts}
            value={params.search}
          />
          <InputGroup.Addon>
            <Search />
          </InputGroup.Addon>
        </InputGroup>
        <CheckTreePicker
          value={ecommerceConnectSelected}
          size="md"
          placeholder="Chọn sàn..."
          id="id"
          data={
            Object.entries(listEcommerceConnect)?.length > 0
              ? handleShowListEcommerceProducts()
              : []
          }
          style={{ width: 250, display: 'block' }}
          onChange={handleSelectedEcommerceProducts}
        />
        <Button
          className="btnAdd__product"
          appearance="primary"
          style={{
            backgroundColor: '#26a69a',
          }}
          onClick={() => setOpenFormConnect(true)}
        >
          Đồng bộ
        </Button>
      </Stack>

      {checkedKeys?.length > 0 && (
        <Stack
          justifyContent="flex-start"
          style={{
            marginLeft: '20px',
            marginBottom: '20px',
          }}
        >
          <Button
            color="red"
            appearance="primary"
            startIcon={<Trash />}
            style={{
              backgroundColor: '#d50101f5',
            }}
            onClick={() => setOpenModalDeleteListProduct(true)}
          >
            Xóa {checkedKeys?.length} sản phẩm
          </Button>
          <ModalCustom
            size="sm"
            title="Thông báo"
            open={openModalDeleteListProduct}
            nameAction="Xóa"
            handleClose={() => setOpenModalDeleteListProduct(false)}
            handleAction={handleRemoveListProduct}
          >
            Bạn có chắc chắn muốn xóa {checkedKeys.length} sản phẩm này không?
          </ModalCustom>
        </Stack>
      )}
      <Table
        autoHeight
        rowHeight={75}
        rowKey={rowKey}
        hover={false}
        data={getData()}
        sortColumn={sortData.column}
        sortType={sortData.type}
        onSortColumn={handleSortColumn}
        loading={sortData.loading}
      >
        <Column width={50} align="center">
          <HeaderCell style={{ padding: 0 }}>
            <div style={{ lineHeight: '40px' }}>
              <Checkbox
                inline
                checked={checked}
                indeterminate={indeterminate}
                onChange={handleCheckAll}
              />
            </div>
          </HeaderCell>
          <CheckCell
            verticalAlign="middle"
            dataKey="id"
            checkedKeys={checkedKeys}
            onChange={handleCheck}
          />
        </Column>
        <Column align="center" width={140}>
          <HeaderCell>Hình ảnh</HeaderCell>
          <ImageCell
            dataKey="main_image"
            styleImage={{
              width: '40px',
              height: '40px',
            }}
          />
        </Column>
        <Column flexGrow={1} sortable>
          <HeaderCell>Tên sản phẩm</HeaderCell>
          <Cell verticalAlign="middle">{(rowData) => rowData.name}</Cell>
        </Column>
        <Column flexGrow={1} sortable>
          <HeaderCell>Giá</HeaderCell>
          <Cell verticalAlign="middle">{(rowData) => rowData.max_price}</Cell>
        </Column>
        <Column flexGrow={1} sortable>
          <HeaderCell>SKU ecommerce</HeaderCell>
          <Cell verticalAlign="middle">
            {(rowData) => rowData.sku_in_ecommerce}
          </Cell>
        </Column>
        {/* <Column flexGrow={1}>
          <HeaderCell>Đồng bộ sản phẩm tự động </HeaderCell>
          <Cell verticalAlign="middle">
            {(rowData) =>
              rowData.type_sync_products ? (
                <div className="icon-ecommerce_table">
                  <CheckOutlineIcon className="text-[1.3rem] text-[#22c55e]" />
                </div>
              ) : (
                <div className="icon-ecommerce_table">
                  <CloseOutlineIcon className="text-[1.3rem] text-[#dc2626]" />
                </div>
              )
            }
          </Cell>
        </Column>
        <Column flexGrow={1}>
          <HeaderCell>Đồng bộ tồn kho tự động</HeaderCell>
          <Cell verticalAlign="middle">
            {(rowData) =>
              rowData.type_sync_inventory ? (
                <div className="icon-ecommerce_table">
                  <CheckOutlineIcon className="text-[1.3rem] text-[#22c55e]" />
                </div>
              ) : (
                <div className="icon-ecommerce_table">
                  <CloseOutlineIcon className="text-[1.3rem] text-[#dc2626]" />
                </div>
              )
            }
          </Cell>
        </Column>
        <Column flexGrow={1}>
          <HeaderCell>Đồng bộ đơn hàng tự động</HeaderCell>
          <Cell
            verticalAlign="middle"
            textAlign="center"
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            {(rowData) =>
              rowData.type_sync_products ? (
                <div className="icon-ecommerce_table">
                  {console.log(rowData)}
                  <CheckOutlineIcon className="text-[1.3rem] text-[#22c55e]" />
                </div>
              ) : (
                <div className="icon-ecommerce_table">
                  <CloseOutlineIcon className="text-[1.3rem] text-[#dc2626]" />
                </div>
              )
            }
          </Cell>
        </Column>
        <Column flexGrow={1}>
          <HeaderCell>Hạn token</HeaderCell>
          <Cell verticalAlign="middle">
            {(rowData) =>
              rowData.expiry_token
                ? formatDateStr(rowData.expiry_token, 'HH:mm:ss DD-MM-YYYY ')
                : 0
            }
          </Cell>
        </Column> */}
        <Column width={140} align="center">
          <HeaderCell>Thao tác</HeaderCell>
          <ActionCell verticalAlign="middle" dataKey="id" />
        </Column>
      </Table>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          size="xs"
          layout={['total', '-', 'pager', 'skip']}
          total={listEcommerceProduct.total}
          limitOptions={[10, 30, 50]}
          limit={params.limit}
          activePage={params.page}
          onChangePage={handleChangePage}
        />
      </div>
      <Modal
        size={'sm'}
        open={openFormConnect}
        backdrop="static"
        keyboard={false}
        onClose={() => setOpenFormConnect(false)}
      >
        <Modal.Header className="">
          <Modal.Title>Đồng bộ Tiki</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="grid grid-cols-3 gap-4">
            <div class="col-span-2">
              <div className="py-[6px] rounded-xl content-center items-center text-white flex align-center justify-center bg-[#33BBF3]">
                <IconTiki />
                <span className="ml-[10px]">Sàn Tiki</span>
              </div>
            </div>
            <div class="col-span-1">
              <Button
                className="w-full h-full border-[#000000] text-[1rem] border-solid rounded-xl border-[1px] hover:bg-[#000000] hover:text-white"
                // onClick={() => handleClickConnect('tiktok')}
              >
                Đồng bộ
              </Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </DataTableStyles>
  );
};

export default DataTable;
