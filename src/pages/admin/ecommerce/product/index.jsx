import React from 'react';
import { Breadcrumb } from 'rsuite';
import Breadcrumbs from '../../../../components/admin/breadcrumb';
import Copyright from '../../../../components/admin/copyright/Copyright';
import DataTable from './table/DataTable';
import styled from 'styled-components';

const EcommerceProductStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const EcommerceProduct = () => {
  return (
    <EcommerceProductStyles>
      <Breadcrumbs
        title="Kết nối sàn TMĐT"
        breadcrumbItems={[
          <Breadcrumb.Item active>Danh sách sàn TMĐT</Breadcrumb.Item>,
        ]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </EcommerceProductStyles>
  );
};

export default EcommerceProduct;
