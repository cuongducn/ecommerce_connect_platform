import { Breadcrumb } from 'rsuite';
import styled from 'styled-components';
import Breadcrumbs from '../../../../components/admin/breadcrumb';
import Copyright from '../../../../components/admin/copyright/Copyright';
import DataTable from './table/DataTable';

const ConnectStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const EcommerceConnect = () => {
  return (
    <ConnectStyles>
      <Breadcrumbs
        title="Kết nối sàn TMĐT"
        breadcrumbItems={[
          <Breadcrumb.Item active>Kết nối sàn TMĐT</Breadcrumb.Item>,
        ]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </ConnectStyles>
  );
};

export default EcommerceConnect;
