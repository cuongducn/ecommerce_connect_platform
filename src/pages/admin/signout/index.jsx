import React from "react";
import { Navigate, useLocation } from "react-router-dom";

const Signout = () => {
  const location = useLocation();
  localStorage.removeItem("admin-token");
  return (
    <Navigate to="/admin/login" state={{ from: location }} replace></Navigate>
  );
};

export default Signout;
