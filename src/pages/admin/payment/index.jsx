import { Breadcrumb } from 'rsuite';
import styled from 'styled-components';
import Breadcrumbs from '../../../components/admin/breadcrumb';
import Copyright from '../../../components/admin/copyright/Copyright';
import DataTable from './table/DataTable';

const PatientsStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const Payment = () => {
  return (
    <PatientsStyles>
      <Breadcrumbs
        title="Xác nhận đã khám bệnh"
        breadcrumbItems={[
          <Breadcrumb.Item active>Xác nhận đã khám bệnh</Breadcrumb.Item>,
        ]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </PatientsStyles>
  );
};
export default Payment;
