import React from "react";
import {
  Avatar,
  IconButton,
  Input,
  InputGroup,
  InputPicker,
  Pagination,
  Stack,
  Table,
} from "rsuite";
import styled from "styled-components";
import { Search, Check, Close } from "@rsuite/icons";
import * as Types from "../../../constants/actionType";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useState } from "react";
import { useCallback } from "react";
import { debounce } from "lodash";
import { useEffect } from "react";
import * as payment from "../../../actions/payment";
import RowExpandedInformation from "./RowExpandedInformation";
import ExpandCell from "../../../components/table/ExpandCell";
import NameCell from "../../../components/table/NameCell";
import { formatNumber } from "../../../utils";
const { Column, HeaderCell, Cell } = Table;
const rowKey = "id";
const statusCensorship = [
  {
    label: "Chờ thanh toán",
    value: Types.PAYMENT_PROGRESSING,
  },
  {
    label: "Đã thanh toán",
    value: Types.PAYMENT_WAIT_CONFIRM,
  },
  {
    label: "Đã duyệt",
    value: Types.PAYMENT_COMPLETED,
  },
  {
    label: "Đã hủy",
    value: Types.PAYMENT_CANCELED,
  },
];
const DataTableStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding-top: 20px;
  border-radius: 10px;
  .header {
    display: flex;
    justify-content: flex-start;
    margin-bottom: 20px;
    padding-left: 20px;
    .input__search-content {
      width: 300px;
    }
  }
  @media only screen and (max-width: 768px) {
    .header {
      .input__search-content {
        width: 170px;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .header {
      flex-direction: column;
      row-gap: 10px;
      padding: 0 20px;
      .rs-stack-item {
        width: 100%;
        overflow: visible !important;
        .input__search-content {
          width: 100%;
        }
        .rs-picker {
          margin-right: 0 !important;
          width: 100% !important;
        }
      }
    }
  }
`;

const DataTable = () => {
  const dispatch = useDispatch();
  const { historyPayment } = useSelector((state) => state.paymentReducers);
  const { data } = historyPayment;

  const [expandedRowKeys, setExpandedRowKeys] = useState([]);
  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get("page")) || 1,
    limit: 20,
    status: Number(searchParams.get("status_payment")) || "",
    search: searchParams.get("search") || "",
    searchInput: searchParams.get("search") || "",
  });
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const navigate = useNavigate();

  // Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        navigate(
          `/xac-nhan-kham-benh?page=1${
            valueSearch ? `&search=${valueSearch}` : ""
          }${
            prevParams.status !== ""
              ? `&status_payment=${prevParams.status}`
              : ""
          }`
        );
        return {
          ...prevParams,
          page: 1,
          search: valueSearch,
          searchInput: valueSearch,
        };
      });
    }, 500),
    []
  );
  const handleSearchDoctors = (e) => {
    const value = e;
    setParams({
      ...params,
      searchInput: value,
    });
    debounceCallBack(value);
  };
  const handleChangePage = (pageChange) => {
    navigate(
      `/xac-nhan-kham-benh?page=${pageChange}${
        params.search ? `&search=${params.search}` : ""
      }${params.status !== "" ? `&status_payment=${params.status}` : ""}`
    );
    setParams({
      ...params,
      page: pageChange,
    });
  };

  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === "string") {
          x = x.charCodeAt();
        }
        if (typeof y === "string") {
          y = y.charCodeAt();
        }
        if (sortData.type === "asc") {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    return data;
  };

  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };

  // Handle Open Expand
  const handleExpanded = (rowData) => {
    let open = false;
    const nextExpandedRowKeys = [];

    expandedRowKeys.forEach((key) => {
      if (key === rowData[rowKey]) {
        open = true;
      } else {
        nextExpandedRowKeys.push(key);
      }
    });

    if (!open) {
      nextExpandedRowKeys.push(rowData[rowKey]);
    }

    setExpandedRowKeys(nextExpandedRowKeys);
  };

  //Handle Change Status
  const handleChangeStatus = (value) => {
    navigate(
      `/xac-nhan-kham-benh?page=1${
        params.search ? `&search=${params.search}` : ""
      }${value !== null ? `&status_payment=${value}` : ""}`
    );

    setParams({
      ...params,
      page: 1,
      status:
        value === Types.PAYMENT_COMPLETED
          ? Types.PAYMENT_COMPLETED
          : value === Types.PAYMENT_CANCELED
          ? Types.PAYMENT_CANCELED
          : value === Types.PAYMENT_PROGRESSING
          ? Types.PAYMENT_PROGRESSING
          : value === Types.PAYMENT_WAIT_CONFIRM
          ? Types.PAYMENT_WAIT_CONFIRM
          : "",
    });
  };

  //Xử lý hiển thị thời gian đặt khám
  const handleTimeBookAppointment = (book_calendar) => {
    if (book_calendar) {
      const time = `${book_calendar.from_time?.split(" ")[1]} - ${
        book_calendar.to_time?.split(" ")[1]
      } ${book_calendar.date?.split("-").reverse().join("-")}`;
      return time;
    }
    return "";
  };

  //Xử lý hiển thị thời gian khám
  const handleTimeAppointment = (history) => {
    if (
      history.time_start_medical &&
      history.time_end_medical &&
      history.date_medical
    ) {
      const time = `${history.time_start_medical?.split(" ")[1]} - ${
        history.time_end_medical?.split(" ")[1]
      } ${history.date_medical?.split("-").reverse().join("-")}`;
      return time;
    }
    return "";
  };
  //Thay đổi trạng thái thanh toán
  const handleUpdateStatusPayment = (id, isChecked) => {
    const data = {
      status_payment: isChecked
        ? Types.STATUS_COMPLETED
        : Types.STATUS_CANCELLED,
    };

    const queryString = `page=${params.page}&limit=${params.limit}&search=${params.search}&status_payment=${params.status}`;
    dispatch(payment.confirmPayment(id, data, queryString));
  };

  // Call API Get History Payment
  useEffect(() => {
    dispatch(
      payment.getListHistoryPayment(
        `page=${params.page}&limit=${params.limit}&search=${params.search}&status_payment=${params.status}`
      )
    );
  }, [dispatch, params.page, params.limit, params.search, params.status]);
  return (
    <DataTableStyles>
      <Stack className="header" justifyContent="space-between">
        <InputGroup inside className="input__search-content">
          <Input
            placeholder="Tìm kiếm..."
            onChange={handleSearchDoctors}
            value={params.searchInput}
          />
          <InputGroup.Addon>
            <Search />
          </InputGroup.Addon>
        </InputGroup>
        <InputPicker
          data={statusCensorship}
          style={{ width: 170, marginRight: 20 }}
          placeholder="Trạng thái: Tất cả"
          onChange={handleChangeStatus}
        />
      </Stack>
      <Table
        autoHeight
        rowKey={rowKey}
        hover={false}
        expandedRowKeys={expandedRowKeys}
        renderRowExpanded={RowExpandedInformation}
        rowExpandedHeight={400}
        data={getData()}
        sortColumn={sortData.column}
        sortType={sortData.type}
        onSortColumn={handleSortColumn}
        loading={sortData.loading}
      >
        <Column align="center" width={50}>
          <HeaderCell>#</HeaderCell>
          <ExpandCell
            dataKey="id"
            expandedRowKeys={expandedRowKeys}
            onChange={handleExpanded}
          />
        </Column>
        <Column width={160} sortable>
          <HeaderCell>Bệnh nhân</HeaderCell>
          <NameCell
            dataKey="patient_name"
            dataPopup={{
              patient_name: "Tên bệnh nhân",
              patient_number_phone: "Số điện thoại",
              patient_address: "Địa chỉ",
            }}
          />
        </Column>
        <Column width={160}>
          <HeaderCell>Bác sĩ</HeaderCell>
          <Cell>
            {(rowData) => (
              <NameCell
                rowData={rowData["doctor"]}
                dataKey="first_and_last_name"
                dataPopup={{
                  first_and_last_name: "Tên",
                  years_of_experience: "Năm kinh nghiệm",
                  cost_mess_suport: "Chi phí nhắn tin hỗ trợ",
                  cost_call_suport: "Chi phí gọi hỗ trợ",
                }}
                cost={[2, 3]}
                isObject
              />
            )}
          </Cell>
        </Column>
        <Column flexGrow={1}>
          <HeaderCell>Gói khám</HeaderCell>
          <Cell>
            {(rowData) => (
              <NameCell
                rowData={rowData["packet_medical"]}
                dataKey="name"
                dataPopup={{
                  name: "Tên gói khám",
                  price_clinic: "Giá gói khám tại phòng",
                  price_home: "Giá gói khám tại nhà",
                  price_online: "Giá gói khám online",
                  percent_discount_for_admin: "Phần trăm cho hệ thống",
                }}
                cost={[1, 2, 3]}
                isObject
              />
            )}
          </Cell>
        </Column>
        <Column width={150}>
          <HeaderCell>Hoa hồng cho bác sĩ</HeaderCell>
          <Cell>{(rowData) => `${rowData["percent_commission_doctor"]}%`}</Cell>
        </Column>
        <Column width={180}>
          <HeaderCell>Loại khám</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData?.type_medical !== null &&
              rowData?.type_medical !== undefined ? (
                <>
                  {rowData?.type_medical === Types.TYPE_MEDICAL_AT_HOME
                    ? "Khám tại nhà"
                    : rowData?.type_medical === Types.TYPE_MEDICAL_ONLINE
                    ? "Khám online"
                    : rowData?.type_medical ===
                      Types.TYPE_MEDICAL_PRIVATE_CLINIC
                    ? "Khám tại phòng khám"
                    : ""}
                </>
              ) : (
                ""
              )
            }
          </Cell>
        </Column>
        {/* <Column width={215}>
          <HeaderCell>Thời gian đặt khám</HeaderCell>
          <Cell>
            {(rowData) => handleTimeBookAppointment(rowData.book_calendar)}
          </Cell>
        </Column> */}
        {/* <Column width={215}>
          <HeaderCell>Thời gian khám</HeaderCell>
          <Cell>{(rowData) => handleTimeAppointment(rowData)}</Cell>
        </Column> */}
        <Column width={215}>
          <HeaderCell>Thời gian thanh toán</HeaderCell>
          <Cell>{(rowData) => rowData.time_cus_paid}</Cell>
        </Column>
        {/* <Column width={180}>
          <HeaderCell>Hình ảnh thanh toán</HeaderCell>

          <ImageCell
            dataKey="images_cus_paid"
            styleImage={{
              width: "40px",
              height: "40px",
            }}
          />
        </Column> */}
        <Column width={180}>
          <HeaderCell>Thanh toán</HeaderCell>
          <Cell>
            {(rowData) => (
              <>
                {rowData.status_payment === Types.PAYMENT_WAIT_CONFIRM ? (
                  <div className="btn__actions" style={{ marginTop: -7 }}>
                    <IconButton
                      color="green"
                      appearance="link"
                      onClick={() =>
                        handleUpdateStatusPayment(rowData[rowKey], true)
                      }
                      icon={<Check />}
                    />
                    <IconButton
                      color="red"
                      appearance="link"
                      onClick={() =>
                        handleUpdateStatusPayment(rowData[rowKey], false)
                      }
                      icon={<Close />}
                    />
                  </div>
                ) : rowData.status_payment === Types.PAYMENT_COMPLETED ? (
                  <div style={{ color: "#22a12a" }}>Đã duyệt</div>
                ) : rowData.status_payment === Types.PAYMENT_CANCELED ? (
                  <div style={{ color: "#b81c07" }}>Đã hủy</div>
                ) : rowData.status_payment === Types.PAYMENT_PROGRESSING ? (
                  <div style={{ color: "#f1c40f" }}>Chờ thanh toán</div>
                ) : (
                  ""
                )}
              </>
            )}
          </Cell>
        </Column>
      </Table>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          size="xs"
          layout={["total", "-", "pager", "skip"]}
          total={historyPayment.total}
          limitOptions={[10, 30, 50]}
          limit={params.limit}
          activePage={params.page}
          onChangePage={handleChangePage}
        />
      </div>
    </DataTableStyles>
  );
};

export default DataTable;
