import React from "react";
import { Col, Grid, Row } from "rsuite";
import styled from "styled-components";
import { formatNumber } from "../../../utils";
import * as Types from "../../../constants/actionType";

const RowExpandedInformationStyles = styled.div`
  h4 {
    margin-bottom: 5px;
  }
  .information__left {
    display: flex;
    flex-direction: column;
    row-gap: 5px;
  }
  .information__left {
    .information__title {
      font-weight: 500;
    }
  }
  .information__right {
    span {
      font-weight: 500;
    }
  }
`;

const RowExpandedInformation = (rowData) => {
  const handleDepartment = (listDepartment) => {
    if (listDepartment?.length > 0) {
      return listDepartment.reduce(
        (prevDepartment, currentDepartment, index) => {
          return (
            prevDepartment +
            `${
              index === listDepartment.length - 1
                ? `${currentDepartment?.name}`
                : `${currentDepartment?.name}, `
            }`
          );
        },
        ""
      );
    }
    return "";
  };
  return (
    <RowExpandedInformationStyles>
      <Grid fluid>
        <Row className="rowExpanded">
          <Col xxl={12} xl={10}>
            <div
              style={{
                marginBottom: "10px",
              }}
            >
              <h4>Thông tin gói khám</h4>
              <div className="information__left">
                <div>
                  <span className="information__title">
                    {rowData["type_medical"] == Types.TYPE_MEDICAL_ONLINE
                      ? "Tên gói khám online: "
                      : Types.TYPE_MEDICAL_AT_HOME
                      ? "Tên gói khám tại nhà: "
                      : Types.TYPE_MEDICAL_PRIVATE_CLINIC
                      ? "Tên gói khám tại phòng khám: "
                      : ""}
                  </span>
                  <span>{rowData["packet_medical"]?.name}</span>
                </div>
                {rowData["type_medical"] == Types.TYPE_MEDICAL_ONLINE ? (
                  <div>
                    <span className="information__title">
                      Giá gói khám online:{" "}
                    </span>
                    <span>
                      {rowData["packet_medical"]?.price_online
                        ? `${formatNumber(
                            rowData["packet_medical"]?.price_online
                          )} ₫`
                        : ""}
                    </span>
                  </div>
                ) : Types.TYPE_MEDICAL_AT_HOME ? (
                  <div>
                    <span className="information__title">
                      Giá gói khám tại nhà:{" "}
                    </span>
                    <span>
                      {rowData["packet_medical"]?.price_home
                        ? `${formatNumber(
                            rowData["packet_medical"]?.price_home
                          )} ₫`
                        : ""}
                    </span>
                  </div>
                ) : Types.TYPE_MEDICAL_PRIVATE_CLINIC ? (
                  <div>
                    <span className="information__title">
                      Giá gói khám tại phòng khám:{" "}
                    </span>
                    <span>
                      {rowData["packet_medical"]?.price_clinic
                        ? `${formatNumber(
                            rowData["packet_medical"]?.price_clinic
                          )} ₫`
                        : ""}
                    </span>
                  </div>
                ) : (
                  ""
                )}

                <div>
                  <span className="information__title">Khoa khám: </span>
                  <span>
                    {rowData["packet_medical"]?.list_medical_department
                      ?.length > 0
                      ? `${handleDepartment(
                          rowData["packet_medical"]?.list_medical_department
                        )}`
                      : ""}
                  </span>
                </div>
              </div>
            </div>
            <div>
              <h4>Thông tin khám bệnh</h4>
              <div className="information__left">
                <div>
                  <span className="information__title">Triệu chứng: </span>
                  <span>{rowData.disease_symptom}</span>
                </div>

                <div>
                  <span className="information__title">Chuẩn đoán: </span>
                  <span>{rowData.diagnose}</span>
                </div>
                <div>
                  <span className="information__title">Kết quả khám: </span>
                  <span>{rowData.result_medical}</span>
                </div>

                <div>
                  <span className="information__title">Ghi chú bác sĩ: </span>
                  <span>{rowData.note_doctor_medical}</span>
                </div>
              </div>
            </div>
          </Col>
          <Col xxl={12} xl={12}>
            <h4>Toa thuốc</h4>
            <div className="information__right">
              {rowData.prescripts?.length > 0 &&
                rowData.prescripts?.map((prescript, index) => (
                  <div key={prescript.id}>
                    <span>{`${index + 1}. Thuốc: ${prescript?.name} - SL: ${
                      prescript?.quantity
                    }`}</span>
                    <div>
                      {prescript?.description ? (
                        <p>- Miêu tả: {prescript?.description}</p>
                      ) : (
                        ""
                      )}
                    </div>
                    <div>
                      {rowData.prescripts?.note ? (
                        <p>- Ghi chú: {rowData.prescripts?.note}</p>
                      ) : (
                        ""
                      )}
                    </div>
                  </div>
                ))}
            </div>
          </Col>
        </Row>
      </Grid>
    </RowExpandedInformationStyles>
  );
};

export default RowExpandedInformation;
