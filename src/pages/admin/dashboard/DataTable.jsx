import React from "react";
import { Table, Panel } from "rsuite";

const { Column, HeaderCell, Cell } = Table;

const data = [
  {
    id: 1,
    url: "https://39medical.karavui.com",
    visits: "105,253",
    unique: "23,361",
    bounce: "11%",
  },
  {
    id: 2,
    url: "https://39medical.karavui.com/bac-si/",
    visits: "103,643",
    unique: "23,385",
    bounce: "17%",
  },
  {
    id: 3,
    url: "https://39medical.karavui.com/phong-kham/",
    visits: "140,013",
    unique: "41,256",
    bounce: "13%",
  },
  {
    id: 4,
    url: "https://39medical.karavui.com/quay-thuoc/",
    visits: "194,532",
    unique: "19,038",
    bounce: "18%",
  },
  {
    id: 5,
    url: "https://39medical.karavui.com/huong-dan/",
    visits: "26,353",
    unique: "1,000",
    bounce: "20%",
  },
  {
    id: 6,
    url: "https://39medical.karavui.com/cai-tien/",
    visits: "11,973",
    unique: "4,786",
    bounce: "24%",
  },
];

const DataTable = () => {
  return (
    <Panel className="card" header="Những trang được truy cập nhiều nhất">
      <Table height={300} data={data} rowKey="id">
        <Column flexGrow={1} minWidth={100}>
          <HeaderCell>Tên trang </HeaderCell>
          <Cell>
            {(rowData) => {
              return (
                <a href={rowData.url} target="_blank" rel="noreferrer">
                  {rowData.url}
                </a>
              );
            }}
          </Cell>
        </Column>

        <Column width={130}>
          <HeaderCell>Số lượng truy cập</HeaderCell>
          <Cell dataKey="visits" />
        </Column>

        <Column width={100}>
          <HeaderCell>Số lượng ở lại</HeaderCell>
          <Cell dataKey="unique" />
        </Column>

        <Column width={130}>
          <HeaderCell>Tỉ lệ</HeaderCell>
          <Cell dataKey="bounce" />
        </Column>
      </Table>
    </Panel>
  );
};

export default DataTable;
