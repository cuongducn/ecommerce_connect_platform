import React from 'react';
import { Button, ButtonGroup, Col, Panel, Row } from 'rsuite';
import styled from 'styled-components';
import * as images from '../../../assets/charts';
import BarChart from './BarChart';
import PieChart from './PieChart';
import DataTable from './DataTable';

const barChartData = [
  {
    name: 'Trang web',
    data: [
      11, 8, 9, 10, 3, 11, 11, 11, 12, 13, 2, 12, 5, 8, 22, 6, 8, 6, 4, 1, 8,
      24, 29, 51, 40, 47, 23, 26, 50, 26, 22, 27, 46, 47, 81, 46, 40,
    ],
    locale: 'en-US',
  },
  {
    name: 'Ứng dụng',
    data: [
      7, 5, 4, 3, 3, 11, 4, 7, 5, 12, 12, 15, 13, 12, 6, 7, 7, 1, 5, 5, 2, 12,
      4, 6, 18, 3, 5, 2, 13, 15, 20, 47, 18, 15, 11, 10, 9,
    ],
  },
  {
    name: 'Khác',
    data: [
      4, 9, 11, 7, 8, 3, 6, 5, 5, 4, 6, 4, 11, 10, 3, 6, 7, 5, 2, 8, 4, 9, 9, 2,
      6, 7, 5, 1, 8, 3, 12, 3, 4, 9, 7, 11, 10,
    ],
  },
];

const DashboardContentStyles = styled.div`
  .bg-gradient-orange {
    background: linear-gradient(87deg, #fb6340 0, #fbb140 100%);
  }

  .bg-gradient-red {
    background: linear-gradient(87deg, #f5365c 0, #f56036 100%);
  }

  .bg-gradient-green {
    background: linear-gradient(87deg, #2dce89 0, #2dcecc 100%);
  }

  .bg-gradient-blue {
    background: linear-gradient(87deg, #11cdef 0, #1171ef 100%);
  }
  .dashboard-header {
    .rs-panel {
      // background: #fff;
      color: #fff;
    }
    .chart-img {
      width: 100px;
      position: absolute;
      left: 26px;
      top: 34px;
      opacity: 0.5;
    }
    .trend-box {
      .rs-panel-body {
        text-align: right;
        .value {
          font-size: 36px;
        }
      }
    }
  }

  .colorful-chart {
    color: #fff;
    margin-top: 30px;
    border-radius: 6px;
    h3 {
      line-height: 22px;
      text-align: right;
      color: rgba(255, 255, 255, 0.5);
      padding: 10px;
      font-size: 18px;
    }
  }

  .ct-chart-magenta {
    background: linear-gradient(60deg, #ec407a, #d81b60);
  }

  .ct-chart-orange {
    background: linear-gradient(60deg, #ffa726, #fb8c00);
  }

  .ct-chart-azure {
    background: linear-gradient(60deg, #26c6da, #00acc1);
  }

  .card {
    background: #fff;
    margin-top: 30px;
    border-radius: 6px;

    h3 {
      line-height: 22px;
      margin-bottom: 20px;
      font-size: 18px;
    }
  }
`;
const DashBoardContent = () => {
  return (
    <DashboardContentStyles>
      <Row gutter={30} className="dashboard-header">
        <Col xs={8}>
          <Panel className="mb-[12px] trend-box bg-gradient-red">
            <img className="chart-img" src={images.PVIcon} alt="img" />
            <div className="title">Lượt xem trang </div>
            <div className="value">281,358</div>
          </Panel>
        </Col>
        <Col xs={6}>
          <Panel className="mb-[12px] trend-box bg-gradient-blue">
            <img className="chart-img" src={images.UVIcon} alt="img" />
            <div className="title">Lượt quan tâm</div>
            <div className="value">25,135</div>
          </Panel>
        </Col>
        <Col xs={6}>
          <Panel className="mb-[12px] trend-box bg-gradient-blue">
            <img className="chart-img" src={images.UVIcon} alt="img" />
            <div className="title">Lượt quan tâm</div>
            <div className="value">25,135</div>
          </Panel>
        </Col>
        {/* <Col xs={6}>
          <Panel className="mb-[12px] trend-box bg-gradient-blue">
            <img className="chart-img" src={images.UVIcon} alt="img" />
            <div className="title">Lượt quan tâm</div>
            <div className="value">25,135</div>
          </Panel>
        </Col>
        <Col xs={6}>
          <Panel className="mb-[12px] trend-box bg-gradient-blue">
            <img className="chart-img" src={images.UVIcon} alt="img" />
            <div className="title">Lượt quan tâm</div>
            <div className="value">25,135</div>
          </Panel>
        </Col>
        <Col xs={6}>
          <Panel className="mb-[12px] trend-box bg-gradient-blue">
            <img className="chart-img" src={images.UVIcon} alt="img" />
            <div className="title">Lượt quan tâm</div>
            <div className="value">25,135</div>
          </Panel>
        </Col>
        <Col xs={6}>
          <Panel className="mb-[12px] trend-box bg-gradient-blue">
            <img className="chart-img" src={images.UVIcon} alt="img" />
            <div className="title">Lượt quan tâm</div>
            <div className="value">25,135</div>
          </Panel>
        </Col> */}
      </Row>
      <Row gutter={30}>
        <Col xs={16}>
          <BarChart
            title="Số lượng đơn hàng"
            actions={
              <ButtonGroup>
                <Button active>Ngày</Button>
                <Button>Tuần</Button>
                <Button>Tháng</Button>
              </ButtonGroup>
            }
            data={barChartData}
            type="bar"
            labels={[
              '2022-01-20',
              '2022-01-21',
              '2022-01-22',
              '2022-01-23',
              '2022-01-24',
              '2022-01-25',
              '2022-01-26',
              '2022-01-27',
              '2022-01-28',
              '2022-01-29',
              '2022-01-30',
              '2022-02-01',
              '2022-02-02',
              '2022-02-03',
              '2022-02-04',
              '2022-02-05',
              '2022-02-06',
              '2022-02-07',
              '2022-02-08',
              '2022-02-09',
              '2022-02-10',
              '2022-02-11',
              '2022-02-12',
              '2022-02-13',
              '2022-02-14',
              '2022-02-15',
              '2022-02-16',
              '2022-02-17',
              '2022-02-18',
              '2022-02-19',
              '2022-02-20',
              '2022-02-21',
              '2022-02-22',
              '2022-02-23',
              '2022-02-24',
              '2022-02-25',
              '2022-02-26',
            ]}
          />
        </Col>
        <Col xs={8}>
          <PieChart
            title="Nguồn doanh thu"
            data={[112332, 123221, 432334, 342334, 133432]}
            type="donut"
            labels={[
              'Chuyển khoản QR',
              'Ship COD',
              'Thanh toán MOMO',
              'Thanh toán VNPay',
              'Thanh toán ZaloPay',
            ]}
          />
        </Col>
      </Row>
      <Row gutter={30}>
        <Col xs={16}>
          <DataTable />
        </Col>
        <Col xs={8}>
          <PieChart
            title="Các trình duyệt"
            data={[10000, 3000, 2000, 1000, 900]}
            type="pie"
            labels={['Chrome', 'Edge', 'Firefox', 'Safari', 'Khác']}
          />
        </Col>
      </Row>
    </DashboardContentStyles>
  );
};

export default DashBoardContent;
