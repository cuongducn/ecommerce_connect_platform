import { Panel } from 'rsuite';
import styled from 'styled-components';
import Copyright from '../../../components/admin/copyright/Copyright';
import DashBoardContent from './DashBoardContent';

const DashboardStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const Dashboard = () => {
  return (
    <DashboardStyles>
      <Panel header={<h3 className="title">Trang chủ</h3>}>
        <DashBoardContent></DashBoardContent>
        <Copyright />
      </Panel>
    </DashboardStyles>
  );
};
export default Dashboard;
