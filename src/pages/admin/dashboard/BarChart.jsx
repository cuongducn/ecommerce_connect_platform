import React from "react";
import Chart from "react-apexcharts";
import { Panel, Stack } from "rsuite";

const defaultOptions = {
  chart: {
    fontFamily: "inherit",
    parentHeightOffset: 0,
    toolbar: {
      show: false,
    },
    animations: {
      enabled: false,
    },
    stacked: true,
    locales: [
      {
        name: "en",
        options: {
          months: [
            "Tháng 1",
            "Tháng 2",
            "Tháng 3",
            "Tháng 4",
            "Tháng 5",
            "Tháng 6",
            "Tháng 7",
            "Tháng 8",
            "Tháng 9",
            "Tháng 10",
            "Tháng 11",
            "Tháng 12",
          ],
          shortMonths: [
            "th1",
            "th2",
            "th3",
            "th4",
            "th5",
            "th6",
            "th7",
            "th8",
            "th9",
            "th10",
            "th11",
            "th12",
          ],
          days: [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
          ],
          shortDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
          toolbar: {
            exportToSVG: "Download SVG",
            exportToPNG: "Download PNG",
            menu: "Menu",
            selection: "Selection",
            selectionZoom: "Selection Zoom",
            zoomIn: "Zoom In",
            zoomOut: "Zoom Out",
            pan: "Panning",
            reset: "Reset Zoom",
          },
        },
      },
    ],
    defaultLocale: "en",
  },
  plotOptions: {
    bar: {
      columnWidth: "50%",
    },
  },
  dataLabels: {
    enabled: false,
  },
  fill: {
    opacity: 1,
  },
  grid: {
    padding: {
      top: -20,
      right: 0,
      left: -4,
      bottom: -4,
    },
    strokeDashArray: 4,
    xaxis: {
      lines: {
        show: true,
      },
    },
  },
  xaxis: {
    tooltip: {
      enabled: false,
    },
    axisBorder: {
      show: false,
    },
    type: "datetime",
  },
  yaxis: {
    labels: {
      padding: 4,
    },
  },
  colors: ["#206bc4", "#79a6dc", "#bfe399"],
  legend: {
    show: false,
  },
};

const BarChart = ({ title, actions, data, type, labels, options }) => (
  <Panel
    className="card"
    header={
      <Stack justifyContent="space-between">
        {title}
        {actions}
      </Stack>
    }
  >
    <Chart
      series={data}
      type={type}
      height={284}
      options={Object.assign({}, defaultOptions, options, { labels })}
    />
  </Panel>
);

export default BarChart;
