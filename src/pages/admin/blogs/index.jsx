import {
  PlusOutlined,
  EditOutlined,
  DeleteOutlined,
  UploadOutlined,
} from '@ant-design/icons';

import {
  Button,
  Card,
  Col,
  Form,
  Image,
  Input,
  Modal,
  Row,
  Select,
  Switch,
  Table,
  Upload,
} from 'antd';
import { useForm } from 'antd/es/form/Form';
import { debounce } from 'lodash';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSearchParams } from 'react-router-dom';
import * as blogsAction from '../../../actions/admin/blogs';
import { fileType } from '../../../constants/ConstantModel';
import TextEditor from '../../../components/admin/textEditor';
import dayjs from 'dayjs';
import { getQueryParams } from '../../../utils/helper';
import history from '../../../history';

const Blogs = () => {
  const columns = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
      width: 50,
      align: 'center',
      render: (a, b, index) => (
        <span>{filter.limit * (filter.page - 1) + index + 1}</span>
      ),
    },
    // {
    //   title: "Tên",
    //   dataIndex: "title",
    //   align: "center",
    //   render: (text, row) => (
    //     <a
    //       className="text-blue-700 hover:cursor-pointer"
    //       onClick={() => {
    //         setId(row.id);
    //       }}
    //     >
    //       {text}
    //     </a>
    //   ),
    // },
    {
      title: 'tiêu đề',
      dataIndex: 'title',
      align: 'center',
      render: (text) => <div className="line-clamp-1">{text} </div>,
    },

    {
      title: 'Ngày tạo',
      dataIndex: 'created_at',
      align: 'center',
      render: (text) => <div>{dayjs(text).format('DD/MM/YYYY') || ''} </div>,
    },

    {
      title: 'Số lượt xem',
      dataIndex: 'count_view',
      align: 'center',
    },
    {
      title: 'Hiển thị',
      dataIndex: 'published',
      align: 'center',
      render: (text, row) => {
        return (
          <Switch
            style={{ border: '1px solid #808080' }}
            checked={text}
            onChange={(checked) => {
              onStatus(row.id, checked);
            }}
          />
        );
      },
    },

    {
      title: 'Thao tác',
      dataIndex: 'delete',

      width: 150,
      align: 'center',

      render: (val, row) => (
        <Row className="flex justify-center items-center">
          <Col className="mr-1">
            <Button
              onClick={() => {
                setId(row.id);
                setMode('update');
                showModal();
              }}
              shape="circle"
              className="flex justify-center items-center border-white hover:border-none"
            >
              <EditOutlined />
            </Button>
          </Col>
          <Col>
            <Button
              shape="circle"
              className="flex justify-center items-center border-white hover:border-none"
              onClick={() => {
                setId(row.id);
                showModalDelete();
              }}
            >
              <DeleteOutlined />
            </Button>
          </Col>
        </Row>
      ),
    },
  ];

  const [mode, setMode] = useState('add');

  const [isModalDelete, setIsModalDelete] = useState(false);
  const [modalCreate, setModalCreate] = useState(false);
  const [id, setId] = useState(0);
  const [loading] = useState(false);
  const dispatch = useDispatch();
  const [searchParams] = useSearchParams();
  const [form] = useForm();

  const [imageUrl, setImageUrl] = useState('');
  const [imageFile, setImageFile] = useState(null);
  const [videoFile, setVideoFile] = useState(null);
  const [videoUrl, setVideoUrl] = useState(null);

  const handleCancel = () => {
    setModalCreate(false);
    setIsModalDelete(false);
  };

  const showModal = () => {
    setModalCreate(true);
  };
  const showModalDelete = () => {
    setIsModalDelete(true);
  };

  const { data: blogs } = useSelector(
    (state) => state.blogsReducers.blogs.listBlogs
  );
  const { data: imgUrl } = useSelector(
    (state) => state.blogsReducers.blogs.uploadImage
  );
  const { data: videoUrlBlogs } = useSelector(
    (state) => state.blogsReducers.blogs.uploadVideo
  );

  const onStatus = (id, val) => {
    const newStatus = val ? 1 : 0;
    blogsAction.updateBlogs({
      id,
      published: newStatus,
    })(dispatch);
  };

  const onCreateBlogs = (val) => {
    blogsAction.createBlogs(
      {
        id,
        ...val,
        image: imgUrl[fileType.IMAGE_BLOGS],
        video: videoUrlBlogs[fileType.VIDEO_BLOGS],
      },
      () => {
        form.resetFields();
        setModalCreate(false);
      }
    )(dispatch);
  };

  const onUpdateBlogs = (val) => {
    blogsAction.updateBlogs(
      {
        id,
        ...val,
        image: imgUrl[fileType.IMAGE_BLOGS],
        video: videoUrlBlogs[fileType.VIDEO_BLOGS],
      },
      () => {
        form.resetFields();
        setModalCreate(false);
      }
    )(dispatch);
  };
  const onDelete = () => {
    blogsAction.deleteBlogs(
      {
        blogs_ids: [id],
      },
      () => setIsModalDelete(false)
    )(dispatch);
  };
  const [filter, setFilter] = useState({
    page: getQueryParams('page') || 1,
    limit: getQueryParams('limit') || 10,
    search: getQueryParams('search') || '',
  });

  const onSearch = debounce((val) => {
    const values = {
      ...filter,
      ...val,
      page: 1,
    };
    setFilter(values);
    const params = getParams(1, filter.limit, val.search);
    history.push(`/admin/blogs?${params}`);
  }, 1000);

  const getParams = (page, limit, search) => {
    let params = '';
    if (page) {
      params += `page=${page}`;
    }
    if (limit) {
      params += `&limit=${limit}`;
    }
    if (search) {
      params += `&search=${search}`;
    }

    return params;
  };

  const { data: blogsDetail } = useSelector(
    (state) => state.blogsReducers.blogs.detailBlogs
  );

  useEffect(() => {
    if (id) {
      blogsAction.detailBlogs(id)(dispatch);
    }
  }, [id]);

  useEffect(() => {
    blogsAction.listBlogs(filter)(dispatch);
  }, [filter]);

  useEffect(() => {
    if (mode === 'update') form.setFieldsValue(blogsDetail);
  }, [mode, blogsDetail]);

  const onVideoUpload = (info) => {
    console.log(info);
    const file = info.file;
    setVideoFile(file);
  };

  const onAvatarUpload = (e) => {
    const file = e.target.files[0];
    setImageFile(file);
    const reader = new FileReader();
    reader.onload = (event) => {
      setImageUrl(event.target.result);
    };
    reader.readAsDataURL(file);
  };

  useEffect(() => {
    if (imageFile) {
      blogsAction.uploadImgBlogs(fileType.IMAGE_BLOGS, imageFile)(dispatch);
    }
  }, [imageFile, dispatch]);

  useEffect(() => {
    if (videoFile)
      blogsAction.uploadVideoBlogs(fileType.VIDEO_BLOGS, videoFile)(dispatch);
  }, [videoFile, dispatch]);

  return (
    <div className="mx-6 my-6 w-full">
      <Card className="w-full">
        <div>
          <div className="grid grid-cols-2">
            <div className="text-[#00897b] font-sans text-xl col-span-1">
              Danh sách Blogs
            </div>
            <div className="col-span-1 flex justify-end">
              <Button
                onClick={() => {
                  setMode('add');
                  showModal(true);
                }}
                size="large"
                className="bg-[#00897b] text-white mr-3 "
                icon={<PlusOutlined className="justify-center" />}
              >
                Thêm blogs
              </Button>
            </div>
          </div>
        </div>
        <Form
          name="basic"
          autoComplete="off"
          labelAlign="left"
          layout="vertical"
          onValuesChange={(c, v) => onSearch(v)}
          initialValues={{
            search: searchParams.get('search') || '',
          }}
        >
          <div className="grid gap-2 mt-8 ">
            <div className="gird-col-1 w-[400px]">
              <Form.Item name="search">
                <Input
                  className="h-[40px] "
                  placeholder="Tìm kiếm tên thành viên, số điện thoại,.. "
                  prefix={<EditOutlined />}
                />
              </Form.Item>
            </div>
          </div>
        </Form>
        <div>
          <Table
            size="small"
            columns={columns}
            dataSource={blogs.data || []}
            pagination={{
              current: Number(filter.page),
              pageSize: Number(filter.limit),
              total: blogs.total,
              onChange: (page, pageSize) => {
                setFilter({
                  page: page,
                  limit: pageSize,
                });
                const params = getParams(page, pageSize, filter.search);
                history.push(`/admin/blogs?${params}`);
              },
            }}
          />
        </div>
      </Card>

      <Modal
        width={700}
        onCancel={handleCancel}
        open={modalCreate}
        title={mode === 'add' ? 'Thêm blogs' : ' Cập nhật blogs'}
        footer={[
          <Button key="back" type="text" onClick={handleCancel}>
            Hủy bỏ
          </Button>,
          <Button
            style={{
              backgroundColor: '#00897b',
              color: '#ffffff',
            }}
            loading={loading}
            form="form-add-blogs"
            htmlType="submit"
            key="submit"
          >
            Lưu
          </Button>,
        ]}
      >
        <Form
          form={form}
          layout="vertical"
          name="form-add-blogs"
          onFinish={(val) => {
            if (mode === 'add') onCreateBlogs(val);
            else if (mode === 'update') onUpdateBlogs(val);
          }}
          initialValues={mode === 'update' ? blogsDetail : {}}
        >
          <Form.Item label="tiêu đề" name="title">
            <Input value={blogsDetail.title} />
          </Form.Item>
          <Form.Item label="tiêu đề phụ" name="sub_title" shouldUpdate={true}>
            <Input />
          </Form.Item>
          <Form.Item label="Nội dung" name="summary">
            <TextEditor value={blogsDetail.summary} />
          </Form.Item>
          <Form.Item label="Hiển thị" name="published">
            <Switch
              style={{ border: '1px solid #808080' }}
              checked={blogsDetail.published}
            />
          </Form.Item>
          <Form.Item label="video">
            <Upload
              accept="video/*"
              beforeUpload={() => false}
              onChange={onVideoUpload}
              src={videoUrl}
            >
              <Button icon={<UploadOutlined />}>Tải lên Video</Button>
            </Upload>
          </Form.Item>
          <Form.Item label="hình ảnh">
            <div className="rounded-md relative overflow-visible w-[100px] h-[100px]">
              <Image
                className="rounded-[10px]"
                width={'100%'}
                height={'100%'}
                placeholder={
                  <Image
                    preview={false}
                    width={'100%'}
                    height={'100%'}
                    src={imageUrl}
                  />
                }
                src={blogsDetail.image || imgUrl[fileType.IMAGE_BLOGS]}
              />
              <div>
                <label
                  htmlFor="image_blogs"
                  className=" absolute cursor-pointer 
      z-30  bg-gray-700/50 rounded-full flex items-center justify-center text-white 
       p-2 mb-3 w-10 h-10 bottom-0 right-0 translate-x-1/3 translate-y-1/3"
                >
                  <input
                    onChange={onAvatarUpload}
                    type="file"
                    className="hidden"
                    id="image_blogs"
                  />{' '}
                  {/* <HiOutlineCamera className="text-[18px] translate-y-[2px]" /> */}
                </label>
              </div>
            </div>
          </Form.Item>
          <Form.Item label="tiêu đề seo" name="seo_title">
            <Input />
          </Form.Item>
          <Form.Item label="Mô tả seo" name="seo_description">
            <Input />
          </Form.Item>
        </Form>
      </Modal>

      <Modal
        onCancel={handleCancel}
        open={isModalDelete}
        title="Xóa blogs"
        footer={[
          <Button key="back" type="text" onClick={handleCancel}>
            Hủy
          </Button>,
          <Button
            style={{
              backgroundColor: '#00897b',
            }}
            // loading={loadingDelete}
            onClick={() => onDelete()}
            htmlType="submit"
            key="submit"
            type="primary"
          >
            Xóa
          </Button>,
        ]}
      >
        <span>Bạn có chắc chắn muốn xóa Blogs này không ?</span>
      </Modal>
    </div>
  );
};

export default Blogs;
