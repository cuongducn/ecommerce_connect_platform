import { Breadcrumb } from "rsuite";
import styled from "styled-components";
import Breadcrumbs from "../../components/breadcrumb";
import Copyright from "../../components/copyright/Copyright";
import DataTable from "./table/DataTable";

const ClinicsStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const Clinics = () => {
  return (
    <ClinicsStyles>
      <Breadcrumbs
        title="Phòng khám"
        breadcrumbItems={[<Breadcrumb.Item active>Phòng khám</Breadcrumb.Item>]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </ClinicsStyles>
  );
};
export default Clinics;
