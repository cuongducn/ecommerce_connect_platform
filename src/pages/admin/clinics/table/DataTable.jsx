import {
  Table,
  Pagination,
  InputGroup,
  Input,
  Stack,
  InputPicker,
  IconButton,
} from "rsuite";
import { useCallback, useEffect, useState } from "react";
import ImageCell from "../../../components/table/ImageCell";
import NameCell from "../../../components/table/NameCell";
import { Search, Check, Close } from "@rsuite/icons";
import { useDispatch, useSelector } from "react-redux";
import * as clinic from "../../../actions/clinic";
import { combineAddress, formatNumber } from "../../../utils";
import styled from "styled-components";
import { useNavigate, useSearchParams } from "react-router-dom";
import { debounce } from "lodash";
import ToggleLoading from "../../../components/button/ToggleLoading";
import * as Types from "../../../constants/actionType";
const { Column, HeaderCell, Cell } = Table;
const rowKey = "id";
const statusCensorship = [
  {
    label: "Chưa được duyệt",
    value: Types.STATUS_PROGRESSING,
  },
  {
    label: "Đã được duyệt",
    value: Types.STATUS_COMPLETED,
  },
  {
    label: "Đã hủy",
    value: Types.STATUS_CANCELLED,
  },
];
const DataTableStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding-top: 20px;
  border-radius: 10px;
  .header {
    display: flex;
    justify-content: flex-end;
    margin-bottom: 20px;
    padding: 0 20px;
    .input__search-content {
      width: 300px;
    }
  }
  @media only screen and (max-width: 768px) {
    .header {
      .input__search-content {
        width: 190px;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .header {
      flex-direction: column;
      row-gap: 10px;
      padding: 0 20px;
      .rs-stack-item {
        width: 100%;
        overflow: visible !important;
        .input__search-content {
          width: 100%;
        }
        .rs-picker {
          margin-right: 0 !important;
          width: 100% !important;
        }
      }
    }
  }
`;
const DataTable = () => {
  const dispatch = useDispatch();
  const { isLoadingClinic } = useSelector((state) => state.loadingReducers);
  const { clinics } = useSelector((state) => state.clinicReducers);
  const { data } = clinics;

  const [clinicSelected, setClinicSelected] = useState({});
  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get("page")) || 1,
    limit: 20,
    status: Number(searchParams.get("status")) || "",
    search: searchParams.get("search") || "",
    searchInput: searchParams.get("search") || "",
  });
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const navigate = useNavigate();

  // Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        navigate(
          `/phong-kham?page=1${valueSearch ? `&search=${valueSearch}` : ""}${
            prevParams.status !== "" ? `&status=${prevParams.status}` : ""
          }`
        );
        return {
          ...prevParams,
          page: 1,
          search: valueSearch,
          searchInput: valueSearch,
        };
      });
    }, 500),
    []
  );
  const handleSearchClinics = (e) => {
    const value = e;
    setParams({
      ...params,
      searchInput: value,
    });
    debounceCallBack(value);
  };

  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === "string") {
          x = x.charCodeAt();
        }
        if (typeof y === "string") {
          y = y.charCodeAt();
        }
        if (sortData.type === "asc") {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    let dataIndex = [];
    if (data?.length > 0) {
      dataIndex = data.map((item, index) => {
        item.index =
          (Number(clinics.current_page) - 1) * Number(clinics.per_page) +
          index +
          1;
        return item;
      });
    }
    return dataIndex;
  };
  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };

  // Handle change params
  const handleChangePage = (pageChange) => {
    navigate(
      `/phong-kham?page=${pageChange}${
        params.search ? `&search=${params.search}` : ""
      }${params.status !== "" ? `&status=${params.status}` : ""}`
    );
    setParams({
      ...params,
      page: pageChange,
    });
  };

  //Handle Consorship clinic
  const handleConsorshipClinic = (idClinic, isChecked) => {
    const data = {
      status: isChecked ? Types.STATUS_COMPLETED : Types.STATUS_CANCELLED,
    };

    dispatch(clinic.updateClinic(idClinic, data));
  };

  //Handle Change Status
  const handleChangeStatus = (value) => {
    navigate(
      `/phong-kham?page=1${params.search ? `&search=${params.search}` : ""}${
        value !== null ? `&status=${value}` : ""
      }`
    );
    setParams({
      ...params,
      page: 1,
      status:
        value === Types.STATUS_COMPLETED
          ? Types.STATUS_COMPLETED
          : value === Types.STATUS_CANCELLED
          ? Types.STATUS_CANCELLED
          : value === Types.STATUS_PROGRESSING
          ? Types.STATUS_PROGRESSING
          : "",
    });
  };

  //Handle List Packet Medical
  const handleListPacketMedical = (listPacketsMedical) => {
    if (listPacketsMedical.length > 0) {
      const listPacketsMedicalConvert = listPacketsMedical.reduce(
        (prevPacketMedical, currentPacketMedical, index) => {
          return (
            prevPacketMedical +
            `${
              index !== listPacketsMedical.length - 1
                ? ` ${currentPacketMedical.info_packet_medical.name}, `
                : currentPacketMedical.info_packet_medical.name
            }`
          );
        },
        ""
      );
      return listPacketsMedicalConvert;
    }
    return "";
  };

  // Call API Get Clinics
  useEffect(() => {
    dispatch(
      clinic.getClinics(
        `page=${params.page}&limit=${params.limit}&search=${params.search}&status=${params.status}`
      )
    );
  }, [dispatch, params.page, params.limit, params.search, params.status]);
  return (
    <DataTableStyles>
      <Stack className="header" justifyContent="space-between">
        <InputGroup inside className="input__search-content">
          <Input
            placeholder="Tìm kiếm phòng khám..."
            onChange={handleSearchClinics}
            value={params.searchInput}
          />
          <InputGroup.Addon>
            <Search />
          </InputGroup.Addon>
        </InputGroup>
        <InputPicker
          data={statusCensorship}
          style={{ width: 170 }}
          placeholder="Trạng thái: Tất cả"
          onChange={handleChangeStatus}
        />
      </Stack>
      <Table
        autoHeight
        wordWrap="break-word"
        rowKey={rowKey}
        hover={false}
        data={getData()}
        sortColumn={sortData.column}
        sortType={sortData.type}
        onSortColumn={handleSortColumn}
        loading={sortData.loading}
      >
        <Column align="center" width={50}>
          <HeaderCell>STT</HeaderCell>
          <Cell dataKey="index" />
        </Column>
        <Column align="center" width={70}>
          <HeaderCell>Hình ảnh</HeaderCell>
          <ImageCell
            dataKey="images"
            styleImage={{
              width: "40px",
              height: "40px",
            }}
          />
        </Column>
        <Column width={150} sortable>
          <HeaderCell>Tên phòng khám</HeaderCell>
          <NameCell
            dataKey="clinic_name"
            dataPopup={{
              clinic_name: "Tên phòng khám",
              time_open: "Giờ mở của",
              time_close: "Giờ đóng cửa",
            }}
          />
        </Column>
        <Column flexGrow={1}>
          <HeaderCell>Địa chỉ</HeaderCell>
          <Cell>
            {(rowData) => (
              <span
                title={combineAddress(
                  rowData.address_detail,
                  rowData.wards_name,
                  rowData.district_name,
                  rowData.province_name
                )}
              >
                {combineAddress(
                  rowData.address_detail,
                  rowData.wards_name,
                  rowData.district_name,
                  rowData.province_name
                )}
              </span>
            )}
          </Cell>
        </Column>
        <Column width={250}>
          <HeaderCell>Các gói khám bệnh</HeaderCell>
          <Cell>
            {(rowData) => handleListPacketMedical(rowData.list_packet_medical)}
          </Cell>
        </Column>
        <Column width={120}>
          <HeaderCell>Giờ mở cửa</HeaderCell>
          <Cell>{(rowData) => rowData.time_open}</Cell>
        </Column>
        <Column width={120}>
          <HeaderCell>Giờ đóng cửa</HeaderCell>
          <Cell>{(rowData) => rowData.time_open}</Cell>
        </Column>

        <Column width={120}>
          <HeaderCell>Trạng thái</HeaderCell>
          <Cell>
            {(rowData) => (
              <>
                {rowData.status === Types.STATUS_PROGRESSING ? (
                  <div className="btn__actions" style={{ marginTop: -9 }}>
                    <IconButton
                      color="green"
                      appearance="link"
                      onClick={() =>
                        handleConsorshipClinic(rowData[rowKey], true)
                      }
                      icon={<Check />}
                    />
                    <IconButton
                      color="red"
                      appearance="link"
                      onClick={() =>
                        handleConsorshipClinic(rowData[rowKey], false)
                      }
                      icon={<Close />}
                    />
                  </div>
                ) : (
                  <ToggleLoading
                    isLoading={isLoadingClinic}
                    status={rowData["status"]}
                    data={rowData}
                    dataSelected={clinicSelected}
                    setDataSelected={setClinicSelected}
                    handleAction={handleConsorshipClinic}
                  />
                )}
              </>
            )}
          </Cell>
        </Column>
      </Table>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          size="xs"
          layout={["total", "-", "pager", "skip"]}
          total={clinics.total}
          limitOptions={[10, 30, 50]}
          limit={params.limit}
          activePage={params.page}
          onChangePage={handleChangePage}
        />
      </div>
    </DataTableStyles>
  );
};

export default DataTable;
