import { Breadcrumb } from "rsuite";
import styled from "styled-components";
import Breadcrumbs from "../../components/breadcrumb";
import Copyright from "../../components/copyright/Copyright";
import DataTable from "./table/DataTable";

const DoctorsStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const Doctors = () => {
  return (
    <DoctorsStyles>
      <Breadcrumbs
        title="Bác sĩ"
        breadcrumbItems={[<Breadcrumb.Item active>Bác sĩ</Breadcrumb.Item>]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </DoctorsStyles>
  );
};
export default Doctors;
