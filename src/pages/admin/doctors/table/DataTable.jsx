import {
  Table,
  Pagination,
  InputGroup,
  Input,
  Stack,
  InputPicker,
  IconButton,
} from "rsuite";
import { useCallback, useEffect, useState } from "react";
import ImageCell from "../../../components/table/ImageCell";
import NameCell from "../../../components/table/NameCell";
import { Search, Check, Close } from "@rsuite/icons";
import { useDispatch, useSelector } from "react-redux";
import * as doctor from "../../../actions/doctor";
import { combineAddress, formatNumber } from "../../../utils";
import styled from "styled-components";
import ExpandCell from "../../../components/table/ExpandCell";
import RowExpandedInformation from "./RowExpandedInformation";
import { useNavigate, useSearchParams } from "react-router-dom";
import { debounce } from "lodash";
import ToggleLoading from "../../../components/button/ToggleLoading";
import * as Types from "../../../constants/actionType";
const { Column, HeaderCell, Cell } = Table;
const rowKey = "id";
const statusCensorship = [
  {
    label: "Chưa được duyệt",
    value: Types.STATUS_PROGRESSING,
  },
  {
    label: "Đã được duyệt",
    value: Types.STATUS_COMPLETED,
  },
  {
    label: "Đã hủy",
    value: Types.STATUS_CANCELLED,
  },
];

const DataTableStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding-top: 20px;
  border-radius: 10px;
  .header {
    display: flex;
    justify-content: flex-start;
    margin-bottom: 20px;
    padding-left: 20px;
    .input__search-content {
      width: 300px;
    }
  }
  @media only screen and (max-width: 768px) {
    .header {
      .input__search-content {
        width: 170px;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .header {
      flex-direction: column;
      row-gap: 10px;
      padding: 0 20px;
      .rs-stack-item {
        width: 100%;
        overflow: visible !important;
        .input__search-content {
          width: 100%;
        }
        .rs-picker {
          margin-right: 0 !important;
          width: 100% !important;
        }
      }
    }
  }
`;
const DataTable = () => {
  const dispatch = useDispatch();
  const { isLoadingDoctor } = useSelector((state) => state.loadingReducers);
  const { doctors } = useSelector((state) => state.doctorReducers);
  const { data } = doctors;

  const [expandedRowKeys, setExpandedRowKeys] = useState([]);
  const [doctorSelected, setDoctorSelected] = useState([]);
  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get("page")) || 1,
    limit: 20,
    status: Number(searchParams.get("status")) || "",
    search: searchParams.get("search") || "",
    searchInput: searchParams.get("search") || "",
  });
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const navigate = useNavigate();

  // Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        navigate(
          `/bac-si?page=1${valueSearch ? `&search=${valueSearch}` : ""}${
            prevParams.status !== "" ? `&status=${prevParams.status}` : ""
          }`
        );
        return {
          ...prevParams,
          page: 1,
          search: valueSearch,
          searchInput: valueSearch,
        };
      });
    }, 500),
    []
  );
  const handleSearchDoctors = (e) => {
    const value = e;
    setParams({
      ...params,
      searchInput: value,
    });
    debounceCallBack(value);
  };

  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === "string") {
          x = x.charCodeAt();
        }
        if (typeof y === "string") {
          y = y.charCodeAt();
        }
        if (sortData.type === "asc") {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    return data;
  };
  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };

  const handleChangePage = (pageChange) => {
    navigate(
      `/bac-si?page=${pageChange}${
        params.search ? `&search=${params.search}` : ""
      }${params.status !== "" ? `&status=${params.status}` : ""}`
    );
    setParams({
      ...params,
      page: pageChange,
    });
  };

  //Handle Consorship doctor
  const handleConsorshipDoctor = (idDoctor, isChecked) => {
    const data = {
      status: isChecked ? Types.STATUS_COMPLETED : Types.STATUS_CANCELLED,
    };

    dispatch(doctor.updateDoctor(idDoctor, data));
  };

  // Handle Open Expand
  const handleExpanded = (rowData) => {
    let open = false;
    const nextExpandedRowKeys = [];

    expandedRowKeys.forEach((key) => {
      if (key === rowData[rowKey]) {
        open = true;
      } else {
        nextExpandedRowKeys.push(key);
      }
    });

    if (!open) {
      nextExpandedRowKeys.push(rowData[rowKey]);
    }

    setExpandedRowKeys(nextExpandedRowKeys);
  };

  //Handle Change Status
  const handleChangeStatus = (value) => {
    navigate(
      `/bac-si?page=1${params.search ? `&search=${params.search}` : ""}${
        value !== null ? `&status=${value}` : ""
      }`
    );
    setParams({
      ...params,
      page: 1,
      status:
        value === Types.STATUS_COMPLETED
          ? Types.STATUS_COMPLETED
          : value === Types.STATUS_CANCELLED
          ? Types.STATUS_CANCELLED
          : value === Types.STATUS_PROGRESSING
          ? Types.STATUS_PROGRESSING
          : "",
    });
  };
  const handleLevelAcademic = (levelAcademic) => {
    if (levelAcademic?.length > 0) {
      const levelAcademicDetail = levelAcademic.reduce(
        (prevLevelAcademic, currentLevelAcademic, index) => {
          return (
            prevLevelAcademic +
            `${
              index === levelAcademic?.length - 1
                ? currentLevelAcademic?.degree
                : `${currentLevelAcademic?.degree}, `
            }`
          );
        },
        ""
      );

      return levelAcademicDetail;
    }
    return "";
  };

  const handleSpecialist = (specialist) => {
    const departmentsSpecialFilter = specialist.reduce(
      (prevData, nextData, index) => {
        return (
          prevData +
          `${
            index === specialist.length - 1
              ? ` ${nextData.name}`
              : `${nextData.name}, `
          }`
        );
      },
      ""
    );
    return departmentsSpecialFilter;
  };

  // Call API Get Doctors
  useEffect(() => {
    dispatch(
      doctor.getDoctors(
        `page=${params.page}&limit=${params.limit}&search=${params.search}&status=${params.status}`
      )
    );
  }, [dispatch, params.page, params.limit, params.search, params.status]);
  return (
    <DataTableStyles>
      <Stack className="header" justifyContent="space-between">
        <InputGroup inside className="input__search-content">
          <Input
            placeholder="Tìm kiếm bác sĩ..."
            onChange={handleSearchDoctors}
            value={params.searchInput}
          />
          <InputGroup.Addon>
            <Search />
          </InputGroup.Addon>
        </InputGroup>
        <InputPicker
          data={statusCensorship}
          style={{ width: 170, marginRight: 20 }}
          placeholder="Trạng thái: Tất cả"
          onChange={handleChangeStatus}
        />
      </Stack>
      <Table
        autoHeight
        rowKey={rowKey}
        hover={false}
        expandedRowKeys={expandedRowKeys}
        renderRowExpanded={RowExpandedInformation}
        rowExpandedHeight={420}
        data={getData()}
        sortColumn={sortData.column}
        sortType={sortData.type}
        onSortColumn={handleSortColumn}
        loading={sortData.loading}
      >
        <Column align="center" width={50}>
          <HeaderCell>#</HeaderCell>
          <ExpandCell
            dataKey="id"
            expandedRowKeys={expandedRowKeys}
            onChange={handleExpanded}
          />
        </Column>
        <Column align="center" width={70}>
          <HeaderCell>Hình ảnh</HeaderCell>
          <ImageCell
            dataKey="doctor_image_1"
            styleImage={{
              width: "40px",
              height: "40px",
            }}
          />
        </Column>
        <Column width={170} sortable>
          <HeaderCell>Tên</HeaderCell>
          <NameCell
            dataKey="first_and_last_name"
            dataPopup={{
              first_and_last_name: "Tên",
              years_of_experience: "Năm kinh nghiệm",
              cost_mess_suport: "Chi phí nhắn tin hỗ trợ",
              cost_call_suport: "Chi phí gọi hỗ trợ",
            }}
            cost={[2, 3]}
          />
        </Column>
        <Column width={130}>
          <HeaderCell>Số năm kinh nghiệm</HeaderCell>
          <Cell>{(rowData) => rowData.years_of_experience}</Cell>
        </Column>
        <Column flexGrow={1}>
          <HeaderCell>Địa chỉ</HeaderCell>
          <Cell>
            {(rowData) =>
              combineAddress(
                rowData.address_detail,
                rowData.wards_name,
                rowData.district_name,
                rowData.province_name
              )
            }
          </Cell>
        </Column>
        <Column width={220}>
          <HeaderCell>Khoa khám</HeaderCell>
          <Cell>
            {(rowData) => (
              <span title={handleSpecialist(rowData.specialist)}>
                {handleSpecialist(rowData.specialist)}
              </span>
            )}
          </Cell>
        </Column>
        <Column width={200}>
          <HeaderCell>Học vị</HeaderCell>
          <Cell>
            {(rowData) => handleLevelAcademic(rowData.academic_level)}
          </Cell>
        </Column>

        <Column width={120}>
          <HeaderCell>Trạng thái</HeaderCell>
          <Cell>
            {(rowData) => (
              <>
                {rowData.status === Types.STATUS_PROGRESSING ? (
                  <div className="btn__actions" style={{ marginTop: -7 }}>
                    <IconButton
                      color="green"
                      appearance="link"
                      onClick={() =>
                        handleConsorshipDoctor(rowData[rowKey], true)
                      }
                      icon={<Check />}
                    />
                    <IconButton
                      color="red"
                      appearance="link"
                      onClick={() =>
                        handleConsorshipDoctor(rowData[rowKey], false)
                      }
                      icon={<Close />}
                    />
                  </div>
                ) : (
                  <ToggleLoading
                    isLoading={isLoadingDoctor}
                    status={rowData["status"]}
                    data={rowData}
                    dataSelected={doctorSelected}
                    setDataSelected={setDoctorSelected}
                    handleAction={handleConsorshipDoctor}
                  />
                )}
              </>
            )}
          </Cell>
        </Column>
      </Table>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          size="xs"
          layout={["total", "-", "pager", "skip"]}
          total={doctors.total}
          limitOptions={[10, 30, 50]}
          limit={params.limit}
          activePage={params.page}
          onChangePage={handleChangePage}
        />
      </div>
    </DataTableStyles>
  );
};

export default DataTable;
