import React from "react";
import { Avatar, Col, Grid, Row } from "rsuite";
import UserIcon from "@rsuite/icons/legacy/User";
import styled from "styled-components";

const RowExpandedInformationStyles = styled.div`
  .identityCard {
    display: flex;
    column-gap: 15px;
    div {
      flex-shrink: 0;
      img,
      .rs-avatar {
        width: 230px;
        height: 150px;
        border-radius: 5px;
        object-fit: cover;
      }
      p {
        text-align: center;
        font-weight: 500;
        margin-top: 5px;
      }
    }
  }
  .informationWork {
    display: flex;
    flex-direction: column;
    row-gap: 5px;
    .informationWork__title {
      font-weight: 500;
    }
  }
  @media only screen and (max-width: 1200px) {
    .rowExpanded {
      display: flex;
      flex-direction: column;
      row-gap: 15px;
    }
    .identityCard {
      div {
        img {
          height: 120px;
          width: 180px;
        }
      }
    }
  }
`;

const RowExpandedInformation = (rowData) => {
  // Handle Display interface
  const handleProgressWork = (progressWork) => {
    let progressDetail = "";
    progressWork.forEach(
      (progress, index) =>
        (progressDetail += `${progress.position} tại
            ${progress.hospital_name}(${progress.from_year} - ${
          progress.to_year
        })${index === progressWork.length - 1 ? "" : "; "}`)
    );
    return progressDetail;
  };
  const handleLevelAcademic = (levelAcademic) => {
    let levelAcademicDetail = "";
    levelAcademic.forEach(
      (level, index) =>
        (levelAcademicDetail += `${level.degree} tại
            ${level.graduation_place}(${level.from_year} - ${level.to_year})${
          index === levelAcademic.length - 1 ? "" : "; "
        }`)
    );
    return levelAcademicDetail;
  };
  const handleCertificates = (certificates) => {
    const certificatesDetail = certificates.reduce(
      (prevCertificate, nextCertificate, index) => {
        return index !== certificates.length - 1
          ? prevCertificate + ", " + nextCertificate
          : nextCertificate;
      },
      ""
    );
    return certificatesDetail;
  };
  return (
    <RowExpandedInformationStyles>
      <Grid fluid>
        <Row className="rowExpanded">
          <Col xxl={12} xl={10}>
            <div className="informationWork">
              <div>
                <span className="informationWork__title">Số CMND/CCCD:</span>
                <span>{rowData.number_identity_card}</span>
              </div>
              <div>
                <span className="informationWork__title">
                  Giấy chứng nhận:{" "}
                </span>
                <span>{handleCertificates(rowData.certificate_list)}</span>
              </div>
              <div>
                <span className="informationWork__title">
                  Quá trình làm việc:{" "}
                </span>
                <span>{handleProgressWork(rowData.work_progress)}</span>
              </div>
              <div>
                <span className="informationWork__title">
                  Trình độ học vấn:{" "}
                </span>
                <span>{handleLevelAcademic(rowData.academic_level)}</span>
              </div>
              {rowData.introduce ? (
                <div>
                  <span className="informationWork__title">Miêu tả: </span>
                  <span>{rowData.introduce}</span>
                </div>
              ) : null}
            </div>
          </Col>
          <Col xxl={12} xl={12}>
            <div
              style={{
                fontWeight: "500",
                marginBottom: "10px",
              }}
            >
              Ảnh CMND/CCCD
            </div>
            <div className="identityCard">
              <div>
                {rowData.cmnd_front ? (
                  <img src={rowData.cmnd_front} alt="identityCard_front" />
                ) : (
                  <Avatar>
                    <UserIcon />
                  </Avatar>
                )}
                <p>Mặt trước</p>
              </div>
              <div>
                {rowData.cmnd_back ? (
                  <img src={rowData.cmnd_back} alt="identityCard_back" />
                ) : (
                  <Avatar>
                    <UserIcon />
                  </Avatar>
                )}
                <p>Mặt sau</p>
              </div>
            </div>
            <div
              style={{
                fontWeight: "500",
                margin: "10px 0",
              }}
            >
              Ảnh giấy chứng nhận
            </div>
            <div className="identityCard">
              <div>
                {rowData.images_certificate?.length > 0 ? (
                  <img
                    src={rowData.images_certificate[0]}
                    alt="images_certificate"
                  />
                ) : (
                  <Avatar>
                    <UserIcon />
                  </Avatar>
                )}
              </div>
            </div>
          </Col>
        </Row>
      </Grid>
    </RowExpandedInformationStyles>
  );
};

export default RowExpandedInformation;
