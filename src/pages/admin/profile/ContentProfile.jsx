import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, Schema, Stack } from 'rsuite';
import StackItem from 'rsuite/esm/Stack/StackItem';
import styled from 'styled-components';
import DatePickerCalendar from '../../../components/admin/form/DatePicker';
import Input from '../../../components/admin/form/Input';
import RadioGroupCustom from '../../../components/admin/form/RadioGroup';
import { genders } from '../../../utils/data/genders';
import * as profileActions from '../../../actions/profile';
import { regexs } from '../../../utils/regexs';
import UploadImage from '../../../components/admin/form/UploadImage';
import { formatDateObj, formatDateStr } from '../../../utils/date';
import { uploadImage } from '../../../actions/upload';

const ContentProfileStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding: 20px;
  border-radius: 10px;
  .profile__header {
    margin-bottom: 20px;
    .profile__title {
      font-size: 18px;
      font-weight: 500;
    }
  }
  .profile__form {
    display: flex;
    flex-direction: column;
    row-gap: 20px;
  }
  .profile__mainTime {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
  }
  @media only screen and (max-width: 768px) {
    padding: 20px 10px !important;
    .profile__main {
      flex-direction: column;
      gap: 15px !important;
      .profile__item {
        width: 100%;
        margin-right: 0 !important;
      }
    }
    .profile__mainTime {
      grid-template-columns: repeat(1, 1fr);
    }
  }
  @media only screen and (max-width: 576px) {
    .profile__header {
      flex-direction: column;
      row-gap: 10px;
      align-items: flex-start !important;
    }
  }
`;

const { StringType } = Schema.Types;
const model = Schema.Model({
  phone_number: StringType()
    .isRequired('Trường SĐT bắt buộc phải nhập.')
    .addRule((value) => {
      const phoneRegex = regexs.phone;
      let isValidPhone = phoneRegex.test(value);
      if (isValidPhone) {
        return true;
      }
      return false;
    }, 'SĐT không đúng định dạng'),
});

const ContentProfile = () => {
  const dispatch = useDispatch();
  const { information } = useSelector((state) => state.profileReducers);
  const { isLoadingProfile } = useSelector((state) => state.loadingReducers);

  const formRef = useRef();
  const [formValue, setFormValue] = useState({
    username: '',
    phone_number: '',
    name: '',
    date_of_birth: null,
    email: '',
    sex: '',
    avatar_image: '',
  });

  const [fileImage, setFileImage] = useState(null);

  // Handle Change Value Form
  const handleChangeValueForm = (values, e) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormValue({
      ...values,
      [name]: name === 'sex' ? Number(value) : value,
    });
  };

  //Handle Reset Value Form
  const handleRessetValueForm = () => {
    setFormValue({
      username: '',
      phone_number: '',
      name: '',
      date_of_birth: null,
      email: '',
      sex: '',
      avatar_image: '',
    });
  };

  // Handle Update
  const handleUpdateProfile = async () => {
    if (!formRef.current.check()) {
      return;
    }
    const newFormValue = {
      ...formValue,
      date_of_birth: formValue.date_of_birth
        ? formatDateStr(formValue.date_of_birth)
        : '',
    };
    if (fileImage) {
      const formDataImage = new FormData();
      formDataImage.append('image', fileImage);
      const responseImage = await uploadImage(formDataImage);
      newFormValue.avatar_image = responseImage;
    }
    dispatch(profileActions.updateProfile(newFormValue));
  };

  //Get Data Profile
  useEffect(() => {
    dispatch(profileActions.getProfile());
  }, [dispatch]);
  useEffect(() => {
    if (Object.keys(information).length > 0) {
      setFormValue({
        username: information.username,
        phone_number: information.phone_number,
        name: information.name,
        date_of_birth:
          formatDateObj(information.date_of_birth) != 'Invalid Date'
            ? formatDateObj(information.date_of_birth)
            : null,
        email: information.email ? information.email : '',
        sex: information.sex,
        avatar_image: information.avatar_image,
      });
    }
  }, [information]);
  return (
    <ContentProfileStyles>
      <div className="profile__content">
        <Form
          fluid
          checkTrigger="change"
          ref={formRef}
          onChange={handleChangeValueForm}
          formValue={formValue}
          model={model}
        >
          <Stack justifyContent="space-between" className="profile__header">
            <div className="profile__title">Thông tin</div>
            <Stack spacing={15}>
              <Button
                appearance="primary"
                type="submit"
                style={{
                  backgroundColor: '#26a69a',
                }}
                onClick={handleUpdateProfile}
                loading={isLoadingProfile}
              >
                Cập nhập
              </Button>

              <Button appearance="default" onClick={handleRessetValueForm}>
                Làm mới
              </Button>
            </Stack>
          </Stack>
          <div className="profile__form">
            <div>
              <UploadImage
                name="avatar_file"
                image_url={formValue.avatar_image}
                setFile={setFileImage}
                style={{
                  width: '100px',
                  height: '100px',
                  borderRadius: '10px',
                }}
              ></UploadImage>
            </div>
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="profile__main"
            >
              <StackItem grow={1} className="profile__item">
                <Input
                  name="username"
                  label="Tên tài khoản"
                  placeholder="Nhập tên tài khoản..."
                />
              </StackItem>
              <StackItem grow={1} className="profile__item">
                <Input
                  name="phone_number"
                  label="SĐT"
                  placeholder="Nhập SĐT..."
                />
              </StackItem>
            </Stack>
            <div
              className="profile__mainTime"
              style={{
                gap: '30px',
              }}
            >
              <div grow={1} className="profile__item">
                <Input name="name" label="Tên" placeholder="Nhập tên..." />
              </div>
              <div grow={1} className="profile__item">
                <DatePickerCalendar
                  name="date_of_birth"
                  label="Ngày sinh"
                  placement="topStart"
                  format={'yyyy-MM-dd'}
                ></DatePickerCalendar>
              </div>
            </div>
            <div
              className="profile__mainTime"
              style={{
                gap: '30px',
              }}
            >
              <StackItem grow={1} className="profile__item">
                <Input name="email" label="Email" placeholder="Nhập email..." />
              </StackItem>
              <StackItem grow={1} className="profile__item">
                <div className="profile__form__gender">
                  <RadioGroupCustom
                    label="Giới tính"
                    name="sex"
                    options={genders}
                  ></RadioGroupCustom>
                </div>
              </StackItem>
            </div>
          </div>
        </Form>
      </div>
    </ContentProfileStyles>
  );
};

export default ContentProfile;
