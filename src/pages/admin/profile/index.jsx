import React from 'react';
import { Breadcrumb } from 'rsuite';
import styled from 'styled-components';
import Breadcrumbs from '../../../components/admin/breadcrumb';
import Copyright from '../../../components/admin/copyright/Copyright';
import ContentProfile from './ContentProfile';
const PacketMedicalsStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;
const Profile = () => {
  return (
    <PacketMedicalsStyles>
      <Breadcrumbs
        title="Hồ sơ cá nhân"
        breadcrumbItems={[<Breadcrumb.Item active>Hồ sơ</Breadcrumb.Item>]}
      >
        {/* <ContentProfile></ContentProfile> */}
        <Copyright></Copyright>
      </Breadcrumbs>
    </PacketMedicalsStyles>
  );
};

export default Profile;
