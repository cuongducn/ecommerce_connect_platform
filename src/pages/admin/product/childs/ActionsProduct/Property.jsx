import { Trash } from '@rsuite/icons';
import { Button, Input } from 'rsuite';
import UploadImageV2 from '../../../../../components/admin/form/UploadImageV2';
import { v4 } from 'uuid';
import { formatNumber } from '../../../../../utils';

const mainPropertyDefault = {
  property_name: '',
  sub_property_name: '',
  list_properties: [
    {
      id: v4(),
      name: '',
      image_url: '',
      price: '',
      supplier_price: '',
      quantity_in_stock: '',
      product_sub_property: [],
    },
  ],
};

export default function Property({ property, setProperty }) {
  // console.log("🚀 ~ file: Property.jsx:24 ~ Property ~ property:", property)

  const handleAddMainProperty = () => {
    setProperty(mainPropertyDefault);
  };

  const addProperty = () => {
    const newProperty = cloneProperty(property);
    console.log(
      '🚀 ~ file: Property.jsx:75 ~ addProperty ~ newProperty:',
      newProperty
    );
    if (newProperty.list_properties?.length === 0) {
      newProperty.list_properties?.push({
        id: v4(),
        name: '',
        image_url: '',
        price: '',
        supplier_price: '',
        quantity_in_stock: '',
        product_sub_property: [],
      });
    } else {
      const newProductSubProperty =
        newProperty.list_properties?.[0].product_sub_property;
      newProperty.list_properties?.push({
        id: v4(),
        name: '',
        image_url: '',
        price: '',
        supplier_price: '',
        quantity_in_stock: '',
        product_sub_property: newProductSubProperty.map(
          (productSubProperty) => ({
            id: v4(),
            name: productSubProperty.name,
            image_url: '',
            price: '',
            supplier_price: '',
            quantity_in_stock: '',
          })
        ),
      });
    }

    setProperty(newProperty);
  };

  const addSubProperty = () => {
    const newProperty = cloneProperty(property);
    property.list_properties.forEach((element, index) => {
      newProperty.list_properties[index].product_sub_property.push({
        id: v4(),
        name: '',
        image_url: element.image_url,
        price: '',
        supplier_price: '',
        quantity_in_stock: '',
      });
    });

    setProperty(newProperty);
  };

  const handleChange = (value, e, index) => {
    const name = e.target.name;
    if (name === 'property_name') {
      setProperty((prevProperty) => ({ ...prevProperty, [name]: value }));
    } else {
      const _value =
        value !== '' &&
        (name === 'price' ||
          name === 'supplier_price' ||
          name === 'quantity_in_stock')
          ? formatNumber(value)
          : value;
      const newProperty = cloneProperty(property);
      newProperty.list_properties[index][name] = _value;

      setProperty(newProperty);
    }
  };

  const handleChangeFile = (file, index) => {
    const newProperty = cloneProperty(property);
    newProperty.list_properties[index].image_url = file;
    if (property.list_properties[index]?.product_sub_property?.length > 0) {
      property.list_properties[index].product_sub_property.forEach(
        (element, i) => {
          newProperty.list_properties[index].product_sub_property[i].image_url =
            file;
        }
      );
    }

    setProperty(newProperty);
  };

  const handleChangeSub = (value, e, index, indexParent) => {
    const name = e.target.name;
    if (name === 'sub_property_name') {
      setProperty((prevProperty) => ({ ...prevProperty, [name]: value }));
    } else {
      const newProperty = cloneProperty(property);

      if (indexParent === undefined) {
        property.list_properties?.forEach((element, i) => {
          newProperty.list_properties[i].product_sub_property[index][name] =
            value;
          newProperty.list_properties[i].price = '';
          newProperty.list_properties[i].supplier_price = '';
          newProperty.list_properties[i].quantity_in_stock = '';
        });
      } else {
        const _value =
          value !== '' &&
          (name === 'price' ||
            name === 'supplier_price' ||
            name === 'quantity_in_stock')
            ? formatNumber(value)
            : value;
        newProperty.list_properties[indexParent].product_sub_property[index][
          name
        ] = _value;
      }

      setProperty(newProperty);
    }
  };

  const handleAddSubProperty = () => {
    const newProperty = cloneProperty(property);
    property.list_properties.forEach((element, index) => {
      newProperty.list_properties[index].product_sub_property.push({
        id: v4(),
        name: '',
        image_url: element.image_url,
        price: '',
        supplier_price: '',
        quantity_in_stock: '',
      });
    });

    setProperty(newProperty);
  };

  const removeSubProperty = (index) => {
    const newProperty = cloneProperty(property);
    if (property.list_properties?.[0]?.product_sub_property?.length === 1) {
      newProperty.sub_property_name = '';
      property.list_properties.forEach((element, i) => {
        newProperty.list_properties[i] = {
          id: v4(),
          price: '',
          supplier_price: '',
          quantity_in_stock: '',
          product_sub_property: [],
          name: newProperty.list_properties[i].name,
          image_url: newProperty.list_properties[i].image_url,
        };
      });

      setProperty(newProperty);
    } else {
      property.list_properties.forEach((element, indexDefault) => {
        const newProductSubProperty = element.product_sub_property.filter(
          (propertySub, i) => i !== index
        );
        newProperty.list_properties[indexDefault].product_sub_property =
          newProductSubProperty;
      });

      setProperty(newProperty);
    }
  };

  const removeProperty = (index) => {
    if (property.list_properties?.length === 1) {
      setProperty();
    } else {
      const newProperty = cloneProperty(property);
      const newListProperties = property.list_properties?.filter(
        (property, i) => i !== index
      );
      newProperty.list_properties = newListProperties;

      setProperty(newProperty);
    }
  };

  const cloneProperty = (property) => {
    return JSON.parse(JSON.stringify(property));
  };

  return (
    <div className="p-4 rounded-xl bg-[#fff]">
      <div className="flex w-full py-2 border-b border-gray-200 font-semibold">
        <div className="w-[35%] pr-4">Tên phân loại chính</div>
        <div className="w-full flex">
          <div className="w-[50%] pr-4">Giá trị</div>
          <div className="w-[35%] pr-4">Hình ảnh</div>
          <div className="w-[15%]">Hành động</div>
        </div>
      </div>
      {property ? (
        <>
          <div className="flex w-full py-5">
            <div className="w-[35%] pr-4 h-[117px] flex items-center">
              <Input
                value={property.property_name || ''}
                name="property_name"
                placeholder="Nhập phân loại chính..."
                onChange={(value, e) => handleChange(value, e)}
              />
            </div>
            <div className="w-full">
              {property.list_properties?.length > 0
                ? property.list_properties?.map((p, index) => (
                    <div className="w-full flex items-center" key={p.id}>
                      <div className="w-[50%] pr-4">
                        {' '}
                        <Input
                          name="name"
                          placeholder="Nhập gía trị..."
                          value={p.name}
                          onChange={(value, e) => handleChange(value, e, index)}
                        />
                      </div>
                      <div className="w-[35%] pr-4">
                        <UploadImageV2
                          name="image_url"
                          image_url={p.image_url}
                          setFile={(file) => handleChangeFile(file, index)}
                          style={{
                            width: '100px',
                            height: '100px',
                            borderRadius: '10px',
                          }}
                        />
                      </div>
                      <div className="w-[15%]">
                        {' '}
                        <Button
                          color="red"
                          appearance="primary"
                          startIcon={<Trash />}
                          style={{
                            backgroundColor: '#d50101f5',
                          }}
                          onClick={() => removeProperty(index)}
                        >
                          Xóa
                        </Button>
                      </div>
                    </div>
                  ))
                : null}
              <div className="w-full flex items-center">
                <button
                  type="button"
                  className="text-gray-900 bg-gradient-to-r from-teal-200 to-lime-200 hover:bg-gradient-to-l hover:from-teal-200 hover:to-lime-200 focus:ring-4 focus:outline-none focus:ring-lime-200 dark:focus:ring-teal-700 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                  onClick={addProperty}
                >
                  Thêm thuộc tính
                </button>
              </div>
            </div>
          </div>
        </>
      ) : null}
      {!property ? (
        <button
          type="button"
          className="text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mt-3"
          onClick={handleAddMainProperty}
        >
          Thêm phân loại
        </button>
      ) : null}
      {property &&
      property.list_properties?.[0]?.product_sub_property?.length > 0 ? (
        <>
          <div className="flex w-full py-2 border-b border-gray-200">
            <div className="w-[35%] pr-4">Tên phân loại phụ</div>
          </div>
          <div className="flex w-full py-5">
            <div className="w-[35%] pr-4 h-[117px] flex items-start">
              <Input
                value={property.sub_property_name || ''}
                name="sub_property_name"
                placeholder="Nhập phân loại phụ..."
                onChange={(value, e) => handleChangeSub(value, e)}
              />
            </div>
            <div className="w-full flex flex-col gap-y-5">
              {property.list_properties[0]?.product_sub_property?.length > 0 &&
                property.list_properties[0]?.product_sub_property?.map(
                  (p, index) => (
                    <div className="w-full flex " key={p.id}>
                      <div className="w-[50%] pr-4">
                        {' '}
                        <Input
                          name="name"
                          placeholder="Nhập gía trị..."
                          value={p.name}
                          onChange={(value, e) =>
                            handleChangeSub(value, e, index)
                          }
                        />
                      </div>
                      <div className="w-[35%] pr-4"></div>
                      <div className="w-[15%]">
                        {' '}
                        <Button
                          color="red"
                          appearance="primary"
                          startIcon={<Trash />}
                          style={{
                            backgroundColor: '#d50101f5',
                          }}
                          onClick={() => removeSubProperty(index)}
                        >
                          Xóa
                        </Button>
                      </div>
                    </div>
                  )
                )}
              <div className="w-full flex items-center mt-5">
                <button
                  type="button"
                  className="text-gray-900 bg-gradient-to-r from-teal-200 to-lime-200 hover:bg-gradient-to-l hover:from-teal-200 hover:to-lime-200 focus:ring-4 focus:outline-none focus:ring-lime-200 dark:focus:ring-teal-700 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                  onClick={addSubProperty}
                >
                  Thêm thuộc tính
                </button>
              </div>
            </div>
          </div>
        </>
      ) : null}
      {property &&
      property.list_properties?.[0]?.product_sub_property?.length === 0 ? (
        <button
          type="button"
          className="text-white bg-gradient-to-r from-cyan-500 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-cyan-300 dark:focus:ring-cyan-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mt-3"
          onClick={handleAddSubProperty}
        >
          Thêm phân loại
        </button>
      ) : null}
      {property ? (
        <>
          <div className="mt-10">
            <h4>Danh sách thuộc tính sản phẩm</h4>
            <div className="flex w-full py-2 border-b border-gray-200 font-semibold">
              <div className="w-[25%] pr-4">Tên thuộc tính</div>
              <div className="w-[25%] pr-4">Giá bán lẻ</div>
              <div className="w-[25%] pr-4">Giá nhập</div>
              <div className="w-[25%]">Số lượng tồn kho</div>
            </div>
          </div>
          {property ? (
            <>
              <div className="flex w-full py-5 flex-col gap-y-3">
                {property.list_properties?.length > 0
                  ? property.list_properties?.map((p, index) => {
                      if (
                        p.name !== '' &&
                        p.name !== null &&
                        (p.product_sub_property?.length === 0 ||
                          (p.product_sub_property?.length > 0 &&
                            p.product_sub_property.some(
                              (productChild) =>
                                productChild.name !== '' &&
                                productChild.name !== null
                            ) === false))
                      ) {
                        return (
                          <div className="w-full flex items-center" key={p.id}>
                            <div className="w-[25%] pr-4">{p.name}</div>
                            <div className="w-[25%] pr-4">
                              {' '}
                              <Input
                                name="price"
                                placeholder="Nhập gía bán lẻ..."
                                value={p.price}
                                onChange={(value, e) =>
                                  handleChange(value, e, index)
                                }
                              />
                            </div>
                            <div className="w-[25%] pr-4">
                              {' '}
                              <Input
                                name="supplier_price"
                                placeholder="Nhập gía nhập..."
                                value={p.supplier_price}
                                onChange={(value, e) =>
                                  handleChange(value, e, index)
                                }
                              />
                            </div>
                            <div className="w-[25%] pr-4">
                              {' '}
                              <Input
                                name="quantity_in_stock"
                                placeholder="Nhập số lượng tồn kho..."
                                value={p.quantity_in_stock}
                                onChange={(value, e) =>
                                  handleChange(value, e, index)
                                }
                              />
                            </div>
                          </div>
                        );
                      } else if (p.name !== '' && p.name !== null) {
                        return p.product_sub_property.map(
                          (productChild, indexChild) => {
                            return (
                              <div
                                className="w-full flex items-center"
                                key={productChild.id}
                              >
                                <div className="w-[25%] pr-4">{`${p.name}${
                                  productChild.name
                                    ? `,${productChild.name}`
                                    : ''
                                }`}</div>
                                <div className="w-[25%] pr-4">
                                  {' '}
                                  <Input
                                    name="price"
                                    placeholder="Nhập gía bán lẻ..."
                                    value={productChild.price}
                                    onChange={(value, e) =>
                                      handleChangeSub(
                                        value,
                                        e,
                                        indexChild,
                                        index
                                      )
                                    }
                                  />
                                </div>
                                <div className="w-[25%] pr-4">
                                  {' '}
                                  <Input
                                    name="supplier_price"
                                    placeholder="Nhập gía nhập..."
                                    value={productChild.supplier_price}
                                    onChange={(value, e) =>
                                      handleChangeSub(
                                        value,
                                        e,
                                        indexChild,
                                        index
                                      )
                                    }
                                  />
                                </div>
                                <div className="w-[25%] pr-4">
                                  {' '}
                                  <Input
                                    name="quantity_in_stock"
                                    placeholder="Nhập số lượng tồn kho..."
                                    value={productChild.quantity_in_stock}
                                    onChange={(value, e) =>
                                      handleChangeSub(
                                        value,
                                        e,
                                        indexChild,
                                        index
                                      )
                                    }
                                  />
                                </div>
                              </div>
                            );
                          }
                        );
                      } else {
                        return <div key={p.id}></div>;
                      }
                    })
                  : null}
              </div>
            </>
          ) : null}
        </>
      ) : null}
    </div>
  );
}
