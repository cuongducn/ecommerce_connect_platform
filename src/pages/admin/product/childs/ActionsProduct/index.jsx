import { Breadcrumb } from "rsuite";
import styled from "styled-components";
import Breadcrumbs from "../../../../../components/admin/breadcrumb";
import Copyright from "../../../../../components/admin/copyright/Copyright";
import Content from "./Content";
import { useParams } from "react-router-dom";
import NavLink from "../../../../../components/admin/navLink/NavLink";

const ProductStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const ActionsProduct = () => {
  const params = useParams();
  const idProduct = params.id;
  return (
    <ProductStyles>
      <Breadcrumbs
        title={idProduct ? "Cập nhập sản phẩm" : "Thêm sản phẩm"}
        breadcrumbItems={[
          <Breadcrumb.Item to="/admin/products" as={NavLink}>
            Sản phẩm
          </Breadcrumb.Item>,
          <Breadcrumb.Item active>
            {idProduct ? "Cập nhật" : "Thêm"}
          </Breadcrumb.Item>,
        ]}
      >
        <Content></Content>
        <Copyright></Copyright>
      </Breadcrumbs>
    </ProductStyles>
  );
};
export default ActionsProduct;
