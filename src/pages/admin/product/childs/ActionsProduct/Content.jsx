import { CheckTreePicker, Form, Schema } from 'rsuite';
import SunEditorCustom from '../../../../../components/admin/editor';
import Input from '../../../../../components/admin/form/Input';
import { useEffect, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import {
  formatNumber,
  getQueryParams,
  randomString,
} from '../../../../../utils';
import RadioGroupCustom from '../../../../../components/admin/form/RadioGroup';
import ToggleCustom from '../../../../../components/admin/form/Toggle';
import { useDispatch, useSelector } from 'react-redux';
import * as categoryAction from '../../../../../actions/category';
import * as productAction from '../../../../../actions/admin/product';
import UploadListImage from '../../../../../components/form/UploadListImage';
import Property from './Property';

const { StringType } = Schema.Types;
const model = Schema.Model({
  name: StringType().isRequired('Tên sản phẩm bắt buộc nhập'),
});

export default function Content() {
  const params = useParams();
  const idProduct = params.id;

  const dispatch = useDispatch();
  const { categories } = useSelector((state) => state.categoryReducers);
  const { product } = useSelector((state) => state.productAdminReducers);

  const formRef = useRef();
  const navigate = useNavigate();
  const [formValue, setFormValue] = useState({
    name: '',
    sku: randomString(9),
    barcode: '',
    type_for: true,
    status: true,
    quantity_in_stock: '',
    price: '',
    image_link: [],
    description: '',
    categories: [],
    children_categories: [],
    properties: {},
  });

  const [property, setProperty] = useState();
  const [filesImage, setFilesImage] = useState([]);
  const [categoriesSelected, setCategoriesSelected] = useState([]);

  //Handle Category
  const handleShowListCategories = () => {
    if (Object.entries(categories)?.length > 0) {
      const newListCategories = [];
      for (var category of categories.data) {
        let newCategory = {};
        newCategory.label = category.name;
        newCategory.value = category.id;
        if (category.category_childrens?.length > 0) {
          newCategory.children = category.category_childrens?.map(
            (categoryChild) => ({
              label: categoryChild.name,
              value: categoryChild.id,
            })
          );
        } else {
          newCategory.children = [];
        }

        newListCategories.push(newCategory);
      }

      return newListCategories;
    }

    return [];
  };
  const handleSelectedCategories = (values) => {
    const new_category_ids = [];
    const new_category_child_ids = [];

    categories?.data.forEach((category) => {
      if (values?.includes(category.id)) {
        new_category_ids.push(category.id);
      }
      if (category.category_childrens?.length > 0) {
        category.category_childrens?.forEach((categoryChild) => {
          if (values?.includes(categoryChild.id)) {
            new_category_child_ids.push(categoryChild.id);
          }
        });
      }
    });

    setFormValue({
      ...formValue,
      categories: new_category_ids,
      children_categories: new_category_child_ids,
    });
    setCategoriesSelected(values);
  };

  // Handle Change Value Form
  const handleChangeValueForm = (values, e) => {
    const name = e.target.name;
    const value = e.target.value;

    setFormValue({
      ...values,
      [name]:
        name === 'price' || name === 'quantity_in_stock'
          ? formatNumber(value)
          : name === 'type_for'
          ? value === 'true'
          : name === 'status'
          ? values.status
          : value,
    });
  };

  // Handle Action
  const handleAction = () => {
    if (!formRef.current.check()) {
      return;
    }
    const newFormValue = {
      ...formValue,
      image_link: filesImage,
      status: formValue.status ? 2 : 1,
      price: formValue.price
        ? formValue.price?.toString().replace(/\./g, '')
        : '',
      quantity_in_stock: formValue.quantity_in_stock
        ? formValue.quantity_in_stock?.toString().replace(/\./g, '')
        : '',
      properties: handleParseProperty(property),
    };

    if (!idProduct) {
      dispatch(
        productAction.addProduct(newFormValue, `page=1`, () => {
          navigate('/admin/products');
        })
      );
    } else {
      const page = getQueryParams('page') || 1;
      const limit = getQueryParams('limit') || 20;
      const status = getQueryParams('status') || '';
      const search = getQueryParams('search') || '';
      const date_from = getQueryParams('date_from') || '';
      const date_to = getQueryParams('date_to') || '';
      const category_ids = getQueryParams('category_ids') || '';
      const category_children_ids =
        getQueryParams('category_children_ids') || '';
      const params = getParams(
        page,
        limit,
        status,
        search,
        date_from,
        date_to,
        category_ids,
        category_children_ids
      );
      dispatch(
        productAction.updateProduct(idProduct, newFormValue, params, () => {
          navigate(`/admin/products?${params}`);
        })
      );
    }
  };
  const handleParseProperty = (property) => {
    if (property) {
      const newProperty = JSON.parse(JSON.stringify(property));

      if (property.list_properties?.length > 0) {
        property.list_properties.forEach((element, i) => {
          newProperty.list_properties[i].price = element.price
            ? element.price?.toString()?.replace(/\./g, '')
            : 0;
          newProperty.list_properties[i].supplier_price = element.supplier_price
            ? element.supplier_price?.toString()?.replace(/\./g, '')
            : 0;
          newProperty.list_properties[i].quantity_in_stock =
            element.quantity_in_stock
              ? element.quantity_in_stock?.toString()?.replace(/\./g, '')
              : 0;
          if (element.product_sub_property?.length > 0) {
            element.product_sub_property.forEach((elementChild, indexChild) => {
              newProperty.list_properties[i].product_sub_property[
                indexChild
              ].price = elementChild.price
                ? elementChild.price?.toString()?.replace(/\./g, '')
                : 0;
              newProperty.list_properties[i].product_sub_property[
                indexChild
              ].supplier_price = elementChild.supplier_price
                ? elementChild.supplier_price?.toString()?.replace(/\./g, '')
                : 0;
              newProperty.list_properties[i].product_sub_property[
                indexChild
              ].quantity_in_stock = elementChild.quantity_in_stock
                ? elementChild.quantity_in_stock?.toString()?.replace(/\./g, '')
                : 0;
            });
          }
        });
      }

      return newProperty;
    }
    return {};
  };
  const handleConvertProperty = (product) => {
    if (product) {
      const newProperty = JSON.parse(JSON.stringify(product));
      newProperty.status = product.status === 2;
      newProperty.sku = product.sku ? product.sku : '';
      newProperty.barcode = product.barcode ? product.barcode : '';
      newProperty.price = product.price ? formatNumber(product.price) : '';
      newProperty.quantity_in_stock = product.quantity_in_stock
        ? formatNumber(product.quantity_in_stock)
        : '';
      const categories = [];
      const category_childrens = [];

      product.categories?.forEach((category) => {
        categories.push(category.id);

        if (category.category_childrens?.length > 0) {
          category.category_childrens?.forEach((categoryChild) => {
            category_childrens.push(categoryChild.id);
          });
        }
      });
      newProperty.categories = categories;
      newProperty.children_categories = category_childrens;
      setCategoriesSelected(categories.concat(category_childrens));
      if (product.properties?.list_properties?.length > 0) {
        product.properties?.list_properties.forEach((element, i) => {
          newProperty.properties.list_properties[i].price = element.price
            ? formatNumber(element.price)
            : '';
          newProperty.properties.list_properties[i].supplier_price =
            element.supplier_price ? formatNumber(element.supplier_price) : '';
          newProperty.properties.list_properties[i].quantity_in_stock =
            element.quantity_in_stock
              ? formatNumber(element.quantity_in_stock)
              : '';
          if (element.product_sub_property?.length > 0) {
            element.product_sub_property.forEach((elementChild, indexChild) => {
              newProperty.properties.list_properties[i].product_sub_property[
                indexChild
              ].price = elementChild.price
                ? formatNumber(elementChild.price)
                : '';
              newProperty.properties.list_properties[i].product_sub_property[
                indexChild
              ].supplier_price = elementChild.supplier_price
                ? formatNumber(elementChild.supplier_price)
                : '';
              newProperty.properties.list_properties[i].product_sub_property[
                indexChild
              ].quantity_in_stock = elementChild.quantity_in_stock
                ? formatNumber(elementChild.quantity_in_stock)
                : '';
            });
          }
        });
      }

      setProperty(newProperty.properties);
      return newProperty;
    }
    return {};
  };
  const getParams = (
    page,
    limit,
    status,
    search,
    date_from,
    date_to,
    category_ids,
    category_children_ids
  ) => {
    let params = '';
    if (page) {
      params += `page=${page}`;
    }
    if (limit) {
      params += `&limit=${limit}`;
    }
    if (status !== '' && status !== null) {
      params += `&status=${status}`;
    }
    if (search !== '') {
      params += `&search=${search}`;
    }
    if (date_from !== '') {
      params += `&date_from=${date_from}`;
    }
    if (date_to !== '') {
      params += `&date_to=${date_to}`;
    }
    if (category_ids !== '') {
      params += `&category_ids=${category_ids}`;
    }
    if (category_children_ids !== '') {
      params += `&category_children_ids=${category_children_ids}`;
    }
    return params;
  };

  //Get Data Detail
  useEffect(() => {
    if (Object.entries(product).length === 0 || !idProduct) return;

    setFormValue((prevForm) => ({
      ...prevForm,
      ...handleConvertProperty(product),
    }));
    const newProperty = JSON.parse(JSON.stringify(product));
    setFilesImage(newProperty.image_link ?? []);
  }, [idProduct, product]);

  // Call API Get Category
  useEffect(() => {
    dispatch(categoryAction.getCategories());
  }, [dispatch]);

  // Call API Get Detail Product
  useEffect(() => {
    if (!idProduct) {
      return;
    }
    dispatch(productAction.getOneProduct(idProduct));
  }, [dispatch, idProduct]);
  return (
    <div>
      <Form
        fluid
        ref={formRef}
        onChange={handleChangeValueForm}
        formValue={formValue}
        model={model}
      >
        <div className="flex flex-col  gap-y-5">
          <div className="grid grid-cols-2 gap-x-5 p-4 rounded-xl bg-[#fff]">
            <Input
              name="name"
              label="Tên sản phẩm"
              placeholder="Nhập tên sản phẩm..."
            />
            <Input
              name="barcode"
              label="Barcode"
              placeholder="Nhập barcode..."
            />
            <Input name="sku" label="Mã SKU" placeholder="Nhập mã sku..." />
            <Input
              name="quantity_in_stock"
              label="Số lượng tồn kho"
              placeholder="Nhập số lượng tồn kho..."
            />
            <Input
              name="price"
              label="Giá bán lẻ"
              placeholder="Nhập giá bán lẻ..."
            />{' '}
            <div className="flex flex-col gap-y-1">
              <label htmlFor="">Chọn thể loại</label>
              <CheckTreePicker
                className="w-full block"
                value={categoriesSelected}
                size="md"
                placeholder="Chọn thể loại..."
                data={
                  Object.entries(categories)?.length > 0
                    ? handleShowListCategories()
                    : []
                }
                onChange={handleSelectedCategories}
              />
            </div>
            <RadioGroupCustom
              label="Dành cho"
              name="type_for"
              options={[
                { value: true, label: 'Nam' },
                { value: false, label: 'Nữ' },
              ]}
            ></RadioGroupCustom>
            <ToggleCustom name="status" label="Ẩn/Hiện sản phẩm" />
            <div className="flex flex-col gap-y-4">
              <label htmlFor="">Ảnh sản phẩm (Tối đa 10 hình)</label>
              <UploadListImage
                style={{
                  width: '120px',
                  height: '120px',
                }}
                images={formValue.image_link}
                setFiles={setFilesImage}
              ></UploadListImage>
            </div>
          </div>
          <div className="p-4 rounded-xl bg-[#fff]">
            <div className="flex gap-x-5">
              <button
                type="button"
                className="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center flex"
                onClick={handleAction}
              >
                {idProduct ? (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-5 h-5"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                    />
                  </svg>
                ) : (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-5 h-5"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M12 6v12m6-6H6"
                    />
                  </svg>
                )}
                {idProduct ? 'Cập nhật sản phẩm' : 'Tạo sản phẩm'}
              </button>
              <button
                type="button"
                className="text-gray-900 bg-gradient-to-r from-lime-200 via-lime-400 to-lime-500 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-lime-300 dark:focus:ring-lime-800 shadow-lg shadow-lime-500/50 dark:shadow-lg dark:shadow-lime-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center flex"
                onClick={() => navigate(-1)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-5 h-5"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19.5 12h-15m0 0l6.75 6.75M4.5 12l6.75-6.75"
                  />
                </svg>
                Quay lại
              </button>
            </div>
          </div>
          <Property setProperty={setProperty} property={property} />

          <div className="p-4 rounded-xl bg-[#fff]">
            <SunEditorCustom
              name="description"
              title="Mô tả sản phẩm"
              setFormValue={setFormValue}
              formValue={formValue}
            ></SunEditorCustom>
          </div>
          <div className="p-4 rounded-xl bg-[#fff]">
            <div className="flex gap-x-5">
              <button
                type="button"
                className="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 shadow-lg shadow-blue-500/50 dark:shadow-lg dark:shadow-blue-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center flex"
                onClick={handleAction}
              >
                {idProduct ? (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-5 h-5"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                    />
                  </svg>
                ) : (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-5 h-5"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M12 6v12m6-6H6"
                    />
                  </svg>
                )}

                {idProduct ? 'Cập nhật sản phẩm' : 'Tạo sản phẩm'}
              </button>
              <button
                type="button"
                className="text-gray-900 bg-gradient-to-r from-lime-200 via-lime-400 to-lime-500 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-lime-300 dark:focus:ring-lime-800 shadow-lg shadow-lime-500/50 dark:shadow-lg dark:shadow-lime-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center flex"
                onClick={() => navigate(-1)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="w-5 h-5"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M19.5 12h-15m0 0l6.75 6.75M4.5 12l6.75-6.75"
                  />
                </svg>
                Quay lại
              </button>
            </div>
          </div>
        </div>
      </Form>
    </div>
  );
}
