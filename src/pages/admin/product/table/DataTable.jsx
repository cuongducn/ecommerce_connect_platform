import {
  Table,
  Pagination,
  InputGroup,
  Input,
  Stack,
  InputPicker,
  Checkbox,
  IconButton,
  Button,
  CheckTreePicker,
  DateRangePicker,
} from 'rsuite';
import { useCallback, useEffect, useState } from 'react';
import ImageCell from '../../../../components/admin/table/ImageCell';
import { Search, Check, Close, Edit, Trash } from '@rsuite/icons';
import { useDispatch, useSelector } from 'react-redux';
import ToggleLoading from '../../../../components/admin/button/ToggleLoading';
import * as productAction from '../../../../actions/admin/product';
import * as categoryAction from '../../../../actions/category';
import { formatNumber } from '../../../../utils';
import styled from 'styled-components';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { debounce } from 'lodash';
import * as Types from '../../../../constants/actionType';
import CheckCell from '../../../../components/admin/table/CheckCell';
import ModalCustom from '../../../../components/admin/modal';
import { formatDateObj, formatDateStr } from '../../../../utils/date';
const { Column, HeaderCell, Cell } = Table;
const rowKey = 'id';
const statusCensorship = [
  {
    label: 'Đã được duyệt',
    value: Types.STATUS_COMPLETED,
  },
  {
    label: 'Đã ẩn',
    value: Types.STATUS_CANCEL,
  },
];
const DataTableStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding-top: 20px;
  border-radius: 10px;
  .header {
    display: flex;
    justify-content: flex-end;
    margin-bottom: 20px;
    padding: 0 20px;
    .input__search-content {
      width: 250px;
    }
  }
  @media only screen and (max-width: 768px) {
    .header {
      .input__search-content {
        width: 190px;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .header {
      flex-direction: column;
      row-gap: 10px;
      padding: 0 20px;
      .rs-stack-item {
        width: 100%;
        overflow: visible !important;
        .input__search-content {
          width: 100%;
        }
        .rs-picker {
          margin-right: 0 !important;
          width: 100% !important;
        }
      }
    }
  }
`;
const DataTable = () => {
  const dispatch = useDispatch();
  const { isLoadingProductAdmin } = useSelector(
    (state) => state.loadingReducers
  );
  const { listProducts } = useSelector((state) => state.productAdminReducers);
  const { categories } = useSelector((state) => state.categoryReducers);
  const { data } = listProducts;
  const [productSelected, setProductSelected] = useState({});
  const [openModalDeleteListProduct, setOpenModalDeleteListProduct] =
    useState(false);
  const [categoriesSelected, setCategoriesSelected] = useState([]);
  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get('page')) || 1,
    limit: Number(searchParams.get('limit')) || 20,
    status: Number(searchParams.get('status')) || '',
    search: searchParams.get('search') || '',
    date_from: searchParams.get('date_from') || '',
    date_to: searchParams.get('date_to') || '',
    category_ids: searchParams.get('category_ids') || '',
    category_children_ids: searchParams.get('category_children_ids') || '',
    searchInput: searchParams.get('search') || '',
  });
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const navigate = useNavigate();

  // Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        const queries = getParams(
          1,
          prevParams.limit,
          prevParams.status,
          valueSearch,
          prevParams.date_from,
          prevParams.date_to,
          prevParams.category_ids,
          prevParams.category_children_ids
        );
        navigate(`/admin/products?${queries}`);
        return {
          ...prevParams,
          page: 1,
          search: valueSearch,
          searchInput: valueSearch,
        };
      });
    }, 500),
    []
  );
  const handleSearchProducts = (e) => {
    const value = e;
    setParams({
      ...params,
      searchInput: value,
    });
    debounceCallBack(value);
  };

  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === 'string') {
          x = x.charCodeAt();
        }
        if (typeof y === 'string') {
          y = y.charCodeAt();
        }
        if (sortData.type === 'asc') {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    let dataIndex = [];
    if (data?.length > 0) {
      dataIndex = data.map((item, index) => {
        item.index =
          (Number(listProducts.current_page) - 1) *
            Number(listProducts.per_page) +
          index +
          1;
        return item;
      });
    }
    return dataIndex;
  };
  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };

  // Handle change params
  const handleChangePage = (pageChange) => {
    const queries = getParams(
      pageChange,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      params.category_ids,
      params.category_children_ids
    );
    navigate(`/admin/products?${queries}`);
    setParams({
      ...params,
      page: pageChange,
    });
  };

  //Handle Consorship clinic
  const handleConsorshipClinic = (idClinic, isChecked) => {
    const data = {
      status: isChecked ? Types.STATUS_COMPLETED : Types.STATUS_CANCEL,
    };

    dispatch(productAction.updateStatusProduct(idClinic, data));
  };

  //Handle Change Status
  const handleChangeStatus = (value) => {
    const queries = getParams(
      1,
      params.limit,
      value,
      params.search,
      params.date_from,
      params.date_to,
      params.category_ids,
      params.category_children_ids
    );

    navigate(`/admin/products?${queries}`);
    setParams({
      ...params,
      page: 1,
      status:
        value === Types.STATUS_COMPLETED
          ? Types.STATUS_COMPLETED
          : value === Types.STATUS_CANCEL
          ? Types.STATUS_CANCEL
          : '',
    });
  };

  //Handle Check Box
  const [checkedKeys, setCheckedKeys] = useState([]);

  let checked = false;
  let indeterminate = false;

  if (checkedKeys.length === data?.length) {
    checked = true;
  } else if (checkedKeys.length === 0) {
    checked = false;
  } else if (checkedKeys.length > 0 && checkedKeys.length < data?.length) {
    indeterminate = true;
  }

  const handleCheckAll = (value, checked) => {
    const keys = checked ? data.map((item) => item.id) : [];
    setCheckedKeys(keys);
  };
  const handleCheck = (value, checked) => {
    const keys = checked
      ? [...checkedKeys, value]
      : checkedKeys.filter((item) => item !== value);
    setCheckedKeys(keys);
  };

  //Handle Actions
  const ActionCell = ({ rowData, dataKey, ...props }) => {
    const [open, setOpen] = useState(false);
    const handleClose = () => setOpen(false);

    const handleDeletePacketMedical = () => {
      const data = {
        product_ids: [rowData[dataKey]],
      };

      const queryString = getParams(
        params.page,
        params.limit,
        params.status,
        params.search,
        params.date_from,
        params.date_to,
        params.category_ids,
        params.category_children_ids
      );
      dispatch(productAction.deleteProduct(data, queryString));
    };
    return (
      <Cell {...props} style={{ padding: 5 }} className="link-group">
        <div className="btn__actions">
          <IconButton
            appearance="subtle"
            onClick={() =>
              navigate(
                `/admin/products/update/${rowData[dataKey]}?${getParams(
                  params.page,
                  params.limit,
                  params.status,
                  params.search,
                  params.date_from,
                  params.date_to,
                  params.category_ids,
                  params.category_children_ids
                )}`
              )
            }
            icon={<Edit />}
          />
          <IconButton
            appearance="subtle"
            onClick={() => setOpen(true)}
            icon={<Trash />}
          />
        </div>

        <ModalCustom
          size="sm"
          title="Thông báo"
          open={open}
          nameAction="Xóa"
          handleClose={handleClose}
          handleAction={handleDeletePacketMedical}
        >
          Bạn có chắc chắn muốn xóa sản phẩm này không?
        </ModalCustom>
      </Cell>
    );
  };

  const handleRemoveListProduct = () => {
    const data = {
      product_ids: checkedKeys,
    };

    const queryString = getParams(
      params.page,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      params.category_ids,
      params.category_children_ids
    );
    dispatch(
      productAction.deleteProduct(data, queryString, () => {
        setCheckedKeys([]);
        setOpenModalDeleteListProduct(false);
      })
    );
  };

  const getParams = (
    page,
    limit,
    status,
    search,
    date_from,
    date_to,
    category_ids,
    category_children_ids
  ) => {
    let params = '';
    if (page) {
      params += `page=${page}`;
    }
    if (limit) {
      params += `&limit=${limit}`;
    }
    if (status !== '' && status !== null) {
      params += `&status=${status}`;
    }
    if (search !== '') {
      params += `&search=${search}`;
    }
    if (date_from !== '') {
      params += `&date_from=${date_from}`;
    }
    if (date_to !== '') {
      params += `&date_to=${date_to}`;
    }
    if (category_ids !== '') {
      params += `&category_ids=${category_ids}`;
    }
    if (category_children_ids !== '') {
      params += `&category_children_ids=${category_children_ids}`;
    }
    return params;
  };

  //Handle Category
  const handleShowListCategories = () => {
    if (Object.entries(categories)?.length > 0) {
      const newListCategories = [];
      for (var category of categories.data) {
        let newCategory = {};
        newCategory.label = category.name;
        newCategory.value = category.id;
        if (category.category_childrens?.length > 0) {
          newCategory.children = category.category_childrens?.map(
            (categoryChild) => ({
              label: categoryChild.name,
              value: categoryChild.id,
            })
          );
        } else {
          newCategory.children = [];
        }

        newListCategories.push(newCategory);
      }

      return newListCategories;
    }

    return [];
  };
  const handleSelectedCategories = (values) => {
    const new_category_ids = [];
    const new_category_child_ids = [];

    categories?.data.forEach((category) => {
      if (values?.includes(category.id)) {
        new_category_ids.push(category.id);
      }
      if (category.category_childrens?.length > 0) {
        category.category_childrens?.forEach((categoryChild) => {
          if (values?.includes(categoryChild.id)) {
            new_category_child_ids.push(categoryChild.id);
          }
        });
      }
    });

    const queries = getParams(
      1,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      new_category_ids.join(','),
      new_category_child_ids.join(',')
    );
    navigate(`/admin/products?${queries}`);
    setParams({
      ...params,
      page: 1,
      category_ids: new_category_ids.join(','),
      category_children_ids: new_category_child_ids.join(','),
    });
    setCategoriesSelected(values);
  };

  //Handle Date
  const handleSelectedDate = (values) => {
    if (values) {
      setParams({
        ...params,
        page: 1,
        date_from: formatDateStr(values[0], 'YYYY-MM-DD'),
        date_to: formatDateStr(values[1], 'YYYY-MM-DD'),
      });
    } else {
      setParams({
        ...params,
        date_from: '',
        date_to: '',
      });
    }
    const queries = getParams(
      1,
      params.limit,
      params.status,
      params.search,
      values ? formatDateStr(values[0], 'YYYY-MM-DD') : '',
      values ? formatDateStr(values[1], 'YYYY-MM-DD') : '',
      params.category_ids,
      params.category_children_ids
    );
    navigate(`/admin/products?${queries}`);
  };

  useEffect(() => {
    const newCategoryIds = searchParams.get('category_ids') || '';
    const newcategoryChildIds = searchParams.get('category_children_ids') || '';
    const newCategorySelected =
      newCategoryIds?.split(',')?.concat(newcategoryChildIds?.split(',')) || [];

    setCategoriesSelected(
      newCategorySelected
        .filter((element) => element !== '')
        ?.map((e) => Number(e))
    );
  }, []);
  // Call API Get listProducts
  useEffect(() => {
    const queryString = getParams(
      params.page,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      params.category_ids,
      params.category_children_ids
    );
    dispatch(productAction.getListProducts(queryString));
  }, [
    dispatch,
    params.page,
    params.limit,
    params.search,
    params.status,
    params.date_from,
    params.date_to,
    params.category_ids,
    params.category_children_ids,
  ]);

  // Call API Get Category
  useEffect(() => {
    dispatch(categoryAction.getCategories());
  }, [dispatch]);

  return (
    <DataTableStyles>
      <Stack
        style={{
          margin: '20px',
        }}
      >
        <Button
          className="btnAdd__product"
          appearance="primary"
          style={{
            backgroundColor: '#26a69a',
          }}
          onClick={() => navigate('/admin/products/add')}
        >
          Thêm sản phẩm
        </Button>
      </Stack>
      <Stack className="header" justifyContent="space-between" spacing={20}>
        <Stack spacing={20}>
          <InputGroup inside className="input__search-content">
            <Input
              placeholder="Tìm kiếm sản phẩm..."
              onChange={handleSearchProducts}
              value={params.searchInput}
            />
            <InputGroup.Addon>
              <Search />
            </InputGroup.Addon>
          </InputGroup>
          <CheckTreePicker
            value={categoriesSelected}
            size="md"
            placeholder="Chọn thể loại..."
            data={
              Object.entries(categories)?.length > 0
                ? handleShowListCategories()
                : []
            }
            style={{ width: 250, display: 'block' }}
            onChange={handleSelectedCategories}
          />
          <DateRangePicker
            value={
              params.date_from && params.date_to
                ? [
                    formatDateObj(params.date_from),
                    formatDateObj(params.date_to),
                  ]
                : []
            }
            format="yyyy-MM-dd"
            ranges={[]}
            style={{ width: 230 }}
            onChange={handleSelectedDate}
          />
        </Stack>
        <InputPicker
          value={params.status}
          data={statusCensorship}
          style={{ width: 170 }}
          placeholder="Trạng thái: Tất cả"
          onChange={handleChangeStatus}
        />
      </Stack>

      {checkedKeys?.length > 0 && (
        <Stack
          justifyContent="flex-start"
          style={{
            marginLeft: '20px',
            marginBottom: '20px',
          }}
        >
          <Button
            color="red"
            appearance="primary"
            startIcon={<Trash />}
            style={{
              backgroundColor: '#d50101f5',
            }}
            onClick={() => setOpenModalDeleteListProduct(true)}
          >
            Xóa {checkedKeys?.length} sản phẩm
          </Button>
          <ModalCustom
            size="sm"
            title="Thông báo"
            open={openModalDeleteListProduct}
            nameAction="Xóa"
            handleClose={() => setOpenModalDeleteListProduct(false)}
            handleAction={handleRemoveListProduct}
          >
            Bạn có chắc chắn muốn xóa {checkedKeys.length} sản phẩm này không?
          </ModalCustom>
        </Stack>
      )}
      <Table
        autoHeight
        rowKey={rowKey}
        hover={false}
        data={getData()}
        sortColumn={sortData.column}
        sortType={sortData.type}
        onSortColumn={handleSortColumn}
        loading={sortData.loading}
      >
        <Column width={50} align="center">
          <HeaderCell style={{ padding: 0 }}>
            <div style={{ lineHeight: '40px' }}>
              <Checkbox
                inline
                checked={checked}
                indeterminate={indeterminate}
                onChange={handleCheckAll}
              />
            </div>
          </HeaderCell>
          <CheckCell
            dataKey="id"
            checkedKeys={checkedKeys}
            onChange={handleCheck}
          />
        </Column>
        <Column align="center" width={50}>
          <HeaderCell>STT</HeaderCell>
          <Cell dataKey="index" />
        </Column>
        <Column align="center" width={70}>
          <HeaderCell>Hình ảnh</HeaderCell>
          <ImageCell
            dataKey="image_link"
            styleImage={{
              width: '40px',
              height: '40px',
            }}
          />
        </Column>
        <Column width={150}>
          <HeaderCell>Mã SKU</HeaderCell>
          <Cell>{(rowData) => rowData.sku}</Cell>
        </Column>
        <Column flexGrow={1} sortable>
          <HeaderCell>Tên sản phẩm</HeaderCell>
          <Cell>{(rowData) => rowData.name}</Cell>
        </Column>
        <Column width={150}>
          <HeaderCell>Giá bán lẻ</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData.price ? `${formatNumber(rowData.price)}đ` : '0đ'
            }
          </Cell>
        </Column>
        <Column width={170}>
          <HeaderCell>Số lượng tồn kho</HeaderCell>
          <Cell>
            {(rowData) =>
              rowData.quantity_in_stock
                ? formatNumber(rowData.quantity_in_stock)
                : 0
            }
          </Cell>
        </Column>
        <Column width={150}>
          <HeaderCell>Lượt thích</HeaderCell>
          <Cell>
            {(rowData) => (rowData.like ? formatNumber(rowData.like) : 0)}
          </Cell>
        </Column>
        <Column width={120}>
          <HeaderCell>Trạng thái</HeaderCell>
          <Cell>
            {(rowData) => (
              <ToggleLoading
                isLoading={isLoadingProductAdmin}
                status={rowData['status']}
                data={rowData}
                dataSelected={productSelected}
                setDataSelected={setProductSelected}
                handleAction={handleConsorshipClinic}
              />
            )}
          </Cell>
        </Column>
        <Column width={140} align="center">
          <HeaderCell>Hành động</HeaderCell>
          <ActionCell dataKey="id" />
        </Column>
      </Table>
      <div style={{ padding: 20 }}>
        <Pagination
          prev
          next
          first
          last
          ellipsis
          boundaryLinks
          maxButtons={5}
          size="xs"
          layout={['total', '-', 'pager', 'skip']}
          total={listProducts.total}
          limitOptions={[10, 30, 50]}
          limit={params.limit}
          activePage={params.page}
          onChangePage={handleChangePage}
        />
      </div>
    </DataTableStyles>
  );
};

export default DataTable;
