import { Breadcrumb } from "rsuite";
import styled from "styled-components";
import Breadcrumbs from "../../../components/admin/breadcrumb";
import Copyright from "../../../components/admin/copyright/Copyright";
import DataTable from "./table/DataTable";

const ProductStyles = styled.div`
  width: 100%;
  background-color: #f5f8fa;
`;

const Product = () => {
  return (
    <ProductStyles>
      <Breadcrumbs
        title="Sản phẩm"
        breadcrumbItems={[<Breadcrumb.Item active>Sản phẩm</Breadcrumb.Item>]}
      >
        <DataTable></DataTable>
        <Copyright></Copyright>
      </Breadcrumbs>
    </ProductStyles>
  );
};
export default Product;
