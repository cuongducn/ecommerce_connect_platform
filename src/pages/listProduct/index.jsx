import { FilterOutlined } from '@ant-design/icons';
import { Select } from 'antd';
import React from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import * as categoryActions from '../../actions/category';
import * as productActions from '../../actions/product';
import ProductCard from '../../components/product/ProductCard';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useSearchParams } from 'react-router-dom';
import { useRef } from 'react';
import lodash from 'lodash';

const ProductStyles = styled.div`
  .select-option:hover {
    color: white;
    font-weight: 500;
  }
`;

const sortOption = [
  { value: 'price', label: 'Giá cao nhất' },
  { value: 'price', label: 'Giá thấp' },
];

const ListProduct = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);
  const [hasMore, setHasMore] = useState(true);
  const [listProduct, setListProduct] = useState([]);
  const [categoriesSelected, setCategoriesSelected] = useState([]);
  const { categories } = useSelector((state) => state.categoryReducers);
  const { products } = useSelector((state) => state.productReducers);
  const listProductsRef = useRef();
  const [searchParams] = useSearchParams();
  const [params, setParams] = useState({
    page: Number(searchParams.get('page')) || 1,
    limit: Number(searchParams.get('limit')) || 10,
    status: Number(searchParams.get('status')) || '',
    search: searchParams.get('search') || '',
    date_from: searchParams.get('date_from') || '',
    date_to: searchParams.get('date_to') || '',
    category_ids: searchParams.get('category_ids') || '',
    category_children_ids: searchParams.get('category_children_ids') || '',
    searchInput: searchParams.get('search') || '',
  });

  const handleOptionCategory = (data) => {
    if (data?.data) {
      const newData = data?.data.reduce((prevData, currentData) => {
        return [
          ...prevData,
          {
            value: currentData.id,
            label: currentData.name,
          },
        ];
      }, []);

      return newData;
    }
  };

  const handleFetchProduct = () => {
    if (products?.data?.length >= products?.total) {
      setHasMore(false);
      return;
    }
    setPage((pag) => pag + 1);
    setParams((params) => {
      return {
        ...params,
        page: params.page + 1,
      };
    });
  };

  useEffect(() => {
    if (
      Object.entries(products).length > 0 &&
      !lodash.isEqual(listProductsRef.current, products)
    ) {
      setListProduct((prevMessages) => [...prevMessages, ...products?.data]);
      listProductsRef.current = products;
    }
  }, [dispatch, products, listProduct.length]);

  const handleSelectedCategories = (values) => {
    const new_category_ids = [];
    const new_category_child_ids = [];

    categories?.data.forEach((category) => {
      if (values?.includes(category.id)) {
        new_category_ids.push(category.id);
      }
      if (category.category_childrens?.length > 0) {
        category.category_childrens?.forEach((categoryChild) => {
          if (values?.includes(categoryChild.id)) {
            new_category_child_ids.push(categoryChild.id);
          }
        });
      }
    });

    const queries = getParams(
      1,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      new_category_ids.join(','),
      new_category_child_ids.join(',')
    );
    // navigate(`/customer/products?${queries}`);
    setParams({
      ...params,
      page: 1,
      category_ids: new_category_ids.join(','),
      category_children_ids: new_category_child_ids.join(','),
    });
    setCategoriesSelected(values);
  };

  const getParams = (
    page,
    limit,
    status,
    search,
    date_from,
    date_to,
    category_ids,
    category_children_ids
  ) => {
    let params = '';
    if (page) {
      params += `page=${page}`;
    }
    if (limit) {
      params += `&limit=${limit}`;
    }
    if (status !== '' && status !== null) {
      params += `&status=${status}`;
    }
    if (search !== '') {
      params += `&search=${search}`;
    }
    if (date_from !== '') {
      params += `&date_from=${date_from}`;
    }
    if (date_to !== '') {
      params += `&date_to=${date_to}`;
    }
    if (category_ids !== '') {
      params += `&category_ids=${category_ids}`;
    }
    if (category_children_ids !== '') {
      params += `&category_children_ids=${category_children_ids}`;
    }
    return params;
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    dispatch(categoryActions.getCategories());
  }, [dispatch]);

  useEffect(() => {
    const queryString = getParams(
      params.page,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      params.category_ids,
      params.category_children_ids
    );
    dispatch(productActions.getProducts(queryString));
  }, [
    dispatch,
    params.page,
    params.limit,
    params.search,
    params.status,
    params.date_from,
    params.date_to,
    params.category_ids,
    params.category_children_ids,
    // products,
  ]);

  return (
    <ProductStyles>
      <div className="mt-[20px] mx-[80px]">
        <h2 className="text-[1.3rem] font-bold">FOR HIM</h2>
        <div className="flex justify-between">
          <Select
            options={handleOptionCategory(categories)}
            onChange={handleSelectedCategories}
            value={categoriesSelected}
            mode="multiple"
            allowClear
            placeholder="Danh mục"
            className="text-[black] text-[1rem] select-option"
            maxTagCount={'responsive'}
            style={{
              fontSize: '1rem',
              fontWeight: '600',
              color: '#000000',
              width: '280px',
              margin: '20px 0',
            }}
            size="large"
          />
          <Select
            placeholder="Lọc"
            allowClear
            options={sortOption}
            className="text-[black] text-[1rem] select-option"
            suffixIcon={<FilterOutlined />}
            style={{
              fontSize: '1rem',
              fontWeight: '600',
              color: '#000000',
              width: '150px',
              margin: '20px 0',
            }}
            size="large"
          />
        </div>
      </div>
      <div className="mt-[5] p-[10px]">
        {products?.total > 0 ? (
          <InfiniteScroll
            dataLength={listProduct?.length}
            next={handleFetchProduct}
            className="item-group grid grid-cols-2 sm:grid-cols-4 gap-4"
            hasMore={hasMore}
            inverse={false}
            key={'id'}
            endMessage={<p>No more products to load.</p>}
          >
            {listProduct?.map((product) => (
              <div
                onClick={() => navigate(`/product/${product.slug}`)}
                key={product.id}
              >
                <ProductCard product={product} />
              </div>
            ))}
          </InfiniteScroll>
        ) : null}
      </div>
    </ProductStyles>
  );
};

export default ListProduct;
