import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as blogActions from '../../actions/blog';
import { useEffect } from 'react';
import { Button } from 'antd';
import { formatDateStr } from '../../utils/date';

const Blog = () => {
  const dispatch = useDispatch();

  const { blogs } = useSelector((state) => state.cusBlogsReducers);

  useEffect(() => {
    dispatch(blogActions.getBlogs());
  }, [dispatch]);

  return (
    <div>
      <div className="px-[120px]">
        <h1 className=" text-[1.4rem] uppercase mt-2 sm:mt-5 ">
          BLOG
          {/* <span className="text-[.8rem]">- Đơn giản mà đẹp</span> */}
        </h1>
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 sm:gap-10 my-[50px] px-[80px]">
        {blogs?.data?.map((blog, idx) => (
          <div key={idx}>
            <a href={'/blog/' + blog.slug} className="mt-4">
              {/* <video className="mx-auto">
                <source
                  className="portrait relative sm:landscape group "
                  src="https://data3gohomy.ikitech.vn/videos/SHUVideos/ANOTHER_FILES_FOLDER/ZGsSLdrMIA1685417568.mp4"
                  type="video/mp4"
                />
                <div className="group-hover:grid transition-all bg-grayOverlay absolute w-full h-full left-0 top-0 hidden place-content-center text-white">
                  <div className="flex justify-center mt-10"></div>
                  <Button
                    size="large"
                    className=" border-2 font-bold uppercase text-white border-white hover:text-white hover:border-white"
                  >
                    Xem thêm
                  </Button>
                </div>
              </video> */}
              <div
                className="portrait relative sm:landscape group"
                style={{
                  backgroundImage: `url(${blog.image})`,
                }}
              >
                <div className="group-hover:grid transition-all bg-grayOverlay absolute w-full h-full left-0 top-0 hidden place-content-center text-white">
                  <div className="flex justify-center mt-10"></div>
                  <Button
                    size="large"
                    className=" border-2 font-bold uppercase text-white border-white hover:text-white hover:border-white"
                  >
                    Xem thêm
                  </Button>
                </div>
              </div>
              <h3 className="text-[1rem] sm:text-1.9xl uppercase font-bold mt-2 sm:mt-1">
                {blog.title}
              </h3>
            </a>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Blog;
