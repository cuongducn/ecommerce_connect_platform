import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import * as orderAction from "../../../actions/order";
import { useEffect, useRef } from "react";
import { orderStatuses, paymentStatuses } from "../../../utils/data/order";
import NoImage from "../../../assets/no_img.png";
import { formatNumber } from "../../../utils";

export default function OrderDetailCustomer() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { order } = useSelector((state) => state.orderReducers);
  const params = useParams();
  const orderCode = params.order_code;

  const listOrderStatusRef = useRef(orderStatuses);
  const listPaymentStatusRef = useRef(paymentStatuses);

  useEffect(() => {
    if (!orderCode) return;
    dispatch(orderAction.getDetailOrder(orderCode));
  }, [dispatch, orderCode]);
  return (
    <div className="md:text-base text-sm">
      <div className="flex gap-x-5 ">
        <div
          className="text-gray-900  font-medium rounded-lg text-sm py-2 text-center flex gap-x-1 cursor-pointer"
          onClick={() => navigate(-1)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-5 h-5"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M19.5 12h-15m0 0l6.75 6.75M4.5 12l6.75-6.75"
            />
          </svg>
          Quay lại
        </div>
      </div>
      <div className="flex flex-col gap-5 mt-6">
        <div className="flex justify-center items-center">
          <div className="py-5 px-10 border  border-black rounded-2xl flex gap-x-2 justify-center items-center">
            ĐƠN HÀNG #{orderCode}{" "}
            <div
              className={`px-3 py-1 text-[#fff] rounded-md`}
              style={{
                backgroundColor: listOrderStatusRef.current?.filter(
                  (orderStatus) => order.order_status === orderStatus.status
                )?.[0]?.color,
              }}
            >
              {
                listOrderStatusRef.current?.filter(
                  (orderStatus) => order.order_status === orderStatus?.status
                )?.[0]?.title
              }
            </div>
            <span></span>
          </div>
        </div>
        <div className="flex-1 flex flex-col gap-y-5">
          <div className="flex flex-col xl:flex-row gap-5 ">
            <div className="rounded-md bg-[#f7f7f7] p-4 flex-1">
              <div className="mb-5 font-semibold text-base md:text-lg">
                Thông tin khách hàng
              </div>
              <div className="flex flex-col gap-y-2 ">
                {" "}
                <div>
                  Khách hàng:{" "}
                  <span className="font-semibold">{order.name_sender}</span>
                </div>
                <div>
                  SĐT khách hàng:{" "}
                  <span className="font-semibold">{order.phone_number}</span>
                </div>
                <div>
                  Người nhận:{" "}
                  <span className="font-semibold">{order.customer_name}</span>
                </div>
                <div>
                  SĐT người nhận:{" "}
                  <span className="font-semibold">{order.customer_phone}</span>
                </div>
                <div>
                  Địa chỉ nhận:{" "}
                  <span className="font-semibold">{`${
                    order.customer_address_detail
                      ? `${order.customer_address_detail},`
                      : ""
                  }${
                    order.customer_wards_name
                      ? `${order.customer_wards_name},`
                      : ""
                  }${
                    order.customer_district_name
                      ? `${order.customer_district_name},`
                      : ""
                  }${
                    order.customer_province_name
                      ? `${order.customer_province_name}`
                      : ""
                  }`}</span>
                </div>
                <div>
                  Email:{" "}
                  <span className="font-semibold">{order.customer_email}</span>
                </div>
                <div>
                  Thời gian đặt:{" "}
                  <span className="font-semibold">{order.created_at}</span>
                </div>
                <div>
                  Phương thức thanh toán:{" "}
                  <span className="font-semibold">
                    {
                      paymentStatuses.filter(
                        (paymentStatus) =>
                          paymentStatus.status === order.payment_status
                      )?.[0]?.title
                    }
                  </span>
                </div>
                <div>
                  Ghi chú: <span className="font-semibold">{order.note}</span>
                </div>
              </div>
            </div>
            <div className="w-full xl:w-[300px] xl:h-[360px] bg-[#f7f7f7]">
              <div className=" p-4 rounded-md">
                <div className="font-semibold mb-4">Tổng tiền</div>
                <div className="flex flex-col gap-y-2">
                  <div className="flex justify-between">
                    <span>Tạm tính</span>
                    <span>
                      {order.total_before_discount
                        ? formatNumber(order.total_before_discount)
                        : 0}
                      đ
                    </span>
                  </div>
                  <div className="flex justify-between">
                    <span>Phí vận chuyển</span>
                    <span>
                      {order.total_shipping_fee
                        ? formatNumber(order.total_shipping_fee)
                        : 0}
                      đ
                    </span>
                  </div>
                  <div className="flex justify-between">
                    <span>Giảm giá</span>
                    <span>
                      {order.discount ? formatNumber(order.discount) : 0}%
                    </span>
                  </div>
                  <div className="flex justify-between ">
                    <span>Thành tiền</span>
                    <span className="font-semibold text-base md:text-xl">
                      {order.total_final ? formatNumber(order.total_final) : 0}đ
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="rounded-md bg-[#f7f7f7] p-4 ">
            <div className="mb-5 font-semibold text-base md:text-lg">
              Mã đơn: #{orderCode} | {order.time_line_product?.length} sản phẩm
            </div>
            <div className="flex flex-col gap-y-4 h-[217px] overflow-y-auto">
              {order.time_line_product?.length > 0 &&
                order.time_line_product?.map((product, index) => (
                  <div
                    key={index}
                    className={`flex gap-x-3 ${
                      index !== order.time_line_product?.length - 1
                        ? "border-b border-gray-100"
                        : ""
                    }`}
                  >
                    <div className="">
                      <img
                        className="md:w-[100px] md:h-[100px] w-[70px] h-[70px]"
                        src={
                          product.image_link?.length > 0
                            ? product.image_link[0]
                            : NoImage
                        }
                        alt={product.name}
                      />
                    </div>
                    <div className="flex-1 md:text-base text-sm">
                      <div className="font-medium mb-2 ">{product.name}</div>
                      <div className="w-full flex justify-between items-center">
                        <span>Tổng số lượng</span>
                        <span>x{product.quantity}</span>
                      </div>
                      <div className="w-full flex justify-between items-center">
                        <span>Giá sản phẩm</span>
                        <span>{formatNumber(product.price)}đ</span>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
