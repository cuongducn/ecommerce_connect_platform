import { useCallback, useRef } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Input, InputGroup, Table } from 'rsuite';
import styled from 'styled-components';
import * as orderAction from '../../actions/order';
import SearchIcon from '@rsuite/icons/Search';
import ReactPagination from '../../components/pagination/ReactPagination';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { debounce } from 'lodash';
import { formatNumber } from '../../utils';
import { orderStatuses, paymentStatuses } from '../../utils/data/order';
const { Column, HeaderCell, Cell } = Table;
const rowKey = 'id';

const OrderStyles = styled.div`
  .historyMedical {
    box-shadow: 1px 4px 15px 2px rgba(38, 166, 154, 0.2);
  }
`;

const OrderCustomer = () => {
  const dispatch = useDispatch();
  const { allOrder } = useSelector((state) => state.orderReducers);
  const { data } = allOrder;
  const [searchParams] = useSearchParams();
  const [searchInput, setSearchInput] = useState(
    searchParams.get('search') || ''
  );

  const [params, setParams] = useState({
    page: searchParams.get('page') || 1,
    limit: searchParams.get('limit') || 9,
    search: searchParams.get('search') || '',
  });
  const [sortData, setSortData] = useState({
    column: undefined,
    type: undefined,
    loading: false,
  });
  const listOrderStatusRef = useRef(orderStatuses);
  const listPaymentStatusRef = useRef(paymentStatuses);
  const navigate = useNavigate();

  const queryParams = (page, limit, search) => {
    return `page=${page}${limit ? `&limit=${limit}` : ''}${
      search ? `&search=${search}` : ''
    }`;
  };
  const handleSelectedPage = (event) => {
    setParams({
      ...params,
      page: Number(event.selected) + 1,
    });
    navigate(
      `/c/order?${queryParams(
        Number(event.selected) + 1,
        params.limit,
        params.search
      )}`
    );
  };

  //Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        return {
          ...prevParams,
          search: valueSearch,
          page: 1,
        };
      });
      navigate(`/c/order?${queryParams(1, params.limit, valueSearch)}`);
    }, 500),
    []
  );
  const handleSearch = (value) => {
    setSearchInput(value);
    debounceCallBack(value);
  };
  // Handle Sort Data
  const getData = () => {
    if (sortData.column && sortData.type) {
      return data.sort((a, b) => {
        let x = a[sortData.column];
        let y = b[sortData.column];
        if (typeof x === 'string') {
          x = x.charCodeAt();
        }
        if (typeof y === 'string') {
          y = y.charCodeAt();
        }
        if (sortData.type === 'asc') {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    let dataIndex = [];
    if (data?.length > 0) {
      dataIndex = data.map((item, index) => {
        item.index =
          (Number(allOrder.current_page) - 1) * Number(allOrder.per_page) +
          index +
          1;
        return item;
      });
    }
    return dataIndex;
  };
  const handleSortColumn = (sortColumn, sortType) => {
    setSortData((prevSortAble) => ({
      ...prevSortAble,
      loading: true,
    }));
    setTimeout(() => {
      setSortData((prevSortAble) => ({
        ...prevSortAble,
        type: sortType,
        column: sortColumn,
        loading: false,
      }));
    }, 500);
  };
  const getParams = (page, limit, search) => {
    let params = '';
    if (page) {
      params += `page=${page}`;
    }
    if (limit) {
      params += `&limit=${limit}`;
    }
    if (search !== '') {
      params += `&search=${search}`;
    }

    return params;
  };

  useEffect(() => {
    const query = queryParams(params.page, params.limit, params.search);
    dispatch(orderAction.getListOrders(query));
  }, [dispatch, params.limit, params.page, params.search]);
  return (
    <OrderStyles>
      <h4 className="text-xl font-medium">Đơn hàng</h4>
      <div className="max-w-[300px] mt-5">
        <InputGroup inside>
          <Input
            placeholder="Tìm kiếm đơn hàng"
            value={searchInput}
            onChange={handleSearch}
          />
          <InputGroup.Button>
            <SearchIcon />
          </InputGroup.Button>
        </InputGroup>
      </div>
      <div className="text-sm">
        <Table
          autoHeight
          rowKey={rowKey}
          hover={false}
          data={getData()}
          sortColumn={sortData.column}
          sortType={sortData.type}
          onSortColumn={handleSortColumn}
          loading={sortData.loading}
        >
          <Column align="center" width={50}>
            <HeaderCell>STT</HeaderCell>
            <Cell dataKey="index" />
          </Column>
          <Column width={170}>
            <HeaderCell>Mã đơn</HeaderCell>
            <Cell>
              {(rowData) => (
                <span
                  className="text-blue-500 cursor-pointer hover:text-blue-600"
                  onClick={() =>
                    navigate(
                      `/c/order/detail/${rowData.order_code}?${getParams(
                        params.page,
                        params.limit,
                        params.search
                      )}`
                    )
                  }
                >
                  {rowData.order_code}
                </span>
              )}
            </Cell>
          </Column>
          <Column flexGrow={1}>
            <HeaderCell>Tên người nhận</HeaderCell>
            <Cell>{(rowData) => rowData.customer_name}</Cell>
          </Column>
          <Column width={110}>
            <HeaderCell>Tổng tiền</HeaderCell>
            <Cell>
              {(rowData) =>
                rowData.total_final
                  ? `${formatNumber(rowData.total_final)}đ`
                  : '0đ'
              }
            </Cell>
          </Column>
          <Column width={150}>
            <HeaderCell>Thời gian tạo đơn</HeaderCell>
            <Cell>{(rowData) => rowData.created_at}</Cell>
          </Column>
          <Column width={150}>
            <HeaderCell>Trạng thái thanh toán</HeaderCell>
            <Cell>
              {(rowData) => (
                <span
                  style={{
                    color: listPaymentStatusRef.current?.filter(
                      (orderPayment) =>
                        orderPayment.status === rowData.payment_status
                    )?.[0]?.color,
                  }}
                >
                  {
                    listPaymentStatusRef.current?.filter(
                      (orderPayment) =>
                        orderPayment.status === rowData.payment_status
                    )?.[0]?.title
                  }
                </span>
              )}
            </Cell>
          </Column>
          <Column width={150}>
            <HeaderCell>Trạng thái đơn hàng</HeaderCell>
            <Cell>
              {(rowData) => (
                <span
                  style={{
                    color: listOrderStatusRef.current?.filter(
                      (orderStatus) =>
                        orderStatus.status === rowData.order_status
                    )?.[0]?.color,
                  }}
                >
                  {
                    listOrderStatusRef.current?.filter(
                      (orderStatus) =>
                        orderStatus.status === rowData.order_status
                    )?.[0]?.title
                  }
                </span>
              )}
            </Cell>
          </Column>
        </Table>
      </div>
      {allOrder?.data?.length > 0 && allOrder.last_page > 1 && (
        <div className="flex justify-center mt-8 text-sm pagination__manage">
          <ReactPagination
            pageCount={allOrder.last_page}
            handleClickPage={handleSelectedPage}
            marginPagesDisplayed={3}
            pageRangeDisplayed={3}
            page={params.page - 1}
          />
        </div>
      )}
    </OrderStyles>
  );
};

export default OrderCustomer;
