import { QrcodeOutlined } from '@ant-design/icons';
import IconTruck from '../../components/icon/truck/truck';
import { Button, Input } from 'antd';
import { Select } from 'antd';
import { Radio } from 'antd';
import { Space } from 'antd';
import { Form } from 'antd';
import React from 'react';
import styled from 'styled-components';
import ItemPayment from '../../components/cart/itemPayment';
import { Drawer } from 'antd';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as cartAction from '../../actions/cart';
import InputSelect from '../../components/form/InputSelect';
import { useEffect } from 'react';
import { useForm } from 'antd/es/form/Form';

const ItemCheckoutStyles = styled.div``;

const ItemCheckout = ({ handleSubmit }) => {
  const dispatch = useDispatch();
  const [isShowVoucher, setIsShowVoucher] = useState(false);
  const { cartItems } = useSelector((state) => state.cartReducers);

  return (
    <ItemCheckoutStyles>
      <div className="overflow-y-auto max-h-[75vh] ">
        {cartItems?.map((item, idx) => (
          <div key={idx}>
            <ItemPayment product={item} />
          </div>
        ))}
      </div>
      <div>
        <div className="flex mt-[25px] mb-[15px] text-[15px] text-[black] justify-between content-center items-center">
          <div className="flex">
            <img
              src="../../../src/assets/image-payment/ticket.png"
              alt=""
              className="mr-[10px] w-[20px]"
            />
            <span className=" text-[14px]">Voucher</span>
          </div>
          <Button
            className="bg-[white] text-[black] "
            size="middle"
            onClick={() => setIsShowVoucher(!isShowVoucher)}
          >
            Chọn mã code
          </Button>
          <Button
            className="bg-[black] text-[white] "
            size="middle"
            onClick={() => setIsShowVoucher(!isShowVoucher)}
          >
            Áp dụng
          </Button>
        </div>
        <div className="flex flex-col my-[15px] text-[1.1rem] text-[black] justify-between content-center items-center border-b">
          <div className="flex my-[5px] text-[1.1rem] text-[black] justify-between w-full">
            <p className="text-[14px]">Tổng đơn</p>
            <span className="">12.000.000</span>
          </div>
          <div className="flex my-[5px] text-[1.1rem] text-[black] justify-between w-full">
            <p className="text-[14px]">Ưu đãi (voucher / thành viên)</p>
            <span className="">-0</span>
          </div>
          <div className="flex my-[5px] text-[1.1rem] text-[black] justify-between w-full">
            <p className="text-[14px]">Phí ship</p>
            <span className="">0</span>
          </div>
        </div>
        <div className="flex my-[15px] text-[1.1rem] text-[black] justify-between">
          <p className="text-[1.1rem]">THÀNH TIỀN</p>
          <span className="">12.000.000</span>
        </div>
        <Button
          className="w-[100%] my-[10px] mb-[20px] bg-[black] text-[white] btn-custom_submit"
          size="large"
          onClick={() => handleSubmit()}
        >
          HOÀN TẤT ĐƠN HÀNG
        </Button>
      </div>
      <Drawer
        title="Chọn voucher"
        placement="right"
        onClose={() => setIsShowVoucher(!isShowVoucher)}
        open={isShowVoucher}
      >
        <p>Chưa có voucher áp dụng</p>
      </Drawer>
    </ItemCheckoutStyles>
  );
};

export default ItemCheckout;
