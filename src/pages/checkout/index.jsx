import { Button, Form, Input, Modal, Radio, Select } from 'antd';
import { useForm } from 'antd/es/form/Form';
import React, { useEffect, useState } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import * as orderAction from '../../actions/order';
import * as placeAction from '../../actions/place';
import IconTruck from '../../components/icon/truck/truck';
import { alert } from '../../utils/alerts';
import ItemCheckout from './itemCheckout';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';

const CheckoutStyles = styled.div`
  .input-checkout {
    border-bottom: 1px solid #b2b2b2;
    border-radius: 0;
  }

  .btn-handle_hover {
  }
`;
const Checkout = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [form] = useForm();
  const { cartItems } = useSelector((state) => state.cartReducers);
  const { profile } = useSelector((state) => state.profileReducers);
  const { order } = useSelector((state) => state.orderReducers);
  const [isShowVoucher, setIsShowVoucher] = useState(false);
  const [isFormValid, setIsFormValid] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState(null);
  const [isShowEndCheckout, setIsShowEndCheckout] = useState(false);
  // const { createdOrder } = this.props;

  const [formValue, setFormValue] = useState({
    customer_address_detail: profile?.address_detail || '',
    customer_province: profile?.province || '',
    customer_district: profile?.district || '',
    customer_wards: profile?.wards || '',
    customer_phone: profile?.phone_number || '',
    customer_name: profile?.name || '',
    customer_email: profile?.email || '',
    customer_note: '',
    payment_method_id: null,
    time_line_product: null,
    customer_id: profile?.id || null,
  });
  const { province, district, wards } = useSelector(
    (state) => state.placeReducers
  );

  const handleFilterPlace = (value, data) => {
    let newData;
    data.forEach((element) => {
      if (element.id !== value) return;
      newData = {
        value: element.id,
        label: element.name,
      };
    });
    return newData;
  };

  const handleOptionPlace = (data) => {
    const newData = data.reduce((prevData, currentData) => {
      return [
        ...prevData,
        {
          value: currentData.id,
          label: currentData.name,
        },
      ];
    }, []);
    return newData;
  };

  const handleMethodPayment = (e) => {
    setPaymentMethod(e.target.value);

    setFormValue({
      ...formValue,
      payment_method_id: e.target.value || 2,
    });
  };

  // Handle Change Value Form
  const handleChangeValueForm = (e, values, area = null) => {
    if (e.target) {
      const updatedFormValue = { ...formValue };

      e.forEach(({ name, value }) => {
        updatedFormValue[name] = value;
      });
    } else {
      const name = Object.keys(e)[0];
      // const name = area;
      let valuesPlace = {};
      if (name.includes('province')) {
        dispatch(placeAction.getDistrict(e.customer_province));
        valuesPlace = {
          district: '',
          wards: '',
        };
      } else if (name.includes('district')) {
        dispatch(placeAction.getWards(e.customer_district));
        valuesPlace = {
          wards: '',
        };
      }

      setFormValue({
        ...values,
        ...valuesPlace,
      });
    }

    form
      .validateFields()
      .then(() => {
        setIsFormValid(true);
      })
      .catch(() => {
        setIsFormValid(false);
      });
  };
  const handleSubmit = async () => {
    try {
    } catch (error) {
      alert.error('Yêu cầu nhập đủ thông tin đơn hàng');
    }

    if (paymentMethod === null) {
      alert.error('Chưa chọn phương thức thanh toán');
      return;
    }

    setFormValue({
      ...formValue,
      time_line_product: cartItems || null,
    });

    // form.submit();
    dispatch(orderAction.addOrder(formValue));
    setIsShowEndCheckout(true);
  };

  const validatePhoneNumber = (_, value) => {
    // Custom validation for phone number field
    if (value && !/^\d{10}$/.test(value)) {
      return Promise.reject('Số điện thoại không hợp lệ, phải là 10 số.');
    }
    return Promise.resolve();
  };
  useEffect(() => {
    dispatch(placeAction.getProvince());
  }, [dispatch]);

  useEffect(() => {
    const closeShowEndCheckoutAfterDelay = setTimeout(() => {
      setIsShowEndCheckout(false);
    }, 6000);

    return () => {
      clearTimeout(closeShowEndCheckoutAfterDelay);
    };
  }, []);

  // useEffect(() => {
  //   setFormValue((prevFormValue) => ({
  //     ...prevFormValue,
  //     customer_address_detail:
  //       profile.address_detail || prevFormValue.customer_address_detail,
  //     customer_province: profile.province || prevFormValue.customer_province,
  //     customer_district: profile.district || prevFormValue.customer_district,
  //     customer_wards: profile.wards || prevFormValue.customer_wards,
  //     customer_phone: profile.phone_number || prevFormValue.customer_phone,
  //     customer_name: profile.name || prevFormValue.customer_name,
  //     customer_email: profile.email || prevFormValue.customer_email,
  //     customer_id: profile.id || prevFormValue.customer_id,
  //   }));
  // }, [profile]);

  return (
    <CheckoutStyles className="sm:grid sm:grid-cols-3 sm:px-10 sm:pb-5 gap-20 ">
      <div className="p-4">
        <h1 className="uppercase text-[1.2rem]">thông tin giao hàng</h1>
        <div className="px-2">
          <Form
            form={form}
            name="form-checkout"
            autoComplete="off"
            labelAlign="left"
            layout="vertical"
            onValuesChange={handleChangeValueForm}
            initialValues={formValue}
          >
            <div className="">
              <Form.Item
                name="customer_name"
                label={
                  <p style={{ fontSize: '13px', color: '#b2b2b2' }}>
                    Họ và tên
                  </p>
                }
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập họ và tên',
                  },
                ]}
              >
                <Input
                  placeholder="Nhập..."
                  size="medium"
                  bordered={false}
                  inputMode="text"
                  className="input-checkout"
                />
              </Form.Item>
            </div>
            <div className="">
              <Form.Item
                name="customer_email"
                label={
                  <p style={{ fontSize: '13px', color: '#b2b2b2' }}>Email</p>
                }
                rules={[
                  {
                    type: 'email',
                    message: 'Email không hợp lệ',
                  },
                  {
                    required: true,
                    message: 'Vui lòng nhập email',
                  },
                ]}
              >
                <Input
                  placeholder="Nhập..."
                  size="medium"
                  bordered={false}
                  inputMode="email"
                  className="input-checkout"
                />
              </Form.Item>
            </div>
            <div className="">
              <Form.Item
                name="customer_phone"
                label={
                  <p style={{ fontSize: '13px', color: '#b2b2b2' }}>
                    Số điện thoại
                  </p>
                }
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập số điện thoại',
                  },
                  {
                    validator: validatePhoneNumber,
                  },
                ]}
              >
                <Input
                  placeholder="Nhập..."
                  size="medium"
                  bordered={false}
                  className="input-checkout"
                />
              </Form.Item>
            </div>
            <div>
              <Form.Item
                name="customer_province"
                label={
                  <p style={{ fontSize: '13px', color: '#b2b2b2' }}>
                    Chọn Tỉnh/ Thành phố
                  </p>
                }
              >
                <Select
                  size="medium"
                  options={handleOptionPlace(province)}
                  value={handleFilterPlace(formValue.province, province)}
                  // onChange={(vau) =>
                  //   // handleFilterPlace(e)
                  //   console.log(e, value)
                  // }
                  onChange={(value) =>
                    setFormValue({ ...formValue, province: value })
                  }
                ></Select>
              </Form.Item>
            </div>
            <div>
              <Form.Item
                name="customer_district"
                label={
                  <p style={{ fontSize: '13px', color: '#b2b2b2' }}>
                    Chọn Quận/ Huyện
                  </p>
                }
              >
                <Select
                  size="medium"
                  options={handleOptionPlace(district)}
                  value={handleFilterPlace(formValue.district, district)}
                  onChange={(value) =>
                    setFormValue({ ...formValue, district: value })
                  }
                ></Select>
              </Form.Item>
            </div>
            <div>
              <Form.Item
                name="customer_wards"
                label={
                  <p style={{ fontSize: '13px', color: '#b2b2b2' }}>
                    Chọn Phường/ Xã
                  </p>
                }
              >
                <Select
                  size="medium"
                  options={handleOptionPlace(wards)}
                  value={handleFilterPlace(formValue.wards, wards)}
                  onChange={(value) =>
                    setFormValue({ ...formValue, wards: value })
                  }
                ></Select>
              </Form.Item>
            </div>
            <div className="">
              <Form.Item
                name="customer_address_detail"
                label={
                  <p style={{ fontSize: '13px', color: '#b2b2b2' }}>
                    Số nhà tên đường
                  </p>
                }
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng số nhà tên đường',
                  },
                ]}
              >
                <Input
                  placeholder="Nhập..."
                  size="medium"
                  bordered={false}
                  className="input-checkout"
                />
              </Form.Item>
            </div>
            <div className="">
              <Form.Item
                name="customer_note"
                label={
                  <p style={{ fontSize: '13px', color: '#b2b2b2' }}>Ghi chú</p>
                }
              >
                <Input
                  placeholder="Nhập..."
                  size="medium"
                  bordered={false}
                  className="input-checkout"
                />
              </Form.Item>
            </div>
            <div className="">
              <Form.Item name="payment_method_id" hidden={true}>
                <Input
                  placeholder="Nhập..."
                  size="medium"
                  hidden={true}
                  className="input-checkout"
                />
              </Form.Item>
            </div>
            <div className="">
              <Form.Item name="time_line_product" hidden={true}>
                <Input
                  placeholder="Nhập..."
                  size="medium"
                  hidden={true}
                  className="input-checkout"
                />
              </Form.Item>
            </div>
            <div className="">
              <Form.Item name="customer_id" hidden={true}>
                <Input
                  placeholder="Nhập..."
                  size="medium"
                  hidden={true}
                  className="input-checkout"
                />
              </Form.Item>
            </div>
          </Form>
        </div>
      </div>
      <div className="p-4 ">
        <h1 className="uppercase text-[1.2rem]">PHƯƠNG THỨC THANH TOÁN</h1>
        <Radio.Group
          className="w-full mt-[10px]"
          value={paymentMethod}
          onChange={(e) => handleMethodPayment(e)}
        >
          <div className="flex flex-col ">
            {/* <Radio
              value={1}
              className="p-2 mb-3 rounded-md border-[1px] border-gray-300"
            >
              <div className="flex content-center items-center">
                <QrcodeOutlined
                  style={{
                    fontSize: '1.5rem',
                    lineHeight: '0',
                    padding: '0 10px',
                  }}
                />
                <div className="flex flex-col ">
                  <span>Chuyển khoản bằng QR Code</span>
                </div>
              </div>
            </Radio> */}
            <Radio
              value={2}
              className="p-2 mb-3 rounded-md border-[1px] border-gray-300"
            >
              <div className="flex content-center items-center">
                <IconTruck className="mx-[10px] w-[25px]" />
                <span>Thanh toán khi nhận hàng (COD)</span>
              </div>
            </Radio>
            <Radio
              value={4}
              className="p-2 mb-3 rounded-md border-[1px] border-gray-300"
            >
              <div className="flex content-center items-center">
                <img
                  src="../../../src/assets/image-payment/vnpay.webp"
                  alt=""
                  className="mx-[10px] w-[25px]"
                />
                <span>Thanh toán VNPAY</span>
              </div>
            </Radio>
            {/* <Radio
              value={4}
              disabled
              className="p-2 mb-3 rounded-md border-[1px] border-gray-300"
            >
              <div className="flex content-center items-center">
                <img
                  src="../../../src/assets/image-payment/momo-icon.webp"
                  alt=""
                  className="mx-[10px] w-[25px]"
                />
                <span>Thanh toán MOMO</span>
              </div>
            </Radio>
            <Radio
              value={5}
              disabled
              className="p-2 mb-3 rounded-md border-[1px] border-gray-300"
            >
              <div className="flex content-center items-center">
                <img
                  src="../../../src/assets/image-payment/logo-zalopay.svg"
                  alt=""
                  className="mx-[10px] w-[25px]"
                />
                <span>Thanh toán ZaloPay</span>
              </div>
            </Radio> */}
          </div>
        </Radio.Group>
        {paymentMethod === false && (
          <div className="text-red-500 text-[13.5px]">
            Vui lòng chọn phương thức thanh toán
          </div>
        )}
      </div>
      <div className="mt-10">
        <ItemCheckout handleSubmit={handleSubmit} />
      </div>
      <Modal title="Basic Modal" open={isShowEndCheckout}>
        <div>
          <Button>Tiếp tục mua sắm</Button>
          {order && (
            <Button
              onClick={() => navigate('/c/order/detail/' + order.order_code)}
            >
              Xem đơn hàng
            </Button>
          )}
        </div>
      </Modal>
    </CheckoutStyles>
  );
};

// export default Checkout;
const mapStateToProps = (state) => {
  return {
    createdOrder:
      state.orderReducers.allOrder[state.orderReducers.allOrder.length - 1],
  };
};

export default connect(mapStateToProps)(Checkout);
