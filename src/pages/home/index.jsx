import React from 'react';
import Banner from './Banner';
import Choice from './Choice';
import Introduction from './Introduction';
import IntroductionModal from './IntroductionModal';
import Opinion from './Opinion';
import Statisfaction from './Statisfaction';
import Service from './Service';
import Networking from './Networking';
import Product from './product/Product';
import * as homeAction from '../../actions/home';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

const Home = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(homeAction.getHome());
  }, [dispatch]);

  return (
    <div className="relative">
      <div className="">
        {/* <IntroductionModal></IntroductionModal> */}
        <Banner />
        <Product></Product>
        {/* <Service />
        <Choice /> */}
        {/* <Networking /> */}
        {/* <Blog /> */}
        {/* <Statisfaction></Statisfaction>
        <Introduction></Introduction> */}
        {/* <Opinion></Opinion> */}
        <div className="mb-10"></div>
      </div>
    </div>
  );
};

export default Home;
