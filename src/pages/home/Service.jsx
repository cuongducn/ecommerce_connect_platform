import React from 'react';
import { v4 } from 'uuid';
const services = [
  {
    id: v4(),
    name: 'Bác sĩ riêng',
    icon: 'https://aihealth.vn/app/uploads/2021/05/e-doctor.png',
  },
  {
    id: v4(),
    name: 'Giao thuốc tận nơi',
    icon: 'https://aihealth.vn/app/uploads/2021/05/e-delivery-man.png',
  },
  {
    id: v4(),
    name: 'Chăm sóc tận nhà',
    icon: 'https://aihealth.vn/app/uploads/2021/05/e-nurse.png',
  },
  {
    id: v4(),
    name: 'Sản phẩm sức khỏe',
    icon: 'https://aihealth.vn/app/uploads/2021/05/e-medicine.png',
  },
  {
    id: v4(),
    name: 'Lấy mẫu tại nhà',
    icon: 'https://aihealth.vn/app/uploads/2021/05/e-blood-tube.png',
  },
  {
    id: v4(),
    name: 'Đặt lịch khám',
    icon: 'https://aihealth.vn/app/uploads/2021/05/e-hospital.png',
  },
];
const Service = () => {
  return (
    <div className="grid grid-cols-1 gap-10 pt-20 md:grid-cols-2">
      <div className="px-[15px] md:px-0">
        <img
          decoding="async"
          src="https://aihealth.vn/app/uploads/2021/09/da-app-phone-1-1.png"
          alt=""
        />
      </div>
      <div className="text-center md:pr-20 md:text-left">
        <p className="mb-5 text-3xl font-semibold uppercase ">
          Mỗi người dân có một <span className="text-main">bác sĩ riêng</span>
        </p>

        <p className="mb-5 text-lg text-[#93928c]">
          Asahaa là nền tảng kết nối bệnh nhân và hàng ngàn Bác sĩ giỏi với tất
          cả 30 chuyên khoa đang công tác tại các phòng khám lớn trên cả nước.
        </p>
        <ul className="grid grid-cols-2 md:grid-cols-3">
          {services.map((service) => (
            <li className="text-center my-5 px-[10px]" key={service.id}>
              <span className="block">
                <img
                  decoding="async"
                  src={service.icon}
                  alt={service.name}
                  className="inline-block max-w-[80px] max-h-[60px]"
                />
              </span>

              <span className="mt-[10px] block text-sm font-semibold">
                {service.name}
              </span>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Service;
