import React from "react";
import { Link, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { choice } from "../../utils/helper";
import Slider from "react-slick";
import { useState } from "react";
import { stepAppointment } from "../../utils/stepAppointment";
import * as Types from "../../constants/actionType";
import { Modal } from "rsuite";

const ChoiceStyles = styled.div`
  .choice__item {
    box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.1);
    transition: all 0.5s;
    &:hover {
      box-shadow: 2px 2px 4px 1px rgba(38, 166, 154, 0.3);
      transform: scale(1.05);
    }
  }
  .slick-list {
    margin: 0 -10px;
    padding: 10px 0;
  }
  .slick-slide > div {
    padding: 0 10px;
  }

  .slick-track {
    margin: 0;
  }
  .roundedChoice {
    box-shadow: 0 0 0 0px rgba(255, 255, 0, 0.35);
    animation: roundedChoice 2.5s ease-in-out infinite;
  }
  @keyframes roundedChoice {
    0% {
      box-shadow: 0 0 0 0px rgba(255, 255, 0, 0.35);
    }
    50% {
      box-shadow: 0 0 0 10px rgba(255, 255, 0, 0.35);
    }
    100% {
      box-shadow: 0 0 0 0px rgba(255, 255, 0, 0.35);
    }
  }
`;

const Choice = () => {
  const [indexActive, setIndexActive] = useState(0);
  const [openModal, setOpenModal] = useState(false);
  const [url, setUrl] = useState();
  const stepAppointmentParams = stepAppointment.getParamsPathName();

  const navigate = useNavigate();

  const handleShowModal = (url) => {
    setOpenModal(true);
    setUrl(url);
  };
  const handleCloseModal = () => {
    setOpenModal(false);
    setUrl();
  };
  const handleTypeAppointment = (type) => {
    if (type === "old") {
      const location = stepAppointment.getPathName();
      navigate(location);
    } else {
      navigate(url);
    }
  };

  const choiceSetting = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: true,

    beforeChange: (oldIndex, newIndex) => {
      setIndexActive(newIndex);
    },
    customPaging: (i) => {
      return (
        <div className="pt-5">
          <div
            className={`w-5 h-1 rounded-full ${
              i === indexActive ? "bg-main" : "bg-[rgba(38,166,154,0.3)]"
            }`}
          ></div>
        </div>
      );
    },
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 920,
        settings: {
          infinite: true,
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 650,
        settings: {
          infinite: true,
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 450,
        settings: {
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <ChoiceStyles className="pt-5 bg-slate-50 pb-14 mt-[50px]">
      <div className="containerWidth">
        <div className="">
          <div className="mb-10 text-center">
            <h3 className="mb-2 text-2xl font-semibold text-main">
              Nhiều lựa chọn hiện đại, hấp dẫn
            </h3>
          </div>
          <Slider {...choiceSetting}>
            {choice.map((item) => (
              <div key={item.id} className="rounded-lg choice__item bg-[#fff]">
                {item.type === 0 || item.type === 1 || item.type === 2 ? (
                  <div>
                    {stepAppointmentParams.type === "" ? (
                      <Link
                        to={item.url}
                        className="flex flex-col items-center p-3 text-center hover:no-underline hover:text-bodyColor"
                      >
                        <img
                          className="w-[56px] h-[56px]"
                          src={item.icon}
                          alt={item.name}
                        />
                        <div className="mt-3 mb-2 text-lg font-semibold">
                          {item.name}
                        </div>
                        <p className="text-sm text-gray8F h-[40px]">
                          {item.title}
                        </p>
                      </Link>
                    ) : (stepAppointmentParams.type ===
                        Types.TYPE_MEDICAL_AT_HOME ||
                        stepAppointmentParams.type ==
                          Types.TYPE_MEDICAL_ONLINE ||
                        stepAppointmentParams.type ==
                          Types.TYPE_MEDICAL_PRIVATE_CLINIC) &&
                      stepAppointmentParams.type == item.type ? (
                      <Link
                        to={`${stepAppointment.getPathName()}`}
                        className="relative flex flex-col items-center p-3 text-center hover:no-underline hover:text-bodyColor"
                      >
                        <div className="absolute w-5 h-5 bg-yellow-300 rounded-full roundedChoice top-3 right-3"></div>
                        <img
                          className="w-[56px] h-[56px]"
                          src={item.icon}
                          alt={item.name}
                        />
                        <div className="mt-3 mb-2 text-lg font-semibold">
                          {item.name}
                        </div>
                        <p className="text-sm text-gray8F h-[40px]">
                          {item.title}
                        </p>
                      </Link>
                    ) : (
                      <div
                        className="flex flex-col items-center p-3 text-center hover:no-underline hover:text-bodyColor"
                        onClick={() => handleShowModal(item.url)}
                      >
                        <img
                          className="w-[56px] h-[56px]"
                          src={item.icon}
                          alt={item.name}
                        />
                        <div className="mt-3 mb-2 text-lg font-semibold">
                          {item.name}
                        </div>
                        <p className="text-sm text-gray8F h-[40px]">
                          {item.title}
                        </p>
                      </div>
                    )}
                  </div>
                ) : (
                  <Link
                    to={item.url}
                    className="flex flex-col items-center p-3 text-center hover:no-underline hover:text-bodyColor"
                  >
                    <img
                      className="w-[56px] h-[56px]"
                      src={item.icon}
                      alt={item.name}
                    />
                    <div className="mt-3 mb-2 text-lg font-semibold">
                      {item.name}
                    </div>
                    <p className="text-sm text-gray8F h-[40px]">{item.title}</p>
                  </Link>
                )}
              </div>
            ))}
          </Slider>
        </div>
      </div>
      {url && (
        <Modal size="xs" open={openModal} onClose={handleCloseModal}>
          <Modal.Header>
            <Modal.Title>
              <div className="text-xl font-medium text-center">
                Lựa chọn loại thao tác đặt lịch
              </div>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <p className="text-center text-gray4B w-[70%] mx-auto">
                Vui lòng cho biết bạn muốn lựa chọn loại đặt lịch
              </p>
              <div className="flex flex-col items-center justify-center mt-4 gap-y-2">
                <button
                  className="px-4 py-2 bg-[#3471eb] text-[#fff] rounded-2xl flex items-center gap-x-2 w-[220px] justify-center  hover:scale-105 duration-300"
                  onClick={() => handleTypeAppointment("old")}
                >
                  <span>Đặt lịch khám cũ</span>
                </button>
                <div className="w-[200px] h-[2px] bg-gray-200 my-3 relative">
                  <span className="absolute top-0 text-xs -translate-x-1/2 -translate-y-1/2 left-1/2 px-2 bg-[#fff] font-medium">
                    OR
                  </span>
                </div>
                <button
                  className="px-4 py-2 bg-main text-[#fff] rounded-2xl flex items-center justify-center gap-x-2 w-[220px]  hover:scale-105 duration-300"
                  onClick={() => handleTypeAppointment("new")}
                >
                  <span>Đặt lịch khám mới</span>
                </button>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      )}
    </ChoiceStyles>
  );
};

export default Choice;
