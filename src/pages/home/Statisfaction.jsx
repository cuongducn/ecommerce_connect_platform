import React from "react";
import styled from "styled-components";
import { v4 } from "uuid";
import IconClinic from "../../components/icon/clinic/IconClinic";
import IconFileMedical from "../../components/icon/file/IconFileMedical";
import IconPlus from "../../components/icon/plus/IconPlus";
import IconUserDoctor2 from "../../components/icon/user/IconUserDoctor2";
import IconUserPatient2 from "../../components/icon/user/IconUserPatient2";
import CountUp from "react-countup";
import VisibilitySensor from "react-visibility-sensor";

const StatisfactionStyles = styled.div`
  .choice__item {
    box-shadow: 2px 2px 4px 1px rgba(0, 0, 0, 0.1);
    transition: all 0.5s;
    &:hover {
      box-shadow: 2px 2px 4px 1px rgba(38, 166, 154, 0.3);
      transform: scale(1.05);
    }
  }
  .slick-list {
    margin: 0 -10px;
    padding: 10px 0;
  }
  .slick-slide > div {
    padding: 0 10px;
  }

  .slick-track {
    margin: 0;
  }
`;

const statisfactions = [
  {
    id: v4(),
    icon: <IconUserDoctor2 className="w-16 h-16"></IconUserDoctor2>,
    title: "Đội ngũ bác sĩ chuyên nghiệp",
    count: 2200,
  },
  {
    id: v4(),
    icon: <IconClinic className="w-16 h-16"></IconClinic>,
    title: "Những phòng khám hàng đầu",
    count: 30,
  },
  {
    id: v4(),
    icon: <IconUserPatient2 className="w-16 h-16"></IconUserPatient2>,
    title: "Trải nghiệm dành cho bệnh nhân",
    count: 1200000,
  },
  {
    id: v4(),
    icon: <IconFileMedical className="w-16 h-16"></IconFileMedical>,
    title: "Tư vấn với các bác sĩ miễn phí",
    count: 1500,
  },
];
const Statisfaction = () => {
  return (
    <StatisfactionStyles className="pt-5 bg-main pb-14 mt-[50px] text-[#fff]">
      <div className="containerWidth">
        <div className="">
          <div className="mb-10 text-center">
            <h3 className="mb-2 text-2xl font-semibold">
              Mang đến sự hài lòng cho người khám
            </h3>
          </div>
          <div className="grid grid-cols-1 gap-5 sm:grid-cols-2 md:grid-cols-4">
            {statisfactions.map((element) => (
              <div
                key={element.id}
                className="flex flex-col items-center justify-center gap-y-2"
              >
                <div>{element.icon}</div>
                <div className="flex items-center gap-x-1">
                  <span className="text-[35px]">
                    <VisibilitySensor partialVisibility>
                      {({ isVisible }) => (
                        <>
                          {isVisible ? (
                            <div>
                              {" "}
                              <CountUp end={element.count} />
                            </div>
                          ) : (
                            <div>0</div>
                          )}
                        </>
                      )}
                    </VisibilitySensor>
                  </span>
                  <IconPlus className="w-6 h-6" strokeWidth="3"></IconPlus>
                </div>
                <div className="text-center h-[48px]">{element.title}</div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </StatisfactionStyles>
  );
};

export default Statisfaction;
