import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import * as productAction from '../../../actions/product';
import NoImage from '../../../assets/no_img.png';
import Slider from 'react-slick';
import { Modal } from 'rsuite';
import IconArrowLeft from '../../../components/icon/arrow/IconArrowLeft';
import IconArrowRight from '../../../components/icon/arrow/IconArrowRight';
import IconLocationSolid from '../../../components/icon/location/IconLocationSolid';
import { combineAddress } from '../../../utils';
import IconClock from '../../../components/icon/clock/IconClock';
import { useNavigate } from 'react-router-dom';
import { stepAppointment } from '../../../utils/stepAppointment';
import ProductCard from './ProductCard';

const ProductStyles = styled.div``;

const Product = () => {
  const dispatch = useDispatch();

  let products = null;

  const { product_layout: productLayout } = useSelector(
    (state) => state.homeReducers.home
  );

  const navigate = useNavigate();
  const [params, setParams] = useState({
    search: '',
    page: 1,
    limit: 20,
    province: '',
  });
  const [indexActive, setIndexActive] = useState(0);
  const [openModal, setOpenModal] = useState(false);
  const [productSelected, setProductSelected] = useState();

  const [openModalAppointment, setOpenModalAppointment] = useState(false);

  const handleShowModal = (product) => {
    setOpenModal(true);
    setProductSelected(product);
  };
  const handleCloseModal = () => {
    setOpenModal(false);
    setProductSelected();
  };

  //Handle show modal appointment
  const handleShowModalAppointment = (product) => {
    setOpenModalAppointment(true);
    setProductSelected(product);
  };
  //Handle close modal appointment
  const handleCloseModalAppointment = (product) => {
    setOpenModalAppointment(false);
    setProductSelected(product);
  };

  const queryParams = (page, limit, search, province, latitude, longitude) => {
    return `page=${page}${limit ? `&limit=${limit}` : ''}${
      search ? `&search=${search}` : ''
    }${province ? `&province=${province}` : ''}`;
  };

  //Custom Prev, Next
  function NextArrow(props) {
    const { onClick } = props;
    return (
      <div
        onClick={onClick}
        className="absolute right-0 flex items-center justify-center text-gray-400  border-2 border-gray-300 rounded-full w-9 h-9 top-1/2 bg-[#fff] cursor-pointer -translate-y-1/2 z-10 hover:border-gray-400 transition-all"
      >
        <IconArrowRight className="w-6 h-6"></IconArrowRight>
      </div>
    );
  }

  function PrevArrow(props) {
    const { onClick } = props;
    return (
      <div
        onClick={onClick}
        className="absolute left-0 flex items-center justify-center text-gray-400 border-2 border-gray-300 rounded-full w-9 h-9 top-1/2 bg-[#fff] cursor-pointer -translate-y-1/2 z-10 hover:border-gray-400 transition-all"
      >
        <IconArrowLeft className="w-6 h-6"></IconArrowLeft>
      </div>
    );
  }
  const imageProductSetting = {
    infinite: productSelected?.images?.length > 1,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };

  const productSetting = {
    infinite: products?.data?.length > 4,
    speed: 800,
    slidesToShow: 3,
    slidesToScroll: 1,
    lazyLoad: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,

    beforeChange: (oldIndex, newIndex) => {
      setIndexActive(newIndex);
    },
    customPaging: (i) => {
      return (
        <div className="pt-5">
          <div
            className={`w-5 h-1 rounded-full ${
              i === indexActive ? 'bg-main' : 'bg-[rgba(38,166,154,0.3)]'
            }`}
          ></div>
        </div>
      );
    },
    responsive: [
      {
        breakpoint: 768,
        settings: {
          infinite: products?.data?.length > 2,
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 500,
        settings: {
          infinite: products?.data?.length > 1,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  const newProductSetting = {
    infinite: products?.data?.length > 4,
    speed: 800,
    slidesToShow: 4,
    slidesToScroll: 1,
    lazyLoad: true,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,

    beforeChange: (oldIndex, newIndex) => {
      setIndexActive(newIndex);
    },
    customPaging: (i) => {
      return (
        <div className="pt-5">
          <div
            className={`w-5 h-1 rounded-full ${
              i === indexActive ? 'bg-main' : 'bg-[rgba(38,166,154,0.3)]'
            }`}
          ></div>
        </div>
      );
    },
    responsive: [
      {
        breakpoint: 768,
        settings: {
          infinite: products?.data?.length > 2,
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 500,
        settings: {
          infinite: products?.data?.length > 1,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  useEffect(() => {
    const query = queryParams(
      params.page,
      params.limit,
      params.search,
      params.province
    );
    dispatch(productAction.getProducts(query));
  }, [dispatch, params.limit, params.page, params.province, params.search]);

  return (
    <ProductStyles>
      <div className="containerWidthFull ">
        <div className="mt-[200px]">
          <div className="mb-10 text-center">
            <h3 className="mb-2 text-6xl font-semibold text-main">
              BEST SELLER
            </h3>
          </div>
          <div className="list__products">
            {productLayout?.best_seller?.length > 0 && (
              <Slider {...productSetting}>
                {productLayout?.best_seller?.map((product) => (
                  <ProductCard
                    product={product}
                    key={product.id}
                    handleShowModal={handleShowModal}
                    handleShowModalAppointment={handleShowModalAppointment}
                  />
                ))}
              </Slider>
            )}
          </div>
        </div>
        <div className="mt-[200px]">
          <div className="mb-10 text-center">
            <h3 className="mb-2 text-6xl font-semibold text-main">
              WHAT'S NEW?
            </h3>
          </div>
          <div className="list__products">
            {productLayout?.what_news?.length > 0 && (
              <Slider {...newProductSetting}>
                {productLayout?.what_news?.map((product) => (
                  <ProductCard
                    product={product}
                    key={product.id}
                    handleShowModal={handleShowModal}
                    handleShowModalAppointment={handleShowModalAppointment}
                  />
                ))}
              </Slider>
            )}
          </div>
        </div>
        <div className="mt-[200px]">
          <div className="mb-10 text-center">
            <h3 className="mb-2 text-6xl font-semibold text-main">
              STYLE PICK!
            </h3>
          </div>
          <div className="list__products">
            {productLayout?.best_seller?.length > 0 && (
              <Slider {...productSetting}>
                {productLayout?.best_seller?.map((product) => (
                  <ProductCard
                    product={product}
                    key={product.id}
                    handleShowModal={handleShowModal}
                    handleShowModalAppointment={handleShowModalAppointment}
                  />
                ))}
              </Slider>
            )}
          </div>
        </div>
      </div>
    </ProductStyles>
  );
};

export default Product;
