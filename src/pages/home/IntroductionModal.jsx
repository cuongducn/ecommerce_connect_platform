import React from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Modal } from 'rsuite';
import styled from 'styled-components';
import * as Types from '../../constants/actionType';
import DoctorBackground from '../../assets/doctor_background.jpg';
import { store } from '../../utils/store';
import GooglePlay from '../../assets/googlePlay.png';
import AppStore from '../../assets/appStore.png';

const IntroductionModalStyles = styled.div``;

const IntroductionModal = () => {
  const dispatch = useDispatch();
  const { isShowPopUp } = useSelector((state) => state.homeReducers);

  const handleCloseModal = () => {
    dispatch({
      type: Types.HOME_SHOW_POPUP,
      data: false,
    });
  };
  useEffect(() => {
    if (isShowPopUp === null) {
      dispatch({
        type: Types.HOME_SHOW_POPUP,
        data: true,
      });
    }
  }, []);
  return (
    <IntroductionModalStyles>
      {isShowPopUp && (
        <Modal
          size="sm"
          open={isShowPopUp}
          onClose={() => handleCloseModal()}
          className="p-0 modalHome w-[700px]"
        >
          <Modal.Header>
            <Modal.Title></Modal.Title>
          </Modal.Header>
          <div className="grid grid-cols-5">
            <div className="col-span-2">
              <img
                src={DoctorBackground}
                className="object-cover w-full h-full"
                alt="bg_doctor"
              />
            </div>
            <div className="col-span-3">
              <div className="flex flex-col items-center justify-center mb-8 mt-14 gap-y-1">
                <div className="text-2xl font-medium text-main">Asahaa</div>
                <div className="font-medium">Đặt lịch khám online</div>
                <div className="font-medium">
                  Chất lượng dịch vụ hàng đầu Việt Nam
                </div>
                <div className="mt-2">
                  <img
                    src="https://ivie.vn/_next/static/img_qr_app.svg"
                    alt="qrcode"
                  />
                </div>
                <div>
                  <div className="flex items-center mt-2 gap-x-2">
                    <a
                      href={store.googlePlay_url}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <img
                        src={GooglePlay}
                        alt="google_play"
                        className="h-10 w-[122px]"
                      />
                    </a>
                    <a
                      href={store.appStore_url}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <img
                        src={AppStore}
                        alt="app_store"
                        className="h-10 w-[122px]"
                      />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      )}
    </IntroductionModalStyles>
  );
};

export default IntroductionModal;
