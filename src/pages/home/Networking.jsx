import React from 'react';
import { v4 } from 'uuid';
import CountUp from 'react-countup';
import VisibilitySensor from 'react-visibility-sensor';
const networking = [
  {
    id: v4(),
    name: 'Tỉnh thành phố',
    count: 56,
  },
  {
    id: v4(),
    name: 'Bác sĩ',
    count: 1250,
  },
  {
    id: v4(),
    name: ' Bệnh viện phòng khám',
    count: 372,
  },
  {
    id: v4(),
    name: ' Trung tâm xét nghiệm',
    count: 280,
  },
  {
    id: v4(),
    name: 'Điều dưỡng',
    count: 476,
  },
];
const Networking = () => {
  return (
    <section className="pt-20">
      <div className="containerWidth">
        <div className="flex flex-col-reverse gap-8 lg:flex-row">
          <div className="lg:w-[calc(100%-520px)] w-full">
            <p className="text-center font-medium text-main mb-[50px] sm:text-4xl text-2xl">
              Mạng lưới y tế Asahaa
            </p>
            <div>
              <ul className="flex flex-wrap items-center justify-center">
                {networking.map((network) => (
                  <li
                    className="flex flex-col mb-[50px] w-1/3"
                    key={network.id}
                  >
                    <span className="h-[90px] w-[90px] sm:h-[115px] sm:w-[115px] rounded-full flex justify-center items-center text-[#fff] bg-main sm:text-[36px] text-2xl mb-4 font-medium mx-auto ">
                      <VisibilitySensor partialVisibility>
                        {({ isVisible }) => (
                          <>
                            {isVisible ? (
                              <div>
                                {' '}
                                <CountUp end={network.count} />
                              </div>
                            ) : (
                              <div>0</div>
                            )}
                          </>
                        )}
                      </VisibilitySensor>
                    </span>
                    <span className="text-[13px] font-semibold text-center uppercase sm:text-[15px] line-clamp-1">
                      {network.name}
                    </span>
                  </li>
                ))}
              </ul>
              <ul className="home-top-network-normal"></ul>
            </div>
          </div>
          <div className="w-[360px] sm:w-[480px] mx-auto lg:mx-0">
            <img
              decoding="async"
              src="https://aihealth.vn/app/themes/Aihealth/resources/assets/images/home_map.png"
              alt="mạng lưới Aihealth"
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Networking;
