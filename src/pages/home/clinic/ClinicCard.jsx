import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import NoImage from "../../../assets/no_img.png";
import IconClock from "../../../components/icon/clock/IconClock";
import IconLocationSolid from "../../../components/icon/location/IconLocationSolid";
import { combineAddress } from "../../../utils";
import { stepAppointment } from "../../../utils/stepAppointment";
import * as Types from "../../../constants/actionType";

const ClinicCardStyles = styled.div`
  box-shadow: 3px 3px 8px 1px rgba(38, 166, 154, 0.3);
  .slick-list {
    margin: 0 -15px;
    padding: 10px 0;
  }
  .slick-slide > div {
    padding: 0 15px;
  }
  .slick-track {
    margin: 0;
  }
  .roundedClinic {
    box-shadow: 0 0 0 0px rgba(255, 255, 0, 0.35);
    animation: roundedClinic 2.5s ease-in-out infinite;
  }
  @keyframes roundedClinic {
    0% {
      box-shadow: 0 0 0 0px rgba(255, 255, 0, 0.35);
    }
    50% {
      box-shadow: 0 0 0 10px rgba(255, 255, 0, 0.35);
    }
    100% {
      box-shadow: 0 0 0 0px rgba(255, 255, 0, 0.35);
    }
  }
`;

const ClinicCard = ({
  clinic,
  handleShowModal,
  handleShowModalAppointment,
}) => {
  const stepAppointmentParams = stepAppointment.getParamsPathName();

  return (
    <ClinicCardStyles className="flex flex-col card__item rounded-2xl">
      <div className="w-full h-[250px] overflow-hidden group cursor-pointer p-3">
        <div className="w-full h-full" onClick={() => handleShowModal(clinic)}>
          <img
            className="object-cover w-full h-full duration-300 rounded-2xl group-hover:scale-105"
            src={clinic?.images?.length > 0 ? clinic.images[0] : NoImage}
            loading="lazy"
            alt="phòng_khám"
          />
        </div>
      </div>
      <div className="flex flex-col justify-between flex-1 px-4 pt-2 pb-4">
        <div className="mb-2 text-xl font-medium clinic-name">
          <div className="hover:no-underline hover:text-bodyColor active:text-bodyColor focus:text-bodyColor line-clamp-1">
            {clinic.clinic_name}
          </div>
        </div>
        <div>
          <div className="flex text-sm gap-x-[6px] line-clamp-2">
            <IconLocationSolid className="w-5 h-5 text-[#35aaf8]" />
            <span>
              {combineAddress(
                clinic.address_detail,
                clinic.wards_name,
                clinic.district_name,
                clinic.province_name
              )}
            </span>
          </div>
          <div className="flex items-center text-sm gap-x-[6px] mt-1 mb-[6px]">
            <IconClock className="w-4 h-4 text-teal-400" />
            <span>
              {clinic.time_open} - {clinic.time_close}
            </span>
          </div>
          <div className="flex items-center justify-between mt-2">
            <div className="flex gap-x-3">
              <a
                className="bg-blue-700 text-[#fff] px-3 py-1 rounded-xl text-sm focus:no-underline hover:no-underline hover:text-[#fff]  focus:text-[#fff]"
                href={`tel:${clinic.phone_number_clinic}`}
              >
                Gọi ngay
              </a>
              {stepAppointmentParams.type ==
                Types.TYPE_MEDICAL_PRIVATE_CLINIC &&
              stepAppointmentParams.clinicId == clinic.id ? (
                <Link
                  className="bg-main text-[#fff] px-3 py-1 rounded-xl text-sm focus:no-underline hover:no-underline hover:text-[#fff]  focus:text-[#fff]"
                  to={`${stepAppointment.getPathName()}`}
                >
                  Đặt khám
                </Link>
              ) : stepAppointmentParams.type === "" ? (
                <Link
                  className="bg-main text-[#fff] px-3 py-1 rounded-xl text-sm focus:no-underline hover:no-underline hover:text-[#fff]  focus:text-[#fff]"
                  to={`/phong-kham/dat-hen?clinicId=${clinic.id}`}
                >
                  Đặt khám
                </Link>
              ) : (
                <button
                  className="bg-main text-[#fff] px-3 py-1 rounded-xl text-sm focus:no-underline hover:no-underline hover:text-[#fff]  focus:text-[#fff]"
                  onClick={() => handleShowModalAppointment(clinic)}
                >
                  Đặt khám
                </button>
              )}
            </div>
            {stepAppointmentParams.type == Types.TYPE_MEDICAL_PRIVATE_CLINIC &&
            stepAppointmentParams.clinicId == clinic.id ? (
              <div className="w-5 h-5 bg-yellow-300 rounded-full roundedClinic"></div>
            ) : null}
          </div>
        </div>
      </div>
    </ClinicCardStyles>
  );
};

export default ClinicCard;
