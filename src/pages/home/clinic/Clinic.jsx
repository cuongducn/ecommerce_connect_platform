import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import * as clinicAction from "../../../actions/clinic";
import NoImage from "../../../assets/no_img.png";
import ClinicCard from "./ClinicCard";
import Slider from "react-slick";
import { Modal } from "rsuite";
import IconArrowLeft from "../../../components/icon/arrow/IconArrowLeft";
import IconArrowRight from "../../../components/icon/arrow/IconArrowRight";
import IconLocationSolid from "../../../components/icon/location/IconLocationSolid";
import { combineAddress } from "../../../utils";
import IconClock from "../../../components/icon/clock/IconClock";
import { useNavigate } from "react-router-dom";
import { stepAppointment } from "../../../utils/stepAppointment";

const ClinicStyles = styled.div``;

const Clinic = () => {
  const dispatch = useDispatch();
  const { clinics } = useSelector((state) => state.clinicReducers);

  const navigate = useNavigate();
  const [params, setParams] = useState({
    search: "",
    page: 1,
    limit: 9,
    province: "",
    latitude: "",
    longitude: "",
  });
  const [indexActive, setIndexActive] = useState(0);
  const [openModal, setOpenModal] = useState(false);
  const [clinicSelected, setClinicSelected] = useState();

  const [openModalAppointment, setOpenModalAppointment] = useState(false);

  const handleShowModal = (clinic) => {
    setOpenModal(true);
    setClinicSelected(clinic);
  };
  const handleCloseModal = () => {
    setOpenModal(false);
    setClinicSelected();
  };

  //Handle show modal appointment
  const handleShowModalAppointment = (clinic) => {
    setOpenModalAppointment(true);
    setClinicSelected(clinic);
  };
  //Handle close modal appointment
  const handleCloseModalAppointment = (clinic) => {
    setOpenModalAppointment(false);
    setClinicSelected(clinic);
  };
  const handleTypeAppointment = (type) => {
    if (type === "old") {
      const location = stepAppointment.getPathName();
      navigate(location);
    } else {
      navigate(`/phong-kham/dat-hen?clinicId=${clinicSelected.id}`);
    }
  };

  const queryParams = (page, limit, search, province, latitude, longitude) => {
    return `page=${page}${limit ? `&limit=${limit}` : ""}${
      search ? `&search=${search}` : ""
    }${province ? `&province=${province}` : ""}${
      latitude && longitude
        ? `&latitude=${latitude}&longitude=${longitude}`
        : ""
    }`;
  };

  //Custom Prev, Next
  function NextArrow(props) {
    const { onClick } = props;
    return (
      <div
        onClick={onClick}
        className="absolute right-0 flex items-center justify-center text-gray-400  border-2 border-gray-300 rounded-full w-9 h-9 top-1/2 bg-[#fff] cursor-pointer -translate-y-1/2 z-10 hover:border-gray-400 transition-all"
      >
        <IconArrowRight className="w-6 h-6"></IconArrowRight>
      </div>
    );
  }

  function PrevArrow(props) {
    const { onClick } = props;
    return (
      <div
        onClick={onClick}
        className="absolute left-0 flex items-center justify-center text-gray-400 border-2 border-gray-300 rounded-full w-9 h-9 top-1/2 bg-[#fff] cursor-pointer -translate-y-1/2 z-10 hover:border-gray-400 transition-all"
      >
        <IconArrowLeft className="w-6 h-6"></IconArrowLeft>
      </div>
    );
  }
  const imageClinicSetting = {
    infinite: clinicSelected?.images?.length > 1,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };

  const clinicSetting = {
    dots: true,
    infinite: clinics?.data?.length > 3,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,

    beforeChange: (oldIndex, newIndex) => {
      setIndexActive(newIndex);
    },
    customPaging: (i) => {
      return (
        <div className="pt-5">
          <div
            className={`w-5 h-1 rounded-full ${
              i === indexActive ? "bg-main" : "bg-[rgba(38,166,154,0.3)]"
            }`}
          ></div>
        </div>
      );
    },
    responsive: [
      {
        breakpoint: 768,
        settings: {
          infinite: clinics?.data?.length > 2,
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 500,
        settings: {
          infinite: clinics?.data?.length > 1,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  useEffect(() => {
    const query = queryParams(
      params.page,
      params.limit,
      params.search,
      params.province,
      params.latitude,
      params.longitude
    );
    dispatch(clinicAction.getClinics(query));
  }, [
    dispatch,
    params.latitude,
    params.limit,
    params.longitude,
    params.page,
    params.province,
    params.search,
  ]);
  return (
    <ClinicStyles>
      <div className="containerWidth">
        <div className="mt-[50px]">
          <div className="mb-10 text-center">
            <h3 className="mb-2 text-2xl font-semibold text-main">
              Khám phá các phòng khám
            </h3>
          </div>
          <div className="list__clinics">
            {clinics?.data?.length > 0 && (
              <Slider {...clinicSetting}>
                {clinics?.data?.map((clinic) => (
                  <ClinicCard
                    clinic={clinic}
                    key={clinic.id}
                    handleShowModal={handleShowModal}
                    handleShowModalAppointment={handleShowModalAppointment}
                  />
                ))}
              </Slider>
            )}
          </div>
          {clinicSelected && (
            <Modal size="sm" open={openModal} onClose={handleCloseModal}>
              <Modal.Header>
                <Modal.Title>
                  <div className="text-xl font-medium text-center">
                    Thông tin phòng khám
                  </div>
                </Modal.Title>
              </Modal.Header>
              <Modal.Body
                style={
                  {
                    // overflow: "visible",
                  }
                }
              >
                <div>
                  {clinicSelected?.images?.length > 0 ? (
                    <Slider {...imageClinicSetting}>
                      {clinicSelected?.images.map((url, index) => (
                        <div key={index} className="h-[300px] rounded-3xl">
                          <img
                            src={url}
                            alt="clinic_image"
                            className="object-cover w-full h-full rounded-3xl"
                          />
                        </div>
                      ))}
                    </Slider>
                  ) : (
                    <div className="h-[300px] rounded-3xl">
                      <img
                        src={NoImage}
                        alt="clinic_image"
                        className="object-cover w-full h-full rounded-3xl"
                      />
                    </div>
                  )}
                </div>
                <div className="flex flex-col justify-between flex-1 pt-2 pb-4">
                  <div className="mb-2 text-2xl font-medium clinic-name">
                    {clinicSelected.clinic_name}
                  </div>
                  <div className="flex flex-col">
                    <div>
                      <div className="flex text-sm gap-x-[6px] ml-[-2px]">
                        <IconLocationSolid className="w-5 h-5 text-[#35aaf8]" />
                        <span>
                          {combineAddress(
                            clinicSelected.address_detail,
                            clinicSelected.wards_name,
                            clinicSelected.district_name,
                            clinicSelected.province_name
                          )}
                        </span>
                      </div>
                      <div className="flex items-center text-sm gap-x-[6px] mt-1 mb-[6px]">
                        <IconClock className="w-4 h-4 text-teal-400" />
                        <span>
                          {clinicSelected.time_open} -{" "}
                          {clinicSelected.time_close}
                        </span>
                      </div>
                      {clinicSelected.description ? (
                        <div className="mb-4 overflow-auto text-sm">
                          {clinicSelected.description}
                        </div>
                      ) : null}
                    </div>
                  </div>
                  <div className="fixed bottom-0 left-0 w-full px-4">
                    <div
                      className="bg-gradient-to-br to-[#fff432] from-main rounded-lg px-4 py-[10px] text-[#fff] mb-3 text-center cursor-pointer text-lg font-medium transition-all"
                      onClick={() =>
                        navigate(
                          `/phong-kham/dat-hen?clinicId=${clinicSelected.id}`
                        )
                      }
                    >
                      Đặt khám
                    </div>
                  </div>
                </div>
              </Modal.Body>
            </Modal>
          )}
          {clinicSelected && (
            <Modal
              size="xs"
              open={openModalAppointment}
              onClose={handleCloseModalAppointment}
            >
              <Modal.Header>
                <Modal.Title>
                  <div className="text-xl font-medium text-center">
                    Lựa chọn loại thao tác đặt lịch
                  </div>
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <div>
                  <p className="text-center text-gray4B w-[70%] mx-auto">
                    Vui lòng cho biết bạn muốn lựa chọn loại đặt lịch
                  </p>
                  <div className="flex flex-col items-center justify-center mt-4 gap-y-2">
                    <button
                      className="px-4 py-2 bg-[#3471eb] text-[#fff] rounded-2xl flex items-center gap-x-2 w-[220px] justify-center  hover:scale-105 duration-300"
                      onClick={() => handleTypeAppointment("old")}
                    >
                      <span>Đặt lịch khám cũ</span>
                    </button>
                    <div className="w-[200px] h-[2px] bg-gray-200 my-3 relative">
                      <span className="absolute top-0 text-xs -translate-x-1/2 -translate-y-1/2 left-1/2 px-2 bg-[#fff] font-medium">
                        OR
                      </span>
                    </div>
                    <button
                      className="px-4 py-2 bg-main text-[#fff] rounded-2xl flex items-center justify-center gap-x-2 w-[220px]  hover:scale-105 duration-300"
                      onClick={() => handleTypeAppointment("new")}
                    >
                      <span>Đặt lịch khám mới</span>
                    </button>
                  </div>
                </div>
              </Modal.Body>
            </Modal>
          )}
        </div>
      </div>
    </ClinicStyles>
  );
};

export default Clinic;
