import React from "react";
import styled from "styled-components";
import Slider from "react-slick";
import { useState } from "react";
import { v4 } from "uuid";

const OpinionStyles = styled.div`
  .slick-list {
    margin: 0 -10px;
    padding: 10px 0;
  }
  .slick-slide > div {
    padding: 0 10px;
  }

  .slick-track {
    margin: 0;
  }
`;

const opinions = [
  {
    id: v4(),
    url: "https://i.pinimg.com/736x/d5/a4/01/d5a4019042a4f9e6517df77489ff8a74.jpg",
    title: "Hà Trang",
    content:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fuga nostrum praesentium, impedit repellat reiciendis aliquam",
  },
  {
    id: v4(),
    url: "https://i.zoomtventertainment.com/story/Men_beauty_trends.jpg",
    title: "Đức Trần",
    content:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fuga nostrum praesentium, impedit repellat reiciendis aliquam",
  },
  {
    id: v4(),
    url: "https://i.pinimg.com/originals/87/f2/6f/87f26f03b9326f2cd85d9399ed403ab5.jpg",
    title: "Đoan Trang",
    content:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fuga nostrum praesentium, impedit repellat reiciendis aliquam",
  },
  {
    id: v4(),
    url: "https://img.freepik.com/free-photo/beautiful-girl-stands-near-walll-with-leaves_8353-5377.jpg",
    title: "Linh Nguyễn",
    content:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fuga nostrum praesentium, impedit repellat reiciendis aliquam",
  },
];

const Opinion = () => {
  const [indexActive, setIndexActive] = useState(0);

  const opinionSetting = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    pauseOnHover: true,

    beforeChange: (oldIndex, newIndex) => {
      setIndexActive(newIndex);
    },
    customPaging: (i) => {
      return (
        <div className="pt-5">
          <div
            className={`w-5 h-1 rounded-full ${
              i === indexActive ? "bg-main" : "bg-[rgba(38,166,154,0.3)]"
            }`}
          ></div>
        </div>
      );
    },
    responsive: [
      {
        breakpoint: 920,
        settings: {
          infinite: true,
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 450,
        settings: {
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return (
    <OpinionStyles className="pt-5 bg-slate-50 pb-14 mt-[50px]">
      <div className="containerWidth">
        <div className="">
          <div className="mb-10 text-center">
            <h3 className="mb-2 text-2xl font-semibold text-main">
              Cảm nhận từ phía bệnh nhân
            </h3>
          </div>
          <Slider {...opinionSetting}>
            {opinions.map((opinion) => (
              <div
                key={opinion.id}
                className="flex flex-col items-center justify-center text-center"
              >
                <div className="w-[100px] h-[100px] rounded-full border-4 border-main mx-auto">
                  <img
                    src={opinion.url}
                    alt={opinion.title}
                    className="object-cover w-full h-full rounded-full"
                  />
                </div>
                <div className="my-5 text-2xl font-semibold">
                  {opinion.title}
                </div>
                <div className="text-sm text-gray-400">{opinion.content}</div>
              </div>
            ))}
          </Slider>
        </div>
      </div>
    </OpinionStyles>
  );
};

export default Opinion;
