import React, { useEffect } from 'react';
import styled from 'styled-components';

const IntroductionStyles = styled.div`
  .slick-list {
    margin: 0 -10px;
    padding: 10px 0;
  }
  .slick-slide > div {
    padding: 0 10px;
  }

  .slick-track {
    margin: 0;
  }
`;

const Introduction = () => {
  return (
    <IntroductionStyles className="pt-5 mt-[50px] bg-slate-50 pb-14 bg-[url(https://donggia.vn/wp-content/uploads/2019/11/thiet-ke-noi-that-phong-khach-chung-cu-dep-2020-1.jpg)] bg-cover bg-fixed min-h-[520px] md:min-h-[400px] lg:min-h-[300px] relative">
      <div className="absolute top-0 w-full h-full bg-[rgba(0,0,0,0.6)]"></div>
      <div className="absolute z-10 w-full h-full">
        <div className="containerWidth">
          <div className="text-[#fff]">
            <div className="mb-10 text-center">
              <h3 className="mb-2 text-2xl font-semibold text-">
                Asahaa - Đặt lịch khám bệnh
              </h3>
            </div>
            <div className="text-center">
              <div className="inline-block mr-2 text-main">
                <i className="text-2xl fa fa-quote-left"></i>
              </div>
              <span className="leading-7 text-slate-200 introduction__content">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam
                modi laborum dolores. Eum cumque ut libero, laudantium enim
                incidunt eveniet exercitationem ipsam, laboriosam quos
                distinctio, debitis inventore? Fugit nisi ullam sapiente vel
                qui, repellendus facilis sint quos commodi iste laborum alias!
                Cum dignissimos sed, saepe quos voluptatum iure impedit unde
                non, rerum quo atque. Tempore quos quaerat libero ea sequi a
                sunt, maiores vitae omnis iure ipsum ducimus sint nihil adipisci
                incidunt natus. Aliquid culpa numquam nesciunt, doloremque
                voluptatum dolores molestias magnam possimus fugiat temporibus
                expedita deleniti tenetur autem ab, qui praesentium quidem
                voluptates.
              </span>
              <div className="inline-block ml-2 text-main">
                <i className="text-2xl fa fa-quote-right"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </IntroductionStyles>
  );
};

export default Introduction;
