import React from 'react';
import GooglePlay from '../../assets/googlePlay.png';
import AppStore from '../../assets/appStore.png';
import { store } from '../../utils/store';
import IconArrowLeft from '../../components/icon/arrow/IconArrowLeft';
import IconArrowRight from '../../components/icon/arrow/IconArrowRight';
import Slider from 'react-slick';
import { useSelector } from 'react-redux';

const Banner = () => {
  const { slide: slide = [] } = useSelector((state) => state.homeReducers.home);

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,

    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };

  //Custom Prev, Next
  function NextArrow(props) {
    const { onClick } = props;
    return (
      <div
        onClick={onClick}
        className="absolute right-4 flex items-center justify-center text-white border-gray-600 rounded-full w-11 h-11 top-1/2 bg-transparent cursor-pointer -translate-y-1/2 z-10 hover:border-gray-200 transition-all"
      >
        <IconArrowRight className="w-11 h-11"></IconArrowRight>
      </div>
    );
  }

  function PrevArrow(props) {
    const { onClick } = props;
    return (
      <div
        onClick={onClick}
        className="absolute left-4 flex items-center justify-center text-white border-gray-600 rounded-full w-11 h-11 top-1/2 bg-transparent cursor-pointer -translate-y-1/2 z-10 hover:border-gray-200 transition-all"
      >
        <IconArrowLeft className="w-11 h-11"></IconArrowLeft>
      </div>
    );
  }

  return (
    <div className=" p-[0px] part-slider">
      <Slider {...settings} className="">
        {slide?.map((val, idx) => (
          <div key={idx}>
            <img
              className="w-full h-[calc(100vh-100px)]  object-cover"
              src={val.image + '?new-width=1918&image-type=webp'}
              alt=""
            />
          </div>
        ))}
      </Slider>
    </div>
  );
};

export default Banner;
