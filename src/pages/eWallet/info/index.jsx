import React from "react";
import { useRef } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Form, Modal, Schema } from "rsuite";
import StackItem from "rsuite/esm/Stack/StackItem";
import styled from "styled-components";
import { v4 } from "uuid";
import EWallet from "../../../assets/e-wallet.png";
import Input from "../../../components/form/Input";
import { formatNumber, isNumberString } from "../../../utils";
import * as profileActions from "../../../actions/profile";
import * as badgeAction from "../../../actions/badge";
import { useEffect } from "react";
const accounts = [
  {
    id: v4(),
    name: "Hệ thống",
    label: "he-thong",
  },
  {
    id: v4(),
    name: "Cá nhân",
    label: "ca-nhan",
  },
];
const { StringType } = Schema.Types;
const model = Schema.Model({});

const InformationWalletStyles = styled.div`
  .wallet__item {
    box-shadow: 0px 0px 8px 1px rgba(0, 0, 0, 0.08);
  }
`;

const InformationWallet = () => {
  const dispatch = useDispatch();
  const { badges } = useSelector((state) => state.badgeReducers);
  const { profile } = useSelector((state) => state.profileReducers);
  const navigate = useNavigate();
  const [tabActive, setTabActive] = useState("he-thong");
  const [openModal, setOpenModal] = useState();
  const formRef = useRef();
  const [formValue, setFormValue] = useState({
    name: "",
    bank_account_name: "",
    bank_account_number: "",
    bank_name: "",
    number_identify_card: "",
  });

  // Handle Change Value Form
  const handleChangeValueForm = (values, e) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormValue({
      ...values,
      [name]:
        name === "number_identify_card" || name === "bank_account_number"
          ? isNumberString(value)
          : value,
    });
  };
  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleChangeTab = (label) => {
    setTabActive(label);
  };
  const handleConfirm = () => {
    if (!formRef.current.check()) {
      return;
    }
    dispatch(
      profileActions.updateProfile(formValue, () => {
        dispatch(profileActions.getProfile());
        setOpenModal(false);
      })
    );
  };

  useEffect(() => {
    dispatch(badgeAction.getBadges());
  }, [dispatch]);
  useEffect(() => {
    if (Object.entries(profile)?.length > 0) {
      setFormValue({
        name: profile.name,
        bank_account_name: profile.bank_account_name,
        bank_account_number: profile.bank_account_number,
        bank_name: profile.bank_name,
        number_identify_card: profile.number_identify_card,
      });
    }
  }, [profile]);
  return (
    <InformationWalletStyles>
      <div className="flex flex-col gap-y-8">
        <div>
          <div className="flex justify-between">
            <h4 className="mb-4 text-xl font-medium">
              Thông tin ví khách hàng
            </h4>
            <div className="flex gap-x-3">
              <button
                className="py-2 px-4 rounded-lg hover:bg-main hover:text-[#fff] border cursor-pointer border-main text-main transition-all bg-transparent "
                onClick={() =>
                  navigate("/vi-khach-hang/lich-su-nap-rut/rut-vi")
                }
              >
                Rút tiền
              </button>
              <button
                className="py-2 px-4 rounded-lg bg-main text-[#fff] border border-transparent cursor-pointer hover:border-main hover:bg-transparent hover:text-main transition-all"
                onClick={() =>
                  navigate("/vi-khach-hang/lich-su-nap-rut/nap-vi")
                }
              >
                Nạp tiền
              </button>
            </div>
          </div>
          <div className=" rounded-lg bg-gradient-to-br to-[#ece67a] from-main w-[350px] flex flex-col items-center py-4 mx-auto">
            <div className="w-[74px] h-[74px]">
              <img className="w-full h-full" src={EWallet} alt="e-wallet" />
            </div>
            <div className="font-medium text-slate-100">Số dư</div>
            <div className="text-3xl font-medium text-[#fff]">
              {badges?.e_wallet_customer?.account_balance
                ? `${formatNumber(
                    badges?.e_wallet_customer?.account_balance
                  )} đ`
                : "0 đ"}
            </div>
          </div>
        </div>
        <div className="w-full">
          <div className="mb-5 text-sm font-medium text-center text-gray-500 border-b border-gray-200">
            <ul className="flex flex-wrap -mb-px">
              {accounts.map((account) => (
                <li className="mr-2" key={account.id}>
                  <div
                    className={`inline-block p-4 border-b-2 rounded-t-lg cursor-pointer ${
                      account.label === tabActive
                        ? "text-blue-600 border-blue-600 active "
                        : "hover:text-gray-600 hover:border-gray-300 border-transparent"
                    }`}
                    onClick={() => handleChangeTab(account.label)}
                  >
                    {account.name}
                  </div>
                </li>
              ))}
            </ul>
          </div>
          {tabActive === "he-thong" ? (
            <div className="w-full p-4 rounded-lg wallet__item">
              <h5 className="my-2 font-medium">Tài khoản ngân hàng</h5>
              <div className="flex justify-between">
                <span>Tên người thụ hưởng:</span>
                <span>{badges?.admin_contact?.bank_account_name}</span>
              </div>
              <div className="flex justify-between">
                <span>Số tài khoản:</span>
                <span>{badges?.admin_contact?.bank_account_number}</span>
              </div>
              <div className="flex justify-between">
                <span>Tên ngân hàng:</span>
                <span>{badges?.admin_contact?.bank_name}</span>
              </div>
              <h5 className="my-2 font-medium">Thông tin liên lạc</h5>
              <div className="flex justify-between">
                <span>Hotline:</span>
                <span>{badges?.admin_contact?.hotline}</span>
              </div>
              <div className="flex justify-between">
                <span>Email:</span>
                <span>{badges?.admin_contact?.email}</span>
              </div>
              <div className="flex justify-between">
                <span>Facebook:</span>
                <span>{badges?.admin_contact?.facebook}</span>
              </div>
            </div>
          ) : (
            <div className="w-full p-4 rounded-lg wallet__item">
              <div className="flex items-center justify-between my-2">
                <h5 className="font-medium">Tài khoản ngân hàng</h5>
                <button
                  className="py-2 px-4 rounded-lg bg-main text-[#fff]"
                  onClick={() => setOpenModal(true)}
                >
                  Cập nhập
                </button>
              </div>
              <div className="flex justify-between">
                <span>Tên người thụ hưởng:</span>
                <span>{profile.bank_account_name}</span>
              </div>
              <div className="flex justify-between">
                <span>Số tài khoản:</span>
                <span>{profile.bank_account_number}</span>
              </div>
              <div className="flex justify-between">
                <span>Tên ngân hàng:</span>
                <span>{profile.bank_name}</span>
              </div>
              <h5 className="my-2 font-medium">Thông tin liên lạc</h5>
              <div className="flex justify-between">
                <span>Họ và tên:</span>
                <span>{profile.name}</span>
              </div>

              <div className="flex justify-between">
                <span>CMND/CCCD:</span>
                <span>{profile.number_identify_card}</span>
              </div>
            </div>
          )}
        </div>
      </div>
      <Modal size="xs" open={openModal} onClose={handleCloseModal}>
        <Modal.Header>
          <Modal.Title>
            <div className="text-xl font-medium text-center">
              Cập nhập thông tin
            </div>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <div className="my-3">
              <Form
                fluid
                checkTrigger="change"
                ref={formRef}
                onChange={handleChangeValueForm}
                formValue={formValue}
                model={model}
              >
                <div className="flex flex-col gap-y-3">
                  <StackItem grow={1} className="profile__item">
                    <Input
                      name="name"
                      label="Họ và tên"
                      placeholder="Nhập họ và tên..."
                    />
                  </StackItem>
                  <StackItem grow={1} className="profile__item">
                    <Input
                      name="bank_account_name"
                      label="Tên tài khoản"
                      placeholder="Nhập tài khoản..."
                    />
                  </StackItem>
                  <StackItem grow={1} className="profile__item">
                    <Input
                      name="bank_account_number"
                      label="Số tài khoản"
                      placeholder="Nhập số khoản..."
                    />
                  </StackItem>
                  <StackItem grow={1} className="profile__item">
                    <Input
                      name="bank_name"
                      label="Tên ngân hàng"
                      placeholder="Nhập tên ngân hàng..."
                    />
                  </StackItem>
                  <StackItem grow={1} className="profile__item">
                    <Input
                      name="number_identify_card"
                      label="CMND/CCCD"
                      placeholder="Nhập CMND/CCCD..."
                    />
                  </StackItem>
                </div>
              </Form>
            </div>
            <div className="flex justify-end mt-5 gap-x-3">
              <button
                className="w-[100px] py-1 border  transition-all rounded-lg bg-main text-[#fff]"
                onClick={handleConfirm}
              >
                Xác nhận
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </InformationWalletStyles>
  );
};

export default InformationWallet;
