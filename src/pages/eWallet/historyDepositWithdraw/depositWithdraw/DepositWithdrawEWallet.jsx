import React, { useEffect } from 'react';
import { useState } from 'react';
import { useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { Form, Schema, Stack } from 'rsuite';
import StackItem from 'rsuite/esm/Stack/StackItem';
import styled from 'styled-components';
import Input from '../../../../components/form/Input';
import IconArrowLeft from '../../../../components/icon/arrow/IconArrowLeft';
import { formatNumber } from '../../../../utils';
import * as eWalletAction from '../../../../actions/ewallet';
import UploadListImage from '../../../../components/form/UploadListImage';

const DepositWithdrawEWalletStyles = styled.div`
  .depositWithdraw_item {
    box-shadow: 1px 4px 15px 2px rgba(38, 166, 154, 0.2);
  }
`;

const { StringType } = Schema.Types;
const model = Schema.Model({
  money: StringType().isRequired('Vui lòng nhập số tiền muốn nạp rút.'),
});

const DepositWithdrawEWallet = () => {
  const dispatch = useDispatch();
  const { badges } = useSelector((state) => state.badgeReducers);

  const formRef = useRef();
  const navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const paidNeed = searchParams.get('paid_need');
  const [formValue, setFormValue] = useState({
    note: '',
    money: '',
    images: [],
  });
  const [filesImage, setFilesImage] = useState([]);

  // Handle Change Value Form
  const handleChangeValueForm = (values, e) => {
    const name = e.target.name;
    const value = e.target.value;
    setFormValue({
      ...values,
      [name]: name === 'money' ? formatNumber(value) : value,
    });
  };

  const checkTypeEWalletDeposit = () => {
    const pathname = window.location.pathname;
    const type = pathname?.split('/').slice(-1)[0];
    return type === 'nap-vi' ? true : false;
  };
  const handleDepositWithdraw = () => {
    if (!formRef.current.check()) {
      return;
    }
    const newFormValue = {
      content_transfer: formValue.note,
      money: formValue.money?.toString()?.replace(/\./g, ''),
      is_withdrawal: checkTypeEWalletDeposit() === true ? false : true,
      images: filesImage,
    };

    dispatch(
      eWalletAction.requireCancelDepositWithdrawals(newFormValue, navigate)
    );
  };
  useEffect(() => {
    const rechargeEWallet = checkTypeEWalletDeposit();
    if (rechargeEWallet && Number(paidNeed) > 0) {
      const paidNeedFormat = formatNumber(paidNeed);
      setFormValue((prevFormValue) => ({
        ...prevFormValue,
        money: paidNeedFormat?.toString(),
      }));
    }
  }, [paidNeed]);

  const goBack = () => {
    navigate(-1);
  };

  return (
    <DepositWithdrawEWalletStyles>
      <div
        className="flex items-center mb-2 text-sm cursor-pointer text-main"
        onClick={goBack}
      >
        <IconArrowLeft className="w-4 h-4"></IconArrowLeft>
        <span>Quay lại</span>
      </div>
      <h4 className="mb-5 text-xl font-medium">
        Thông tin {checkTypeEWalletDeposit() === true ? 'nạp ví' : 'rút ví'}
      </h4>
      <div>
        <div className="text-center max-w-[350px] mx-auto text-main font-medium mb-5">
          Vui lòng chuyển khoản vào tài khoản ngân hàng sau để được nạp tiền vào
          tài khoản
        </div>
        <div className="flex flex-col gap-y-5">
          <div className="p-4 rounded-lg depositWithdraw_item">
            <div className="font-medium">Thông tin chuyển khoản</div>
            <div className="flex flex-col mt-2 gap-y-1">
              <div className="flex justify-between">
                <span>Tên ngân hàng</span>
                <span>{badges?.admin_contact?.bank_name}</span>
              </div>
              <div className="flex justify-between">
                <span>Số tài khoản</span>
                <span>{badges?.admin_contact?.bank_account_number}</span>
              </div>
              <div className="flex justify-between">
                <span>Tên người thụ hưởng</span>
                <span>{badges?.admin_contact?.bank_account_name}</span>
              </div>
            </div>
          </div>
          <div className="p-4 rounded-lg depositWithdraw_item">
            <div className="mt-2">
              <Form
                fluid
                checkTrigger="change"
                ref={formRef}
                onChange={handleChangeValueForm}
                formValue={formValue}
                model={model}
              >
                <Stack
                  spacing={30}
                  justifyContent="space-between"
                  className="mb-3"
                >
                  <div>
                    <div className="font-medium">
                      Thông tin{' '}
                      {checkTypeEWalletDeposit() === true
                        ? 'nạp tiền'
                        : 'rút tiền'}
                    </div>
                    <div className="text-sm text-red-500">
                      Nội dung chuyển khoản: SĐT + Asahaa + Số tiền
                    </div>
                  </div>
                  <button
                    className="py-2 px-4 rounded-lg bg-main text-[#fff]"
                    type="submit"
                    onClick={handleDepositWithdraw}
                    // loading={isLoadingProfile}
                  >
                    {checkTypeEWalletDeposit() === true
                      ? 'Nạp tiền'
                      : 'Rút tiền'}
                  </button>
                </Stack>

                <div className="flex flex-col gap-y-3">
                  <Stack
                    spacing={30}
                    justifyContent="space-between"
                    className="profile__main"
                  >
                    <StackItem grow={1}>
                      <Input
                        name="note"
                        label="Ghi chú"
                        placeholder="Ghi chú nội dung"
                      />
                    </StackItem>
                    <StackItem grow={1}>
                      <Input
                        name="money"
                        label={`Số tiền cần ${
                          checkTypeEWalletDeposit() === true ? 'nạp' : 'rút'
                        }`}
                        placeholder={`Nhập số tiền bạn muốn ${
                          checkTypeEWalletDeposit() === true ? 'nạp' : 'rút'
                        }`}
                        isRequired
                      />
                    </StackItem>
                  </Stack>
                  <div className="flex flex-col gap-y-4">
                    <label htmlFor="">Ảnh thanh toán (Tối đa 10 hình)</label>
                    <UploadListImage
                      style={{
                        width: '120px',
                        height: '120px',
                      }}
                      images={formValue.images}
                      setFiles={setFilesImage}
                    ></UploadListImage>
                  </div>
                </div>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </DepositWithdrawEWalletStyles>
  );
};

export default DepositWithdrawEWallet;
