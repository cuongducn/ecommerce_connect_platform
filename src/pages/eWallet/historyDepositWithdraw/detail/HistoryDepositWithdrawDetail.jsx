import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { Input, Modal } from "rsuite";
import styled from "styled-components";
import * as eWallet from "../../../../actions/ewallet";
import IconArrowLeft from "../../../../components/icon/arrow/IconArrowLeft";
import * as Types from "../../../../constants/actionType";
import { formatNumber } from "../../../../utils";

const HistoryDepositWithdrawStyles = styled.div`
  .historyDepositWithdrawDetail {
    box-shadow: 1px 4px 15px 2px rgba(38, 166, 154, 0.2);
  }
`;

const HistoryDepositWithdrawDetail = () => {
  const dispatch = useDispatch();
  const { depositWithdrawalsHistory } = useSelector(
    (state) => state.eWalletReducers
  );
  const { badges } = useSelector((state) => state.badgeReducers);

  const navigate = useNavigate();
  const params = useParams();
  const idHistory = params.id;

  const [openModalDelete, setOpenModalDelete] = useState();
  const [note, setNote] = useState("");

  const handleCancel = () => {
    const data = {
      note,
      is_withdrawal:
        depositWithdrawalsHistory?.is_withdrawal == true ? true : false,
    };
    dispatch(
      eWallet.cancelDepositWithdrawals(idHistory, data, () => {
        setOpenModalDelete(false);
        setNote("");
      })
    );
  };
  const handleChange = (value) => {
    setNote(value);
  };
  const goBack = () => {
    navigate(-1);
  };

  useEffect(() => {
    if (!idHistory) return;
    dispatch(eWallet.getDetailDepositWithdrawalsHistory(idHistory));
  }, [dispatch, idHistory]);
  return (
    <HistoryDepositWithdrawStyles>
      <div
        className="flex items-center mb-2 text-sm cursor-pointer text-main"
        onClick={goBack}
      >
        <IconArrowLeft className="w-4 h-4"></IconArrowLeft>
        <span>Quay lại</span>
      </div>
      <h4 className="text-xl font-medium">Chi tiết nạp rút</h4>
      <div className="flex flex-col mt-5 text-sm gap-y-5">
        <div className="overflow-hidden rounded-lg historyDepositWithdrawDetail">
          <div className="bg-gradient-to-br to-[#fff432] from-main px-4 py-2 text-[#fff] font-medium text-base">
            Thông tin nạp rút
          </div>
          <div className="p-4">
            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">
                Số tiền yêu cầu{" "}
                {depositWithdrawalsHistory?.is_withdrawal == true
                  ? " rút"
                  : " nạp"}{" "}
              </div>
              <div className="text-xl font-medium uppercase text-main">
                {depositWithdrawalsHistory?.money
                  ? `${formatNumber(depositWithdrawalsHistory?.money)} đ`
                  : "0 đ"}
              </div>
            </div>
            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Yêu cầu</div>
              <div className="font-medium">
                {depositWithdrawalsHistory?.is_withdrawal == true
                  ? " Rút tiền"
                  : " Nạp tiền"}
              </div>
            </div>
            <div className="flex items-center mt-3 gap-x-5">
              <div className="w-[25%]">Ảnh chuyển khoản</div>
              <div className="flex gap-5">
                {depositWithdrawalsHistory?.images?.length <= 0 &&
                  depositWithdrawalsHistory?.images?.map((image, index) => (
                    <div key={index} className="w-[250px]">
                      <img
                        className="object-cover w-full h-full rounded-md"
                        src={image}
                        alt={"images_des"}
                      />
                    </div>
                  ))}
              </div>
            </div>
          </div>
        </div>
        <div className="overflow-hidden rounded-lg historyDepositWithdrawDetail">
          <div className="bg-gradient-to-br to-[#fff432] from-main px-4 py-2 text-[#fff] font-medium text-base">
            Thông tin tài khoản admin
          </div>
          <div className="p-4">
            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Tên ngân hàng</div>
              <div className="text-xl font-medium uppercase">
                {badges?.admin_contact?.bank_name}
              </div>
            </div>

            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Số tài khoản</div>
              <div className="font-medium">
                {" "}
                {badges?.admin_contact?.bank_account_number}
              </div>
            </div>
            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Tên người thụ hưởng</div>
              <div className="font-medium">
                {" "}
                {badges?.admin_contact?.bank_account_name}
              </div>
            </div>
          </div>
        </div>
        <div className="overflow-hidden rounded-lg historyDepositWithdrawDetail">
          <div className="bg-gradient-to-br to-[#fff432] from-main px-4 py-2 text-[#fff] font-medium text-base">
            Thông tin tài khoản cá nhân
          </div>
          <div className="p-4">
            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Tên ngân hàng</div>
              <div className="text-xl font-medium uppercase">
                {badges?.customer?.bank_name}
              </div>
            </div>

            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Số tài khoản</div>
              <div className="font-medium">
                {" "}
                {badges?.customer?.bank_account_number}
              </div>
            </div>
            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Tên người thụ hưởng</div>
              <div className="font-medium">
                {" "}
                {badges?.customer?.bank_account_name}
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-center mt-5">
          {depositWithdrawalsHistory.status === Types.STATUS_PROGRESSING ? (
            <button
              className={`w-[200px] py-2 border transition-all
             border-red-500 bg-transparent text-red-500 hover:bg-red-500 hover:text-[#fff] rounded-lg`}
              onClick={() => setOpenModalDelete(true)}
            >
              Hủy
              {depositWithdrawalsHistory?.is_withdrawal == true
                ? " rút tiền"
                : " nạp tiền"}
            </button>
          ) : null}
          <Modal size="xs" open={openModalDelete} onClose={setOpenModalDelete}>
            <Modal.Header>
              <Modal.Title>
                <div className="text-xl font-medium text-center">
                  Hủy{" "}
                  {depositWithdrawalsHistory?.is_withdrawal == true
                    ? " rút tiền"
                    : " nạp tiền"}
                </div>
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div>
                <p className="mx-auto text-center text-gray4B">
                  Bạn có muốn hủy giao dịch
                  {depositWithdrawalsHistory?.is_withdrawal == true
                    ? " rút tiền "
                    : " nạp tiền "}
                  này không?
                </p>
                <div className="my-3">
                  <Input
                    placeholder="Nhập lý do"
                    value={note}
                    name="note"
                    onChange={handleChange}
                  ></Input>
                </div>
                <div className="flex justify-end mt-5 gap-x-3">
                  <button
                    className="w-[100px] py-1 border border-red-500 bg-transparent text-red-500 hover:bg-red-500 hover:text-[#fff] transition-all rounded-lg"
                    onClick={handleCancel}
                  >
                    Xác nhận
                  </button>
                </div>
              </div>
            </Modal.Body>
          </Modal>
        </div>
      </div>
    </HistoryDepositWithdrawStyles>
  );
};

export default HistoryDepositWithdrawDetail;
