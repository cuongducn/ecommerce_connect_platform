import React from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useSearchParams } from "react-router-dom";
import { Input, InputGroup, Modal } from "rsuite";
import SearchIcon from "@rsuite/icons/Search";
import styled from "styled-components";
import { useCallback } from "react";
import { debounce } from "lodash";
import { useEffect } from "react";
import * as eWalletAction from "../../../actions/ewallet";
import ReactPagination from "../../../components/pagination/ReactPagination";
import { formatDateStr } from "../../../utils/date";
import { formatNumber } from "../../../utils";
import * as Types from "../../../constants/actionType";

const HistoryDepositWithdrawStyles = styled.div`
  .historyDepositWithdraw {
    box-shadow: 1px 4px 15px 2px rgba(38, 166, 154, 0.2);
  }
`;

const HistoryDepositWithdraw = () => {
  const dispatch = useDispatch();
  const { listDepositWithdrawalsHistory } = useSelector(
    (state) => state.eWalletReducers
  );

  const [searchParams] = useSearchParams();
  const [searchInput, setSearchInput] = useState(
    searchParams.get("search") || ""
  );

  const navigate = useNavigate();
  const [params, setParams] = useState({
    page: searchParams.get("page") || 1,
    limit: searchParams.get("limit") || 6,
    search: searchParams.get("search") || "",
  });
  const [openModalDelete, setOpenModalDelete] = useState();
  const [note, setNote] = useState("");
  const [historySelected, setHistorySelected] = useState();

  const handleCancel = () => {
    const data = {
      note,
      is_withdrawal: historySelected?.is_withdrawal == true ? true : false,
    };
    const query = queryParams(params.page, params.limit, params.search);
    dispatch(
      eWalletAction.cancelDepositWithdrawals(
        historySelected?.id,
        data,
        query,
        () => {
          handleCloseModalCancel();
        }
      )
    );
  };
  const handleChange = (value) => {
    setNote(value);
  };
  const queryParams = (page, limit, search) => {
    return `page=${page}${limit ? `&limit=${limit}` : ""}${
      search ? `&search=${search}` : ""
    }`;
  };
  const handleSelectedPage = (event) => {
    setParams({
      ...params,
      page: Number(event.selected) + 1,
    });
    navigate(
      `/vi-khach-hang/lich-su-nap-rut?${queryParams(
        Number(event.selected) + 1,
        params.limit,
        params.search
      )}`
    );
  };

  //Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        return {
          ...prevParams,
          search: valueSearch,
          page: 1,
        };
      });
      navigate(
        `/vi-khach-hang/lich-su-so-du?${queryParams(
          1,
          params.limit,
          valueSearch
        )}`
      );
    }, 500),
    []
  );
  const handleSearch = (value) => {
    setSearchInput(value);
    debounceCallBack(value);
  };

  const handleTime = (date) => {
    const day = date ? formatDateStr(date, "DD") : "";
    const duringTime = date ? formatDateStr(date, "MM/YYYY HH:mm") : "";

    return (
      <div className="flex flex-col items-center transition-all duration-300 group-hover:scale-105">
        <div className="text-4xl font-semibold text-main">{day}</div>
        <div className="text-sm font-medium">{duringTime}</div>
      </div>
    );
  };
  const handleShowModalCancel = (history) => {
    setHistorySelected(history);
    setOpenModalDelete(true);
  };
  const handleCloseModalCancel = () => {
    setHistorySelected(null);
    setOpenModalDelete(false);
    setNote("");
  };

  useEffect(() => {
    const query = queryParams(params.page, params.limit, params.search);
    dispatch(eWalletAction.getListDepositWithdrawalsHistory(query));
  }, [dispatch, params.limit, params.page, params.search]);

  return (
    <HistoryDepositWithdrawStyles>
      <h4 className="mb-4 text-xl font-medium">Lịch sử nạp rút</h4>
      <div className="flex justify-between">
        <div className="max-w-[300px] mt-5">
          <InputGroup inside>
            <Input
              placeholder="Tìm kiếm lịch sử nạp rút"
              value={searchInput}
              onChange={handleSearch}
            />
            <InputGroup.Button>
              <SearchIcon />
            </InputGroup.Button>
          </InputGroup>
        </div>
        <div className="flex items-center gap-x-3">
          <button
            className="px-4 py-2 bg-transparent border rounded-lg border-main text-main hover:bg-main hover:text-[#fff] transition-all cursor-pointer"
            onClick={() => navigate("/vi-khach-hang/lich-su-nap-rut/rut-vi")}
          >
            Rút tiền
          </button>
          <button
            className="py-2 px-4 rounded-lg bg-main text-[#fff] border border-transparent cursor-pointer hover:border-main hover:bg-transparent hover:text-main transition-all"
            onClick={() => navigate("/vi-khach-hang/lich-su-nap-rut/nap-vi")}
          >
            Nạp tiền
          </button>
        </div>
      </div>
      <div className="grid grid-cols-1 mt-10 gap-y-5">
        {listDepositWithdrawalsHistory.data?.length > 0 &&
          listDepositWithdrawalsHistory.data.map((history) => (
            <div
              key={history.id}
              className="flex historyDepositWithdraw rounded-xl group"
              // onClick={() =>
              //   navigate(`/vi-khach-hang/lich-su-nap-rut/${history.id}`)
              // }
            >
              <div className="w-[130px] flex flex-shrink-0 justify-center items-center bg-yellow-50 rounded-xl">
                {handleTime(history?.created_at)}
              </div>
              <div className="w-full py-4">
                <div className="text-sm">
                  <span className={`px-4 flex justify-between`}>
                    <span> Yêu cầu:</span>
                    <span>
                      {history?.is_withdrawal == true
                        ? " Rút tiền"
                        : " Nạp tiền"}
                    </span>
                  </span>
                </div>
                <div className="px-4">
                  <div className="flex justify-between text-sm">
                    <span>Số tiền:</span>
                    <span className="font-medium">
                      {history?.money
                        ? `${formatNumber(history?.money)} đ`
                        : "0 đ"}
                    </span>
                  </div>
                  <div className="flex justify-between text-sm">
                    <span>Trạng thái:</span>
                    <span
                      className={`font-medium  ${
                        history.status === Types.STATUS_PROGRESSING
                          ? "text-orange-500"
                          : history.status === Types.STATUS_CANCEL
                          ? "text-red-500"
                          : history.status === Types.STATUS_COMPLETED
                          ? "text-green-500"
                          : history.status === Types.STATUS_DOCTOR_CONFIRMED
                          ? "text-blue-500"
                          : ""
                      }`}
                    >
                      {history.status === Types.STATUS_PROGRESSING
                        ? "Chờ xác nhận"
                        : history.status === Types.STATUS_CANCEL
                        ? "Đã hủy"
                        : history.status === Types.STATUS_COMPLETED
                        ? "Đã duyệt"
                        : ""}
                    </span>
                  </div>
                  <div className="flex justify-between text-sm">
                    <span>Nội dung chuyển khoản:</span>
                    <span className="font-medium">
                      {history?.content_transfer}
                    </span>
                  </div>
                  <div className="flex justify-between text-sm">
                    <span>Ghi chú từ hệ thống:</span>
                    <span className="font-medium">{history?.note}</span>
                  </div>
                  <div className="flex justify-end text-sm">
                    {history.status === Types.STATUS_PROGRESSING ? (
                      <div
                        className="w-[100px] h-[30px] border transition-all
             border-red-500 bg-transparent text-red-500 hover:bg-red-500 hover:text-[#fff] rounded-lg flex items-center justify-center cursor-pointer"
                        onClick={() => handleShowModalCancel(history)}
                      >
                        Hủy
                        {history?.is_withdrawal == true
                          ? " rút tiền"
                          : " nạp tiền"}
                      </div>
                    ) : (
                      <div className="h-[30px]"></div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          ))}
      </div>
      <Modal size="xs" open={openModalDelete} onClose={handleCloseModalCancel}>
        <Modal.Header>
          <Modal.Title>
            <div className="text-xl font-medium text-center">
              Hủy{" "}
              {historySelected?.is_withdrawal == true
                ? " rút tiền"
                : " nạp tiền"}
            </div>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <p className="mx-auto text-center text-gray4B">
              Bạn có muốn hủy giao dịch
              {historySelected?.is_withdrawal == true
                ? " rút tiền "
                : " nạp tiền "}
              này không?
            </p>
            <div className="my-3">
              <Input
                placeholder="Nhập lý do"
                value={note}
                name="note"
                onChange={handleChange}
              ></Input>
            </div>
            <div className="flex justify-end mt-5 gap-x-3">
              <button
                className="w-[100px] py-1 border border-red-500 bg-transparent text-red-500 hover:bg-red-500 hover:text-[#fff] transition-all rounded-lg"
                onClick={handleCancel}
              >
                Xác nhận
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      {listDepositWithdrawalsHistory?.data?.length > 0 &&
        listDepositWithdrawalsHistory.last_page > 1 && (
          <div className="flex justify-center mt-8 text-sm pagination__manage--actor">
            <ReactPagination
              pageCount={listDepositWithdrawalsHistory.last_page}
              handleClickPage={handleSelectedPage}
              marginPagesDisplayed={3}
              pageRangeDisplayed={3}
              page={params.page - 1}
            />
          </div>
        )}
    </HistoryDepositWithdrawStyles>
  );
};

export default HistoryDepositWithdraw;
