import React from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { Input, InputGroup } from 'rsuite';
import SearchIcon from '@rsuite/icons/Search';
import styled from 'styled-components';
import { useCallback } from 'react';
import { debounce } from 'lodash';
import { useEffect } from 'react';
import * as eWalletAction from '../../../actions/ewallet';
import ReactPagination from '../../../components/pagination/ReactPagination';
import { formatDateStr } from '../../../utils/date';
import { formatNumber } from '../../../utils';
import * as Types from '../../../constants/actionType';

const HistoryBalanceStyles = styled.div`
  .historyBalance {
    box-shadow: 1px 4px 15px 2px rgba(38, 166, 154, 0.2);
    .historyBalance__title {
      display: inline-block;
    }
  }
`;

const HistoryBalance = () => {
  const dispatch = useDispatch();
  const { eWalletHistory } = useSelector((state) => state.eWalletReducers);

  const [searchParams] = useSearchParams();
  const [searchInput, setSearchInput] = useState(
    searchParams.get('search') || ''
  );

  const navigate = useNavigate();
  const [params, setParams] = useState({
    page: searchParams.get('page') || 1,
    limit: searchParams.get('limit') || 6,
    search: searchParams.get('search') || '',
  });
  const queryParams = (page, limit, search) => {
    return `page=${page}${limit ? `&limit=${limit}` : ''}${
      search ? `&search=${search}` : ''
    }`;
  };
  const handleSelectedPage = (event) => {
    setParams({
      ...params,
      page: Number(event.selected) + 1,
    });
    navigate(
      `/vi-khach-hang/lich-su-so-du?${queryParams(
        Number(event.selected) + 1,
        params.limit,
        params.search
      )}`
    );
  };

  //Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        return {
          ...prevParams,
          search: valueSearch,
          page: 1,
        };
      });
      navigate(
        `/vi-khach-hang/lich-su-so-du?${queryParams(
          1,
          params.limit,
          valueSearch
        )}`
      );
    }, 500),
    []
  );
  const handleSearch = (value) => {
    setSearchInput(value);
    debounceCallBack(value);
  };

  const handleTime = (date) => {
    const day = date ? formatDateStr(date, 'DD') : '';
    const duringTime = date ? formatDateStr(date, 'MM/YYYY HH:mm') : '';

    return (
      <div className="flex flex-col items-center transition-all duration-300 group-hover:scale-105">
        <div className="text-4xl font-semibold text-main">{day}</div>
        <div className="text-sm font-medium">{duringTime}</div>
      </div>
    );
  };

  useEffect(() => {
    const query = queryParams(params.page, params.limit, params.search);
    dispatch(eWalletAction.getEWalletHistory(query));
  }, [dispatch, params.limit, params.page, params.search]);

  return (
    <HistoryBalanceStyles>
      <h4 className="mb-4 text-xl font-medium">Lịch sử số dư</h4>
      <div className="max-w-[300px] mt-5">
        <InputGroup inside>
          <Input
            placeholder="Tìm kiếm lịch sử số dư"
            value={searchInput}
            onChange={handleSearch}
          />
          <InputGroup.Button>
            <SearchIcon />
          </InputGroup.Button>
        </InputGroup>
      </div>
      <div className="grid grid-cols-1 mt-10 gap-y-5">
        {eWalletHistory.data?.length > 0 &&
          eWalletHistory.data.map((history) => (
            <div
              key={history.id}
              className="flex historyBalance rounded-xl group"
            >
              <div className="flex items-center justify-center flex-shrink-0 px-4 bg-yellow-50 rounded-xl w-[130px]">
                {handleTime(history?.created_at)}
              </div>
              <div className="w-full py-4">
                <div className="px-4 mb-2 font-medium ">
                  <div className={`historyBalance__title`}>
                    {history?.title}
                  </div>
                </div>
                <div className="px-4">
                  <div className="flex justify-between text-sm ">
                    <span>Số tiền trước đó:</span>
                    <span>
                      {history?.balance_origin
                        ? `${formatNumber(history?.balance_origin)} đ`
                        : '0'}
                    </span>
                  </div>
                  <div className="flex justify-between text-sm ">
                    <span> Số tiền thay đổi:</span>
                    {history?.type_money_from ===
                    Types.TYPE_MONEY_FROM_DEPOSIT ? (
                      <span className="font-medium text-green-500">
                        {history?.money_change
                          ? `+${formatNumber(history?.money_change)} đ`
                          : '0'}
                      </span>
                    ) : (
                      <span className="font-medium text-red-500">
                        {history?.money_change
                          ? `-${formatNumber(history?.money_change)} đ`
                          : '0'}
                      </span>
                    )}
                  </div>
                  <div className="flex justify-between text-sm ">
                    <span>Tổng tiền sau cùng:</span>
                    <span className="font-medium">
                      {history?.account_balance_changed
                        ? `${formatNumber(history?.account_balance_changed)} đ`
                        : '0'}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          ))}
      </div>
      {eWalletHistory?.data?.length > 0 && eWalletHistory.last_page > 1 && (
        <div className="flex justify-center mt-8 text-sm pagination__manage--actor">
          <ReactPagination
            pageCount={eWalletHistory.last_page}
            handleClickPage={handleSelectedPage}
            marginPagesDisplayed={3}
            pageRangeDisplayed={3}
            page={params.page - 1}
          />
        </div>
      )}
    </HistoryBalanceStyles>
  );
};

export default HistoryBalance;
