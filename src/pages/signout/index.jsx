import React from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Navigate, useLocation } from "react-router-dom";
import * as Types from "../../constants/actionType";
import { stepAppointment } from "../../utils/stepAppointment";

const Signout = () => {
  // const dispatch = useDispatch();
  const location = useLocation();

  // useEffect(() => {
  //   dispatch({
  //     type: Types.LOGIN,
  //     data: {},
  //   });
  // }, []);
  localStorage.removeItem("token");
  localStorage.removeItem("locationBefore");
  stepAppointment.removePathName();
  return <Navigate to="/" state={{ from: location }} replace></Navigate>;
};

export default Signout;
