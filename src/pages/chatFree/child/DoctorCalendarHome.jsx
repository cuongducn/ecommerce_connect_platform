import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSearchParams } from 'react-router-dom';
import Slider from 'react-slick';
import { Modal } from 'rsuite';
import styled from 'styled-components';
import * as doctorAction from '../../../actions/doctor';
import NoImage from '../../../assets/no_img.png';
import ChatPopup from '../../../components/chat/ChatPopup.jsx';
import IconCheck from '../../../components/icon/check/IconCheck';
import { departments } from '../../../utils/helper';
import * as Types from '../../../constants/actionType';
const DoctorCalendarHomeStyles = styled.div`
  .doctor__item {
    box-shadow: 2px 6px 18px 3px rgba(38, 166, 154, 0.2);
    perspective: 1500px;
    position: relative;
    min-height: 320px;
    &:hover .doctor__item__content {
      transform: rotateY(180deg);
    }
    .doctor__header {
      padding-top: 20px;
      &:after {
        content: '';
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: -1;
        border-radius: 0px 0px 50% 0px;
      }
    }
    .doctor__item__content {
      position: relative;
      perspective: 1500px;
      width: 100%;
      height: 100%;
      transition: all 0.3s;
      transform-style: preserve-3d;
      .card-face {
        position: absolute;
        inset: 0;
        width: 100%;
        height: 100%;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        overflow: hidden;
        &.card-back {
          transform: rotateY(-180deg);
          cursor: inherit;
        }
      }
    }
  }
`;

const DoctorCalendarHome = () => {
  const dispatch = useDispatch();
  const { doctors } = useSelector((state) => state.doctorReducers);

  const [searchParams] = useSearchParams();
  const idPacket = searchParams.get('chatService');
  const [doctorSelected, setDoctorSelected] = useState();

  const [openModal, setOpenModal] = useState(false);

  //Xử lý hiển thị chọn gói khám
  const handleOpenModal = (doctor) => {
    setOpenModal(true);
    setDoctorSelected(doctor);
  };
  const handleCloseModal = () => {
    setOpenModal(false);
    setDoctorSelected();
  };
  const showImages = (doctor) => {
    const image1 = doctor?.doctor_info?.doctor_image_1;
    const image2 = doctor?.doctor_info?.doctor_image_2;
    const listImages = [];
    if (image1) {
      listImages.push(image1);
    }
    if (image2) {
      listImages.push(image2);
    }
    return listImages;
  };
  const imageDoctorSetting = {
    infinite: showImages(doctorSelected)?.length > 1,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,
  };

  const handleLevelAcademic = (level) => {
    if (level?.length > 0) {
      const levelDetail = level.reduce((prevData, nextData, index) => {
        return prevData + nextData.degree + '.';
      }, '');
      return levelDetail;
    }

    return '';
  };
  const handleCertificate = (certificate) => {
    if (certificate?.length > 0) {
      const certificateDetail = certificate.reduce(
        (prevData, nextData, index) => {
          return (
            prevData +
            `${
              index === certificate.length - 1 ? `${nextData}` : `${nextData}, `
            }`
          );
        },
        ''
      );
      return certificateDetail;
    }

    return '';
  };
  const handleSpecialist = (specialist) => {
    const departmentsSpecialFilter = specialist?.reduce(
      (prevData, nextData, index) => {
        return (
          prevData +
          `${
            index === specialist.length - 1
              ? ` ${nextData.name}`
              : `${nextData.name}, `
          }`
        );
      },
      ''
    );
    return departmentsSpecialFilter;
  };

  const handleSelectedDoctor = (doctor) => {
    setDoctorSelected(doctor);
    dispatch({
      type: Types.HOME_SHOW_CHAT_POPUP,
      data: {
        show: true,
        data: doctor,
      },
    });
  };
  useEffect(() => {
    return () => {
      dispatch({
        type: Types.GET_MESSAGES,
        data: {},
      });
    };
  }, []);
  useEffect(() => {
    dispatch(doctorAction.getDoctors(`packet_medical_id=${idPacket}`));
  }, [dispatch, idPacket]);
  return (
    <DoctorCalendarHomeStyles>
      <div>
        <div className="flex flex-col items-center mb-10">
          <h4 className="mb-4 text-2xl font-medium text-center">
            Danh sách bác sĩ
          </h4>
          <div className="w-[120px] h-[2px] bg-gray-200"></div>
        </div>
        {doctors?.data?.length > 0 && (
          <div className="grid grid-cols-3 gap-5">
            {doctors.data.map((element) => (
              <div
                key={element.id}
                className="overflow-hidden border rounded-xl doctor__item"
              >
                <div className="doctor__item__content">
                  <div className="card-face card-front">
                    <div className="h-[220px] relative doctor__header after:bg-gradient-to-bl after:to-main after:from-yellowFFF4">
                      <div className="w-[100px] h-[100px] mx-auto rounded-full border-4 border-[#fff]">
                        <img
                          className="object-cover w-full h-full  rounded-full bg-[#fff]"
                          src={
                            element.doctor_info?.doctor_image_1
                              ? element.doctor_info.doctor_image_1
                              : NoImage
                          }
                          alt={element.doctor_info?.first_and_last_name}
                        />
                      </div>
                      <h4 className="mt-3 text-xl text-center font-semibold  text-[#fff] ">
                        <span className="uppercase">
                          {handleLevelAcademic(
                            element.doctor_info?.academic_level
                          )}
                        </span>
                        {element.doctor_info?.first_and_last_name}
                      </h4>
                      {/* <div className="text-sm text-center gap-x-1 text-[#fff] w-[75%] mx-auto">
                        {handleSpecialist(element.doctor_info?.specialist)}
                      </div> */}
                    </div>
                    <div className="p-4">
                      <div className="flex text-sm gap-x-1">
                        <span>Kinh nghiệm:</span>
                        <span>
                          {element.doctor_info?.years_of_experience} năm
                        </span>
                      </div>
                      <div className="text-sm">
                        <span className="mr-1 whitespace-nowrap">
                          Chứng chỉ:
                        </span>
                        <span>
                          {handleCertificate(
                            element.doctor_info?.certificate_list
                          )}{' '}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="card-back card-face from-main to-yellowFFF4 bg-gradient-to-bl gap-y-3">
                    <div className="flex flex-col items-center justify-center w-full h-full gap-y-3">
                      <button
                        className="w-[180px] py-3 rounded-2xl bg-[#fff] hover:opacity-90"
                        onClick={() => handleSelectedDoctor(element)}
                      >
                        <span className="bg-gradient-to-br from-[#fff432] to-main bg-clip-text text-transparent text-sm font-medium">
                          Tư vấn khám bệnh
                        </span>
                      </button>
                      <button
                        className="w-[180px] py-3 rounded-2xl border border-[#fff] bg-transparent hover:bg-[#fff] hover:text-bodyColor text-[#fff]"
                        onClick={() => handleOpenModal(element)}
                      >
                        <span className="text-sm font-medium">
                          Xem thông tin Bác sĩ
                        </span>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
      {doctorSelected && (
        <Modal size="md" open={openModal} onClose={handleCloseModal}>
          <Modal.Header>
            <Modal.Title>
              <div className="text-xl font-medium text-center">
                Thông tin bác sĩ
              </div>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="grid grid-cols-3 px-[10px] rounded-[30px] shadow-[2px_6px_18px_3px_rgba(38,166,154,0.2)] m-4">
              <div className="col-span-1">
                {showImages(doctorSelected)?.length === 0 ? (
                  <div className="h-[200px] rounded-3xl block">
                    <img
                      src={NoImage}
                      alt="doctor_image"
                      className="object-cover w-full h-full rounded-3xl"
                      style={{
                        display: 'inline-block',
                      }}
                    />
                  </div>
                ) : (
                  <Slider {...imageDoctorSetting}>
                    {showImages(doctorSelected).map((url, index) => (
                      <div key={index} className="h-[200px] rounded-3xl block">
                        <img
                          src={url}
                          alt="doctor_image"
                          className="object-cover w-full h-full rounded-3xl"
                          style={{
                            display: 'inline-block',
                          }}
                        />
                      </div>
                    ))}
                  </Slider>
                )}
              </div>
              <div className="col-span-2 pl-4 py-[10px] pr-[10px]">
                <div className="text-xl font-semibold">
                  <span className="uppercase">
                    {doctorSelected.doctor_info?.academic_level
                      ? handleLevelAcademic(
                          doctorSelected.doctor_info?.academic_level
                        )
                      : ''}
                  </span>
                  {doctorSelected.doctor_info?.first_and_last_name}
                </div>
                {doctorSelected.doctor_info?.years_of_experience > 0 ? (
                  <div className="text-sm text-gray-400">
                    {doctorSelected.doctor_info?.years_of_experience} năm kinh
                    nghiệm
                  </div>
                ) : null}
                <div className="w-[70px] h-[2px] bg-gray-300 rounded-full mt-2 mb-4"></div>
                <div className="line-clamp-5">
                  {doctorSelected.doctor_info?.introduce}
                </div>
              </div>
            </div>
            <div className="flex flex-col col-span-2 mx-4 mt-8 mb-4 gap-y-4">
              <h4 className="mb-2 text-xl font-medium">
                Học vấn và kinh nghiệm
              </h4>
              {doctorSelected.doctor_info?.introduce ? (
                <div> {doctorSelected.doctor_info?.introduce}</div>
              ) : null}
              {doctorSelected.doctor_info?.academic_level?.length > 0 && (
                <div>
                  <div className="mb-2 font-medium">
                    <span className="uppercase ">
                      {doctorSelected.doctor_info?.academic_level
                        ? handleLevelAcademic(
                            doctorSelected.doctor_info?.academic_level
                          )
                        : ''}
                    </span>
                    {doctorSelected.doctor_info?.first_and_last_name} chuyên
                    điều trị:
                  </div>
                  <div>
                    {doctorSelected.doctor_info?.specialist?.map((element) => (
                      <div key={element.id} className="text-sm text-gray-500">
                        {element.name}
                      </div>
                    ))}
                  </div>
                </div>
              )}
              {doctorSelected.doctor_info?.certificate_list?.length > 0 && (
                <div>
                  <div className="mb-2 font-medium">
                    Các chứng chỉ chuyên môn
                  </div>
                  <div>
                    {doctorSelected.doctor_info?.certificate_list?.map(
                      (element, index) => (
                        <div
                          key={index}
                          className="flex items-center text-sm text-gray-500 gap-x-2"
                        >
                          <span className="text-xs text-green-500">
                            <i className="fa fa-check"></i>
                          </span>
                          <span>{element}</span>
                        </div>
                      )
                    )}
                  </div>
                </div>
              )}
              {doctorSelected.doctor_info?.work_progress?.length > 0 && (
                <div className="p-4 rounded-xl bg-teal-50">
                  <div className="mb-2 text-xl font-medium text-main">
                    Quá trình đào tạo
                  </div>
                  <div>
                    {doctorSelected.doctor_info?.work_progress?.map(
                      (progress) => (
                        <div key={progress.id} className="text-sm ">
                          <strong>
                            {progress?.from_year?.split('-').slice(-1)} -{' '}
                            {progress?.to_year?.split('-').slice(-1)}:
                          </strong>
                          <span className="text-gray-500">
                            {' '}
                            {progress?.position}{' '}
                            {progress?.hospital_name
                              ? `tại ${progress?.hospital_name}`
                              : null}
                          </span>
                        </div>
                      )
                    )}
                  </div>
                </div>
              )}
            </div>
          </Modal.Body>
        </Modal>
      )}
      <ChatPopup />
    </DoctorCalendarHomeStyles>
  );
};

export default DoctorCalendarHome;
