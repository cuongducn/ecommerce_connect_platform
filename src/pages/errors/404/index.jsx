import React from "react";
import ErrorPage from "../../../components/error/Error";
import { IconButton } from "rsuite";
import ArrowLeftLine from "@rsuite/icons/ArrowLeftLine";
import Link from "../../../components/navLink/Link";

const Error404 = () => (
  <ErrorPage code={404}>
    <p className="error-page-title">
      Rất tiếc... Bạn vừa tìm thấy một trang lỗi
    </p>
    <p className="error-page-subtitle text-muted ">
      Chúng tôi xin lỗi nhưng trang bạn đang tìm kiếm không được tìm thấy
    </p>
    <IconButton
      icon={<ArrowLeftLine />}
      appearance="primary"
      as={Link}
      href="/"
    >
      Đưa tôi về Home
    </IconButton>
  </ErrorPage>
);

export default Error404;
