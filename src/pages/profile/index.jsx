import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, Schema, Stack } from 'rsuite';
import StackItem from 'rsuite/esm/Stack/StackItem';
import styled from 'styled-components';
import DatePickerCalendar from '../../components/form/DatePicker';
import Input from '../../components/form/Input';
import { genders } from '../../utils/helper';
import * as profileActions from '../../actions/profile';
import { regexs } from '../../utils/regexs';
import { formatDateObj, formatDateStr } from '../../utils/date';
import UploadImage from '../../components/form/UploadImage';
import * as placeAction from '../../actions/place';
import InputSelect from '../../components/form/InputSelect';

const ProfileStyles = styled.div`
  font-size: 14px;
  background-color: #ffffff;
  padding: 20px;
  border-radius: 10px;
  .profile__header {
    margin-bottom: 20px;
    .profile__title {
      font-size: 18px;
      font-weight: 500;
    }
  }
  .profile__form {
    display: flex;
    flex-direction: column;
    row-gap: 20px;
  }
  @media only screen and (max-width: 768px) {
    padding: 20px 10px !important;
    .profile__main {
      flex-direction: column;
      gap: 15px !important;
      .profile__item {
        width: 100%;
        margin-right: 0 !important;
      }
    }
  }
  @media only screen and (max-width: 576px) {
    .profile__header {
      flex-direction: column;
      row-gap: 10px;
      align-items: flex-start !important;
    }
  }
`;

const { StringType } = Schema.Types;
const model = Schema.Model({
  phone_number: StringType()
    .isRequired('Trường SĐT bắt buộc phải nhập.')
    .addRule((value) => {
      const phoneRegex = regexs.phone;
      let isValidPhone = phoneRegex.test(value);
      if (isValidPhone) {
        return true;
      }
      return false;
    }, 'SĐT không đúng định dạng'),
});

const Profile = () => {
  const dispatch = useDispatch();
  const { profile } = useSelector((state) => state.profileReducers);
  const { province, district, wards } = useSelector(
    (state) => state.placeReducers
  );

  const { isLoadingProfile } = useSelector((state) => state.loadingReducers);

  const formRef = useRef();
  const [formValue, setFormValue] = useState({
    address_detail: '',
    province: '',
    district: '',
    wards: '',
    phone_number: '',
    name: '',
    date_of_birth: null,
    email: '',
    sex: '',
    avatar_image: '',
  });

  const [fileImage, setFileImage] = useState(null);

  // Handle Change Value Form
  const handleChangeValueForm = (values, e) => {
    if (e.target) {
      setFormValue({
        ...values,
      });
    } else {
      const name = e.name;
      let valuesPlace = {};
      if (name === 'province') {
        if (values[name]) {
          dispatch(placeAction.getDistrict(values[name]));
        }
        valuesPlace = {
          district: '',
          wards: '',
        };
      } else if (name === 'district') {
        if (values[name]) {
          dispatch(placeAction.getWards(values[name]));
        }
        valuesPlace = {
          wards: '',
        };
      }

      setFormValue({
        ...values,
        ...valuesPlace,
      });
    }
  };

  const handleOptionPlace = (data) => {
    const newData = data.reduce((prevData, currentData) => {
      return [
        ...prevData,
        {
          value: currentData.id,
          label: currentData.name,
        },
      ];
    }, []);

    return newData;
  };

  // Handle Update
  const handleUpdateProfile = async () => {
    if (!formRef.current.check()) {
      return;
    }
    const newFormValue = {
      address_detail: formValue.address_detail,
      wards: formValue.wards,
      district: formValue.district,
      province: formValue.province,
      name: formValue.name,
      phone_number: formValue.phone_number,
      email: formValue.email,
      sex: formValue.sex,
      date_of_birth: formValue.date_of_birth
        ? formatDateStr(formValue.date_of_birth)
        : '',
    };
    if (fileImage) {
      newFormValue.avatar_image = fileImage;
    }
    dispatch(profileActions.updateProfile(newFormValue));
  };
  const handleFilterPlace = (value, data) => {
    let newData;
    data.forEach((element) => {
      if (element.id !== value) return;
      newData = {
        value: element.id,
        label: element.name,
      };
    });

    return newData;
  };
  const handleFilterGender = (value) => {
    const newData = genders.filter((gender) => gender.value === value);
    return newData;
  };

  useEffect(() => {
    if (profile?.id) {
      if (profile.province) {
        dispatch(placeAction.getDistrict(profile.province));
      }
      if (profile.district) {
        dispatch(placeAction.getWards(profile.district));
      }
      setFormValue({
        address_detail: profile.address_detail,
        province:
          profile.province !== null && profile.province !== ''
            ? {
                value: profile.province,
                label: profile.province_name,
              }
            : '',
        district:
          profile.district !== null && profile.district !== ''
            ? {
                value: profile.district,
                label: profile.district_name,
              }
            : '',
        wards:
          profile.wards !== null && profile.wards !== ''
            ? {
                value: profile.wards,
                label: profile.wards_name,
              }
            : '',
        phone_number: profile.phone_number,
        name: profile.name,
        date_of_birth:
          formatDateObj(profile.date_of_birth) != 'Invalid Date'
            ? formatDateObj(profile.date_of_birth)
            : null,
        email: profile.email ? profile.email : '',
        sex: profile.sex,
        avatar_image: profile.avatar_image ? profile.avatar_image : '',
      });
    }
  }, [profile?.id]);
  useEffect(() => {
    dispatch(placeAction.getProvince());
  }, [dispatch]);
  return (
    <ProfileStyles>
      <div className="profile__content">
        <Form
          fluid
          checkTrigger="change"
          ref={formRef}
          onChange={handleChangeValueForm}
          formValue={formValue}
          model={model}
        >
          <Stack justifyContent="space-between" className="profile__header">
            <div className="profile__title">Thông tin</div>
            <Stack spacing={15}>
              <Button
                appearance="primary"
                type="submit"
                style={{
                  backgroundColor: '#26a69a',
                }}
                onClick={handleUpdateProfile}
                loading={isLoadingProfile}
              >
                Cập nhập
              </Button>

              {/* <Button appearance="default" onClick={handleRessetValueForm}>
                Làm mới
              </Button> */}
            </Stack>
          </Stack>
          <div className="profile__form">
            <div>
              <UploadImage
                name="avatar_file"
                image_url={formValue.avatar_image}
                setFile={setFileImage}
                style={{
                  width: '100px',
                  height: '100px',
                  borderRadius: '10px',
                }}
                limit={10}
              ></UploadImage>
            </div>
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="profile__main"
            >
              <StackItem grow={1} className="profile__item">
                <Input name="name" label="Tên" placeholder="Nhập tên..." />
              </StackItem>
              <StackItem grow={1} className="profile__item">
                <Input
                  name="phone_number"
                  label="SĐT"
                  placeholder="Nhập SĐT..."
                />
              </StackItem>
            </Stack>
            <div className="profile__mainTime gap-[30px] grid grid-cols-2">
              <div className="profile__item">
                <Input name="email" label="Email" placeholder="Nhập email..." />
              </div>
              <div className="profile__item">
                <DatePickerCalendar
                  name="date_of_birth"
                  label="Ngày sinh"
                  placement="topStart"
                  format="yyyy-MM-dd"
                ></DatePickerCalendar>
              </div>
            </div>
            <div className="profile__main  gap-[30px] grid grid-cols-2">
              <div className="profile__item">
                <InputSelect
                  name="province"
                  label="Tỉnh/Thành phố"
                  placeholder="VD: Thành phố Hà Nội"
                  className="text-sm"
                  classNameLabel="text-sm"
                  options={handleOptionPlace(province)}
                  value={handleFilterPlace(formValue.province, province)}
                />
              </div>

              <div className="profile__item">
                <InputSelect
                  name="district"
                  label="Quận/Huyện"
                  placeholder="VD: Quận Hoàn Kiếm"
                  className="text-sm"
                  classNameLabel="text-sm"
                  options={handleOptionPlace(district)}
                  value={handleFilterPlace(formValue.district, district)}
                />
              </div>
            </div>
            <div className="profile__main gap-[30px] grid grid-cols-2">
              <div className="profile__item">
                <InputSelect
                  name="sex"
                  label="Giới tính"
                  placeholder="Chọn giới tính"
                  className="text-sm"
                  classNameLabel="text-sm"
                  options={genders}
                  value={handleFilterGender(formValue.sex)}
                />
              </div>

              <div className="profile__item">
                <InputSelect
                  name="wards"
                  label="Xã/Phường"
                  placeholder="VD: Phường Yên Lãng"
                  className="text-sm"
                  classNameLabel="text-sm"
                  options={handleOptionPlace(wards)}
                  value={handleFilterPlace(formValue.wards, wards)}
                />
              </div>
            </div>
            <Stack
              spacing={30}
              justifyContent="space-between"
              className="profile__main"
            >
              <StackItem grow={1} className="profile__item">
                <Input
                  name="address_detail"
                  label="Địa chỉ"
                  placeholder="Nhập địa chỉ..."
                />
              </StackItem>
            </Stack>
          </div>
        </Form>
      </div>
    </ProfileStyles>
  );
};

export default Profile;
