import React, { useState } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { Button } from 'rsuite';
import { Field } from '../../components/form/auth/field';
import { Input, InputPassword } from '../../components/form/auth/input';
import { Label } from '../../components/form/auth/label';
import { useDispatch, useSelector } from 'react-redux';
import * as auth from '../../actions/admin/auth';
import { regexs } from '../../utils/regexs';
import RegisterOTP from './RegisterOTP';
import * as Types from '../../constants/actionType';

const defaultAccount = {
  from: '',
  value: '',
  status: '',
};
const defaultRegister = {
  data: {},
  isContinuing: false,
};
const schemaValidation = yup
  .object({
    name: yup.string().required('Tên không được để trống'),
    phone_number: yup
      .string()
      .required('Số điện thoại không được để trống')
      .length(10, 'Số điện thoại không hợp lệ')
      .matches(regexs.phone, 'Số điện thoại không hợp lệ'),
    password: yup
      .string()
      .required('Mật khẩu không được để trống')
      .min(6, 'Mật khẩu phải ít nhất 6 ký tự'),
  })
  .required();
const Register = ({ account, setAccount }) => {
  const dispatch = useDispatch();

  const {
    handleSubmit,
    control,
    formState: { isValid, errors },
  } = useForm({
    mode: 'onSubmit',
    resolver: yupResolver(schemaValidation),
    defaultValues: {
      name: '',
      email: account.from === 'email' ? account.value : '',
      phone_number: account.from === 'phone_number' ? account.value : '',
      password: '',
    },
  });
  const [register, setRegister] = useState(defaultRegister);

  const handleGoBack = () => {
    setAccount(defaultAccount);
    dispatch({
      type: Types.CHECK_EXIST_ACCOUNT,
      data: [],
    });
  };

  const handleContinue = (values) => {
    if (!isValid) return;
    setRegister({
      data: values,
      isContinuing: true,
    });
  };
  return (
    <>
      {register.isContinuing === false ? (
        <div>
          <div className="mb-3 text-xl cursor-pointer text-whiteAB">
            <span onClick={handleGoBack}>
              <i className="fa fa-chevron-left"></i>
            </span>
          </div>
          <h4 className="mb-[5px] text-2xl font-medium">
            Nhập thông tin đăng ký
          </h4>
          <form className="mt-8" onSubmit={handleSubmit(handleContinue)}>
            <div className="mb-4">
              <Field className="flex-col mb-1">
                <Input
                  className="bg-transparent input py-[10px]"
                  placeholder=" "
                  name="name"
                  control={control}
                ></Input>
                <Label title="Họ tên"></Label>
              </Field>
              {errors.name ? (
                <p className="text-sm text-error">{errors.name?.message}</p>
              ) : null}
            </div>
            <div className="mb-4">
              <Field className="flex-col mb-1">
                <Input
                  className="bg-transparent input py-[10px]"
                  placeholder=" "
                  name="phone_number"
                  control={control}
                ></Input>
                <Label title="Số điện thoại"></Label>
              </Field>
              {errors.phone_number ? (
                <p className="text-sm text-error">
                  {errors.phone_number?.message}
                </p>
              ) : null}
            </div>
            <div className="mb-4">
              <Field className="flex-col mb-1">
                <InputPassword
                  className="bg-transparent input py-[10px]"
                  placeholder=" "
                  name="password"
                  control={control}
                ></InputPassword>
                <Label title="Mật khẩu"></Label>
              </Field>
              {errors.password ? (
                <p className="text-sm text-error">{errors.password?.message}</p>
              ) : null}
            </div>
            <div className="mb-4">
              <Field className="flex-col mb-1">
                <Input
                  className="bg-transparent input py-[10px]"
                  placeholder=" "
                  name="email"
                  control={control}
                ></Input>
                <Label title="Email"></Label>
              </Field>
              {errors.email ? (
                <p className="text-sm text-error">{errors.email?.message}</p>
              ) : null}
            </div>

            <Button
              appearance="primary"
              type="submit"
              className="w-full py-[10px] text-center"
              style={{
                backgroundColor: '#00897b',
              }}
            >
              Tiếp tục
            </Button>
          </form>
        </div>
      ) : (
        <RegisterOTP
          register={register}
          setRegister={setRegister}
          setAccount={setAccount}
        ></RegisterOTP>
      )}
    </>
  );
};

export default Register;
