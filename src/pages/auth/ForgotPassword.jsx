import React, { useEffect, useRef, useState } from 'react';
import { Button } from 'rsuite';
import SetUpNewPassword from './SetUpNewPassword.jsx';

const otpArrayDefault = Array(6).fill('');
const ForgotPassword = ({ setIsForgotPassword, account }) => {
  const [errorOTP, setErrorOTP] = useState('');
  const [isSetUp, setIsSetUp] = useState(false);
  const [otp, setOtp] = useState({
    sendOTPFromPhone: true,
    array: otpArrayDefault,
    timer: 30,
  });

  const timerRef = useRef(null);

  const handleChangeOTP = (e, indexChange) => {
    const regex = /^[0-9\b]+$/;
    if (e.target.value === '' || regex.test(e.target.value)) {
      const newOtpArray = [...otp.array];
      otp.array.forEach((item, index) => {
        if (index === indexChange) {
          newOtpArray[index] = e.target.value;
          return;
        }
        newOtpArray[index] = item;
      });
      setOtp((prevOtp) => {
        return {
          ...prevOtp,
          array: newOtpArray,
        };
      });
      setErrorOTP('');
      const inputsOtp = document.querySelectorAll('.otp__code__item input');
      if (indexChange < 5 && e.target.value !== '') {
        inputsOtp[indexChange + 1].focus();
      }
    }
  };
  const handleKeyDownOTP = (e, indexChange) => {
    const inputsOtp = document.querySelectorAll('.otp__code__item input');
    if (
      inputsOtp[indexChange].value === '' &&
      indexChange > 0 &&
      e.key === 'Backspace'
    ) {
      inputsOtp[indexChange - 1].focus();
    }
  };
  const handleGoBack = () => {
    setIsForgotPassword(false);
  };

  const handleSetTimer = () => {
    clearInterval(timerRef.current);
    setOtp({
      ...otp,
      timer: 30,
    });
    timerRef.current = setInterval(() => {
      setOtp((prevOtp) => {
        return {
          ...prevOtp,
          timer: prevOtp.timer - 1,
        };
      });
    }, 1000);
  };
  const handleSendCodeAgain = () => {
    handleSetTimer();
  };

  const handleForgotAccount = () => {
    const otpStr = otp.array.join('');
    if (otpStr === '') {
      setErrorOTP('Vui lòng nhập mã xác minh(OTP)');
    } else if (otpStr.length !== 6) {
      setOtp({
        ...otp,
        array: otpArrayDefault,
      });
      setErrorOTP('Mã xác thực đã hết hạn');
    } else {
      setIsSetUp(true);
    }
  };

  useEffect(() => {
    timerRef.current = setInterval(() => {
      setOtp((prevOtp) => {
        return {
          ...prevOtp,
          timer: prevOtp.timer - 1,
        };
      });
    }, 1000);

    return () => clearInterval(timerRef.current);
  }, []);
  useEffect(() => {
    if (otp.timer === 0) {
      setOtp((prevOtp) => {
        return {
          ...prevOtp,
          timer: 0,
        };
      });
      clearInterval(timerRef.current);
    }
  }, [otp.timer]);
  return (
    <>
      {!isSetUp ? (
        <div>
          <div className="text-xl mb-3 text-whiteAB cursor-pointer">
            <span onClick={handleGoBack}>
              <i className="fa fa-chevron-left"></i>
            </span>
          </div>
          <h4 className="mb-[5px] text-2xl font-medium">Quên mật khẩu ?</h4>
          <p className="m-0 text-sm">
            Mã xác minh đã được gửi tới{' '}
            <span className="text-bodyColor font-semibold">
              {account.value}
            </span>
          </p>
          <div className="mt-7 mb-6">
            <div className="otp__code flex justify-center">
              {otpArrayDefault.map((item, index) => (
                <div className="otp__code__item flex items-center" key={index}>
                  <input
                    className="w-10 text-center py-1 text-[#434844] text-[40px] mx-[10px] border-b-[1px] border-solid border-[rgb(224, 224, 224)] placeholder:text-[#bbbbc5]"
                    value={otp.array[index]}
                    type="text"
                    placeholder="0"
                    maxLength={1}
                    onChange={(e) => handleChangeOTP(e, index)}
                    onKeyDown={(e) => handleKeyDownOTP(e, index)}
                  />
                </div>
              ))}
            </div>
            <p className="text-sm text-error h-5 text-center mt-4">
              {errorOTP}
            </p>
          </div>
          <Button
            appearance="primary"
            type="submit"
            className="w-full py-[10px] text-center"
            style={{
              backgroundColor: '#00897b',
            }}
            onClick={handleForgotAccount}
          >
            Tiếp theo
          </Button>
          <div className="mt-6 flex justify-between">
            {otp.timer === 0 ? (
              <div className="text-sm flex gap-x-1">
                <span className="text-[#787878]">Không nhận được?</span>
                <span
                  className="text-blue-700 hover:text-blue-800 cursor-pointer"
                  onClick={handleSendCodeAgain}
                >
                  Gửi lại mã
                </span>
              </div>
            ) : (
              <div className="text-sm flex gap-x-1">
                <span className="text-[#787878]">Gửi lại mã sau</span>
                <span className="text-blue-700" onClick={handleSendCodeAgain}>
                  {otp.timer}s
                </span>
              </div>
            )}
          </div>
        </div>
      ) : (
        <SetUpNewPassword
          setIsForgotPassword={setIsForgotPassword}
          account={account}
          otp={otp}
        ></SetUpNewPassword>
      )}
    </>
  );
};

export default ForgotPassword;
