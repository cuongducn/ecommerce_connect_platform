import React, { useState } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { Field } from '../../components/form/auth/field';
import { InputPassword } from '../../components/form/auth/input';
import { Label } from '../../components/form/auth/label';
import { Button } from 'rsuite';
import { useDispatch, useSelector } from 'react-redux';
import * as auth from '../../actions/auth';
import * as Types from '../../constants/actionType';
import { useNavigate } from 'react-router-dom';
import ForgotPassword from './ForgotPassword';
const defaultAccount = {
  from: '',
  value: '',
  status: '',
};
const schemaValidation = yup
  .object({
    password: yup
      .string('Nhập mật khẩu')
      .required('Mật khẩu không được để trống')
      .min(6, 'Mật khẩu không đúng'),
  })
  .required();
const Login = ({ account, setAccount }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { isLoadingAuthentication } = useSelector(
    (state) => state.loadingReducers
  );

  const {
    handleSubmit,
    control,
    formState: { isValid, errors },
  } = useForm({
    mode: 'onSubmit',
    resolver: yupResolver(schemaValidation),
    defaultValues: {
      password: '',
    },
  });
  const [isFogotPassword, setIsForgotPassword] = useState(false);

  const handleGoBack = () => {
    setAccount(defaultAccount);
    dispatch({
      type: Types.CHECK_EXIST_ACCOUNT,
      data: [],
    });
  };

  const handleLogin = (values) => {
    if (!isValid) return;
    const newValues = {
      email_or_phone_number: account.value,
      password: values.password,
    };
    dispatch(auth.login(newValues, navigate));
  };
  return (
    <>
      {!isFogotPassword ? (
        <div>
          <div className="text-xl mb-3 text-whiteAB cursor-pointer">
            <span onClick={handleGoBack}>
              <i className="fa fa-chevron-left"></i>
            </span>
          </div>
          <h4 className="mb-[5px] text-2xl font-medium">Nhập mật khẩu</h4>
          <p className="m-0 text-sm">
            Vui lòng nhập mật khẩu của{' '}
            {account.from === 'phone_number' ? ' số điện thoại ' : ' email '}
            <span className="text-bodyColor font-semibold">
              {account.value}
            </span>
          </p>
          <form className="mt-8" onSubmit={handleSubmit(handleLogin)}>
            <div className="mb-7">
              <Field className="flex-col mb-1">
                <InputPassword
                  className="bg-transparent input py-[10px]"
                  placeholder=" "
                  name="password"
                  control={control}
                ></InputPassword>
                <Label title="Nhập mật khẩu"></Label>
              </Field>
              {errors.password ? (
                <p className="text-sm text-error">{errors.password?.message}</p>
              ) : null}
            </div>
            <Button
              appearance="primary"
              type="submit"
              className="w-full py-[10px] text-center"
              style={{
                backgroundColor: '#00897b',
              }}
              loading={isLoadingAuthentication}
            >
              Đăng nhập
            </Button>
            <div
              className="text-sm mt-3 text-blue-600 hover:text-blue-700 cursor-pointer"
              onClick={() => setIsForgotPassword(true)}
            >
              Quên mật khẩu?
            </div>
          </form>
        </div>
      ) : (
        <ForgotPassword
          setIsForgotPassword={setIsForgotPassword}
          account={account}
        ></ForgotPassword>
      )}
    </>
  );
};

export default Login;
