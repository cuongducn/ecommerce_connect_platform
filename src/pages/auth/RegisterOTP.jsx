import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Button } from 'rsuite';
import * as auth from '../../actions/admin/auth';
import * as Types from '../../constants/actionType';
const defaultRegister = {
  data: {},
  isContinuing: false,
};
const otpArrayDefault = Array(6).fill('');
const RegisterOTP = ({ register, setRegister, setAccount }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [errorOTP, setErrorOTP] = useState('');
  const [otp, setOtp] = useState({
    sendOTPFromPhone: true,
    array: otpArrayDefault,
    timer: 30,
  });

  const timerRef = useRef(null);

  const handleChangeOTP = (e, indexChange) => {
    const regex = /^[0-9\b]+$/;
    if (e.target.value === '' || regex.test(e.target.value)) {
      const newOtpArray = [...otp.array];
      otp.array.forEach((item, index) => {
        if (index === indexChange) {
          newOtpArray[index] = e.target.value;
          return;
        }
        newOtpArray[index] = item;
      });
      setOtp((prevOtp) => {
        return {
          ...prevOtp,
          array: newOtpArray,
        };
      });
      setErrorOTP('');
      const inputsOtp = document.querySelectorAll('.otp__code__item input');
      if (indexChange < 5 && e.target.value !== '') {
        inputsOtp[indexChange + 1].focus();
      }
    }
  };
  const handleKeyDownOTP = (e, indexChange) => {
    const inputsOtp = document.querySelectorAll('.otp__code__item input');
    if (
      inputsOtp[indexChange].value === '' &&
      indexChange > 0 &&
      e.key === 'Backspace'
    ) {
      inputsOtp[indexChange - 1].focus();
    }
  };
  const handleGoBack = () => {
    setRegister(defaultRegister);
  };

  const handleSetTimer = () => {
    clearInterval(timerRef.current);
    setOtp({
      ...otp,
      timer: 30,
    });
    timerRef.current = setInterval(() => {
      setOtp((prevOtp) => {
        return {
          ...prevOtp,
          timer: prevOtp.timer - 1,
        };
      });
    }, 1000);
  };
  const handleSendCodeAgain = () => {
    handleSetTimer();
  };
  const handleChangeMethod = () => {
    handleSetTimer();
    setOtp((prevOtp) => {
      return {
        ...prevOtp,
        sendOTPFromPhone: !otp.sendOTPFromPhone,
      };
    });
  };
  const handleRegisterAccount = () => {
    const otpStr = otp.array.join('');
    if (otpStr === '') {
      setErrorOTP('Vui lòng nhập mã xác minh(OTP)');
    } else if (otpStr.length !== 6) {
      setOtp({
        ...otp,
        array: otpArrayDefault,
      });
      setErrorOTP('Mã xác thực đã hết hạn');
    } else {
      const accountRegister = {
        ...register.data,
        otp: otpStr,
      };
      dispatch(
        auth.register(accountRegister, navigate, () => {
          setAccount({
            from: 'phone_number',
            value: register.data.phone_number,
            status: 'login',
          });
          dispatch({
            type: Types.CHECK_EXIST_ACCOUNT,
            data: [
              {
                name: 'phone_number',
                value: true,
              },
            ],
          });
        })
      );
    }
  };

  useEffect(() => {
    timerRef.current = setInterval(() => {
      setOtp((prevOtp) => {
        return {
          ...prevOtp,
          timer: prevOtp.timer - 1,
        };
      });
    }, 1000);

    return () => clearInterval(timerRef.current);
  }, []);
  useEffect(() => {
    if (otp.timer === 0) {
      setOtp((prevOtp) => {
        return {
          ...prevOtp,
          timer: 0,
        };
      });
      clearInterval(timerRef.current);
    }
  }, [otp.timer]);
  return (
    <div>
      <div className="mb-3 text-xl cursor-pointer text-whiteAB">
        <span onClick={handleGoBack}>
          <i className="fa fa-chevron-left"></i>
        </span>
      </div>
      <h4 className="mb-[5px] text-2xl font-medium">Nhập mã xác minh</h4>
      <p className="m-0 text-sm">
        Mã xác minh đã được gửi tới{' '}
        <span className="font-semibold text-bodyColor">
          {otp.sendOTPFromPhone
            ? register?.data?.phone_number
            : register?.data?.email}
        </span>
      </p>
      <div className="mb-6 mt-7">
        <div className="flex justify-center otp__code">
          {otpArrayDefault.map((item, index) => (
            <div className="flex items-center otp__code__item" key={index}>
              <input
                className="w-10 text-center py-1 text-[#434844] text-[40px] mx-[10px] border-b-[1px] border-solid border-[rgb(224, 224, 224)] placeholder:text-[#bbbbc5]"
                value={otp.array[index]}
                type="text"
                placeholder="0"
                maxLength={1}
                onChange={(e) => handleChangeOTP(e, index)}
                onKeyDown={(e) => handleKeyDownOTP(e, index)}
              />
            </div>
          ))}
        </div>
        <p className="h-5 mt-4 text-sm text-center text-error">{errorOTP}</p>
      </div>
      <Button
        appearance="primary"
        type="submit"
        className="w-full py-[10px] text-center"
        style={{
          backgroundColor: '#00897b',
        }}
        onClick={handleRegisterAccount}
      >
        Đăng ký
      </Button>
      <div className="flex justify-between mt-6">
        {otp.timer === 0 ? (
          <div className="flex text-sm gap-x-1">
            <span className="text-[#787878]">Không nhận được?</span>
            <span
              className="text-blue-700 cursor-pointer hover:text-blue-800"
              onClick={handleSendCodeAgain}
            >
              Gửi lại mã
            </span>
          </div>
        ) : (
          <div className="flex text-sm gap-x-1">
            <span className="text-[#787878]">Gửi lại mã sau</span>
            <span className="text-blue-700" onClick={handleSendCodeAgain}>
              {otp.timer}s
            </span>
          </div>
        )}

        <div className="flex text-sm gap-x-1">
          <span className="text-[#787878]">Đổi phương thức?</span>
          <span
            className="text-blue-700 cursor-pointer hover:text-blue-800"
            onClick={handleChangeMethod}
          >
            {otp.sendOTPFromPhone ? 'Email' : 'Số điện thoại'}
          </span>
        </div>
      </div>
    </div>
  );
};

export default RegisterOTP;
