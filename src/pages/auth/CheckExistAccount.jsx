import React, { useEffect, useState } from 'react';
import { Field } from '../../components/form/auth/field';
import { Input } from '../../components/form/auth/input';
import { Label } from '../../components/form/auth/label';
import { useForm } from 'react-hook-form';
import { Button } from 'rsuite';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { regexs } from '../../utils/regexs';
import * as auth from '../../actions/auth';
import { useDispatch, useSelector } from 'react-redux';
import Login from './Login';
import Register from './Register';
import { isEmail } from '../../utils';
import { Link } from 'react-router-dom';
import { FacebookOutlined, GoogleOutlined } from '@ant-design/icons';

const defaultAccount = {
  from: '',
  value: '',
  status: '',
};
const schemaValidation = yup
  .object({
    email_or_phone_number: yup
      .string('Nhập số điện thoại/email')
      .required('Số điện thoại/email không được để trống')
      .test(
        'email_or_phone_number',
        'Nhập số điện thoại/email không hợp lệ',
        function (value) {
          const emailRegex = regexs.email;
          const phoneRegex = regexs.phone;
          let isValidEmail = emailRegex.test(value);
          let isValidPhone = phoneRegex.test(value);
          if (isValidPhone || isValidEmail) {
            return true;
          }
          return false;
        }
      ),
  })
  .required();
const CheckExistAccount = () => {
  const dispatch = useDispatch();
  const { existAccountFrom } = useSelector((state) => state.authReducers);
  const { isLoadingAuthentication } = useSelector(
    (state) => state.loadingReducers
  );

  const {
    handleSubmit,
    control,
    getValues,
    formState: { isValid, errors },
  } = useForm({
    mode: 'onSubmit',
    resolver: yupResolver(schemaValidation),
    defaultValues: {
      email_or_phone_number: '',
    },
  });
  const [account, setAccount] = useState(defaultAccount);
  // Check Exist Account
  const handleCheckExistAccount = (values) => {
    if (!isValid) return;
    const newValues = {
      email: values.email_or_phone_number,
      phone_number: values.email_or_phone_number,
    };
    dispatch(auth.checkExistAccount(newValues));
  };

  const handleLoginSocial = (from) => {
    window.location.href =
      'https://asahaa-api.cuongdn.top/api/customer/redirect/' + from;
    // dispatch(auth.loginSocial(from));
  };

  //Handle Account From
  useEffect(() => {
    if (existAccountFrom.length > 0) {
      let newAccount = { ...defaultAccount };
      existAccountFrom.forEach((from) => {
        if (from['value'] === true) {
          newAccount = {
            ...newAccount,
            from: from['name'],
            value: getValues('email_or_phone_number'),
          };
        }
      });
      if (newAccount.from) {
        newAccount.status = 'login';
      } else {
        newAccount = {
          from: isEmail(getValues('email_or_phone_number'))
            ? 'email'
            : 'phone_number',
          value: getValues('email_or_phone_number'),
          status: 'register',
        };
      }
      setAccount(newAccount);
    }
  }, [existAccountFrom, getValues]);

  return (
    <>
      {existAccountFrom.length > 0 && account.status === 'login' ? (
        <Login setAccount={setAccount} account={account} />
      ) : existAccountFrom.length > 0 && account.status === 'register' ? (
        <Register setAccount={setAccount} account={account} />
      ) : (
        <div>
          <h4 className="mb-[5px] text-2xl font-medium">Xin chào,</h4>
          <p className="m-0 text-sm">
            Đăng nhập hoặc
            <Link to={'/c/register'}>
              <span className="ml-[5px] underline text-[#173853] font-bold">
                tạo tài khoản
              </span>
            </Link>
          </p>
          <form
            className="mt-8"
            onSubmit={() => handleSubmit(handleCheckExistAccount)}
          >
            <div className="mb-7">
              <Field className="flex-col mb-1">
                <Input
                  type="text"
                  className="bg-transparent input py-[10px]"
                  placeholder=" "
                  name="email_or_phone_number"
                  control={control}
                ></Input>
                <Label title="SĐT/Email"></Label>
              </Field>
              {errors.email_or_phone_number ? (
                <p className="text-sm text-error">
                  {errors.email_or_phone_number?.message}
                </p>
              ) : null}
            </div>
            <Button
              appearance="primary"
              type="submit"
              className="w-full py-[10px] text-center"
              style={{
                backgroundColor: '#00897b',
              }}
              loading={isLoadingAuthentication}
            >
              Tiếp tục
            </Button>

            <div className="flex mt-[10px]">
              <Button
                appearance="primary"
                className=" py-[10px] text-center "
                style={{
                  backgroundColor: 'white',
                  color: 'black',
                  display: 'flex',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  border: '1px solid #3333',
                  marginRight: '10px',
                }}
                loading={isLoadingAuthentication}
                onClick={() => handleLoginSocial('google')}
              >
                <FacebookOutlined className="mr-[10px]" />
                <span>google</span>
              </Button>
              <Button
                appearance="primary"
                className=" py-[10px] text-center"
                style={{
                  backgroundColor: 'white',
                  color: 'black',
                  display: 'flex',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  border: '1px solid #3333',
                }}
                loading={isLoadingAuthentication}
                onClick={() => handleLoginSocial('facebook')}
              >
                <GoogleOutlined className="mr-[10px]" />
                <span>facebook</span>
              </Button>
            </div>
          </form>
        </div>
      )}
    </>
  );
};

export default CheckExistAccount;
