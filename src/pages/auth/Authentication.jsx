import React from 'react';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Header, Navbar } from 'rsuite';
import AsahaaBackground from '../../assets/ASAHAA.png';
import { actionsLocalStorage } from '../../utils/actionsLocalStorage';
import CheckExistAccount from './CheckExistAccount';
import { Link } from 'react-router-dom';
const Authentication = () => {
  const navigate = useNavigate();
  useEffect(() => {
    const token = actionsLocalStorage.getItem('token');
    if (token) {
      navigate('/c/profile');
    }

    const tokenUrl = window.location.href.split('=')?.[1];
    if (tokenUrl?.length > 10) {
      actionsLocalStorage.setItem('token', tokenUrl);
      navigate('/c/profile');
    }
  }, [navigate]);

  return (
    <div className="w-screen h-screen">
      <Header>
        <Navbar
          appearance="inverse"
          style={{
            backgroundColor: '#00897b',
          }}
        >
          <Navbar.Brand>
            <Link to={'/'}>
              <span className="font-semibold">Asahaa</span>
            </Link>
          </Navbar.Brand>
        </Navbar>
      </Header>
      <div className="w-full  flex justify-center">
        <div className="mt-20">
          <div className="flex lg:w-[820px] md:min-w-[350px] rounded-[10px] overflow-hidden shadow-lg mt-[50px] lg:flex-row max-sm:flex-col">
            <div className="flex-grow p-5">
              <CheckExistAccount></CheckExistAccount>
            </div>
            <div className="md:max-w-[150px] lg:max-w-[350px] min-h-full mr-5 mb-5">
              <img className="" src={AsahaaBackground} alt="background" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Authentication;
