import React, { useCallback } from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Input, InputGroup } from "rsuite";
import styled from "styled-components";
import * as historyMedicalAction from "../../actions/historyMedical";
import SearchIcon from "@rsuite/icons/Search";
import * as Types from "../../constants/actionType";
import ReactPagination from "../../components/pagination/ReactPagination";
import { useNavigate, useSearchParams } from "react-router-dom";
import { debounce } from "lodash";

const HistoryMedicalStyles = styled.div`
  .historyMedical {
    box-shadow: 1px 4px 15px 2px rgba(38, 166, 154, 0.2);
  }
`;

const HistoryMedical = () => {
  const dispatch = useDispatch();
  const { allHistory } = useSelector((state) => state.historyMedicalReducers);

  const [searchParams] = useSearchParams();
  const [searchInput, setSearchInput] = useState(
    searchParams.get("search") || ""
  );

  const [params, setParams] = useState({
    page: searchParams.get("page") || 1,
    limit: searchParams.get("limit") || 9,
    search: searchParams.get("search") || "",
  });
  const navigate = useNavigate();

  const handleTime = (date, from, to) => {
    const day = date ? date.split("-")[2] : "";
    const MMYY = date ? date?.split("-")?.slice(0, 2).reverse().join("/") : "";
    const duringTime =
      from && to
        ? `${from?.split(" ")[1]?.split(":")?.slice(0, 2).join(":")} -
          ${to?.split(" ")[1]?.split(":")?.slice(0, 2).join(":")}`
        : "";
    return (
      <div className="flex flex-col items-center transition-all duration-300 group-hover:scale-125">
        <div className="text-4xl font-semibold text-main">{day}</div>
        <div className="text-sm font-medium">{`${MMYY} ${duringTime}`}</div>
      </div>
    );
  };

  const queryParams = (page, limit, search) => {
    return `page=${page}${limit ? `&limit=${limit}` : ""}${
      search ? `&search=${search}` : ""
    }`;
  };
  const handleSelectedPage = (event) => {
    setParams({
      ...params,
      page: Number(event.selected) + 1,
    });
    navigate(
      `/ho-so-suc-khoe?${queryParams(
        Number(event.selected) + 1,
        params.limit,
        params.search
      )}`
    );
  };

  //Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        return {
          ...prevParams,
          search: valueSearch,
          page: 1,
        };
      });
      navigate(`/ho-so-suc-khoe?${queryParams(1, params.limit, valueSearch)}`);
    }, 500),
    []
  );
  const handleSearch = (value) => {
    setSearchInput(value);
    debounceCallBack(value);
  };

  useEffect(() => {
    const query = queryParams(params.page, params.limit, params.search);
    dispatch(historyMedicalAction.getListHistoryMedical(query));
  }, [dispatch, params.limit, params.page, params.search]);
  return (
    <HistoryMedicalStyles>
      <h4 className="text-xl font-medium">Hồ sơ sức khỏe</h4>
      <div className="max-w-[300px] mt-5">
        <InputGroup inside>
          <Input
            placeholder="Tìm kiếm hồ sơ khám"
            value={searchInput}
            onChange={handleSearch}
          />
          <InputGroup.Button>
            <SearchIcon />
          </InputGroup.Button>
        </InputGroup>
      </div>
      <div className="grid grid-cols-3 gap-5 mt-10">
        {allHistory.data?.length > 0 &&
          allHistory.data.map((history) => (
            <div
              key={history.id}
              className="flex flex-col cursor-pointer historyMedical rounded-xl group"
            >
              <div
                className="h-[150px] flex justify-center items-center bg-yellow-50 rounded-xl"
                onClick={() =>
                  navigate(
                    `/ho-so-suc-khoe/${history.id}?page=${params.page}${
                      params.search ? `&search=${params.search}` : ""
                    }`
                  )
                }
              >
                {handleTime(
                  history.date_medical,
                  history.time_start_medical,
                  history.time_end_medical
                )}
              </div>
              <div className="flex flex-col py-4 h-[calc(100%-150px)]">
                <div className="mb-2 font-medium ">
                  <span className="inline-block px-4 rounded-tr-full rounded-br-full bg-main text-[#fff]">
                    {history?.patient_name}
                  </span>
                </div>
                <div className="px-4 mb-3">
                  <div className="text-sm">
                    Gói khám: {history?.packet_medical?.name}
                  </div>
                  <div className="text-sm">
                    Dịch vụ:{" "}
                    {history.type_medical === Types.TYPE_MEDICAL_AT_HOME
                      ? "Khám tại nhà"
                      : history.type_medical === Types.TYPE_MEDICAL_ONLINE
                      ? "Khám online"
                      : history.type_medical ===
                        Types.TYPE_MEDICAL_PRIVATE_CLINIC
                      ? "Khám tại phòng khám"
                      : ""}
                  </div>
                  <div className="text-sm">
                    Kết quả khám:{" "}
                    <span className="font-medium">
                      {history?.result_medical}
                    </span>
                  </div>
                </div>
                <div className="flex justify-end px-4 mt-auto">
                  <div
                    className={`text-xs font-medium rounded-full py-2 px-3 ${
                      history.status_medical === Types.STATUS_PROGRESSING
                        ? "bg-orange-100 text-orange-500"
                        : history.status_medical === Types.STATUS_CANCEL
                        ? "bg-red-100 text-red-500"
                        : history.status_medical === Types.STATUS_COMPLETED
                        ? "bg-green-100 text-green-500"
                        : ""
                    }`}
                  >
                    {history.status_medical === Types.STATUS_PROGRESSING
                      ? "Chưa xác nhận"
                      : history.status_medical === Types.STATUS_CANCEL
                      ? "Đã hủy"
                      : history.status_medical === Types.STATUS_COMPLETED
                      ? "Đã khám"
                      : ""}
                  </div>
                </div>
              </div>
            </div>
          ))}
      </div>
      {allHistory?.data?.length > 0 && allHistory.last_page > 1 && (
        <div className="flex justify-center mt-8 text-sm pagination__manage">
          <ReactPagination
            pageCount={allHistory.last_page}
            handleClickPage={handleSelectedPage}
            marginPagesDisplayed={3}
            pageRangeDisplayed={3}
            page={params.page - 1}
          />
        </div>
      )}
    </HistoryMedicalStyles>
  );
};

export default HistoryMedical;
