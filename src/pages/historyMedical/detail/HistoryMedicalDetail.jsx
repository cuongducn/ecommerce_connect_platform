import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import styled from "styled-components";
import * as historyMedicalAction from "../../../actions/historyMedical";
import IconArrowLeft from "../../../components/icon/arrow/IconArrowLeft";
import * as Types from "../../../constants/actionType";
import { formatNumber } from "../../../utils";
import { departments } from "../../../utils/helper";

const HistoryMedicalDetailStyles = styled.div`
  .historyMedicalDetail {
    box-shadow: 1px 4px 15px 2px rgba(38, 166, 154, 0.2);
  }
`;

const HistoryMedicalDetail = () => {
  const dispatch = useDispatch();
  const { history } = useSelector((state) => state.historyMedicalReducers);

  const navigate = useNavigate();
  const params = useParams();
  const idHistory = params.id;

  const handleLevelAcademic = (level) => {
    if (level?.length > 0) {
      const levelDetail = level.reduce((prevData, nextData, index) => {
        return (
          prevData +
          `${
            index === level?.length - 1
              ? `${nextData?.degree}.`
              : `${nextData?.degree}, `
          }`
        );
      }, "");
      return levelDetail;
    }
    return "";
  };
  const handleSpecialist = (specialist) => {
    if (specialist?.length > 0) {
      const departmentsSpecialist = departments.filter((department) =>
        specialist.includes(department.value)
      );
      const departmentsSpecialFilter = departmentsSpecialist.reduce(
        (prevData, nextData, index) => {
          return (
            prevData +
            `${
              index === departmentsSpecialist.length - 1
                ? ` ${nextData.label}`
                : `${nextData.label}, `
            }`
          );
        },
        ""
      );
      return departmentsSpecialFilter;
    }

    return "";
  };

  const goBack = () => {
    navigate(-1);
  };

  useEffect(() => {
    if (!idHistory) return;
    dispatch(historyMedicalAction.getDetailHistoryMedical(idHistory));
  }, [dispatch, idHistory]);
  return (
    <HistoryMedicalDetailStyles>
      <div
        className="flex items-center mb-2 text-sm cursor-pointer text-main"
        onClick={goBack}
      >
        <IconArrowLeft className="w-4 h-4"></IconArrowLeft>
        <span>Quay lại</span>
      </div>
      <h4 className="text-xl font-medium">Chi tiết hồ sơ</h4>
      <div className="flex flex-col mt-5 text-sm gap-y-5">
        <div className="overflow-hidden rounded-lg historyMedicalDetail">
          <div className="bg-gradient-to-br to-[#fff432] from-main px-4 py-2 text-[#fff] font-medium text-base">
            Thông tin bệnh nhân
          </div>
          <div className="p-4">
            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Người khám bệnh</div>
              <div className="text-xl font-medium uppercase">
                {history?.patient_name}
              </div>
            </div>

            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Số điện thoại</div>
              <div className="font-medium">
                {" "}
                {history?.patient_number_phone}
              </div>
            </div>
            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Dịch vụ</div>
              <div className="font-medium">
                {" "}
                {history?.type_medical === Types.TYPE_MEDICAL_AT_HOME
                  ? "Khám tại nhà"
                  : history.type_medical === Types.TYPE_MEDICAL_ONLINE
                  ? "Khám online"
                  : history.type_medical === Types.TYPE_MEDICAL_PRIVATE_CLINIC
                  ? "Khám tại phòng khám"
                  : ""}
              </div>
            </div>
            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Giờ khám</div>
              <div className="font-medium">
                {" "}
                {history?.time_start_medical && history?.time_end_medical
                  ? `${history?.time_start_medical
                      ?.split(" ")[1]
                      ?.split(":")
                      ?.slice(0, 2)
                      .join(":")} -
          ${history?.time_end_medical
            ?.split(" ")[1]
            ?.split(":")
            ?.slice(0, 2)
            .join(":")}`
                  : ""}
              </div>
            </div>
            <div className="flex items-center gap-x-5">
              <div className="w-[25%]">Ngày khám</div>
              <div className="font-medium">
                {history.date_medical
                  ? history.date_medical?.split("-").reverse().join("/")
                  : ""}
              </div>
            </div>
          </div>
        </div>
        <div className="overflow-hidden rounded-lg historyMedicalDetail">
          <div className="bg-gradient-to-br to-[#fff432] from-main px-4 py-2 text-[#fff] font-medium text-base">
            Bệnh án
          </div>
          <div className="p-4">
            <div className="flex items-center my-1 gap-x-5">
              <div className="w-[25%]">Triệu chứng bệnh</div>
              <div className="font-medium">{history?.disease_symptom}</div>
            </div>
            <div className="flex items-center my-1 gap-x-5">
              <div className="w-[25%]">Chuẩn đoán</div>
              <div className="font-medium">{history?.diagnose}</div>
            </div>
            <div className="flex items-center my-1 gap-x-5">
              <div className="w-[25%]">Kết quả</div>
              <div className="font-medium">{history?.result_medical}</div>
            </div>
            <div className="flex items-center my-1 gap-x-5">
              <div className="w-[25%]">Ghi chú</div>
              <div className="font-medium">{history?.note_doctor_medical}</div>
            </div>
          </div>
        </div>
        <div className="overflow-hidden rounded-lg historyMedicalDetail">
          <div className="bg-gradient-to-br to-[#fff432] from-main px-4 py-2 text-[#fff] font-medium text-base">
            Đơn thuốc
          </div>
          <div className="p-4">
            <div className="flex items-center justify-between mt-1 mb-4 font-medium">
              Đơn thuốc_{history?.patient_name}
              {/* {history?.prescripts?.length > 0 && (
                <button className="px-3 py-2 text-white bg-blue-400 rounded-md">
                  Đặt đơn
                </button>
              )} */}
            </div>
            <div className="overflow-hidden rounded-md">
              <table className="overflow-hidden border-gray-100">
                <thead>
                  <tr>
                    <th className="font-medium bg-teal-50 text-main">STT</th>
                    <th className="font-medium bg-teal-50 text-main">
                      Tên thuốc
                    </th>
                    <th className="font-medium bg-teal-50 text-main">
                      Số lượng
                    </th>

                    <th className="font-medium bg-teal-50 text-main">
                      Ghi chú
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {history?.prescripts?.length > 0 &&
                    history?.prescripts?.map((prescript, index) => (
                      <tr key={prescript.id}>
                        <td>{index}</td>
                        <td>{prescript.name}</td>
                        <td>{prescript.quantity}</td>
                        <td>{prescript.note}</td>
                      </tr>
                    ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {history?.doctor && (
          <div className="flex justify-end mt-5">
            <div className="flex flex-col items-center min-w-[280px] text-center">
              <div className="mb-2 font-medium">Bác sĩ khám bệnh</div>
              <div className="text-2xl font-semibold text-main">
                <span className="uppercase">
                  {handleLevelAcademic(history?.doctor?.academic_level)}
                </span>
                {history?.doctor?.first_and_last_name}
              </div>
              <div className="text-gray-500 w-[280px]">
                {handleSpecialist(history?.doctor?.specialist) ? (
                  <>
                    <span>Chuyên khoa </span>
                    <span className="lowercase">
                      {handleSpecialist(history?.doctor?.specialist)}
                    </span>
                  </>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        )}
      </div>
    </HistoryMedicalDetailStyles>
  );
};

export default HistoryMedicalDetail;
