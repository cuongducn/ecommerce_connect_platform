import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useGeolocated } from "react-geolocated";
import IconLocationSolid from "../../../components/icon/location/IconLocationSolid";
import DoctorCalendarHome from "./DoctorCalendarHome";
import { Input, InputGroup } from "rsuite";
import { getSearchParams } from "../../../utils";
import { useSearchParams } from "react-router-dom";
import { stepAppointment } from "../../../utils/stepAppointment";

const LocationStyles = styled.div``;

const Location = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const { coords, getPosition } = useGeolocated({
    positionOptions: {
      enableHighAccuracy: false,
    },
    suppressLocationOnMount: true,
  });
  const [location, setLocation] = useState("");

  const handleClearInput = () => {
    setLocation("");
  };

  //Xử lý chia route
  const handleRoute = () => {
    const pathPacketParams = searchParams.get("packetId");
    const pathLatitude = searchParams.get("lat");
    const pathLongitude = searchParams.get("long");
    if (pathPacketParams && pathLatitude && pathLongitude) {
      return "doctor";
    }

    return "location";
  };

  useEffect(() => {
    if (coords) {
      const params = getSearchParams();
      setSearchParams(
        `${
          params
            ? `${params}&lat=${coords.latitude}&long=${coords.longitude}`
            : `lat=${coords.latitude}&long=${coords.longitude}`
        }`
      );
    }
  }, [coords]);
  useEffect(() => {
    stepAppointment.setPathName();
  }, []);
  return (
    <LocationStyles>
      {handleRoute() === "doctor" ? (
        <DoctorCalendarHome />
      ) : (
        <>
          <div className="flex flex-col items-center mb-8">
            <h4 className="mb-4 text-2xl font-medium text-center">Vị trí</h4>
            <div className="w-[120px] h-[2px] bg-gray-200"></div>
          </div>
          <div>
            <div className="flex flex-col items-center gap-y-3">
              <div>Cho phép truy cập vị trí để đặt khám</div>
              {/* <div>Cho phép truy cập vị trí hoặc nhập để đặt khám</div>
              <div className="">
                <InputGroup>
                  <Input
                    placeholder="Nhập vị trí đặt khám"
                    className="w-[300px]"
                    value={location}
                    onChange={(value) => setLocation(value)}
                  />

                  <InputGroup.Button
                    className="bg-gray-100"
                    onClick={handleClearInput}
                  >
                    <div>Không phải địa chỉ này</div>
                  </InputGroup.Button>
                </InputGroup>
              </div> */}
              <div>
                <button
                  className="w-[150px] py-2 rounded-2xl bg-gradient-to-br from-[#fff432] to-main hover:opacity-90 flex items-center gap-x-2 justify-center text-[#fff]"
                  onClick={getPosition}
                >
                  <IconLocationSolid className="w-5 h-5" />
                  <span className="font-medium">Lấy vị trí</span>
                </button>
              </div>
            </div>
          </div>
        </>
      )}
    </LocationStyles>
  );
};

export default Location;
