import React from 'react';
import styled from 'styled-components';
import { useState } from 'react';
import DependPerson from '../../dependPerson/index.jsx';
import ConfirmAppointment from '../../../components/confirmAppointment/ConfirmAppointment';
import * as Types from '../../../constants/actionType';
import { useSearchParams } from 'react-router-dom';
import { getSearchParams } from '../../../utils';

const DependentCalendarHomeStyles = styled.div``;

const DependentCalendarHome = ({
  packetMedical,
  timeCalendar,
  location,
  doctor,
}) => {
  const [itemSelected, setItemSelected] = useState();
  const [searchParams, setSearchParams] = useSearchParams();

  //Choose __item
  const handleChooseDependent = (dependPerson, isSelf) => {
    const params = getSearchParams('dependent', dependPerson?.id);
    setSearchParams(
      `${
        params
          ? `${params}&type=${handleTypeMedical()}&confirm=true${
              isSelf ? '&is_self=true' : ''
            }`
          : `confirm=true&type=${handleTypeMedical()}${
              isSelf ? '&is_self=true' : ''
            }`
      }`
    );
    setItemSelected(dependPerson);
  };

  //Xử lý chia route
  const handleRoute = () => {
    const pathPacket = searchParams.get('packetId');
    const pathDoctor = searchParams.get('doctorId');
    const pathDependent = searchParams.get('dependent');
    const pathTimeFrom = searchParams.get('from_time');
    const pathTimeTo = searchParams.get('to_time');
    const pathConfirm = searchParams.get('confirm');
    const pathLatitude = searchParams.get('lat');
    const pathLongitude = searchParams.get('long');

    if (
      pathPacket &&
      pathDoctor &&
      pathDependent &&
      pathConfirm &&
      pathTimeFrom &&
      pathTimeTo &&
      ((pathLatitude && pathLongitude) ||
        handleTypeMedical() == Types.TYPE_MEDICAL_ONLINE)
    ) {
      return 'confirm';
    }

    return 'dependent';
  };
  const handleTypeMedical = () => {
    const pathName = window.location.pathname;
    const type =
      pathName.split('/')[1] === 'kham-tai-nha'
        ? Types.TYPE_MEDICAL_AT_HOME
        : Types.TYPE_MEDICAL_ONLINE;
    return type;
  };

  return (
    <DependentCalendarHomeStyles>
      {handleRoute() === 'confirm' ? (
        <ConfirmAppointment />
      ) : (
        <DependPerson handleChooseDependent={handleChooseDependent} />
      )}
    </DependentCalendarHomeStyles>
  );
};

export default DependentCalendarHome;
