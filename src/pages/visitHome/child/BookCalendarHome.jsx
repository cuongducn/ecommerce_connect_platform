import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSearchParams } from "react-router-dom";
import styled from "styled-components";
import * as doctorAction from "../../../actions/doctor";
import Calendar from "../../../components/calendar/Calendar";
import { getSearchParams } from "../../../utils";
import { formatDateObj } from "../../../utils/date";
import DependentCalendarHome from "./DependentCalendarHome";
import * as Types from "../../../constants/actionType";
const BookCalendarStyles = styled.div`
  .rs-picker-toggle-clean {
    display: none;
  }
  .calendar__doctor {
    box-shadow: 0px 0px 6px 1px rgba(0, 0, 0, 0.08);
  }
`;

const BookCalendarHome = () => {
  const dispatch = useDispatch();
  const { calendarsIndividual } = useSelector((state) => state.doctorReducers);

  const [date, setDate] = useState("");
  const [timeCalendar, setTimeCalendar] = useState();
  const [searchParams, setSearchParams] = useSearchParams();
  const idDoctor = searchParams.get("doctorId");

  const handleDependent = () => {
    const params = getSearchParams();
    setSearchParams(
      `${
        params
          ? `${params}&from_time=${timeCalendar.from_time}&to_time=${timeCalendar.to_time}&dependent=true`
          : `from_time=${timeCalendar.from_time}&to_time=${timeCalendar.to_time}dependent=true`
      }`
    );
  };
  const handleTypeMedical = () => {
    const pathName = window.location.pathname;
    const type =
      pathName.split("/")[1] === "kham-tai-nha"
        ? Types.TYPE_MEDICAL_AT_HOME
        : Types.TYPE_MEDICAL_ONLINE;
    return type;
  };
  //Xử lý chia route
  const handleRoute = () => {
    const pathPacket = searchParams.get("packetId");
    const pathDoctor = searchParams.get("doctorId");
    const pathDependent = searchParams.get("dependent");
    const pathTimeFrom = searchParams.get("from_time");
    const pathTimeTo = searchParams.get("to_time");
    const pathLatitude = searchParams.get("lat");
    const pathLongitude = searchParams.get("long");

    if (
      pathPacket &&
      pathDoctor &&
      pathDependent &&
      pathTimeFrom &&
      pathTimeTo &&
      ((pathLatitude && pathLongitude) ||
        handleTypeMedical() == Types.TYPE_MEDICAL_ONLINE)
    ) {
      return "dependent";
    }

    return "calendar";
  };

  useEffect(() => {
    if (Object.entries(calendarsIndividual).length === 0) return;
    setDate(formatDateObj(calendarsIndividual[0].date));
  }, [calendarsIndividual]);
  useEffect(() => {
    dispatch(doctorAction.getCalendarsIndividual(`doctor_id=${idDoctor}`));
  }, [dispatch, idDoctor]);
  return (
    <BookCalendarStyles>
      {handleRoute() === "dependent" ? (
        <DependentCalendarHome />
      ) : (
        <>
          <Calendar
            calendarsClinic={calendarsIndividual}
            date={date}
            setDate={setDate}
            setTimeCalendar={setTimeCalendar}
            timeCalendar={timeCalendar}
            handleDependent={handleDependent}
          />
          {calendarsIndividual?.length === 0 && (
            <div className="flex flex-col items-center justify-center gap-y-3">
              <div className="font-medium">Chưa có lịch đặt khám bệnh</div>
            </div>
          )}
        </>
      )}
    </BookCalendarStyles>
  );
};

export default BookCalendarHome;
