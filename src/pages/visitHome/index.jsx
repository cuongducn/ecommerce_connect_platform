import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { store } from "../../utils/store";
import * as packetAction from "../../actions/packetMedical";
import NoImage from "../../assets/no_img.png";
import {
  formatNumber,
  getSearchParams,
  setLocationBeforeLogin,
} from "../../utils";
import Location from "./child/Location";
import { useNavigate, useSearchParams } from "react-router-dom";
import { Modal } from "rsuite";
import Slider from "react-slick";
import parse from "html-react-parser";
import { actionsLocalStorage } from "../../utils/actionsLocalStorage";
import * as Types from "../../constants/actionType";
import DoctorCalendarHome from "./child/DoctorCalendarHome";
import { stepAppointment } from "../../utils/stepAppointment";

const VisitHomeStyles = styled.div`
  .packetsMedical {
    box-shadow: 2px 6px 18px 3px rgba(38, 166, 154, 0.2);
    perspective: 1500px;
    position: relative;
    min-height: 340px;
    &:hover .packetsMedical__content {
      transform: rotateY(180deg);
    }
    .packetsMedical__content {
      position: relative;
      perspective: 1500px;
      width: 100%;
      height: 100%;
      transition: all 0.3s;
      transform-style: preserve-3d;
      .card-face {
        position: absolute;
        inset: 0;
        width: 100%;
        height: 100%;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        overflow: hidden;
        &.card-back {
          transform: rotateY(-180deg);
          cursor: inherit;
        }
      }
    }
    .department {
      height: 60px;
      display: -webkit-box;
      -webkit-line-clamp: 3;
      -webkit-box-orient: vertical;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }
`;

const VisitHome = () => {
  const dispatch = useDispatch();
  const { packetsMedical } = useSelector(
    (state) => state.packetMedicalReducers
  );

  const navigate = useNavigate();
  const [packetSelected, setPacketSelected] = useState();
  const [openModalPacketInformation, setOpenModalPacketInformation] =
    useState(false);
  const [searchParams, setSearchParams] = useSearchParams();

  //Xử lý hiển thị thông tin gói khám
  const handleOpenModalPacketInfo = (packet) => {
    setOpenModalPacketInformation(true);
    setPacketSelected(packet);
  };
  const handleCloseModalPacketInfo = () => {
    setOpenModalPacketInformation(false);
    setPacketSelected();
  };

  const convertDepartment = (departments) => {
    if (departments.length > 0) {
      const listString = departments.reduce(
        (prevDepartments, currentDepartments, index) => {
          return (
            prevDepartments +
            `${
              index !== departments.length - 1
                ? `${currentDepartments.name}, `
                : currentDepartments.name
            }`
          );
        },
        ""
      );

      return listString;
    } else {
      return "";
    }
  };

  const handleShowChooseLocation = (packet) => {
    const token = actionsLocalStorage.getItem("token");
    if (!token) {
      setLocationBeforeLogin();
      navigate("/dang-nhap");
      return;
    }

    const params = getSearchParams();
    const paramsType =
      handleTypeMedical() == Types.TYPE_MEDICAL_ONLINE ? `&lat=&long=` : "";
    setSearchParams(
      `${
        params
          ? `${params}&packetId=${packet.id}${paramsType}`
          : `packetId=${packet.id}${paramsType}`
      }`
    );
    setPacketSelected(packet);
  };
  const handleTypeMedical = () => {
    const pathName = window.location.pathname;
    const type =
      pathName.split("/")[1] === "kham-tai-nha"
        ? Types.TYPE_MEDICAL_AT_HOME
        : Types.TYPE_MEDICAL_ONLINE;
    return type;
  };
  //Xử lý chia route
  const handleRoute = () => {
    const pathPacket = searchParams.get("packetId");
    if (handleTypeMedical() == Types.TYPE_MEDICAL_ONLINE && pathPacket) {
      return "doctor";
    } else if (pathPacket) {
      return "location";
    }

    return "packet";
  };
  const imagePacketSetting = {
    infinite: packetSelected?.images?.length > 1,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true,
  };

  useEffect(() => {
    stepAppointment.setPathName();
  }, []);
  useEffect(() => {
    dispatch(packetAction.getPacketsMedical());
  }, [dispatch]);
  return (
    <VisitHomeStyles>
      <div className="relative">
        <div className="w-full h-[500px]">
          <img
            className="w-full h-full"
            src={store.background}
            alt={store.name}
          />
        </div>
        <div className="bg-gray-100">
          <div className="py-10">
            <div className="containerWidthService">
              <div className="bg-[#fff] px-8 py-5 rounded-2xl">
                {handleRoute() === "location" ? (
                  <Location />
                ) : handleRoute() === "doctor" ? (
                  <DoctorCalendarHome />
                ) : (
                  <>
                    <div className="flex flex-col items-center mb-10">
                      <h4 className="mb-4 text-2xl font-medium text-center">
                        Đặt gói khám dịch vụ
                      </h4>
                      <div className="w-[120px] h-[2px] bg-gray-200"></div>
                    </div>
                    {packetsMedical?.data?.length > 0 ? (
                      <div className="grid grid-cols-3 gap-5">
                        {packetsMedical.data.map((packet) => (
                          <div
                            key={packet.id}
                            className="overflow-hidden cursor-pointer rounded-xl packetsMedical"
                          >
                            <div className="packetsMedical__content">
                              <div className="w-full h-full card-face card-front">
                                <div className="p-2 h-[200px]">
                                  <img
                                    className="w-full h-full rounded-xl"
                                    src={
                                      packet.images.length > 0
                                        ? packet.images[0]
                                        : NoImage
                                    }
                                    alt={packet.name}
                                  />
                                </div>
                                <div className="p-4">
                                  <div className="font-medium max-w-[250px] whitespace-nowrap overflow-hidden text-ellipsis">
                                    {packet.name}
                                  </div>
                                  {packet.list_medical_department.length >
                                    0 && (
                                    <p className="text-sm text-gray8F department">
                                      Khoa khám:{" "}
                                      <span>
                                        {convertDepartment(
                                          packet.list_medical_department
                                        )}
                                      </span>
                                    </p>
                                  )}
                                  {handleTypeMedical() ==
                                  Types.TYPE_MEDICAL_ONLINE ? (
                                    <div className="mt-2 text-sm font-medium text-main">
                                      {packet.price_online
                                        ? `${formatNumber(
                                            packet.price_online
                                          )} đ`
                                        : ""}
                                    </div>
                                  ) : (
                                    <div className="mt-2 text-sm font-medium text-main">
                                      {packet.price_home
                                        ? `${formatNumber(packet.price_home)} đ`
                                        : ""}
                                    </div>
                                  )}
                                </div>
                              </div>
                              <div className="card-face card-back bg-gradient-to-br to-[#fff432] from-main">
                                <div className="flex flex-col items-center justify-center w-full h-full gap-y-3">
                                  <button
                                    className="w-[180px] py-3 rounded-2xl bg-[#fff] hover:opacity-90"
                                    onClick={() =>
                                      handleShowChooseLocation(packet)
                                    }
                                  >
                                    <span className="bg-gradient-to-br from-[#fff432] to-main bg-clip-text text-transparent text-sm font-medium">
                                      Chọn gói khám
                                    </span>
                                  </button>
                                  <button
                                    className="w-[180px] py-3 rounded-2xl border border-[#fff] bg-transparent hover:bg-[#fff] hover:text-bodyColor text-[#fff]"
                                    onClick={() =>
                                      handleOpenModalPacketInfo(packet)
                                    }
                                  >
                                    <span className="text-sm font-medium ">
                                      Tìm hiểu thêm
                                    </span>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    ) : (
                      <div className="flex flex-col items-center justify-center gap-y-3">
                        <div className="font-medium">Chưa có gói khám bệnh</div>
                        <button
                          className="w-[150px] py-2 rounded-2xl bg-gradient-to-br from-[#fff432] to-main hover:opacity-90 flex items-center gap-x-2 justify-center text-[#fff]"
                          onClick={() => navigate(-1)}
                        >
                          Quay lại
                        </button>
                      </div>
                    )}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      {packetSelected && (
        <Modal
          size="md"
          open={openModalPacketInformation}
          onClose={handleCloseModalPacketInfo}
        >
          <Modal.Header>
            <Modal.Title>
              <div className="text-xl font-medium text-center">
                Thông tin gói khám
              </div>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="grid grid-cols-3 px-[10px] rounded-[30px] shadow-[2px_6px_18px_3px_rgba(38,166,154,0.2)] m-4">
              <div className="col-span-1">
                {packetSelected?.images?.length === 0 ? (
                  <div className="h-[200px] rounded-3xl block">
                    <img
                      src={NoImage}
                      alt="clinic_image"
                      className="object-cover w-full h-full rounded-3xl"
                      style={{
                        display: "inline-block",
                      }}
                    />
                  </div>
                ) : (
                  <Slider {...imagePacketSetting}>
                    {packetSelected?.images?.map((url, index) => (
                      <div key={index} className="h-[200px] rounded-3xl block">
                        <img
                          src={url}
                          alt="clinic_image"
                          className="object-cover w-full h-full rounded-3xl"
                          style={{
                            display: "inline-block",
                          }}
                        />
                      </div>
                    ))}
                  </Slider>
                )}
              </div>
              <div className="col-span-2 p-[10px]">
                <div className="text-xl font-medium">
                  {packetSelected?.name}
                </div>
                {packetSelected?.list_medical_department?.length > 0 && (
                  <p className="text-sm text-gray8F department">
                    Khoa khám:{" "}
                    <span>
                      {convertDepartment(
                        packetSelected?.list_medical_department
                      )}
                    </span>
                  </p>
                )}
                <div className="mt-2 text-3xl font-medium text-main">
                  {handleTypeMedical() == Types.TYPE_MEDICAL_ONLINE
                    ? `${
                        packetSelected?.price_online
                          ? `${formatNumber(packetSelected?.price_online)} đ`
                          : "0 đ"
                      }`
                    : `${
                        packetSelected?.price_home
                          ? `${formatNumber(packetSelected?.price_home)} đ`
                          : "0 đ"
                      }`}
                </div>
              </div>
            </div>
            <div className="col-span-2 mx-4 mt-8 mb-4">
              <h4 className="mb-2 text-xl font-medium">Chi tiết gói khám</h4>
              {packetSelected?.info_packet_medical ? (
                <div className="text-sm">
                  {parse(packetSelected?.info_packet_medical)}
                </div>
              ) : null}
            </div>
          </Modal.Body>
        </Modal>
      )}
    </VisitHomeStyles>
  );
};

export default VisitHome;
