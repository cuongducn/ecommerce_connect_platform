import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Routes, useNavigate } from 'react-router-dom';
import Layout from './components/layout/Layout';
import Sidebar from './components/sidebar/Sidebar';
import Blog from './pages/blog';
import ChatManage from './pages/chatManage';
import Home from './pages/home';
import Profile from './pages/profile';
import Login from './pages/auth/Login';
import Authentication from './pages/auth/Authentication';
import Signout from './pages/signout';
//admin
import LayoutAdmin from './components/admin/layout';
import Loading from './components/admin/loading/Loading';

import Error404 from './pages/admin/auth/404';
import Error500 from './pages/admin/auth/500';
import Blogs from './pages/admin/blogs';
import Category from './pages/admin/category';
import CategoryChildren from './pages/admin/categoryChildren';
import Dashboard from './pages/admin/dashboard';
import LoginAdmin from './pages/admin/login';
import Order from './pages/admin/order';
import Product from './pages/admin/product';
import ActionsProduct from './pages/admin/product/childs/ActionsProduct';
import ProfileAdmin from './pages/admin/profile';
import SignoutAdmin from './pages/admin/signout';
import Sliders from './pages/admin/sliders';
import Checkout from './pages/checkout';
import DetailBlog from './pages/detailBlog';
import ListProduct from './pages/listProduct';
import ProductDetail from './pages/productDetail';
import { isRoleAdmin, isRoleCus } from './utils';
import OrderUpdate from './pages/admin/order/childs/update';
import OrderDetail from './pages/admin/order/childs/detail';
import EcommerceConnect from './pages/admin/ecommerce/connect';
import EcommerceOrder from './pages/admin/ecommerce/order';
import EcommerceProduct from './pages/admin/ecommerce/product';
import OrderCustomer from './pages/order';
import OrderDetailCustomer from './pages/order/detail/OrderDetail';
import { alert } from './utils/alerts';
import Register from './pages/auth/Register';

function App() {
  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.authReducers);
  const { isLoading } = useSelector((state) => state.loadingReducers);
  const navigate = useNavigate();

  // useEffect(() => {
  //   if (token) {
  //     dispatch(badgeAction.getBadges());
  //   }
  // }, [dispatch, token]);
  useEffect(() => {
    if (isRoleAdmin()) {
      const adminToken = localStorage.getItem('admin-token');
      const pathName = window.location.pathname;
      if (!adminToken && !pathName.includes('admin/login')) {
        alert.error('Bạn chưa đăng nhập admin');
        navigate('/admin/login');
      }
    }
    if (isRoleCus()) {
      const cusToken = localStorage.getItem('token');
      const pathName = window.location.pathname;
      if (!cusToken && !pathName.includes('c/login')) {
        alert.error('Bạn chưa đăng nhập cus');
        navigate('/c/login');
      }
    }
  });
  return (
    <div className="App">
      {isLoading ? <Loading></Loading> : null}

      <Routes>
        <Route
          path="/c/login"
          element={<Authentication></Authentication>}
        ></Route>
        <Route path="/c/register" element={<Register></Register>}></Route>
        <Route path="/c/logout" element={<Signout></Signout>}></Route>
        <Route element={<Layout></Layout>}>
          <Route path="/" element={<Home></Home>}></Route>
          <Route path="/checkout" element={<Checkout></Checkout>}></Route>
          <Route
            path="/p/for-him"
            element={<ListProduct></ListProduct>}
          ></Route>
          <Route
            path="/product/:slug"
            element={<ProductDetail></ProductDetail>}
          ></Route>
          <Route path="/blog" element={<Blog></Blog>}></Route>
          <Route path="/blog/:slug" element={<DetailBlog></DetailBlog>}></Route>
          <Route element={<Sidebar></Sidebar>}>
            <Route path="/c/profile" element={<Profile></Profile>}></Route>
            <Route
              path="/c/chat-support"
              element={<ChatManage></ChatManage>}
            ></Route>
            <Route
              path="/c/order"
              element={<OrderCustomer></OrderCustomer>}
            ></Route>
            <Route
              path="/c/order/detail/:order_code"
              element={<OrderDetailCustomer></OrderDetailCustomer>}
            ></Route>
            {/* <Route
              path="/lich-su-tu-van"
              element={<HistoryVideoCall></HistoryVideoCall>}
            ></Route> */}
          </Route>
        </Route>

        <Route path="/admin">
          <Route path="login" element={<LoginAdmin></LoginAdmin>}></Route>
          <Route path="logout" element={<SignoutAdmin></SignoutAdmin>}></Route>

          <Route element={<LayoutAdmin></LayoutAdmin>}>
            <Route path="" index element={<Dashboard></Dashboard>}></Route>
            <Route path="ho-so" element={<ProfileAdmin></ProfileAdmin>}></Route>
            <Route path="blogs" element={<Blogs></Blogs>}></Route>
            <Route path="sliders" element={<Sliders></Sliders>}></Route>
            <Route
              path="category-children"
              element={<CategoryChildren></CategoryChildren>}
            ></Route>
            <Route path="categories" element={<Category></Category>}></Route>
            <Route path="products" element={<Product></Product>}></Route>
            <Route
              path="products/add"
              element={<ActionsProduct></ActionsProduct>}
            ></Route>
            <Route
              path="products/update/:id"
              element={<ActionsProduct></ActionsProduct>}
            ></Route>
            <Route path="orders" element={<Order></Order>}></Route>
            <Route
              path="connect-ecommerce"
              element={<EcommerceConnect></EcommerceConnect>}
            ></Route>
            <Route
              path="ecommerce-product/:platform"
              element={<EcommerceProduct></EcommerceProduct>}
            ></Route>
            <Route
              path="ecommerce-order/:platform"
              element={<EcommerceOrder></EcommerceOrder>}
            ></Route>
            <Route
              path="orders/update/:order_code"
              element={<OrderUpdate></OrderUpdate>}
            ></Route>
            <Route
              path="orders/detail/:order_code"
              element={<OrderDetail></OrderDetail>}
            ></Route>
          </Route>

          <Route path="loi-he-thong" element={<Error500></Error500>}></Route>
          <Route
            path="khong-tim-thay-trang"
            element={<Error404></Error404>}
          ></Route>
          <Route path="*" element={<Error404></Error404>}></Route>
        </Route>
      </Routes>
    </div>
  );
}

export default App;
