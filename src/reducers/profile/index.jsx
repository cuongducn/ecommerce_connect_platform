import * as Types from "../../constants/actionType";
const initialState = {
  profile: {},
};

export const profileReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_PROFILE:
      newState.profile = action.data;
      return newState;
    case Types.UPDATE_PROFILE:
      newState.profile = action.data;
      return newState;

    default:
      return newState;
  }
};
