import * as Types from '../../constants/actionType';

const initialState = {
  allOrder: {},
  order: {},
};

export const orderReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.ADD_ORDER:
      newState.order = action.data;
      return newState;
    case Types.GET_LIST_ORDER:
      newState.allOrder = action.data;
      return newState;
    case Types.GET_DETAIL_ORDER:
      newState.order = action.data;
      return newState;
    default:
      return newState;
  }
};
