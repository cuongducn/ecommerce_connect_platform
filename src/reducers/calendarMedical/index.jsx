import * as Types from "../../constants/actionType";

const initialState = {
  appointments: {},
  appointment: {},
  paymentAppointment: {},
};

export const calendarMedicalReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_LIST_APPOINTMENTS_MEDICAL:
      newState.appointments = action.data;
      return newState;
    case Types.GET_APPOINTMENT_MEDICAL:
      newState.appointment = action.data;
      return newState;
    case Types.PAYMENT_APPOINTMENT_MEDICAL:
      newState.paymentAppointment = action.data;
      return newState;
    default:
      return newState;
  }
};
