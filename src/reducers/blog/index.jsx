import * as Types from '../../constants/actionType';

const initialState = {
  blogs: [],
  blog: {},
};

export const cusBlogsReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_BLOGS:
      newState.blogs = action.data;
      return newState;
    case Types.GET_BLOG:
      newState.blog = action.data;
      return newState;
    default:
      return newState;
  }
};
