import * as Types from '../../constants/actionType';
import { actionsLocalStorage } from '../../utils/actionsLocalStorage';
const initialState = {
  token: actionsLocalStorage.getItem('admin-token') || null,
  existAccountFrom: [],
  resetMessage: '',
};

export const authAdminReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.LOGIN:
      newState.token = action.data;
      return newState;
    case Types.REGISTER:
      newState.customer = action.data;
      return newState;
    case Types.RESET_PASSWORD:
      newState.resetMessage = action.data;
      return newState;
    case Types.CHECK_EXIST_ACCOUNT:
      newState.existAccountFrom = action.data;
      return newState;
    default:
      return newState;
  }
};
