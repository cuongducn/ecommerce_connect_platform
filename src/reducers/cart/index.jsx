import * as Types from '../../constants/actionType';

const initialState = {
  cartItems: [],
  totalQuantity: 0,
  totalMoney: 0,
  totalItem: 0,
};

const saveCartToLocalStorage = (state = initialState) => {
  localStorage.setItem(
    'cart',
    JSON.stringify({
      cartItems: state.cartItems || [],
      totalQuantity: state.totalQuantity || 0,
      totalMoney: state.totalMoney || 0,
      totalItem: state.totalItem || 0,
    })
  );
};

const calculateTotalQuantity = (cartItems) => {
  return cartItems.reduce((total, item) => total + item.quantity, 0);
};

const calculateTotalMoney = (cartItems) => {
  return cartItems.reduce(
    (total, item) => total + item.quantity * item.price,
    0
  );
};

const calculateTotalItem = (cartItems) => {
  return cartItems.length;
};

export const cartReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));

  switch (action.type) {
    case Types.GET_CART:
      const cartData = action.payload;

      newState.cartItems = cartData.cartItems || [];
      newState.totalQuantity = cartData.totalQuantity || 0;
      newState.totalMoney = cartData.totalMoney || 0;
      newState.totalItem = cartData.totalItem || 0;
      return newState;
    case Types.ADD_ITEM_CART:
      const newItem = action.payload;
      const existingItem = state.cartItems.find(
        (item) => item.id === newItem.id
      );

      if (existingItem) {
        $dataHandle = {
          ...state,
          cartItems: state.cartItems.map((item) => {
            if (item.id === existingItem.id) {
              return {
                ...item,
                quantity: item.quantity + newItem.quantity,
              };
            }
            return item;
          }),
          totalQuantity: calculateTotalQuantity(state.cartItems),
          totalMoney: calculateTotalMoney(state.cartItems),
          totalItem: calculateTotalItem(state.cartItems),
        };
        console.log(totalItem);
        saveCartToLocalStorage();
        return $dataHandle;
      } else {
        $dataHandle = {
          ...state,
          cartItems: [...state.cartItems, newItem],
          totalQuantity: calculateTotalQuantity(
            state.cartItems.concat(newItem)
          ),
          totalMoney: calculateTotalMoney(state.cartItems.concat(newItem)),
          totalItem: calculateTotalItem(state.cartItems.concat(newItem)),
        };
        saveCartToLocalStorage();
        return $dataHandle;
      }

    case Types.DELETE_ITEM_CART:
      let $dataHandle = {};
      const itemIdToRemove = action.payload;

      const updatedCartItems = state.cartItems.filter(
        (item) => item.id !== itemIdToRemove
      );
      $dataHandle = {
        ...state,
        cartItems: updatedCartItems,
        totalQuantity: calculateTotalQuantity(updatedCartItems),
        totalMoney: calculateTotalMoney(updatedCartItems),
        totalItem: calculateTotalItem(updatedCartItems),
      };
      saveCartToLocalStorage();
      return $dataHandle;

    case Types.UPDATE_ITEM_CART:
      const isIncrease = action.payload;
      $dataHandle = {
        ...state,
        cartItems: state.cartItems.map((item) => {
          if (item.id === action.payload.itemId) {
            return {
              ...item,
              quantity: isIncrease ? item + 1 : item - 1,
            };
          }
          return item;
        }),
        totalQuantity: calculateTotalQuantity(state.cartItems),
        totalMoney: calculateTotalMoney(state.cartItems),
        totalItem: calculateTotalItem(state.cartItems),
      };

      saveCartToLocalStorage();
      return $dataHandle;
    case Types.CLEAR_CART:
      $dataHandle = {
        ...state,
        cartItems: [],
        totalQuantity: 0,
        totalMoney: 0,
        totalItem: 0,
      };
      saveCartToLocalStorage();
      return $dataHandle;
    default:
      return state;
  }
};
