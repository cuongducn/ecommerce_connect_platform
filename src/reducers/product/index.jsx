import * as Types from '../../constants/actionType';

const initialState = {
  listMessages: [],
  products: [],
  product: {},
};

export const productReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_PRODUCTS:
      newState.products = action.data;
      return newState;
    case Types.GET_PRODUCT:
      newState.product = action.data;
      return newState;
    default:
      return newState;
  }
};
