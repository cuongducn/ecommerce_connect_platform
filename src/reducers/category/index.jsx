import * as Types from '../../constants/actionType';

const initialState = {
  categories: [],
  category: {},
};

export const categoryReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_CATEGORIES:
      newState.categories = action.data;
      return newState;
    case Types.GET_CATEGORY:
      newState.category = action.data;
      return newState;
    // case Types.GET_DOCTORS_CLINIC:
    //   newState.doctorsCategory = action.data;
    //   return newState;
    // case Types.GET_CALENDARS_CLINIC:
    //   newState.calendarsCategory = action.data;
    //   return newState;
    // case Types.ADD_APPOINTMENT_CLINIC:
    //   newState.appointmentCategory = action.data;
    //   return newState;
    default:
      return newState;
  }
};
