import * as Types from "../../constants/actionType";
const initialState = {
  listMessages: {},
  chatConversation: {},
  message: {},
};

export const chatReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_MESSAGES:
      newState.listMessages = action.data;
      return newState;
    case Types.GET_CHAT_CONVERSATIONS:
      newState.chatConversation = action.data;
      return newState;
    case Types.SEND_MESSAGE:
      newState.message = action.data;
      return newState;

    default:
      return newState;
  }
};
