import * as Types from "../../constants/actionType";
const initialState = {
  listHistories: {},
};

export const videoCallReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_HISTORY_VIDEO_CALL:
      newState.listHistories = action.data;
      return newState;

    default:
      return newState;
  }
};
