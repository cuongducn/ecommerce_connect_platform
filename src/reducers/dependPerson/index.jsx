import * as Types from "../../constants/actionType";

const initialState = {
  listDependPerson: {},
  dependPerson: {},
};

export const dependPersonReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_LIST_DEPEND_PERSON:
      newState.listDependPerson = action.data;
      return newState;
    case Types.GET_DETAIL_DEPEND_PERSON:
      newState.dependPerson = action.data;
      return newState;
    case Types.ADD_DEPEND_PERSON:
      newState.listDependPerson.data = [
        action.data,
        ...newState.listDependPerson.data,
      ];
      return newState;
    case Types.UPDATE_DEPEND_PERSON:
      const newListDependent = newState.listDependPerson.data?.reduce(
        (prevDependent, currentDependent) => {
          const newDependent =
            currentDependent.id === action.data?.id
              ? action.data
              : currentDependent;
          return [...prevDependent, newDependent];
        },
        []
      );
      newState.listDependPerson.data = newListDependent;
      return newState;
    default:
      return newState;
  }
};
