import * as Types from "../../constants/actionType";

const initialState = {
  packetsMedical: {},
  packetMedical: {},
};

export const packetMedicalReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_PACKET_MEDICALS:
      newState.packetsMedical = action.data;
      return newState;
    case Types.GET_PACKET_MEDICAL:
      newState.packetMedical = action.data;
      return newState;
    default:
      return newState;
  }
};
