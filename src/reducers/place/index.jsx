import * as Types from '../../constants/actionType';

const initialState = {
  province: [],
  district: [],
  wards: [],
};

export const placeReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_PLACE_PROVINCE:
      newState.province = action.data;
      return newState;
    case Types.GET_PLACE_DISTRICT:
      newState.district = action.data;
      return newState;
    case Types.GET_PLACE_WARDS:
      newState.wards = action.data;
      return newState;
    default:
      return newState;
  }
};
