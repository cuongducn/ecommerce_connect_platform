import * as Types from "../../constants/actionType";
const initialState = {
  badges: {},
};

export const badgeReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_BADGES:
      newState.badges = action.data;
      return newState;

    default:
      return newState;
  }
};
