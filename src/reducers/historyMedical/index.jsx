import * as Types from "../../constants/actionType";

const initialState = {
  allHistory: {},
  history: {},
};

export const historyMedicalReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_LIST_MEDICAL_HISTORY:
      newState.allHistory = action.data;
      return newState;
    case Types.GET_MEDICAL_HISTORY:
      newState.history = action.data;
      return newState;
    default:
      return newState;
  }
};
