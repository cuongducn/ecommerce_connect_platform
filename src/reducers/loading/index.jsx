import * as Types from "../../constants/actionType";

const initialState = {
  isLoading: false,
  isLoadingAuthentication: false,
  isLoadingProfile: false,
  isLoadingChat: false,
  isLoadingAddress: false,
  isLoadingAppointment: false,
  isLoadingProductAdmin: false,
};

export const loadingReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.SHOW_LOADING:
      newState.isLoading = action.isLoading;
      return newState;
    case Types.SHOW_LOADING_AUTHENTICATION:
      newState.isLoadingAuthentication = action.isLoading;
      return newState;
    case Types.SHOW_LOADING_PROFILE:
      newState.isLoadingProfile = action.isLoading;
      return newState;
    case Types.SHOW_LOADING_CHAT:
      newState.isLoadingChat = action.isLoading;
      return newState;
    case Types.SHOW_LOADING_ADDRESS:
      newState.isLoadingAddress = action.isLoading;
      return newState;
    case Types.SHOW_LOADING_APPOINTMENT:
      newState.isLoadingAppointment = action.isLoading;
      return newState;
    case Types.ADMIN_SHOW_LOADING_PRODUCT:
      newState.isLoadingProductAdmin = action.isLoading;
      return newState;
    default:
      return newState;
  }
};
