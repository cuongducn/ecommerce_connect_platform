import { combineReducers } from 'redux';
import { blogsReducers } from './admin/blogs';
import { categorysReducers } from './admin/categories';
import { categoryAdminReducers } from './admin/category';
import { childrenReducers } from './admin/categoryChildren';
import { ecommerceConnectReducers } from './admin/ecommerceConnect';
import { orderAdminReducers } from './admin/order';
import { productAdminReducers } from './admin/product';
import { slidersReducers } from './admin/sliders';
import { authReducers } from './auth';
import { badgeReducers } from './badge';
import { cusBlogsReducers } from './blog';
import { calendarMedicalReducers } from './calendarMedical';
import { cartReducers } from './cart';
import { categoryReducers } from './category';
import { chatReducers } from './chat';
import { clinicReducers } from './clinic';
import { dependPersonReducers } from './dependPerson';
import { doctorReducers } from './doctor';
import { eWalletReducers } from './eWallet';
import { historyMedicalReducers } from './historyMedical';
import { homeReducers } from './home';
import { loadingReducers } from './loading';
import { orderReducers } from './order';
import { packetMedicalReducers } from './packetMedical';
import { placeReducers } from './place';
import { productReducers } from './product';
import { profileReducers } from './profile';
import { videoCallReducers } from './videoCall';
import { ecommerceProductReducers } from './admin/ecommerceProduct';

const rootReducers = combineReducers({
  loadingReducers,
  placeReducers,
  authReducers,
  profileReducers,
  clinicReducers,
  dependPersonReducers,
  packetMedicalReducers,
  doctorReducers,
  calendarMedicalReducers,
  historyMedicalReducers,
  eWalletReducers,
  chatReducers,
  badgeReducers,
  homeReducers,
  videoCallReducers,
  productReducers,
  orderAdminReducers,
  blogsReducers,
  categoryReducers,
  slidersReducers,
  childrenReducers,
  categoryAdminReducers,
  productAdminReducers,
  cusBlogsReducers,
  categorysReducers,
  orderReducers,
  cartReducers,
  ecommerceConnectReducers,
  ecommerceProductReducers,
});

export default rootReducers;
