import * as Types from '../../constants/actionType';
const initialState = {
  isShowPopUp: null,
  chatPopup: {
    show: false,
    data: {},
  },
  home: {},
};

export const homeReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_HOME:
      newState.home = action.data;
      return newState;
    case Types.HOME_SHOW_POPUP:
      newState.isShowPopUp = action.data;
      return newState;
    case Types.HOME_SHOW_CHAT_POPUP:
      newState.chatPopup = action.data;
      return newState;
    default:
      return newState;
  }
};
