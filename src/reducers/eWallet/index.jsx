import * as Types from "../../constants/actionType";

const initialState = {
  eWallet: {},
  eWalletHistory: {},
  listDepositWithdrawalsHistory: {},
  depositWithdrawalsHistory: {},
};

export const eWalletReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_WALLET:
      newState.eWallet = action.data;
      return newState;
    case Types.GET_WALLET_HISTORIES:
      newState.eWalletHistory = action.data;
      return newState;
    case Types.GET_LIST_DEPOSIT_WITHDRAWALS:
      newState.listDepositWithdrawalsHistory = action.data;
      return newState;
    case Types.GET_DETAIL_DEPOSIT_WITHDRAWALS:
      newState.depositWithdrawalsHistory = action.data;
      return newState;
    default:
      return newState;
  }
};
