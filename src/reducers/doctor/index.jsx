import * as Types from "../../constants/actionType";

const initialState = {
  doctors: {},
  calendarsClinic: {},
  calendarsIndividual: {},
};

export const doctorReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_LIST_DOCTORS:
      newState.doctors = action.data;
      return newState;
    case Types.GET_CALENDAR_DOCTOR_CLINIC:
      newState.calendarsClinic = action.data;
      return newState;
    case Types.GET_CALENDAR_DOCTOR_INDIVIDUAL:
      newState.calendarsIndividual = action.data;
      return newState;
    default:
      return newState;
  }
};
