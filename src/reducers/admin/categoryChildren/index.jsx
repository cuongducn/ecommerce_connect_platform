import { combineReducers } from 'redux';
import { categoryChildren } from './categoryChildren';

export const childrenReducers = combineReducers({
  categoryChildren,
});
