import * as Types from "../../../constants/actionType";

var initialState = {
  listChildren: {
    data: [],
    loading: false,
    error: null,
  },
  createChildren: {
    loading: false,
    error: null,
  },
  detailChildren: {
    data: null,
    loading: false,
    error: null,
  },
  updateChildren: {
    loading: false,
    error: null,
  },
  deleteChildren: {
    error: null,
    loading: false,
  },
  uploadImage: {
    error: null,
    data: "",
    loading: false,
  },
};

export const categoryChildren = (state = initialState, action) => {
  let newState = { ...state };
  switch (action.type) {
    case Types.INIT_FETCH_ALL_CHILDREN:
      return {
        ...state,
        listChildren: {
          ...state.listChildren,
          loading: true,
        },
      };
    case Types.FETCH_ALL_CHILDREN_SUCCESS:
      return {
        ...state,
        listChildren: {
          ...state.listChildren,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.FETCH_ALL_CHILDREN_FAILED:
      return {
        ...state,
        listChildren: {
          ...state.listChildren,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_CREATE_CHILDREN:
      return {
        ...state,
        createChildren: {
          ...state.createChildren,
          loading: true,
        },
      };
    case Types.CREATE_CHILDREN_SUCCESS:
      return {
        ...state,
        createChildren: {
          ...state.createChildren,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.CREATE_CHILDREN_FAILED:
      return {
        ...state,
        createChildren: {
          ...state.createChildren,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_UPDATE_CHILDREN:
      return {
        ...state,
        updateChildren: {
          ...state.updateChildren,
          loading: true,
        },
      };
    case Types.UPDATE_CHILDREN_SUCCESS:
      return {
        ...state,
        updateChildren: {
          ...state.updateChildren,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.UPDATE_CHILDREN_FAILED:
      return {
        ...state,
        updateChildren: {
          ...state.updateChildren,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_FETCH_CHILDREN:
      return {
        ...state,
        detailChildren: {
          ...state.detailChildren,
          loading: true,
        },
      };
    case Types.FETCH_CHILDREN_SUCCESS:
      return {
        ...state,
        detailChildren: {
          ...state.detailChildren,
          loading: false,
          data: action.payload,
          error: null,
        },
      };

    case Types.FETCH_CHILDREN_FAILED:
      return {
        ...state,
        detailChildren: {
          ...state.detailChildren,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_DELETE_CHILDREN:
      return {
        ...state,
        deleteChildren: {
          ...state.deleteChildren,
          loading: true,
        },
      };
    case Types.DELETE_CHILDREN_SUCCESS:
      return {
        ...state,
        deleteChildren: {
          ...state.deleteChildren,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.DELETE_CHILDREN_FAILED:
      return {
        ...state,
        deleteChildren: {
          ...state.deleteChildren,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_UPLOAD_IMAGE:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
        },
      };
    case Types.UPLOAD_IMAGE_SUCCESS:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
          error: null,
          data: action.payload,
        },
      };
    case Types.UPLOAD_IMAGE_FAILED:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
          error: action.payload,
          data: null,
        },
      };
    default:
      return newState;
  }
};
