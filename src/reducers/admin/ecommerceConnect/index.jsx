import * as Types from '../../../constants/actionType';

const initialState = {
  listEcommerceConnect: {},
  ecommerceConnect: {},
};

export const ecommerceConnectReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.ADMIN_GET_ECOMMERCE_CONNECT:
      newState.listEcommerceConnect = action.data;
      return newState;
    case Types.ADMIN_UPDATE_ECOMMERCE_CONNECT:
      newState.ecommerceConnect = action.data;
      return newState;
    case Types.ADMIN_ADD_ECOMMERCE_CONNECT:
      const newListProducts = [];
      newState?.listEcommerceConnect?.data?.forEach((ecommerceItem) => {
        if (ecommerceItem.id === action.data?.id)
          newListProducts.push(action.data);
        else newListProducts.push(ecommerceItem);
      });

      newState.listEcommerceConnect.data = newListProducts;
      return newState;
    default:
      return newState;
  }
};
