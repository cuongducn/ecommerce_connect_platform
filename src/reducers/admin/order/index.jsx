import * as Types from "../../../constants/actionType";

const initialState = {
  listOrders: {},
  order: {},
};

export const orderAdminReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.ADMIN_GET_LIST_ORDER:
      newState.listOrders = action.data;
      return newState;
    case Types.ADMIN_GET_ONE_ORDER:
      newState.order = action.data;
      return newState;
    case Types.ADMIN_UPDATE_STATUS_ORDER:
      return newState;
    default:
      return newState;
  }
};
