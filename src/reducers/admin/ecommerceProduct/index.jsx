import * as Types from '../../../constants/actionType';

const initialState = {
  listEcommerceProduct: {},
  syncEcommerceProduct: {},
};

export const ecommerceProductReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.ADMIN_GET_ECOMMERCE_PRODUCT:
      newState.listEcommerceProduct = action.data;
      return newState;
    case Types.ADMIN_SYNC_ECOMMERCE_PRODUCT:
      newState.listEcommerceProduct = action.data;
      return newState;
    default:
      return newState;
  }
};
