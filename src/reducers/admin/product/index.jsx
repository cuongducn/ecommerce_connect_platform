import * as Types from '../../../constants/actionType';

const initialState = {
  listProducts: {},
  product: {},
};

export const productAdminReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.ADMIN_GET_LIST_PRODUCT:
      newState.listProducts = action.data;
      return newState;
    case Types.ADMIN_GET_ONE_PRODUCT:
      newState.product = action.data;
      return newState;
    case Types.ADMIN_UPDATE_STATUS_PRODUCT:
      const newListProducts = [];
      newState?.listProducts?.data?.forEach((product) => {
        if (product.id === action.data?.id) newListProducts.push(action.data);
        else newListProducts.push(product);
      });

      newState.listProducts.data = newListProducts;
      return newState;
    default:
      return newState;
  }
};
