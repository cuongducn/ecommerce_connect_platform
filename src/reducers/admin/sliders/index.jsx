import { combineReducers } from "redux";
import { sliders } from "./sliders";

export const slidersReducers = combineReducers({
  sliders,
});
