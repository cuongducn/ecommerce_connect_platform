import * as Types from "../../../constants/actionType";

var initialState = {
  listSliders: {
    data: [],
    loading: false,
    error: null,
  },
  createSliders: {
    loading: false,
    error: null,
  },
  detailSliders: {
    data: null,
    loading: false,
    error: null,
  },
  updateSliders: {
    loading: false,
    error: null,
  },
  deleteSliders: {
    error: null,
    loading: false,
  },
  uploadImage: {
    error: null,
    data: "",
    loading: false,
  },
};

export const sliders = (state = initialState, action) => {
  let newState = { ...state };
  switch (action.type) {
    case Types.INIT_FETCH_ALL_SLIDERS:
      return {
        ...state,
        listSliders: {
          ...state.listSliders,
          loading: true,
        },
      };
    case Types.FETCH_ALL_SLIDERS_SUCCESS:
      return {
        ...state,
        listSliders: {
          ...state.listSliders,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.FETCH_ALL_SLIDERS_FAILED:
      return {
        ...state,
        listSliders: {
          ...state.listSliders,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_CREATE_SLIDERS:
      return {
        ...state,
        createSliders: {
          ...state.createSliders,
          loading: true,
        },
      };
    case Types.CREATE_SLIDERS_SUCCESS:
      return {
        ...state,
        createSliders: {
          ...state.createSliders,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.CREATE_SLIDERS_FAILED:
      return {
        ...state,
        createSliders: {
          ...state.createSliders,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_UPDATE_SLIDERS:
      return {
        ...state,
        updateSliders: {
          ...state.updateSliders,
          loading: true,
        },
      };
    case Types.UPDATE_SLIDERS_SUCCESS:
      return {
        ...state,
        updateSliders: {
          ...state.updateSliders,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.UPDATE_SLIDERS_FAILED:
      return {
        ...state,
        updateSliders: {
          ...state.updateSliders,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_FETCH_SLIDERS:
      return {
        ...state,
        detailSliders: {
          ...state.detailSliders,
          loading: true,
        },
      };
    case Types.FETCH_SLIDERS_SUCCESS:
      return {
        ...state,
        detailSliders: {
          ...state.detailSliders,
          loading: false,
          data: action.payload,
          error: null,
        },
      };

    case Types.INIT_DELETE_SLIDERS:
      return {
        ...state,
        deleteSliders: {
          ...state.deleteSliders,
          loading: true,
        },
      };
    case Types.DELETE_SLIDERS_SUCCESS:
      return {
        ...state,
        deleteSliders: {
          ...state.deleteSliders,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.DELETE_SLIDERS_FAILED:
      return {
        ...state,
        deleteSliders: {
          ...state.deleteSliders,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.FETCH_SLIDERS_FAILED:
      return {
        ...state,
        detailSliders: {
          ...state.detailSliders,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_UPLOAD_IMAGE:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
        },
      };
    case Types.UPLOAD_IMAGE_SUCCESS:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
          error: null,
          data: action.payload,
        },
      };
    case Types.UPLOAD_IMAGE_FAILED:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
          error: action.payload,
          data: null,
        },
      };
    default:
      return newState;
  }
};
