import { combineReducers } from "redux";
import { categories } from "./categories";

export const categorysReducers = combineReducers({
  categories,
});
