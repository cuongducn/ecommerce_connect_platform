import * as Types from "../../../constants/actionType";

var initialState = {
  listCategories: {
    data: [],
    loading: false,
    error: null,
  },
  createCategory: {
    loading: false,
    error: null,
  },
  detailCategory: {
    data: null,
    loading: false,
    error: null,
  },
  updateCategory: {
    loading: false,
    error: null,
  },
  deleteCategory: {
    error: null,
    loading: false,
  },
  uploadImage: {
    error: null,
    data: "",
    loading: false,
  },
};

export const categories = (state = initialState, action) => {
  let newState = { ...state };
  switch (action.type) {
    case Types.INIT_FETCH_ALL_CATEGORY:
      return {
        ...state,
        listCategories: {
          ...state.listCategories,
          loading: true,
        },
      };
    case Types.FETCH_ALL_CATEGORY_SUCCESS:
      return {
        ...state,
        listCategories: {
          ...state.listCategories,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.FETCH_ALL_CATEGORY_FAILED:
      return {
        ...state,
        listCategories: {
          ...state.listCategories,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_CREATE_CATEGORY:
      return {
        ...state,
        createCategory: {
          ...state.createCategory,
          loading: true,
        },
      };
    case Types.CREATE_CATEGORY_SUCCESS:
      return {
        ...state,
        createCategory: {
          ...state.createCategory,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.CREATE_CATEGORY_FAILED:
      return {
        ...state,
        createCategory: {
          ...state.createCategory,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_UPDATE_CATEGORY:
      return {
        ...state,
        updateCategory: {
          ...state.updateCategory,
          loading: true,
        },
      };
    case Types.UPDATE_CATEGORY_SUCCESS:
      return {
        ...state,
        updateCategory: {
          ...state.updateCategory,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.UPDATE_CATEGORY_FAILED:
      return {
        ...state,
        updateCategory: {
          ...state.updateCategory,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_FETCH_CATEGORY:
      return {
        ...state,
        detailCategory: {
          ...state.detailCategory,
          loading: true,
        },
      };
    case Types.FETCH_CATEGORY_SUCCESS:
      return {
        ...state,
        detailCategory: {
          ...state.detailCategory,
          loading: false,
          data: action.payload,
          error: null,
        },
      };

    case Types.FETCH_CATEGORY_FAILED:
      return {
        ...state,
        detailCategory: {
          ...state.detailCategory,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_DELETE_CATEGORY:
      return {
        ...state,
        deleteCategory: {
          ...state.deleteCategory,
          loading: true,
        },
      };
    case Types.DELETE_CATEGORY_SUCCESS:
      return {
        ...state,
        deleteCategory: {
          ...state.deleteCategory,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.DELETE_CATEGORY_FAILED:
      return {
        ...state,
        deleteCategory: {
          ...state.deleteCategory,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_UPLOAD_IMAGE:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
        },
      };
    case Types.UPLOAD_IMAGE_SUCCESS:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
          error: null,
          data: action.payload,
        },
      };
    case Types.UPLOAD_IMAGE_FAILED:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
          error: action.payload,
          data: null,
        },
      };
    default:
      return newState;
  }
};
