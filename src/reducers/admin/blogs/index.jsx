import { combineReducers } from "redux";
import { blogs } from "./blogs";

export const blogsReducers = combineReducers({
  blogs,
});
