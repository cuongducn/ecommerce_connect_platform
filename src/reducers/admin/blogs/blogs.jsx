import * as Types from "../../../constants/actionType";

var initialState = {
  listBlogs: {
    data: [],
    loading: false,
    error: null,
  },
  createBlogs: {
    loading: false,
    error: null,
  },
  detailBlogs: {
    data: {},
    loading: false,
    error: null,
  },
  updateBlogs: {
    loading: false,
    error: null,
  },
  deleteBlogs: {
    error: null,
    loading: false,
  },
  uploadImage: {
    error: null,
    data: "",
    loading: false,
  },
  uploadVideo: {
    error: null,
    data: "",
    loading: false,
  },
};

export const blogs = (state = initialState, action) => {
  let newState = { ...state };
  switch (action.type) {
    case Types.INIT_FETCH_ALL_BLOGS:
      return {
        ...state,
        listBlogs: {
          ...state.listBlogs,
          loading: true,
        },
      };
    case Types.FETCH_ALL_BLOGS_SUCCESS:
      return {
        ...state,
        listBlogs: {
          ...state.listBlogs,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.FETCH_ALL_BLOGS_FAILED:
      return {
        ...state,
        listBlogs: {
          ...state.listBlogs,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_CREATE_BLOGS:
      return {
        ...state,
        createBlogs: {
          ...state.createBlogs,
          loading: true,
        },
      };
    case Types.CREATE_BLOGS_SUCCESS:
      return {
        ...state,
        createBlogs: {
          ...state.createBlogs,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.CREATE_BLOGS_FAILED:
      return {
        ...state,
        createBlogs: {
          ...state.createBlogs,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_UPDATE_BLOGS:
      return {
        ...state,
        updateBlogs: {
          ...state.updateBlogs,
          loading: true,
        },
      };
    case Types.UPDATE_BLOGS_SUCCESS:
      return {
        ...state,
        updateBlogs: {
          ...state.updateBlogs,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.UPDATE_BLOGS_FAILED:
      return {
        ...state,
        updateBlogs: {
          ...state.updateBlogs,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_FETCH_BLOGS:
      return {
        ...state,
        detailBlogs: {
          ...state.detailBlogs,
          loading: true,
        },
      };
    case Types.FETCH_BLOGS_SUCCESS:
      return {
        ...state,
        detailBlogs: {
          ...state.detailBlogs,
          loading: false,
          data: action.payload,
          error: null,
        },
      };
    case Types.FETCH_BLOGS_FAILED:
      return {
        ...state,
        detailBlogs: {
          ...state.detailBlogs,
          loading: false,
          data: null,
          error: action.payload,
        },
      };
    case Types.INIT_UPLOAD_IMAGE:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
        },
      };
    case Types.UPLOAD_IMAGE_SUCCESS:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
          error: null,
          data: action.payload,
        },
      };
    case Types.UPLOAD_IMAGE_FAILED:
      return {
        ...state,
        uploadImage: {
          ...state.uploadImage,
          loading: false,
          error: action.payload,
          data: null,
        },
      };

    case Types.INIT_UPLOAD_VIDEO:
      return {
        ...state,
        uploadVideo: {
          ...state.uploadVideo,
          loading: true,
        },
      };
    case Types.UPLOAD_VIDEO_SUCCESS:
      return {
        ...state,
        uploadVideo: {
          ...state.uploadVideo,
          loading: false,
          error: null,
          data: action.payload,
        },
      };
    case Types.UPLOAD_VIDEO_FAILED:
      return {
        ...state,
        uploadVideo: {
          ...state.uploadVideo,
          loading: false,
          error: action.payload,
          data: null,
        },
      };
    default:
      return newState;
  }
};
