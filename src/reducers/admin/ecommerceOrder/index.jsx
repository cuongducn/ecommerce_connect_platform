import * as Types from '../../../constants/actionType';

const initialState = {
  listEcommerceProduct: {},
};

export const ecommerceProductReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.ADMIN_GET_ECOMMERCE_ORDER:
      newState.listEcommerceProduct = action.data;
      return newState;
    default:
      return newState;
  }
};
