import { combineReducers } from 'redux';
import { categoryAdmin } from './category';

export const categoryAdminReducers = combineReducers({
  categoryAdmin,
});
