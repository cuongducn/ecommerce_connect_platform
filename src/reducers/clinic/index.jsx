import * as Types from "../../constants/actionType";

const initialState = {
  clinics: {},
  clinic: {},
  doctorsClinic: {},
  calendarsClinic: {},
  appointmentsClinic: {},
  appointmentClinic: {},
};

export const clinicReducers = (state = initialState, action) => {
  const newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case Types.GET_CLINICS:
      newState.clinics = action.data;
      return newState;
    case Types.GET_CLINIC:
      newState.clinic = action.data;
      return newState;
    case Types.GET_DOCTORS_CLINIC:
      newState.doctorsClinic = action.data;
      return newState;
    case Types.GET_CALENDARS_CLINIC:
      newState.calendarsClinic = action.data;
      return newState;
    case Types.ADD_APPOINTMENT_CLINIC:
      newState.appointmentClinic = action.data;
      return newState;
    default:
      return newState;
  }
};
