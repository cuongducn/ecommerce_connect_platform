import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const PopupStyles = styled.div`
  transition: all 0.4s;
  opacity: 0;
  visibility: hidden;
  .popup__content {
    box-shadow: 2px 4px 6px 2px rgba(0, 0, 0, 0.1),
      0 2px 4px -2px rgba(0, 0, 0, 0.1);
  }
  &::before {
    content: "";
    position: absolute;
    left: 20%;
    top: -8px;
    width: 15px;
    height: 15px;
    background-color: white;
    transform: rotate(225deg);
    box-shadow: 1px 2px 2px rgba(0, 0, 0, 0.05);
  }
`;

const Popup = ({ item, className = "", children }) => {
  return (
    <PopupStyles
      className={`absolute z-[1000] top-[calc(100%+11px)] bg-[#ffffff] rounded ${className}`}
    >
      <div className="flex flex-col py-3 rounded popup__content">
        {item &&
          item.map((item) => (
            <Link
              to={item.url}
              key={item.id}
              className="transition-all whitespace-nowrap hover:no-underline hover:text-bodyColor"
            >
              <span className="block px-4 py-2 duration-150 hover:bg-main hover:text-white">
                {item.name}
              </span>
            </Link>
          ))}
        {children}
      </div>
    </PopupStyles>
  );
};

export default Popup;
