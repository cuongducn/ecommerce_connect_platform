import data from "@emoji-mart/data";
import Picker from "@emoji-mart/react";

const Emoij = ({ handleSelectEmoji }) => {
  return <Picker data={data} onEmojiSelect={handleSelectEmoji} theme="light" />;
};
export default Emoij;
