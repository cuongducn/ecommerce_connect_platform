import React from 'react';
import styled from 'styled-components';
import { formatNumber } from '../../utils';

const ProductCardStyles = styled.div`
  .slick-list {
    margin: 0 -15px;
    padding: 10px 0;
  }
  .slick-slide > div {
    padding: 0 15px;
  }
  .slick-track {
    margin: 0;
  }
  .roundedProduct {
    box-shadow: 0 0 0 0px rgba(255, 255, 0, 0.35);
    animation: roundedProduct 2.5s ease-in-out infinite;
  }

  @keyframes roundedProduct {
    0% {
      box-shadow: 0 0 0 0px rgba(255, 255, 0, 0.35);
    }
    50% {
      box-shadow: 0 0 0 10px rgba(255, 255, 0, 0.35);
    }
    100% {
      box-shadow: 0 0 0 0px rgba(255, 255, 0, 0.35);
    }
  }
`;

const ProductCard = ({ product, isLazyLoad }) => {
  return (
    <ProductCardStyles className="flex flex-col card__item ">
      <div className=" overflow-auto group cursor-pointer">
        {/* <div className=" h-full" onClick={() => handleShowModal(product)}> */}
        <div className="">
          {/* <img
            className="object-cover h-[] w-full duration-300 group-hover:scale-105"
            // src={product?.images?.length > 0 ? product.images[0] : NoImage}
            src={product.image_link[0]}
            loading="lazy"
            alt="sản_phẩm"
          /> */}
          <h4
            className="portrait relative duration-300 group-hover:scale-95"
            style={{ backgroundImage: `url(${product.image_link[0]})` }}
          >
            {/* <span className="absolute top-3 left-0 py-1 px-2 bg-warning  text-secondary">
              8%
            </span> */}
          </h4>
        </div>
      </div>
      <div className="flex flex-col justify-between flex-1 px-1 pt-2 pb-4">
        <div className="mb-2 text-base font-medium product-name flex justify-between">
          <div className="hover:no-underline hover:text-bodyColor active:text-bodyColor focus:text-bodyColor line-clamp-1 ">
            {product.name}
          </div>
          <div className="hover:no-underline text-xl hover:text-bodyColor active:text-bodyColor focus:text-bodyColor line-clamp-1 min-w-[75px]">
            {formatNumber(product.price, ',')}
          </div>
        </div>
        <div></div>
      </div>
    </ProductCardStyles>
  );
};

export default ProductCard;
