import React from "react";
import ReactPaginate from "react-paginate";
import "./PaginationStyle.css";
const ReactPagination = (props) => {
  return (
    <>
      <ReactPaginate
        nextLabel={
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M8.25 4.5l7.5 7.5-7.5 7.5"
            />
          </svg>
        }
        previousLabel={
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth="1.5"
            stroke="currentColor"
            className="w-6 h-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M15.75 19.5L8.25 12l7.5-7.5"
            />
          </svg>
        }
        pageCount={props.pageCount}
        onPageChange={props.handleClickPage}
        marginPagesDisplayed={props.marginPagesDisplayed}
        pageRangeDisplayed={props.pageRangeDisplayed}
        breakLabel="..."
        containerClassName="manage-paginationContent"
        pageClassName="manage-paginationItem"
        pageLinkClassName="manage-paginationLink"
        previousClassName="manage-paginationItem"
        previousLinkClassName="manage-paginationLink manage-paginationLinkPrev"
        nextClassName="manage-paginationItem"
        nextLinkClassName="manage-paginationLink manage-paginationLinkNext"
        breakClassName="manage-paginationItem"
        breakLinkClassName="manage-paginationLink"
        activeClassName="active"
        disabledLinkClassName="manage-disabled"
        forcePage={props.page}
      />
    </>
  );
};

export default ReactPagination;
