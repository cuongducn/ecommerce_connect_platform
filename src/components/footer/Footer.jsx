import React from 'react';
import { Link } from 'react-router-dom';
import { v4 } from 'uuid';
import { store } from '../../utils/store';
import IconEmailSolid from '../icon/email/IconEmailSolid';
import IconLocationSolid from '../icon/location/IconLocationSolid';
import IconPhone from '../icon/phone/IconPhone';
import GooglePlay from '../../assets/googlePlay.png';
import AppStore from '../../assets/appStore.png';
import BCT from '../../assets/bo_cong_thuong.png';
import Logo from '../../assets/logo.png';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
const bookSchedules = [
  {
    id: v4(),
    name: 'Bác sĩ',
    url: '/bac-si',
  },
  {
    id: v4(),
    name: 'Phòng khám',
    url: '/phong-kham',
  },
  {
    id: v4(),
    name: 'Gói khám bệnh',
    url: '/goi-kham',
  },
];
const services = [
  {
    id: v4(),
    name: 'Liên hệ',
    url: '/lien-he',
  },
  {
    id: v4(),
    name: 'Điều khoản dịch vụ',
    url: '/dieu-khoan-dich-vu',
  },
  {
    id: v4(),
    name: 'Chính sách bảo mật',
    url: '/chinh-sach-bao-mat',
  },
  {
    id: v4(),
    name: 'Quy định sử dụng',
    url: '/quy-dinh-su-dung',
  },
];

const FooterStyles = styled.div`
  .title-column {
    font-size: 1rem;
  }

  @media (max-width: 640px) {
    .footer-column {
      align-items: center;
    }
  }
`;

const Footer = () => {
  const { badges } = useSelector((state) => state.badgeReducers);

  const contacts = [
    {
      id: v4(),
      icon: <IconLocationSolid className="w-6 h-6" />,
      name: badges.admin_contact?.address,
    },
    {
      id: v4(),
      icon: <IconEmailSolid className="w-6 h-6" />,
      name: badges.admin_contact?.email,
    },
    {
      id: v4(),
      icon: <IconPhone className="w-5 h-5" />,
      name: badges.admin_contact?.phone_number,
    },
  ];
  return (
    <FooterStyles>
      <div className="border-t-[1px] border-[#efefef] bg-main text-white">
        <div className="containerWidthFooter">
          <div className="mt-10 mb-[300px]">
            <div className="grid grid-cols-1 gap-10 text-center sm:text-left sm:grid-cols-2 lg:grid-cols-4">
              <div className="flex flex-col justify-start gap-y-2 footer-column">
                <p className="title-column">
                  <strong>ASAHAA</strong>
                </p>
                <p className="w-2/3">
                  Với thông điệp "Refined Life", ASAHAA mong muốn đem đến cho
                  khách hàng một lối sống tinh gọn bằng các sản phẩm thời trang
                  tinh tế.
                </p>
              </div>
              <div className="flex flex-col gap-y-2 footer-column">
                <p className="title-column">
                  <strong>ĐỊA CHỈ</strong>
                </p>
                <p className="w-4/6">
                  105 - D6, ngõ 4B Đặng Văn Ngữ 70 Tô Hiến Thành 342 Cầu Giấy 46
                  Đông Các
                </p>
              </div>
              <div className="flex flex-col gap-y-2 footer-column">
                <p className="title-column">
                  <strong>CHÍNH SÁCH</strong>
                </p>
                <ul>
                  {services.map((service) => (
                    <li
                      key={service.id}
                      className="flex items-center justify-center text-sm gap-x-3 sm:justify-start"
                    >
                      <Link
                        to={service.url}
                        className="transition-all hover:text-white hover:no-underline hover:scale-105"
                      >
                        {service.name}
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
              <div className="flex flex-col gap-y-2 footer-column">
                <p className="title-column">
                  <strong>SOCIAL</strong>
                </p>
                <ul className="py-3 flex items-center gap-5">
                  <li>
                    <a
                      target="_blank"
                      href="https://www.facebook.com/profile.php?id=100009646952354"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                        aria-hidden="true"
                        role="img"
                        className="iconify iconify--mdi"
                        width="25"
                        height="25"
                        preserveAspectRatio="xMidYMid meet"
                        viewBox="0 0 24 24"
                      >
                        <path
                          d="M5 3h14a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2m13 2h-2.5A3.5 3.5 0 0 0 12 8.5V11h-2v3h2v7h3v-7h3v-3h-3V9a1 1 0 0 1 1-1h2V5z"
                          fill="currentColor"
                        ></path>
                      </svg>
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://www.instagram.com/ndc.nt/">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                        aria-hidden="true"
                        role="img"
                        className="iconify iconify--mdi"
                        width="25"
                        height="25"
                        preserveAspectRatio="xMidYMid meet"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          d="M7.8 2h8.4C19.4 2 22 4.6 22 7.8v8.4a5.8 5.8 0 0 1-5.8 5.8H7.8C4.6 22 2 19.4 2 16.2V7.8A5.8 5.8 0 0 1 7.8 2m-.2 2A3.6 3.6 0 0 0 4 7.6v8.8C4 18.39 5.61 20 7.6 20h8.8a3.6 3.6 0 0 0 3.6-3.6V7.6C20 5.61 18.39 4 16.4 4H7.6m9.65 1.5a1.25 1.25 0 0 1 1.25 1.25A1.25 1.25 0 0 1 17.25 8A1.25 1.25 0 0 1 16 6.75a1.25 1.25 0 0 1 1.25-1.25M12 7a5 5 0 0 1 5 5a5 5 0 0 1-5 5a5 5 0 0 1-5-5a5 5 0 0 1 5-5m0 2a3 3 0 0 0-3 3a3 3 0 0 0 3 3a3 3 0 0 0 3-3a3 3 0 0 0-3-3Z"
                        ></path>
                      </svg>
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://www.youtube.com">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                        aria-hidden="true"
                        role="img"
                        className="iconify iconify--mdi"
                        width="25"
                        height="25"
                        preserveAspectRatio="xMidYMid meet"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          d="m10 15l5.19-3L10 9v6m11.56-7.83c.13.47.22 1.1.28 1.9c.07.8.1 1.49.1 2.09L22 12c0 2.19-.16 3.8-.44 4.83c-.25.9-.83 1.48-1.73 1.73c-.47.13-1.33.22-2.65.28c-1.3.07-2.49.1-3.59.1L12 19c-4.19 0-6.8-.16-7.83-.44c-.9-.25-1.48-.83-1.73-1.73c-.13-.47-.22-1.1-.28-1.9c-.07-.8-.1-1.49-.1-2.09L2 12c0-2.19.16-3.8.44-4.83c.25-.9.83-1.48 1.73-1.73c.47-.13 1.33-.22 2.65-.28c1.3-.07 2.49-.1 3.59-.1L12 5c4.19 0 6.8.16 7.83.44c.9.25 1.48.83 1.73 1.73Z"
                        ></path>
                      </svg>
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://www.tiktok.com">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                        aria-hidden="true"
                        role="img"
                        className="iconify iconify--ic"
                        width="25"
                        height="25"
                        preserveAspectRatio="xMidYMid meet"
                        viewBox="0 0 24 24"
                      >
                        <path
                          fill="currentColor"
                          d="M16.6 5.82s.51.5 0 0A4.278 4.278 0 0 1 15.54 3h-3.09v12.4a2.592 2.592 0 0 1-2.59 2.5c-1.42 0-2.6-1.16-2.6-2.6c0-1.72 1.66-3.01 3.37-2.48V9.66c-3.45-.46-6.47 2.22-6.47 5.64c0 3.33 2.76 5.7 5.69 5.7c3.14 0 5.69-2.55 5.69-5.7V9.01a7.35 7.35 0 0 0 4.3 1.38V7.3s-1.88.09-3.24-1.48z"
                        ></path>
                      </svg>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="text-center py-4 bg-[rgba(0,0,0,0.1)] mt-8 text-sm"></div>
      </div>
    </FooterStyles>
  );
};

export default Footer;
