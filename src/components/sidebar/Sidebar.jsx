import React from "react";

import { Link, NavLink, Outlet } from "react-router-dom";
import styled from "styled-components";
import { sidebarMenu } from "../../utils/helper";
const SidebarStyles = styled.div`
  a {
    &:hover,
    &:active,
    &:focus {
      text-decoration: none;
      color: rgb(45, 52, 54);
    }
  }
`;

const Sidebar = () => {
  return (
    <SidebarStyles className="bg-gray-100">
      <div className="containerWidth">
        <div className="py-8">
          <div className="flex flex-col md:flex-row gap-5">
            <div className="flex flex-col w-full md:w-[220px] text-sm md:text-base bg-[#fff] p-4 flex-shrink-0">
              {sidebarMenu.map((item) => (
                <div key={item.id}>
                  {item?.childs?.length > 0 ? (
                    <>
                      <div className="flex items-center gap-x-2 text-[rgb(117,117,117)] cursor-pointer p-3 duration-300">
                        {item.icon}
                        <div>{item.name}</div>
                      </div>
                      {item?.childs?.map((itemChild) => (
                        // <NavLink>

                        // </NavLink>
                        <Link
                          to={itemChild.url}
                          key={itemChild.id}
                          className={`flex items-center gap-x-2 text-[rgb(117,117,117)] cursor-pointer hover:text-[rgb(45,52,54)] p-3 duration-300`}
                        >
                          <span className="w-6 h-6"></span>
                          <div>{itemChild.name}</div>
                        </Link>
                      ))}
                    </>
                  ) : (
                    <Link
                      to={item.url}
                      className={`flex items-center gap-x-2 text-[rgb(117,117,117)] cursor-pointer hover:text-[rgb(45,52,54)] p-3 duration-300`}
                    >
                      {item.icon}
                      <div>{item.name}</div>
                    </Link>
                  )}
                </div>
              ))}
            </div>
            <div className="bg-[#fff] w-full p-4">
              <Outlet></Outlet>
            </div>
          </div>
        </div>
      </div>
    </SidebarStyles>
  );
};

export default Sidebar;
