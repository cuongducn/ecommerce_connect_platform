import {
  CloseOutlined,
  MenuOutlined,
  SearchOutlined,
  ShoppingOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Space } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Drawer, Dropdown, Popover } from 'rsuite';
import styled from 'styled-components';
import * as cartAction from '../../actions/cart';
import * as profileAction from '../../actions/profile';
import { navbarMenu } from '../../utils/helper';
import Cart from '../cart/cart';
import IconArrowDown from '../icon/arrow/IconArrowDown';
import Link from '../navLink/Link';
import Popup from '../popup/Popup';
import Search from '../search';

const HeaderStyles = styled.div`
  .blur-all_size {
    background-color: #0000001a;
  }

  .navbar__item {
    position: sticky;

    &:after {
      content: '';
      display: block;
      transition: transform 0.4s ease-in-out;
    }
    span {
      transition: all 0.4s ease-in-out;
    }
  }
  &.navbar__sticky {
    position: sticky;
    z-index: 100;
    background-color: #ffffff;
    top: 0;
    left: 0;
    box-shadow: 0 4px 20px rgba(0, 0, 0, 0.25);
    animation: headerSticky 1s ease-in-out 1;
  }

  .cart-icon_bag {
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 100px;
    position: absolute;
    right: -2px;
    top: 20px;
    width: 16px;
    height: 16px;
    background-color: #2f5acf;
    color: #fff;
    font-size: 10px;
    font-weight: 700;
    pointer-events: none;
  }
  .navbar-icon_small {
    cursor: pointer;
  }

  @media (max-width: 1024px) {
    .header-icon-user,
    .navbar-small {
      display: none;
    }
    .navbar-icon_small {
      display: block;
    }

    .header-branch {
      margin-right: 10px;
      font-size: 2rem;
    }

    .header-icon {
      font-size: 1.2rem;
    }

    .list-icon_sub {
      column-gap: 0.8rem;
    }
  }

  // // @media (max-width: 768px) {
  // //   .header-navbar_small {
  // //     padding: 0 20px;
  // //   }

  // //   .content-navbar_small {
  // //     padding: 10px 20px 0 20px;
  // //   }
  // // }
  // .header-navbar_small {
  //   padding: 0px 20px;
  // }

  // .content-navbar_small {
  //   padding: 10px 20px 0px 20px;
  // }

  @media (min-width: 1024px) {
    .header-icon-user,
    .navbar-small {
      display: flex;
    }

    .navbar-icon_small {
      display: none;
    }
  }

  @keyframes headerSticky {
    0% {
      transform: translateY(-100%);
    }
    100% {
      transform: translateY(0);
    }
  }
`;

const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.authReducers);
  const { profile } = useSelector((state) => state.profileReducers);
  const { badges } = useSelector((state) => state.badgeReducers);
  const [isShowSearch, setIsShowSearch] = useState(false);
  const [isNavbarSmall, setIsNavbarSmall] = useState(false);
  const [showCart, setShowCart] = useState(false);
  const [cartDataLocal, setCartDataLocal] = useState([]);
  const trigger = useRef();
  const cartRef = useRef(null);
  const toggleButtonRef = useRef(null);
  const [isShowCart, setIsShowCart] = useState(false);
  const [cartData, setCartData] = useState({});

  const { totalItem } = useSelector((state) => state.cartReducers);
  useEffect(() => {
    dispatch(cartAction.getCart());
    dispatch(profileAction.getProfile());
  }, [dispatch]);

  const handleScrollHeader = () => {
    const heightScroll = window.scrollY;
    const navbarMenu = document.querySelector('.navbar__menu');
    if (heightScroll > 250) {
      navbarMenu?.classList?.add('navbar__sticky');
    } else {
      navbarMenu?.classList?.remove('navbar__sticky');
    }
  };

  useEffect(() => {
    function handleClickOutside(event) {
      if (
        cartRef.current &&
        !cartRef.current.contains(event.target) &&
        !toggleButtonRef.current.contains(event.target)
      ) {
        setIsShowCart(false);
      }
    }
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  const handleUser = () => {
    if (!profile) {
      navigate('/c/login');
    } else {
      navigate('/c/profile');
    }
  };

  // useEffect(() => {
  //   const token = actionsLocalStorage.getItem('token');

  // }, [navigate]);

  useEffect(() => {
    window.addEventListener('scroll', handleScrollHeader);
    return () => window.removeEventListener('scroll', handleScrollHeader);
  }, []);
  return (
    <>
      <HeaderStyles className="navbar__menu">
        <div className="containerWidthHeader">
          <div className="flex items-center justify-between py-[10px]">
            <div
              className="text-[1.4rem] navbar-icon_small"
              onClick={() => setIsNavbarSmall(!isNavbarSmall)}
            >
              <MenuOutlined />
            </div>
            <div>
              <a
                className="text-[3rem] mr-[50px] font-bold unselectable header-branch"
                href="/"
              >
                ASAHAA
              </a>
            </div>
            <div className="flex items-center content-center gap-x-7 mr-[auto] navbar-small unselectable">
              {navbarMenu.map((item) => (
                <div key={item.id}>
                  {item.childs?.length > 0 ? (
                    <div className="navbar__item text-[1.2rem] group after:border-main after:border-[1px] after:scale-0 hover:after:scale-100 relative">
                      <div className="flex items-center gap-x-[6px] font-medium">
                        <span className="group-hover:text-main">
                          {item.name}
                        </span>
                        <IconArrowDown
                          className="w-[14px] h-[14px] group-hover:text-main"
                          strokeWidth="3"
                        />
                      </div>
                      <Popup
                        item={item.childs}
                        className="group-hover:opacity-100 group-hover:visible"
                      ></Popup>
                    </div>
                  ) : (
                    <div className="navbar__item text-[1.2rem] group after:border-main after:border-[1px] after:scale-0 hover:after:scale-100">
                      <Link
                        to={item.url}
                        className="font-medium hover:no-underline hover:text-bodyColor"
                      >
                        <span className="group-hover:text-main">
                          {item.name}
                        </span>
                      </Link>
                    </div>
                  )}
                </div>
              ))}
            </div>
            <div className="flex items-center gap-x-7 list-icon_sub">
              <div className="navbar__item group ">
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  className="font-medium hover:no-underline hover:text-bodyColor cursor-pointer text-[1.6rem] header-icon"
                  onClick={() => setIsShowSearch(!isShowSearch)}
                >
                  <SearchOutlined />
                  <span className="group-hover:text-main"></span>
                </a>

                {/* <Link
                  to={item.url}
                  className="font-medium hover:no-underline hover:text-bodyColor"
                >
                  <span className="group-hover:text-main">item name</span>
                </Link> */}
              </div>
              <div className="navbar__item group">
                {/* <Link to={'/c/login'} className="font-medium "> */}
                <span
                  className="font-medium hover:no-underline hover:text-bodyColor cursor-pointer text-[1.6rem] header-icon header-icon-user"
                  onClick={() => handleUser()}
                >
                  <UserOutlined />
                  <span className="group-hover:text-main"></span>
                </span>
                {/* </Link> */}
              </div>
              <div
                className="navbar__item group "
                onClick={() => setIsShowCart(!isShowCart)}
                ref={toggleButtonRef}
              >
                <a
                  // href={'/mua-thuocs'}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="font-medium hover:no-underline hover:text-bodyColor cursor-pointer text-[1.6rem] header-icon relative"
                  // onClick={() => setShowCart(!showCart)}
                >
                  {/* <IconBag /> */}
                  <ShoppingOutlined />
                  <span className="cart-icon_bag">{totalItem}</span>
                </a>
              </div>
            </div>
          </div>
        </div>

        {isShowCart ? (
          <div ref={cartRef}>
            <Cart isShowCart={isShowCart} setIsShowCart={setIsShowCart} />
          </div>
        ) : (
          ''
        )}

        <Drawer
          title="Drawer with extra actions"
          placement={'bottom'}
          onClose={() => setIsShowSearch(false)}
          open={isShowSearch}
          style={{ height: '85vh' }}
          extra={
            <Space>
              {/* <Button onClick={onClose}>Cancel</Button>
              <Button type="primary" onClick={onClose}>
                OK
              </Button> */}
            </Space>
          }
        >
          <div className="">
            <Search
              isShowSearch={isShowSearch}
              setIsShowSearch={setIsShowSearch}
            />
          </div>
        </Drawer>

        <Drawer
          title="Drawer with extra actions"
          placement={'top'}
          width={'100vw'}
          style={{ height: '100vh' }}
          onClose={() => setIsNavbarSmall(false)}
          open={isNavbarSmall}
          extra={<Space></Space>}
        >
          <div>
            <div className="px-[50px] py-[10px] bg-black text-white flex justify-between items-center header-navbar_small">
              <a className="text-[2.1rem]" href="/">
                ASAHAA
              </a>
              <CloseOutlined
                className="text-[1.5rem] font-extrabold cursor"
                onClick={() => setIsNavbarSmall(false)}
              />
            </div>
            <div className="py-2 px-10 sm:grid sm:grid-cols-1 relative content-navbar_small">
              {navbarMenu.map((item) => (
                <div key={item.id}>
                  <div className="navbar__item text-[1.2rem] my-[4px]">
                    <Link
                      to={item.url}
                      className="font-medium "
                      onClick={() => setIsNavbarSmall(false)}
                    >
                      <span className="">{item.name}</span>
                    </Link>
                  </div>
                </div>
              ))}
              <div>
                <div className="navbar__item text-[1.2rem] my-[4px]">
                  <Link
                    to={'/c/login'}
                    className="font-medium "
                    onClick={() => setIsNavbarSmall(false)}
                  >
                    <span className="">LOGIN</span>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </Drawer>
      </HeaderStyles>
    </>
  );
};

const renderAdminSpeaker = ({ onClose, left, top, className }, ref) => {
  const handleSelect = () => {
    onClose();
  };
  return (
    <Popover ref={ref} className={className} style={{ left, top }} full>
      <Dropdown.Menu onSelect={handleSelect}>
        <Dropdown.Item as={Link} href="/thong-tin-ca-nhan">
          Thông tin cá nhân
        </Dropdown.Item>
        <Dropdown.Item as={Link} href="/chat-bac-si">
          Chat với bác sĩ
        </Dropdown.Item>
        <Dropdown.Item as={Link} href="/vi-khach-hang/thong-tin">
          Ví khách hàng
        </Dropdown.Item>
        <Dropdown.Item as={Link} href="/ho-so-suc-khoe">
          Hồ sơ sức khỏe
        </Dropdown.Item>
        <Dropdown.Item as={Link} href="/lich-kham">
          Lịch khám bệnh
        </Dropdown.Item>
        <Dropdown.Item divider />
        <Dropdown.Item as={Link} href="/dang-xuat">
          Đăng xuất
        </Dropdown.Item>
      </Dropdown.Menu>
    </Popover>
  );
};

const mapStateToProps = (state) => {
  return {
    items: state.cartReducers.totalItem,
  };
};

export default connect(mapStateToProps)(Header);
