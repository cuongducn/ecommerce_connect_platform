import React from "react";
import { DatePicker } from "rsuite";
import isBefore from "date-fns/isBefore";
import isAfter from "date-fns/isAfter";
import styled from "styled-components";
import { formatDateObj, formatDateStr } from "../../utils/date";
import moment from "moment";
import IconArrowRightLong from "../icon/arrow/IconArrowRightLong";
import { useEffect } from "react";
import { stepAppointment } from "../../utils/stepAppointment";

const CalendarStyles = styled.div``;

const Calendar = ({
  calendarsClinic,
  date,
  setDate,
  setTimeCalendar,
  timeCalendar,
  handleDependent,
}) => {
  const handleBookDate = (dateObj) => {
    setDate(dateObj);
  };
  const handleBookTimeCalendar = (time) => {
    setTimeCalendar({
      from_time: time.from_time,
      to_time: time.to_time,
    });
  };

  const handleShowTimeByDate = (date, calendars) => {
    const dateString = formatDateStr(date, "YYYY-MM-DD");
    const [dateSelected] = calendars.filter(
      (calendar) => calendar.date === dateString
    );
    return (
      <>
        <div className="flex flex-wrap gap-y-3 gap-x-2 mt-7">
          {dateSelected?.list_time.length > 0 &&
            dateSelected.list_time.map((time, index) => (
              <div
                key={index}
                className={`px-3 py-[10px] border-[1px] rounded border-main font-medium cursor-pointer hover:bg-main hover:text-[#fff] transition-all ${
                  timeCalendar?.from_time === time.from_time &&
                  timeCalendar?.to_time === time.to_time
                    ? "bg-main text-[#fff]"
                    : ""
                }`}
                onClick={() => handleBookTimeCalendar(time)}
              >
                {moment(moment(time.from_time)).format("HH:mm")}
              </div>
            ))}
        </div>
      </>
    );
  };
  useEffect(() => {
    stepAppointment.setPathName();
  }, []);
  return (
    <CalendarStyles>
      <div className="flex flex-col items-center mb-10">
        <h4 className="mb-4 text-2xl font-medium text-center">
          Thời gian khám bệnh
        </h4>
        <div className="w-[120px] h-[2px] bg-gray-200"></div>
      </div>
      <div className="flex flex-col gap-y-3">
        {Object.entries(calendarsClinic).length > 0 && (
          <div className="bg-[#fff] p-5">
            <div className="flex items-center justify-between">
              <div className="flex items-center gap-x-10">
                <div>Chọn thời gian</div>
                {date && (
                  <DatePicker
                    name="date_book"
                    format="yyyy-MM-dd"
                    disabledDate={(date) => {
                      return (
                        isBefore(
                          date,
                          formatDateObj(calendarsClinic[0].date)
                        ) ||
                        isAfter(
                          date,
                          formatDateObj(
                            calendarsClinic[calendarsClinic.length - 1].date
                          )
                        )
                      );
                    }}
                    value={date}
                    onChange={handleBookDate}
                  />
                )}
              </div>
              {timeCalendar ? (
                <button
                  className="px-5 py-2 rounded-md bg-main text-[#fff] flex items-center gap-x-2"
                  onClick={() => handleDependent()}
                >
                  <span>Tiếp tục</span>
                  <IconArrowRightLong className="w-5 h-5" />
                </button>
              ) : null}
            </div>
            {date && handleShowTimeByDate(date, calendarsClinic)}
          </div>
        )}
      </div>
    </CalendarStyles>
  );
};

export default Calendar;
