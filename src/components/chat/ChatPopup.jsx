import React from "react";
import ReactDOM from "react-dom";
import { useState } from "react";
import Content from "./Content";
import { useSelector } from "react-redux";

const ChatPopup = () => {
  const { chatPopup } = useSelector((state) => state.homeReducers);
  return chatPopup.show
    ? ReactDOM.createPortal(<Content></Content>, document.body)
    : null;
};

export default ChatPopup;
