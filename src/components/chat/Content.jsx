import React, { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import Emoij from "../emoij/Emoij";
import IconDelete from "../icon/delete/IconDelete";
import IconEmoij from "../icon/emoij/IconEmoij";
import IconSend from "../icon/send/IconSend";
import * as chatAction from "../../actions/chat";
import InfiniteScroll from "react-infinite-scroll-component";
import { io } from "socket.io-client";
import { useRef } from "react";
import IconImage from "../icon/image/IconImage";
import * as Types from "../../constants/actionType";
import UploadListImage from "../form/UploadListImage";
import lodash from "lodash";
import moment from "moment/moment";

const host = "http://39socket.karavui.com:6441";

const ContentStyles = styled.div`
  box-shadow: 1px 4px 15px 5px rgba(0, 0, 0, 0.07);
`;

const Content = () => {
  const dispatch = useDispatch();
  const { listMessages, message } = useSelector((state) => state.chatReducers);
  const { chatPopup } = useSelector((state) => state.homeReducers);
  const { profile } = useSelector((state) => state.profileReducers);

  const [messages, setMessages] = useState([]);
  const [inputMessage, setInputMessage] = useState("");
  const [images, setImages] = useState([]);
  const [hasMore, setHasMore] = useState(true);
  const [page, setPage] = useState(1);

  const socketRef = useRef();
  const listMessagesRef = useRef();
  const listMessagesBeRef = useRef();

  const handleEnterInputMessage = (e) => {
    if (inputMessage === "" || e.keyCode !== 13) return;
    handleSendMessage();
  };
  const handleSelectEmojiAddComment = (item) => {
    setInputMessage((prevValue) => prevValue + " " + item.native);
  };
  const handleSendMessage = () => {
    const values = {
      content: inputMessage,
      images: images,
    };
    dispatch(chatAction.sendMessage(chatPopup.data?.user_id, values));
  };

  const handleFetchData = () => {
    if (messages.length >= listMessages.total) {
      setHasMore(false);
      return;
    }
    setPage((pag) => pag + 1);
  };

  const showEndMessage = () => {
    return (
      <div className="flex flex-col items-center justify-center w-full py-4 gap-y-1">
        <div className="w-[60px] h-[60px] rounded-full">
          <img
            src={chatPopup.data?.doctor_image_1}
            alt={chatPopup.data?.doctor_info?.first_and_last_name}
            className="object-cover w-full h-full rounded-full"
          />
        </div>
        <div className="font-medium">
          {chatPopup.data?.doctor_info?.first_and_last_name}
        </div>
      </div>
    );
  };

  //Xử lý hiển thị thời gian
  const handleShowTime = (time) => {
    if (time) {
      const total = moment(moment()).diff(time);
      const days = Math.floor(total / 1000 / 60 / 60 / 24);
      const dayNowInWeek = moment().isoWeekday();
      const daySendedInWeek = moment(time).isoWeekday();

      let formatDate = "";

      if (days <= 7) {
        formatDate =
          daySendedInWeek === dayNowInWeek
            ? moment(time).format("HH:mm")
            : daySendedInWeek === 1
            ? `Thứ 2 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 2
            ? `Thứ 3 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 3
            ? `Thứ 4 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 4
            ? `Thứ 5 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 5
            ? `Thứ 6 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 6
            ? `Thứ 7 ${moment(time).format("HH:mm")}`
            : daySendedInWeek === 7
            ? `Chủ nhật ${moment(time).format("HH:mm")}`
            : "";
      } else {
        formatDate = `${moment(time).format("DD/MM/YYYY HH:mm")}`;
      }
      return formatDate;
    }
    return "";
  };

  //Hide Modal Chat
  const handleHideChat = () => {
    dispatch({
      type: Types.HOME_SHOW_CHAT_POPUP,
      data: {
        show: false,
        data: {},
      },
    });
  };

  //Lắng nghe server
  useEffect(() => {
    socketRef.current = io(host);
    if (Object.entries(profile)?.length > 0) {
      socketRef.current.on(
        `chat:message_from_user_to_customer:${chatPopup.data?.user_id}:${profile.id}`,
        (messages) => {
          if (messages?.id) {
            setMessages((oldMsgs) => [messages, ...oldMsgs]);
          }
        }
      ); // mỗi khi có tin nhắn thì mess sẽ được render thêm
    }

    return () => {
      socketRef.current.disconnect();
    };
  }, []);
  //Sau khi gửi tin nhắn
  useEffect(() => {
    if (Object.entries(message).length > 0) {
      setInputMessage("");
      setImages([]);

      dispatch({
        type: Types.SEND_MESSAGE,
        data: {},
      });
    }
  }, [dispatch, message]);

  //Set height list message
  useEffect(() => {
    const boxMessage = document.querySelector(".list-messages");
    const boxMessageContent = document.querySelector(".list-messages__content");
    const menuChat = document.querySelector(".menu__chat");
    if (boxMessage && menuChat && boxMessageContent) {
      boxMessage.style.height = `${436 - menuChat.offsetHeight}px`;
      boxMessageContent.style.height = `${436 - menuChat.offsetHeight}px`;
    }
  });

  //Lấy dữ liệu all tin nhắn trong store lần đầu
  useEffect(() => {
    if (
      Object.entries(listMessages).length > 0 &&
      !lodash.isEqual(listMessagesRef.current, listMessages)
    ) {
      setMessages((prevMessages) => [...prevMessages, ...listMessages?.data]);
      listMessagesRef.current = listMessages;
    }
  }, [dispatch, listMessages, messages.length]);

  // Lấy dữ liệu messages từ be
  useEffect(() => {
    if (
      Object.entries(chatPopup.data).length > 0 &&
      !lodash.isEqual(listMessagesBeRef.current, chatPopup.data)
    ) {
      dispatch(
        chatAction.getListMessages(chatPopup.data?.id, `limit=10&page=${page}`)
      );
      setMessages([]);
      listMessagesBeRef.current = chatPopup.data;
    }
  }, [chatPopup.data, dispatch, page]);
  return (
    <ContentStyles className="fixed bottom-0 z-50 right-10">
      <div className="w-[360px] bg-[#fff]">
        <div
          className={`flex items-center justify-between p-2 border-b border-b-gray3A rounded-t-[10px]`}
        >
          <div className="flex items-center gap-x-2">
            <div className="w-10 h-10 rounded-full">
              <img
                src={chatPopup.data?.doctor_image_1}
                alt={chatPopup.data?.doctor_info?.first_and_last_name}
                className="object-cover w-full h-full rounded-full"
              />
            </div>
            <div>{chatPopup.data?.doctor_info?.first_and_last_name}</div>
          </div>
          <div
            className={`transition-all cursor-pointer  text-gray4B hover:text-gray-800 `}
            onClick={() => handleHideChat()}
          >
            <IconDelete className="w-6 h-6"></IconDelete>
          </div>
        </div>
        <div className="flex flex-col justify-end h-[380px] list-messages">
          <div>
            {messages.length > 0 ? (
              <InfiniteScroll
                dataLength={messages.length}
                next={handleFetchData}
                className="flex flex-col-reverse px-2 pb-2 mt-5 overflow-y-auto gap-y-1 list-messages__content"
                hasMore={hasMore}
                inverse={true}
                height={380}
                endMessage={showEndMessage()}
              >
                {messages.map((message, index) => (
                  <div
                    key={message.id}
                    className={`w-full ${
                      !message.is_user_send ? "justify-end" : ""
                    }`}
                  >
                    {!message.is_user_send ? (
                      <div className="flex flex-col items-end w-full">
                        {message.content !== "" && message.content !== null && (
                          <div
                            className={`px-4 py-[6px] bg-gray-100 rounded-3xl inline-block relative max-w-[68%] group
                                  `}
                          >
                            {message.content}
                            <div className="absolute top-1/2 -translate-y-1/2 z-10 flex items-center py-[6px] px-2 text-xs bg-gray-300 rounded-md right-[calc(100%+1px)] whitespace-nowrap group-hover:visible invisible opacity-0 group-hover:opacity-100 transition-all">
                              {handleShowTime(message.created_at)}
                            </div>
                          </div>
                        )}
                        {message?.images_json?.length > 0 && (
                          <div className="relative flex justify-end my-1">
                            <div
                              className={`relative grid  gap-3 group ${
                                message.images_json?.length === 1
                                  ? "grid-cols-1"
                                  : "grid-cols-2"
                              }`}
                            >
                              <div className="absolute top-1/2 -translate-y-1/2 z-10 flex items-center py-[6px] px-2 text-xs bg-gray-300 rounded-md right-[calc(100%+1px)] whitespace-nowrap group-hover:visible invisible opacity-0 group-hover:opacity-100 transition-all">
                                {handleShowTime(message.created_at)}
                              </div>
                              {message.images_json?.map((url, index) => (
                                <div
                                  key={index}
                                  className="w-[105px] h-[105px] flex items-center justify-center rounded-lg border border-gray-200"
                                >
                                  <img
                                    src={url}
                                    alt={url}
                                    loading="lazy"
                                    className="object-contain h-full"
                                  />
                                </div>
                              ))}
                            </div>
                          </div>
                        )}
                      </div>
                    ) : (
                      <div
                        className={`flex flex-row items-end w-full gap-x-2 ${
                          index < messages.length &&
                          messages[index]?.is_user_send ===
                            messages[index - 1]?.is_user_send
                            ? null
                            : "my-3"
                        }`}
                      >
                        <div className="flex-shrink-0 w-10 h-10 rounded-full">
                          {index > 0 &&
                          index < messages.length &&
                          messages[index]?.is_user_send ===
                            messages[index - 1]?.is_user_send ? null : (
                            <img
                              src={`${chatPopup.data?.doctor_image_1}`}
                              alt={chatPopup.data?.first_and_last_name}
                              className="object-cover w-full h-full rounded-full"
                            />
                          )}
                        </div>
                        <div className="w-full">
                          {message.content !== "" &&
                            message.content !== null && (
                              <div
                                className={`px-4 py-[6px] bg-gray-100 rounded-3xl max-w-[68%] relative group inline-flex items-center
                                  `}
                              >
                                {message.content}
                                <div className="absolute top-1/2 -translate-y-1/2 z-10 flex items-center py-[6px] px-2 text-xs bg-gray-300 rounded-md left-[calc(100%+1px)] whitespace-nowrap group-hover:visible invisible opacity-0 group-hover:opacity-100 transition-all">
                                  {handleShowTime(message.created_at)}
                                </div>
                              </div>
                            )}

                          {message?.images_json?.length > 0 && (
                            <div className="relative flex my-1">
                              <div
                                className={`relative grid  gap-3 group ${
                                  message.images_json?.length === 1
                                    ? "grid-cols-1"
                                    : "grid-cols-2"
                                }`}
                              >
                                <div className="absolute top-1/2 -translate-y-1/2 z-10 flex items-center py-[6px] px-2 text-xs bg-gray-300 rounded-md left-[calc(100%+1px)] whitespace-nowrap group-hover:visible invisible opacity-0 group-hover:opacity-100 transition-all">
                                  {handleShowTime(message.created_at)}
                                </div>
                                {message?.images_json?.map((url, index) => (
                                  <div
                                    key={index}
                                    className="w-[105px] h-[105px] flex items-center justify-center rounded-lg border border-gray-200"
                                  >
                                    <img
                                      src={url}
                                      alt={url}
                                      loading="lazy"
                                      className="object-contain h-full"
                                    />
                                  </div>
                                ))}
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    )}
                  </div>
                ))}
              </InfiniteScroll>
            ) : null}
            {messages.length === 0 &&
              Object.entries(listMessages).length > 0 && (
                <div className="text-sm font-medium text-center">
                  Bạn đã kết nối với {chatPopup.data?.first_and_last_name}
                </div>
              )}
          </div>
        </div>
        <div className="flex w-full p-2 gap-x-2 menu__chat">
          <div className="flex flex-col w-full bg-gray-100 rounded-xl">
            <div className={`${images.length > 0 ? "block" : "hidden"} p-2`}>
              <UploadListImage
                className="uploadListImage"
                style={{
                  width: "60px",
                  height: "60px",
                }}
                images={[]}
                setFiles={setImages}
              ></UploadListImage>
            </div>
            <div className="flex items-center gap-x-2">
              <input
                type="text"
                placeholder="Aa"
                className="px-2 py-[6px] bg-transparent w-full inputSearch flex flex-wrap"
                value={inputMessage}
                onChange={(e) => setInputMessage(e.target.value)}
                onKeyDown={handleEnterInputMessage}
              ></input>
              <div className="relative flex px-2 transition-all rounded-full cursor-pointer hover:text-blue-500 group">
                <IconEmoij className="w-6 h-6"></IconEmoij>
                <div className="absolute right-0 z-50 invisible opacity-0 bottom-full emoji-modal group-hover:opacity-100 group-hover:visible">
                  <Emoij
                    handleSelectEmoji={handleSelectEmojiAddComment}
                  ></Emoij>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-col justify-end">
            <div className="flex">
              <div
                className="flex items-center justify-center p-2 transition-all rounded-full cursor-pointer hover:bg-gray-100 hover:text-blue-500"
                onClick={() => {
                  document.querySelector(".uploadListImage input").click();
                }}
              >
                <IconImage className="w-6 h-6"></IconImage>
              </div>
              <div
                className={`flex items-center justify-center p-2 transition-all rounded-full cursor-pointer hover:bg-gray-100 hover:text-blue-500 ${
                  inputMessage === ""
                    ? "pointer-events-none select-none opacity-70"
                    : ""
                }`}
                onClick={handleSendMessage}
              >
                <IconSend className="w-6 h-6"></IconSend>
              </div>
            </div>
          </div>
        </div>
      </div>
    </ContentStyles>
  );
};

export default Content;
