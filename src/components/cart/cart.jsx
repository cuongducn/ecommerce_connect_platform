import React from 'react';
import { Close } from '@rsuite/icons';
import styled from 'styled-components';
import ItemCart from './itemCart';
import { Button } from 'antd';
import { useState } from 'react';
import { formatNumber } from '../../utils';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import * as cartAction from '../../actions/cart';

const CartStyles = styled.div`
  position: absolute;
  top: 90px;
  right: 20px;
  width: 25vw;
  background-color: white;
  // background-color: #f9f9f9;
  border-radius: 20px;
  height: 75vh;
  z-index: 1000;
  padding: 15px 0 15px 0;
  box-shadow: 0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1);
  border: 1px solid #d1d5db;
  display: flex;
  flex-direction: column;

  .btn-custom_submit:hover {
    color: white;
    border: 1px solid black;
    font-weight: 500;
  }

  @media (max-width: 1024px) {
    top: 0px;
    right: 0px;
    width: 100vw;
    border-radius: 0px;
    height: 100vh;
    z-index: 1000;
    padding: 0px;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px,
      rgba(0, 0, 0, 0.1) 0px 2px 4px -2px;
    display: flex;
    flex-direction: column;
    .containerWidth {
      max-width: 0px;
      padding: 0px;
    }

    .cart-header {
      padding 12px 14px;
      align-items: center;

      p {
        margin-bottom: 0px;
      }
    }
  }

  @media (min-width: 1024px) {
  }
`;

const Cart = (props) => {
  const dispatch = useDispatch();
  const { isShowCart, setIsShowCart } = props;
  const { totalItem, cartItems, totalMoney } = useSelector(
    (state) => state.cartReducers
  );

  useEffect(() => {
    dispatch(cartAction.getCart());
  }, [dispatch]);

  return (
    <CartStyles>
      <div className="flex justify-between px-[15px] border-b border-gray-300 cart-header">
        <p className="mb-[10px]">Giỏ hàng</p>
        <Close
          className="text-[1.2rem] cursor-pointer"
          onClick={() => setIsShowCart(!isShowCart)}
        />
      </div>
      <div className="overflow-y-auto max-h-[55vh]">
        {cartItems?.map((value, idx) => (
          <div key={idx}>{<ItemCart product={value} />}</div>
        ))}
      </div>
      <div className="px-[20px] mt-[auto] border-t border-gray-300">
        <div className="flex my-[15px] text-[15px] text-[black] justify-between">
          <p className="">THÀNH TIỀN</p>
          <span className="text-[17px]">{formatNumber(totalMoney, ',')}</span>
        </div>
        <Button
          className="w-[100%] bg-[black] text-[white] btn-custom_submit"
          size="large"
          href="/checkout"
        >
          THANH TOÁN
        </Button>
      </div>
    </CartStyles>
  );
};

// const mapStateToProps = (state) => {
//   return {
//     cart: state.cart,
//   };
// };

// export default connect(mapStateToProps)(Cart);

const mapStateToProps = (state) => {
  return {
    items: state.cartReducers,
  };
};

export default connect(mapStateToProps)(Cart);

// export default Cart;
