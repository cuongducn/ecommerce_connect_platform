import React from 'react';
import { MinusOutlined, PlusOutlined } from '@ant-design/icons';

import styled from 'styled-components';
import { formatNumber } from '../../utils';

const PaymentItemStyles = styled.div`
  display: flex;
  flex-direction: row;
  max-height: 190px;
  padding: 5px 0;
  border-bottom: 1px solid #d7d7d7;

  .payment-item_image {
    max-width: 130px;
    // height: 150px;
    padding: 10px 0;
    object-fit: cover;
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    align-item: center;
    border-radius: 5px;
    margin-right: 15px;
  }

  .payment-item_name {
    display: -webkit-box;
    -webkit-line-clamp: 2; /* Number of lines to show */
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    color: black;
  }

  .payment-item_price {
    display: flex;
    flex-direction: row;
  }

  .item_price-discount {
    color: red;
    margin-right: 20px;
  }

  .item_price-old {
    color: #b3b3b3;
    text-decoration: line-through;
    margin: 0;
  }
`;

const ItemPayment = ({ product }) => {
  return (
    <PaymentItemStyles>
      <img
        src={
          product?.image_link[0] ||
          'http://asahaa-data.cuongdn.top/api/SHImages/CCWWGAUdkV1686425121.png'
        }
        alt=""
        loading="lazy"
        className="payment-item_image"
      />
      <div className="text-[16px] py-[8px] font-medium">
        <p className="payment-item_name">{product?.name}</p>
        <div className="payment-item_price">
          <p>
            {product?.properties !== null &&
            product?.properties?.property_name !== null
              ? product?.properties?.sub_property_name !== null
                ? formatNumber(
                    product?.properties?.list_properties?.product_sub_property
                      ?.price,
                    ','
                  )
                : formatNumber(product?.properties?.list_properties?.price, ',')
              : formatNumber(product?.price, ',')}
          </p>
          {/* <p className="item_price-discount">22.000.000</p>
          <p className="item_price-old">22.000.000</p> */}
        </div>
        <div className="mt-[10px] flex flex-row content-center items-center">
          <p className="text-[15px] mr-[20px]">Số Lượng</p>
          <div className="text-center text-[12px] flex items-center justify-center  ">
            <MinusOutlined className="font-medium p-[5px] cursor-pointer" />
            <span className="text-[16px] px-[10px]">{product?.quantity}</span>
            <PlusOutlined className="font-medium p-[5px] cursor-pointer" />
          </div>
        </div>
        {/* <div className="flex mt-[5px] text-[15px] content-center items-center">
          <p className=" mr-[20px]">Màu</p>
          <span className="">Trắng</span>
        </div>
        <div className="flex mt-[5px] text-[15px] content-center items-center">
          <p className=" mr-[20px]">Size</p>
          <span className="">L</span>
        </div> */}
        {product?.properties !== null &&
          product?.properties?.property_name !== null && (
            <div className="flex mt-[3px] text-[13px] content-center items-center">
              <p className=" mr-[20px]">Màu</p>
              <span className="">
                {product?.properties?.list_properties.name}
              </span>
            </div>
          )}
        {product?.properties !== null &&
          product?.properties?.sub_property_name !== null && (
            <div className="flex mt-[3px] text-[13px] content-center items-center">
              <p className=" mr-[20px]">Size</p>
              <span className="">
                {product?.properties?.sub_property_name !== null &&
                  product?.properties?.list_properties?.product_sub_property
                    ?.name}
              </span>
            </div>
          )}
      </div>
    </PaymentItemStyles>
  );
};

export default ItemPayment;
