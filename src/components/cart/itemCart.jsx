import React from 'react';
import { CloseOutlined, MinusOutlined, PlusOutlined } from '@ant-design/icons';

import styled from 'styled-components';
import { formatNumber } from '../../utils';
import * as cartActions from '../../actions/cart';
import { useDispatch } from 'react-redux';

const CartItemStyles = styled.div`
  display: flex;
  flex-direction: row;
  max-height: 190px;
  padding: 0 15px;
  border-bottom: 1px solid #d1d5db;
  // box-shadow: 0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1);

  .cart-item_image {
    max-width: 100px;
    height: 150px;
    padding: 10px 0;
    object-fit: cover;
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat;
    align-item: center;
    border-radius: 5px;
    margin-right: 15px;
  }

  .cart-item_name {
    display: -webkit-box;
    -webkit-line-clamp: 2; /* Number of lines to show */
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    color: black;
  }

  .cart-item_price {
    display: flex;
    flex-direction: row;
  }

  .item_price-discount {
    color: red;
    margin-right: 20px;
  }

  .item_price-old {
    color: #b3b3b3;
    text-decoration: line-through;
    margin: 0;
  }
`;

const ItemCart = (props) => {
  const dispatch = useDispatch();
  const { product } = props;

  function removeItemCart(productId) {
    dispatch(cartActions.removeItemCart(productId));
  }
  return (
    <CartItemStyles>
      <a href={'product' + product.slug}>
        <img
          src={
            product?.image_link[0] ||
            'http://asahaa-data.cuongdn.top/api/SHImages/CCWWGAUdkV1686425121.png'
          }
          alt=""
          loading="lazy"
          className="cart-item_image"
        />
      </a>
      <div className="text-[15px] py-[8px] font-medium w-full unselectable">
        <div className="flex justify-between">
          <p className="cart-item_name line-clamp-1">
            <a className="line-clamp-1" href={'product' + product.slug}>
              {product.name}
            </a>
          </p>
          <div
            className="cursor-pointer"
            onClick={() => removeItemCart(product.id)}
          >
            <CloseOutlined />
          </div>
        </div>
        <div className="cart-item_price">
          <p>
            {product?.properties !== null &&
            product?.properties?.property_name !== null
              ? product?.properties?.sub_property_name !== null
                ? formatNumber(
                    product?.properties?.list_properties?.product_sub_property
                      ?.price,
                    ','
                  )
                : formatNumber(product?.properties?.list_properties.price, ',')
              : formatNumber(product?.price, ',')}
          </p>
          {/* <p className="item_price-discount">22.000.000</p>
          <p className="item_price-old">22.000.000</p> */}
        </div>
        <div className="mt-[5px] flex flex-row content-center items-center">
          <p className="text-[13px] mr-[20px]">Số lượng</p>
          <div className="text-center text-[10px] grid grid-cols-3 items-center gap-1 ">
            <MinusOutlined className=" font-bold cursor-pointer px-1" />
            <span className="text-[15px] px-[10px] text-center">
              {product?.quantity}
            </span>
            <PlusOutlined className=" font-bold cursor-pointer px-1" />
          </div>
          {/* <div className="text-center text-[12px] flex items-center justify-center  ">
            <MinusOutlined className="font-medium p-5 cursor-pointer min-w-[6px]" />
            <span className="text-[16px] px-[10px]">{product?.quantity}</span>
            <PlusOutlined className="font-medium p-5 cursor-pointer min-w-[6px]" />
          </div> */}
        </div>
        {product?.properties !== null &&
          product?.properties?.property_name !== null && (
            <div className="flex mt-[3px] text-[13px] content-center items-center">
              <p className=" mr-[20px]">Màu</p>
              <span className="">
                {product?.properties?.list_properties.name}
              </span>
            </div>
          )}
        {product?.properties !== null &&
          product.properties.sub_property_name !== null && (
            <div className="flex mt-[3px] text-[13px] content-center items-center">
              <p className=" mr-[20px]">Size</p>
              <span className="">
                {product?.properties?.sub_property_name !== null &&
                  product?.properties?.list_properties?.product_sub_property
                    ?.name}
              </span>
            </div>
          )}
      </div>
    </CartItemStyles>
  );
};

export default ItemCart;
