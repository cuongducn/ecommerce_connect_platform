import React from "react";
import { Button, Modal } from "rsuite";

const ModalCustom = ({
  title,
  children,
  nameAction,
  open,
  handleClose,
  handleAction,
  size,
}) => {
  return (
    <>
      <Modal open={open} onClose={handleClose} size={size}>
        <Modal.Header>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{children}</Modal.Body>
        <Modal.Footer>
          <Button
            onClick={handleAction}
            appearance="primary"
            style={{
              backgroundColor: "#00897b",
            }}
          >
            {nameAction}
          </Button>
          <Button onClick={handleClose} appearance="subtle">
            Hủy
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ModalCustom;
