import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import SunEditor from 'suneditor-react';
import {
  image as imagePlugin,
  font,
  fontSize,
  formatBlock,
  paragraphStyle,
  blockquote,
  fontColor,
  textStyle,
  list,
  lineHeight,
  table as tablePlugin,
  link as linkPlugin,
  video,
  audio,
  align,
  horizontalRule,
  hiliteColor,
  template,
} from 'suneditor/src/plugins';
import 'suneditor/dist/css/suneditor.min.css';
import { handleImageUploadBefore } from '../../../utils/editor';

const SunEditorCustomStyles = styled.div`
  .editor__title {
    margin-bottom: 10px;
    display: inline-block;
  }
`;

const SunEditorCustom = ({
  name = 'description',
  title,
  formValue,
  setFormValue,
}) => {
  const [content, setContent] = useState('');
  const handleChangeEditor = (editor) => {
    setFormValue((prevForm) => {
      return {
        ...prevForm,
        [name]: editor,
      };
    });
  };
  useEffect(() => {
    setContent(formValue.description);
  }, [formValue.description]);
  return (
    <SunEditorCustomStyles>
      <label className="editor__title">{title}</label>
      <SunEditor
        name={name}
        placeholder={title + '...'}
        setContents={content}
        onImageUploadBefore={handleImageUploadBefore}
        onChange={handleChangeEditor}
        showToolbar={true}
        setDefaultStyle="height: auto"
        setOptions={{
          plugins: [
            blockquote,
            align,
            font,
            fontColor,
            fontSize,
            formatBlock,
            hiliteColor,
            horizontalRule,
            lineHeight,
            list,
            paragraphStyle,
            tablePlugin,
            template,
            textStyle,
            imagePlugin,
            linkPlugin,
            video,
            audio,
          ],

          buttonList: [
            // default
            ['undo', 'redo'],
            [
              ':p-More Paragraph-default.more_paragraph',
              'font',
              'fontSize',
              'formatBlock',
              'paragraphStyle',
              'blockquote',
            ],
            [
              'bold',
              'underline',
              'italic',
              'strike',
              'subscript',
              'superscript',
            ],
            ['fontColor', 'hiliteColor', 'textStyle'],
            ['removeFormat'],
            ['outdent', 'indent'],
            ['align', 'horizontalRule', 'list', 'lineHeight'],
            [
              '-right',
              ':i-More Misc-default.more_vertical',
              'fullScreen',
              'showBlocks',
              'codeView',
              'preview',
              'save',
            ],
            ['-right', ':r-More Rich-default.more_plus', 'table'],
            ['-right', 'image', 'link'],
            // (min-width: 992)
            [
              '%992',
              [
                ['undo', 'redo'],
                [
                  ':p-More Paragraph-default.more_paragraph',
                  'font',
                  'fontSize',
                  'formatBlock',
                  'paragraphStyle',
                  'blockquote',
                ],
                ['bold', 'underline', 'italic', 'strike'],
                [
                  ':t-More Text-default.more_text',
                  'subscript',
                  'superscript',
                  'fontColor',
                  'hiliteColor',
                  'textStyle',
                ],
                ['removeFormat'],
                ['outdent', 'indent'],
                ['align', 'horizontalRule', 'list', 'lineHeight'],
                [
                  '-right',
                  ':i-More Misc-default.more_vertical',
                  'fullScreen',
                  'showBlocks',
                  'codeView',
                  'preview',
                  'save',
                ],
                [
                  '-right',
                  ':r-More Rich-default.more_plus',
                  'table',
                  'link',
                  'image',
                ],
              ],
            ],
            // (min-width: 767)
            [
              '%767',
              [
                ['undo', 'redo'],
                [
                  ':p-More Paragraph-default.more_paragraph',
                  'font',
                  'fontSize',
                  'formatBlock',
                  'paragraphStyle',
                  'blockquote',
                ],
                [
                  ':t-More Text-default.more_text',
                  'bold',
                  'underline',
                  'italic',
                  'strike',
                  'subscript',
                  'superscript',
                  'fontColor',
                  'hiliteColor',
                  'textStyle',
                ],
                ['removeFormat'],
                ['outdent', 'indent'],
                [
                  ':e-More Line-default.more_horizontal',
                  'align',
                  'horizontalRule',
                  'list',
                  'lineHeight',
                ],
                [':r-More Rich-default.more_plus', 'table', 'link', 'image'],
                [
                  '-right',
                  ':i-More Misc-default.more_vertical',
                  'fullScreen',
                  'showBlocks',
                  'codeView',
                  'preview',
                  'save',
                ],
              ],
            ],
            // (min-width: 480)
            [
              '%480',
              [
                ['undo', 'redo'],
                [
                  ':p-More Paragraph-default.more_paragraph',
                  'font',
                  'fontSize',
                  'formatBlock',
                  'paragraphStyle',
                  'blockquote',
                ],
                [
                  ':t-More Text-default.more_text',
                  'bold',
                  'underline',
                  'italic',
                  'strike',
                  'subscript',
                  'superscript',
                  'fontColor',
                  'hiliteColor',
                  'textStyle',
                  'removeFormat',
                ],
                [
                  ':e-More Line-default.more_horizontal',
                  'outdent',
                  'indent',
                  'align',
                  'horizontalRule',
                  'list',
                  'lineHeight',
                ],
                [':r-More Rich-default.more_plus', 'table', 'link', 'image'],
                [
                  '-right',
                  ':i-More Misc-default.more_vertical',
                  'fullScreen',
                  'showBlocks',
                  'codeView',
                  'preview',
                  'save',
                ],
              ],
            ],
          ],
        }}
      />
    </SunEditorCustomStyles>
  );
};

export default SunEditorCustom;
