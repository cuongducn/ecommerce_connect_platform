import { Uploader } from "rsuite";
import CameraRetroIcon from "@rsuite/icons/legacy/CameraRetro";
import React, { useEffect, useState } from "react";
import { previewFile } from "rsuite/esm/utils";

const UploadImage = React.forwardRef((props, ref) => {
  const {
    style,
    multiple = false,
    action = "#",
    image_url,
    setFile,
    ...rest
  } = props;

  const [fileInfo, setFileInfo] = useState(null);
  useEffect(() => {
    if (image_url) {
      setFileInfo(image_url);
    }
  }, [image_url]);
  return (
    <Uploader
      multiple={multiple}
      accept="image/*"
      value={fileInfo}
      fileListVisible={false}
      listType="picture"
      action={action}
      autoUpload={false}
      onChange={(file) => {
        const newFile = file[file.length - 1];
        setFile(newFile.blobFile);
        previewFile(newFile.blobFile, (value) => {
          setFileInfo(value);
          URL.revokeObjectURL(file);
        });
      }}
      {...rest}
      ref={ref}
    >
      <button
        type="button"
        style={{
          ...style,
        }}
      >
        {fileInfo ? (
          <img
            src={fileInfo}
            style={{
              width: "100%",
              height: "100%",
              borderRadius: "inherit",
            }}
            alt="img"
          />
        ) : (
          <CameraRetroIcon />
        )}
      </button>
    </Uploader>
  );
});

export default UploadImage;
