import { Uploader } from "rsuite";
import CameraRetroIcon from "@rsuite/icons/legacy/CameraRetro";
import React, { useEffect, useState } from "react";
import { uploadImage } from "../../../actions/upload";

const UploadImageV2 = React.forwardRef((props, ref) => {
  const {
    style,
    multiple = false,
    action = "#",
    image_url,
    setFile,
    ...rest
  } = props;

  const [fileInfo, setFileInfo] = useState(null);

  useEffect(() => {
    if (image_url) {
      setFileInfo(image_url);
    }
  }, [image_url]);
  return (
    <Uploader
      multiple={multiple}
      accept="image/*"
      value={fileInfo}
      fileListVisible={false}
      listType="picture"
      action={action}
      autoUpload={false}
      onChange={async (file) => {
        const newFile = file[file.length - 1].blobFile;
        const formDataImage = new FormData();
        formDataImage.append("image", newFile);
        const response = await uploadImage(formDataImage);

        setFile(response);
        setFileInfo(response);
      }}
      {...rest}
      ref={ref}
    >
      <button
        type="button"
        style={{
          ...style,
        }}
      >
        {fileInfo ? (
          <img
            src={fileInfo}
            style={{
              width: "100%",
              height: "100%",
              borderRadius: "inherit",
            }}
            alt="img"
          />
        ) : (
          <CameraRetroIcon />
        )}
      </button>
    </Uploader>
  );
});

export default UploadImageV2;
