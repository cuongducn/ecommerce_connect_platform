import React from "react";
import { Form, Radio, RadioGroup } from "rsuite";

const RadioGroupCustom = React.forwardRef((props, ref) => {
  const { name, label, style, options, accepter, ...rest } = props;

  return (
    <Form.Group controlId={`${name}-4`} ref={ref}>
      <Form.ControlLabel>{label} </Form.ControlLabel>
      <Form.Control
        name={name}
        accepter={RadioGroup}
        inline
        style={{ marginLeft: -20, ...style }}
        {...rest}
      >
        {options?.length > 0
          ? options.map((option, index) => (
              <Radio value={option.value} key={index}>
                {option.label}
              </Radio>
            ))
          : null}
      </Form.Control>
    </Form.Group>
  );
});

export default RadioGroupCustom;
