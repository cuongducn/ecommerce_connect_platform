import React from "react";
import { Form, Input } from "rsuite";

const TextareaCustom = React.forwardRef((props, ref) => (
  <Input {...props} as="textarea" ref={ref} />
));
const Textarea = React.forwardRef((props, ref) => {
  const {
    name,
    label,
    rows = 3,
    placeholder = "Textarea...",
    accepter,
    ...rest
  } = props;

  return (
    <Form.Group controlId={`${name}-4`} ref={ref}>
      <Form.ControlLabel>{label} </Form.ControlLabel>
      <Form.Control
        name={name}
        rows={rows}
        placeholder={placeholder}
        accepter={TextareaCustom}
        {...rest}
      />
    </Form.Group>
  );
});

export default Textarea;
