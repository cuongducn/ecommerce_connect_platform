import React from "react";
import { Form, Input, Toggle } from "rsuite";

const ToggleCustom = React.forwardRef((props, ref) => {
  const { name, label, ...rest } = props;

  return (
    <Form.Group controlId={`${name}-4`} ref={ref}>
      <Form.ControlLabel>{label} </Form.ControlLabel>
      <Form.Control name={name} accepter={Toggle} {...rest} />
    </Form.Group>
  );
});

export default ToggleCustom;
