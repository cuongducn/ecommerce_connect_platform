import { Container, Nav, Navbar, Sidebar, Sidenav } from 'rsuite';

import AngleLeftIcon from '@rsuite/icons/legacy/AngleLeft';
import AngleRightIcon from '@rsuite/icons/legacy/AngleRight';
import { Scatter } from '@rsuite/icons';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { menuSidebar } from '../../../utils/data/menuSidebar';
import NavLink from '../navLink/NavLink';
import { v4 } from 'uuid';

const SidebarCustomStyles = styled.div`
  position: sticky;
  top: 0;
  left: 0;
  height: 100vh;
  .header__logo {
    height: 56px;
    background-color: #00897b;
    color: #fff;
    white-space: 'nowrap';
    width: 100%;
    overflow: hidden;
    .rs-nav {
      height: 100%;
    }
    a {
      display: flex;
      align-items: center;
      padding-left: 56px;
      white-space: normal;
      width: 100%;
      height: 100%;
      svg {
        font-size: 16px;
        height: 16px;
        left: 20px;
        line-height: 1.25;
        margin-right: 20px;
        position: absolute;
        top: 50%;
        transform: translateY(-55%);
      }
    }
    .rs-nav-default .rs-nav-item {
      color: white;
      &:hover,
      &:focus {
        background-color: transparent;
        color: white;
      }
    }
  }
  .rs-sidenav-item-active {
    color: #00897b !important;
  }
  .rs-sidenav-subtle
    .rs-dropdown.rs-dropdown-selected-within
    .rs-sidenav-item-icon {
    color: #00897b !important;
  }
  .rs-dropdown-selected-within,
  .rs-dropdown.rs-dropdown-selected-within {
    .rs-sidenav-item-icon.rs-icon {
      color: #00897b !important;
    }
    .rs-dropdown-item.rs-dropdown-item-active {
      color: #00897b !important;
    }
  }
`;

const SidebarCustom = () => {
  const [expand, setExpand] = useState(true);
  const [activeKey, setActiveKey] = useState(() => {
    const pathName = window.location.pathname;
    return pathName.split('/')[1];
  });
  const [openKeys, setOpenKeys] = useState([]);

  const handleResizeDevice = () => {
    const widthDevice = window.innerWidth;
    if (widthDevice <= 992) {
      setExpand(false);
    } else {
      setExpand(true);
    }
  };
  useEffect(() => {
    const widthDevice = window.innerWidth;
    if (widthDevice <= 992) {
      setExpand(false);
    }
    window.addEventListener('resize', handleResizeDevice);
    return () => {
      window.removeEventListener('resize', handleResizeDevice);
    };
  }, []);
  return (
    <SidebarCustomStyles>
      <Container>
        <Sidebar
          style={{
            display: 'flex',
            flexDirection: 'column',
            borderRight: '1px solid rgba(0, 0, 0, 0.1)',
            height: '100vh',
          }}
          width={expand ? 260 : 56}
          collapsible
        >
          <Sidenav.Header>
            <div className="header__logo" onClick={() => setActiveKey('')}>
              <Nav>
                <Nav.Item
                  eventKey="header"
                  icon={<Scatter />}
                  key={v4()}
                  to="/"
                  as={NavLink}
                >
                  Asahaa
                </Nav.Item>
              </Nav>
            </div>
          </Sidenav.Header>
          <Sidenav
            expanded={expand}
            openKeys={openKeys}
            onOpenChange={setOpenKeys}
            defaultOpenKeys={['3']}
            appearance="subtle"
          >
            <Sidenav.Body>
              <Nav activeKey={activeKey} onSelect={setActiveKey}>
                {menuSidebar.length > 0 &&
                  menuSidebar.map((menu) => {
                    if (menu.children.length === 0) {
                      return (
                        <Nav.Item
                          eventKey={menu.eventKey}
                          icon={menu.icon}
                          key={menu.id}
                          to={menu.to}
                          as={NavLink}
                        >
                          {menu.title}
                        </Nav.Item>
                      );
                    } else {
                      return (
                        <Nav.Menu
                          eventKey={menu.eventKey}
                          key={menu.id}
                          trigger="hover"
                          title={menu.title}
                          icon={menu.icon}
                          placement="rightStart"
                        >
                          {menu.children.length > 1 &&
                            menu.children.length != null &&
                            menu.children.map((menuChildren) => {
                              if (menuChildren?.children) {
                                return (
                                  <Nav.Menu
                                    eventKey={menuChildren.eventKey}
                                    key={menuChildren.id}
                                    trigger="hover"
                                    title={menuChildren.title}
                                    to={menuChildren.to}
                                    placement="rightStart"
                                  >
                                    {menuChildren?.children?.length > 0 &&
                                      menuChildren.children.map(
                                        (subMenuChild) => (
                                          <Nav.Item
                                            eventKey={subMenuChild.eventKey}
                                            key={subMenuChild.id}
                                            to={subMenuChild.to}
                                            as={NavLink}
                                          >
                                            {subMenuChild.title}
                                          </Nav.Item>
                                        )
                                      )}
                                  </Nav.Menu>
                                );
                              } else {
                                return (
                                  <Nav.Item
                                    eventKey={menuChildren.eventKey}
                                    key={menuChildren.id}
                                    to={menuChildren.to}
                                    as={NavLink}
                                  >
                                    {menuChildren.title}
                                  </Nav.Item>
                                );
                              }
                            })}
                        </Nav.Menu>
                      );
                    }
                  })}
              </Nav>
            </Sidenav.Body>
          </Sidenav>
          <NavToggle expand={expand} onChange={() => setExpand(!expand)} />
        </Sidebar>
      </Container>
    </SidebarCustomStyles>
  );
};

const NavToggle = ({ expand, onChange }) => {
  return (
    <Navbar appearance="subtle" className="nav-toggle">
      <Nav pullRight>
        <Nav.Item onClick={onChange} style={{ width: 56, textAlign: 'center' }}>
          {expand ? <AngleLeftIcon /> : <AngleRightIcon />}
        </Nav.Item>
      </Nav>
    </Navbar>
  );
};

export default SidebarCustom;
