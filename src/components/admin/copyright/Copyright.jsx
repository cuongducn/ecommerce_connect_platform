import React from 'react';
import { Stack } from 'rsuite';

const Copyright = () => {
  return (
    <Stack
      className="copyright"
      justifyContent="center"
      style={{ height: 40, marginTop: 20 }}
    >
      <div className="container">
        <p>
          © 2022, Được thiết kế bởi{' '}
          <a
            href="https://asahaa.cuongdn.top/"
            target="_blank"
            rel="noreferrer"
          >
            Asahaa
          </a>
        </p>
      </div>
    </Stack>
  );
};

export default Copyright;
