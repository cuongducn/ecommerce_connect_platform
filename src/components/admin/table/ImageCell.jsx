import React from "react";
import { Avatar, Table } from "rsuite";
import UserIcon from "@rsuite/icons/legacy/User";
const ImageCell = ({ rowData, dataKey, styleImage, ...props }) => {
  const { Cell } = Table;
  return (
    <Cell {...props} style={{ padding: 0 }}>
      <div
        style={{
          background: "#f5f5f5",
          borderRadius: 6,
          marginTop: 2,
          overflow: "hidden",
          display: "inline-block",
          ...styleImage,
        }}
      >
        {rowData[dataKey] && typeof rowData[dataKey] === "string" ? (
          <img src={rowData[dataKey]} width="100%" height="100%" alt="img" />
        ) : typeof rowData[dataKey] === "object" &&
          rowData[dataKey]?.length > 0 ? (
          <img src={rowData[dataKey][0]} width="100%" height="100%" alt="img" />
        ) : (
          <Avatar>
            <UserIcon />
          </Avatar>
        )}
      </div>
    </Cell>
  );
};

export default ImageCell;
