import React from 'react';
import { Popover, Table, Whisper } from 'rsuite';
import { formatNumber } from '../../../utils';

const NameCell = ({
  rowData,
  dataKey,
  dataPopup,
  cost = [],
  time,
  isObject,
  isSex = [],
  ...props
}) => {
  const { Cell } = Table;
  const speaker = (
    <Popover
      title="Miêu tả"
      style={{
        maxWidth: '400px',
      }}
    >
      {rowData &&
        Object.keys(dataPopup).map((key, index) => (
          <p key={key}>
            <b>{dataPopup[key]}:</b>{' '}
            {cost.length > 0 ? (
              <>
                {cost.includes(index)
                  ? `${rowData[key] ? formatNumber(rowData[key]) + '₫' : ''}`
                  : rowData[key]}
              </>
            ) : isSex.length > 0 ? (
              <>
                {isSex.includes(index)
                  ? `${
                      rowData[key] === 1
                        ? 'Nam'
                        : rowData[key] === 2
                        ? 'Nữ'
                        : 'Khác'
                    }
                `
                  : rowData[key]}
              </>
            ) : (
              rowData[key]
            )}
          </p>
        ))}
    </Popover>
  );
  return (
    <>
      {isObject ? (
        <Whisper placement="top" speaker={speaker}>
          <span>{rowData ? rowData[dataKey] : ''}</span>
        </Whisper>
      ) : (
        <Cell {...props}>
          <Whisper placement="top" speaker={speaker}>
            <span>{rowData[dataKey]}</span>
          </Whisper>
        </Cell>
      )}
    </>
  );
};

export default NameCell;
