import React from "react";
import styled from "styled-components";
import { RingLoader } from "react-spinners";

const LoadingStyles = styled.div`
  position: fixed;
  z-index: 1000;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;

  .loading__overlay {
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: 998;
    top: 0;
    left: 0;
    background-color: rgba(255, 255, 255, 0.3);
  }
  .loading__content {
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    z-index: 999;
    top: 0;
    left: 0;
  }
`;

const override = {
  display: "block",
  margin: "0 auto",
};
const Loading = ({ loading }) => {
  return (
    <LoadingStyles>
      <div className="loading__overlay"></div>
      <div className="loading__content">
        <RingLoader
          color="#00897b"
          loading={loading}
          cssOverride={override}
          size={80}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      </div>
    </LoadingStyles>
  );
};

export default Loading;
