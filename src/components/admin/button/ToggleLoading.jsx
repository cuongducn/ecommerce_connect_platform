import React, { useCallback } from 'react';
import { Toggle } from 'rsuite';
import * as Types from '../../../constants/actionType';
const ToggleLoading = ({
  isLoading,
  status,
  data,
  handleAction,
  dataSelected,
  setDataSelected,
}) => {
  const statusChecked = status === Types.STATUS_COMPLETED ? true : false;
  const toggle = useCallback(
    (dataIsSelected) => {
      setDataSelected(dataIsSelected);
      handleAction(data.id, !statusChecked);
    },
    [data.id, statusChecked, handleAction, setDataSelected]
  );

  return (
    <Toggle
      loading={isLoading && dataSelected.id === data.id}
      checked={statusChecked}
      onChange={() => toggle(data)}
    />
  );
};

export default ToggleLoading;
