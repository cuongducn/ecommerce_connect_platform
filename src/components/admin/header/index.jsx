import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as profileActions from "../../../actions/profile";
import { Avatar, Dropdown, Popover, Whisper } from "rsuite";
import styled from "styled-components";
import Link from "../navLink/Link";
import UserIcon from "@rsuite/icons/legacy/User";

const HeaderStyles = styled.div`
  position: absolute;
  right: 20px;
  top: 20px;
  z-index: 1;
`;

const Header = () => {
  const dispatch = useDispatch();
  const { information } = useSelector((state) => state.profileReducers);
  const trigger = useRef();

  //Get Data Profile
  // useEffect(() => {
  //   dispatch(profileActions.getProfile());
  // }, [dispatch]);
  return (
    <HeaderStyles>
      <Whisper
        placement="bottomEnd"
        trigger="click"
        ref={trigger}
        speaker={renderAdminSpeaker}
      >
        {/* {information.avatar_image ? (
          <Avatar
            size="sm"
            circle
            src={information.avatar_image}
            alt="@simonguo"
            style={{ cursor: "pointer" }}
          />
        ) : (
          <Avatar size="sm" circle>
            <UserIcon />
          </Avatar>
        )} */}
        <Avatar size="sm" circle>
          <UserIcon />
        </Avatar>
      </Whisper>
    </HeaderStyles>
  );
};

const renderAdminSpeaker = ({ onClose, left, top, className }, ref) => {
  const handleSelect = () => {
    onClose();
  };
  return (
    <Popover ref={ref} className={className} style={{ left, top }} full>
      <Dropdown.Menu onSelect={handleSelect}>
        <Dropdown.Item panel style={{ padding: 10, width: 160 }}>
          <p>Đăng nhập với vai trò</p>
          <strong>Quản trị viên</strong>
        </Dropdown.Item>
        <Dropdown.Item divider />
        <Dropdown.Item as={Link} href="/ho-so">
          Hồ sơ
        </Dropdown.Item>
        <Dropdown.Item as={Link} href="/tai-khoan">
          Tài khoản
        </Dropdown.Item>
        <Dropdown.Item divider />
        <Dropdown.Item as={Link} href="/admin/logout">
          Đăng xuất
        </Dropdown.Item>
      </Dropdown.Menu>
    </Popover>
  );
};

export default Header;
