import { Outlet } from "react-router-dom";
import { Container } from "rsuite";
import Header from "../header";
import Sidebar from "../sidebar";

const Layout = () => {
  return (
    <Container
      style={{
        flexDirection: "row",
      }}
    >
      <Sidebar />
      <Header />
      <Outlet />
    </Container>
  );
};

export default Layout;
