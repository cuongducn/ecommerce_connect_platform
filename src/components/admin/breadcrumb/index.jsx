import React from "react";
import { Breadcrumb, Container, Panel } from "rsuite";
import NavLink from "../navLink/NavLink";

const Breadcrumbs = ({ children, title, breadcrumbItems }) => {
  return (
    <Container>
      <Panel
        header={
          <>
            <h3 className="title">{title}</h3>
            <Breadcrumb>
              <Breadcrumb.Item to="/" as={NavLink}>
                Trang chủ
              </Breadcrumb.Item>
              {breadcrumbItems.map((item, index) => (
                <span key={index}>{item}</span>
              ))}
            </Breadcrumb>
          </>
        }
      >
        {children}
      </Panel>
    </Container>
  );
};

export default Breadcrumbs;
