import React from "react";
import { Link as BaseLink } from "react-router-dom";

const Link = React.forwardRef(({ href, children, ...rest }, ref) => (
  <BaseLink ref={ref} to={href} {...rest}>
    {children}
  </BaseLink>
));

export default Link;
