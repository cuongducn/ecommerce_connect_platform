import React from "react";
import { Form, Radio, RadioGroup } from "rsuite";
import styled from "styled-components";

const RadioGroupStyles = styled.div``;

const RadioGroupCustom = React.forwardRef((props, ref) => {
  const {
    name,
    label,
    classNameLabel = "",
    style,
    options,
    accepter,
    ...rest
  } = props;
  return (
    <RadioGroupStyles>
      <Form.Group controlId={`${name}-4`} ref={ref}>
        <Form.ControlLabel className={`${classNameLabel} mb-0`}>
          {label}{" "}
        </Form.ControlLabel>
        <Form.Control
          name={name}
          accepter={RadioGroup}
          inline
          style={{ marginLeft: -20, ...style }}
          {...rest}
        >
          {options?.length > 0
            ? options.map((option, index) => (
                <Radio
                  value={option.value}
                  key={index}
                  className={`${classNameLabel} `}
                >
                  {option.label}
                </Radio>
              ))
            : null}
        </Form.Control>
      </Form.Group>
    </RadioGroupStyles>
  );
});

export default RadioGroupCustom;
