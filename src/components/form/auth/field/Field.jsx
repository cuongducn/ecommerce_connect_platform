import React from "react";
import styled from "styled-components";

const FieldStyles = styled.div`
  .labelInput,
  .input,
  .iconEye {
    transition: all 0.3s;
  }
  .input:not(:placeholder-shown) ~ .labelInput,
  .input:focus ~ .labelInput {
    top: 0;
    padding: 0 12px;
    background-color: #ffffff;
  }
  .input {
    &:hover,
    &:focus {
      border-color: #00897b;
    }
  }
  .input:focus ~ .labelInput,
  .input:hover ~ .labelInput,
  .input:focus + .iconEye,
  .input:hover + .iconEye {
    color: #00897b;
  }
`;
const Field = ({ children, className = "" }) => {
  return (
    <FieldStyles className={`relative flex  mb-1 ${className}`}>
      {children}
    </FieldStyles>
  );
};

export default Field;
