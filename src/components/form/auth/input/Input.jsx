import React from "react";
import { useController } from "react-hook-form";
const Input = ({
  type = "text",
  name = "",
  placeholder = "",
  className = "",
  control,
  ...props
}) => {
  const { field } = useController({
    control,
    name,
    defaultValue: "",
  });
  return (
    <input
      className={`border  border-gray3A rounded-[10px] px-6 py-[10px]  text-gray4B ${className}`}
      type={type}
      placeholder={placeholder}
      id={name}
      {...field}
      {...props}
    />
  );
};

export default Input;
