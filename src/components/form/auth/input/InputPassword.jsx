import React, { useState } from "react";
import { useController } from "react-hook-form";
import IconEyeClose from "../../../icon/iconEyes/IconEyeClose";
import IconEyeOpen from "../../../icon/iconEyes/IconEyeOpen";

const InputPassword = ({
  placeholder = "",
  name = "",
  className = "",
  control,
  ...props
}) => {
  const [togglePassword, setTogglePassword] = useState(false);
  const { field } = useController({
    control,
    name,
    defaultValue: "",
  });
  return (
    <>
      <input
        type={`${togglePassword ? "text" : "password"}`}
        {...field}
        {...props}
        placeholder={placeholder}
        className={`border  border-gray3A rounded-[10px] px-6 py-[10px]  text-gray4B ${className}`}
      />
      {!togglePassword ? (
        <IconEyeClose
          className="iconEye absolute -translate-y-1/2 cursor-pointer top-1/2 right-3 w-5 h-5"
          onClick={() => setTogglePassword(true)}
        ></IconEyeClose>
      ) : (
        <IconEyeOpen
          className="iconEye absolute -translate-y-1/2 cursor-pointer top-1/2 right-3 w-5 h-5"
          onClick={() => setTogglePassword(false)}
        ></IconEyeOpen>
      )}
    </>
  );
};

export default InputPassword;
