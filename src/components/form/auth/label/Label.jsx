import React from "react";

const Label = ({ name = "", title = "", className = "" }) => {
  return (
    <label
      htmlFor={name}
      className={`labelInput transition-all absolute top-1/2 left-[25px] -translate-y-1/2 ${className} select-none pointer-events-none text-whiteAB text-sm`}
    >
      <span>{title}</span>
    </label>
  );
};

export default Label;
