import React from "react";
import { DatePicker, Form } from "rsuite";
const ranges = [
  {
    label: "Now",
    value: new Date(),
  },
];
const DatePickerCalendarCustom = React.forwardRef((props, ref) => (
  <DatePicker
    format={props.format ? props.format : "yyyy-MM-dd HH:mm:ss"}
    ranges={ranges}
    style={{ width: "100%", ...props.style }}
    ref={ref}
    {...props}
  />
));
const DatePickerCalendar = React.forwardRef((props, ref) => {
  const { name, label, classNameLabel = "", accepter, ...rest } = props;

  return (
    <Form.Group controlId={`${name}-4`} ref={ref}>
      {label && (
        <Form.ControlLabel className={classNameLabel}>
          {label}
        </Form.ControlLabel>
      )}
      <Form.Control name={name} accepter={DatePickerCalendarCustom} {...rest} />
    </Form.Group>
  );
});

export default DatePickerCalendar;
