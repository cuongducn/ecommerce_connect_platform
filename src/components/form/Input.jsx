import React from "react";
import { Form, InputGroup, InputNumber } from "rsuite";
import EyeIcon from "@rsuite/icons/legacy/Eye";
import EyeSlashIcon from "@rsuite/icons/legacy/EyeSlash";
import styled from "styled-components";

const InputPasswordStyles = styled.div`
  position: relative;
  .icon__eyes {
    position: absolute;
    top: 1px;
    right: 1px;
    z-index: 1;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    height: calc(100% - 2px);
    box-sizing: border-box;
    border: 1px solid transparent;
    transition: all 0.4s ease-in-out;
  }
`;
const InputNumberStyles = styled.div`
  .input__number {
    .rs-input-number-btn-group-vertical {
      width: 0;
    }
  }
`;

const InputNumberCustom = React.forwardRef((props, ref) => (
  <InputNumber {...props} ref={ref} />
));

const Input = React.forwardRef((props, ref) => {
  const {
    name,
    label,
    type = "text",
    typeInput = "",
    placeholder = "Input...",
    accepter,
    classNameLabel = "",
    isRequired = false,
    styleLabel = {},
    ...rest
  } = props;
  const [visible, setVisible] = React.useState(false);
  const handleChange = () => {
    setVisible(!visible);
  };
  return (
    <Form.Group controlId={`${name}-4`} ref={ref}>
      <Form.ControlLabel className={classNameLabel} style={styleLabel}>
        <span>{label}</span>
        {isRequired ? (
          <span className="ml-[2px] text-xs text-red-700 align-top">*</span>
        ) : null}
      </Form.ControlLabel>
      {typeInput === "password" ? (
        <InputPasswordStyles>
          <Form.Control
            type={visible ? "text" : "password"}
            name={name}
            accepter={accepter}
            placeholder={placeholder}
            {...rest}
            className="input__password"
          />
          <InputGroup.Button onClick={handleChange} className="icon__eyes">
            {visible ? <EyeIcon /> : <EyeSlashIcon />}
          </InputGroup.Button>
        </InputPasswordStyles>
      ) : typeInput === "number" ? (
        <InputNumberStyles>
          <Form.Control
            type={type}
            name={name}
            placeholder={placeholder}
            accepter={InputNumberCustom}
            {...rest}
            className="input__number"
          />
        </InputNumberStyles>
      ) : (
        <Form.Control
          type={type}
          name={name}
          placeholder={placeholder}
          accepter={accepter}
          {...rest}
        />
      )}
    </Form.Group>
  );
});

export default Input;
