import React from 'react';
import { Form } from 'rsuite';
import Select from 'react-select';
const InputSelectCustom = React.forwardRef((props, ref) => {
  const { value, onChange, ...rest } = props;
  return (
    <Select
      isClearable
      {...rest}
      ref={ref}
      noOptionsMessage={() => 'Không tìm thấy kết quả'}
      value={value}
      onChange={(option, e) => {
        onChange(
          option?.value
            ? option?.value
            : option?.value === 0
            ? option?.value
            : null,
          e
        );
      }}
    />
  );
});
const InputSelect = React.forwardRef((props, ref) => {
  const {
    name,
    label,
    placeholder = 'Select...',
    classNameLabel = '',
    accepter,
    ...rest
  } = props;

  return (
    <Form.Group controlId={`${name}-4`} ref={ref}>
      <Form.ControlLabel className={classNameLabel}>{label} </Form.ControlLabel>
      <Form.Control
        name={name}
        placeholder={placeholder}
        accepter={InputSelectCustom}
        {...rest}
      />
    </Form.Group>
  );
});

export default InputSelect;
