import { Uploader } from "rsuite";
import CameraRetroIcon from "@rsuite/icons/legacy/CameraRetro";
import React, { useEffect, useState } from "react";
import { uploadImage } from "../../actions/upload";
import { alert } from "../../utils/alerts";

const UploadListImage = React.forwardRef((props, ref) => {
  const {
    style,
    multiple = true,
    action = "#",
    images,
    setFiles,
    ...rest
  } = props;

  const [filesInfo, setFilesInfo] = useState([]);
  const [fileList, setFileList] = useState([]);
  const removeFile = (indexDeleted) => {
    const newFiles = filesInfo.filter((file, index) => index !== indexDeleted);
    setFilesInfo(newFiles);
    setFiles(newFiles);
  };
  useEffect(() => {
    if (images?.length > 0) {
      setFilesInfo(images);
    }
  }, [images]);
  return (
    <>
      <div className="flex flex-wrap items-center gap-5">
        {filesInfo?.length < 10 ? (
          <Uploader
            multiple={multiple}
            accept="image/*"
            fileList={fileList}
            fileListVisible={false}
            listType="picture"
            action={action}
            autoUpload={false}
            onChange={async (file) => {
              if (file?.length + filesInfo.length > 10) {
                setFileList([]);
                alert.error("Bạn đã vượt tối đa 10 ảnh!");
                return;
              }
              for (let f of file) {
                const newFile = f.blobFile;
                const formDataImage = new FormData();
                formDataImage.append("image", newFile);
                const response = await uploadImage(formDataImage);
                if (response) {
                  setFiles((prevImages) => [...prevImages, response]);
                  setFilesInfo((prevImages) => [...prevImages, response]);
                }
              }
              setFileList([]);
            }}
            draggable
            {...rest}
            ref={ref}
          >
            <button
              type="button"
              style={{
                ...style,
                margin: 0,
              }}
            >
              <CameraRetroIcon />
            </button>
          </Uploader>
        ) : null}

        {filesInfo?.length > 0 &&
          filesInfo.map((file, index) => (
            <div
              key={index}
              style={{
                ...style,
              }}
              className="relative flex items-center justify-center border border-gray-200 rounded-md"
            >
              <img src={file} alt={file} className="object-contain h-full" />
              <div className="absolute top-0 right-0 translate-x-1/2 -translate-y-1/2 ">
                <span
                  className="cursor-pointer item__delete"
                  onClick={() => removeFile(index)}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="23"
                    height="23"
                    viewBox="0 0 23 23"
                  >
                    <g
                      fill="none"
                      fillRule="evenodd"
                      stroke="none"
                      strokeWidth="1"
                    >
                      <g transform="translate(-301 -387)">
                        <g transform="translate(236 388)">
                          <g transform="translate(65.537)">
                            <circle
                              cx="10.5"
                              cy="10.5"
                              r="10.5"
                              fill="#000"
                              stroke="#FFF"
                            ></circle>
                            <g fill="#FFF" transform="rotate(45 1.564 14.453)">
                              <rect
                                width="1.424"
                                height="10.678"
                                x="4.627"
                                y="0"
                                rx="0.712"
                              ></rect>
                              <path
                                d="M5.339 0c.393 0 .712.319.712.712v9.254a.712.712 0 11-1.424 0V.712c0-.393.319-.712.712-.712z"
                                transform="rotate(90 5.339 5.339)"
                              ></path>
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                </span>
              </div>
            </div>
          ))}
      </div>
    </>
  );
});

export default UploadListImage;
