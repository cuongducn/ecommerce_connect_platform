import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import IconArrowDown from '../icon/arrow/IconArrowDown';
import IconLocationSolid from '../icon/location/IconLocationSolid';
import IconSearch from '../icon/search/IconSearch';
import * as placeAction from '../../actions/place';
import { removeAscent } from '../../utils';
import { debounce } from 'lodash';
import { v4 } from 'uuid';
import { useGeolocated } from 'react-geolocated';
const SearchStyles = styled.div`
  .dropdown {
    box-shadow: 1px 2px 6px 1px rgba(0, 0, 0, 0.1);
  }
`;

const Search = ({ setParams }) => {
  const dispatch = useDispatch();
  const { province } = useSelector((state) => state.placeReducers);

  const [provinceSearch, setProvinceSearch] = useState([]);
  const [valueLocation, setValueLocation] = useState('');
  const [valueLocationSelected, setValueLocationSelected] = useState('');
  const [showDropdown, setShowDropdown] = useState(false);
  const [searchInput, setSearchInput] = useState('');

  const { coords, getPosition } = useGeolocated({
    positionOptions: {
      enableHighAccuracy: false,
    },
    suppressLocationOnMount: true,
  });

  const handleChangeSearch = (e) => {
    const value = e.target.value;
    let newProvinceSearch;

    if (value === '') {
      newProvinceSearch = province;
    } else {
      newProvinceSearch = province.filter((item) =>
        removeAscent(item.name.toLowerCase()).includes(
          removeAscent(value.trim().toLowerCase())
        )
      );
    }

    setValueLocation(value);
    setProvinceSearch(newProvinceSearch);
  };

  const handleChoiceLocation = (location) => {
    setValueLocation(location.name);
    setValueLocationSelected(location);
    setShowDropdown(false);
    if (location?.name === 'Gần tôi') {
      getPosition();
    } else {
      setParams((prevParams) => {
        return {
          ...prevParams,
          province: location.id,
          latitude: '',
          longitude: '',
        };
      });
    }
  };
  const handleShowDropdown = (e) => {
    if (e.target.closest('.dropdown')) return;
    setShowDropdown(true);
  };

  //Handle Search
  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        return {
          ...prevParams,
          search: valueSearch,
          page: 1,
        };
      });
    }, 500),
    []
  );
  const handleSearchClinics = (e) => {
    const value = e.target.value;
    setSearchInput(value);
    debounceCallBack(value);
  };

  useEffect(() => {
    if (coords && valueLocationSelected?.name === 'Gần tôi') {
      setParams((prevParams) => {
        return {
          ...prevParams,
          latitude: coords.latitude,
          longitude: coords.longitude,
          province: '',
        };
      });
    }
  }, [coords, setParams, valueLocationSelected?.name]);

  useEffect(() => {
    const handleClickLocation = (e) => {
      if (e.target.closest('.location')) return;
      setShowDropdown(false);
    };
    window.addEventListener('click', handleClickLocation);
    return () => window.removeEventListener('click', handleClickLocation);
  }, []);
  useEffect(() => {
    if (province.length > 0) {
      setProvinceSearch(province);
    }
  }, [province]);
  useEffect(() => {
    dispatch(placeAction.getProvince());
  }, [dispatch]);
  return (
    <SearchStyles>
      <div className="py-[60px] bg-teal-50">
        <div className="containerWidth">
          <div className="flex flex-col items-center">
            <h2 className="mb-1 text-2xl font-medium text-main">
              Đặt khám tại Asahaa
            </h2>
            <div className="mb-[10px] text-center">
              Tiếp đón chuyên nghiệp, nhanh chóng tại các phòng khám hàng đầu
            </div>
            <div className="flex w-[70%] bg-[#fff] h-12 rounded-full shadow-[0_4px_10px_rgba(0,0,0,0.05)] placeholder:text-gray8F items-center">
              <div className="flex items-center w-[75%] h-full p-4">
                <IconSearch className="w-5 h-5 mr-4 text-gray8F" />
                <input
                  type="text"
                  placeholder="Tìm các phòng khám..."
                  className="w-full text-sm"
                  value={searchInput}
                  onChange={handleSearchClinics}
                />
              </div>
              <div
                className="w-[190px] border-l border-l-[rgba(171,171,171,0.3)] h-[32px] relative flex items-center text-sm pl-2 location"
                onClick={handleShowDropdown}
              >
                <div className="flex items-center">
                  {valueLocationSelected?.name === 'Gần tôi' && (
                    <IconLocationSolid className="w-6 h-6 text-[#35aaf8] mr-1" />
                  )}
                  <input
                    value={valueLocation}
                    onChange={handleChangeSearch}
                    placeholder="Chọn địa điểm"
                    className="w-full pr-7 input-location"
                  />
                </div>
                <IconArrowDown className="absolute w-4 h-4 -translate-y-1/2 top-1/2 right-2" />

                {showDropdown && (
                  <div className="dropdown absolute top-full left-0 bg-[#fff] whitespace-nowrap text-sm  min-w-full h-[266px] rounded-md py-[5px]">
                    <div className="h-[256px] overflow-y-auto">
                      <div
                        className={`flex items-center px-3 py-2 cursor-pointer gap-x-1 hover:bg-gray-100 hover:text-bodyColor ${
                          valueLocationSelected?.name === 'Gần tôi'
                            ? 'bg-gray-100 text-bodyColor'
                            : ''
                        }`}
                        onClick={() =>
                          handleChoiceLocation({ id: v4(), name: 'Gần tôi' })
                        }
                      >
                        <IconLocationSolid className="w-6 h-6 text-[#35aaf8]" />
                        <span>Gần tôi</span>
                      </div>
                      {provinceSearch.length > 0 &&
                        provinceSearch.map((province) => (
                          <div
                            key={province.id}
                            className={`px-3 py-2 cursor-pointer hover:bg-gray-100 hover:text-bodyColor ${
                              valueLocationSelected?.name === province.name
                                ? 'bg-gray-100 text-bodyColor'
                                : ''
                            }`}
                            onClick={() => handleChoiceLocation(province)}
                          >
                            {province.name}
                          </div>
                        ))}
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </SearchStyles>
  );
};

export default Search;
