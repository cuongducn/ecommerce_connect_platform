import { ArrowLeftOutlined } from '@ant-design/icons';
import { Input } from 'antd';
import React from 'react';
import ProductCard from '../product/ProductCard';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import * as productActions from '../../actions/product';
import { useSearchParams } from 'react-router-dom';
import { useCallback } from 'react';
import { debounce } from 'lodash';
import { useState } from 'react';

const Search = (props) => {
  const dispatch = useDispatch();
  const [searchParams] = useSearchParams();
  const { products } = useSelector((state) => state.productReducers);
  const [params, setParams] = useState({
    page: Number(searchParams.get('page')) || 1,
    limit: Number(searchParams.get('limit')) || 20,
    status: Number(searchParams.get('status')) || '',
    search: searchParams.get('search') || '',
    date_from: searchParams.get('date_from') || '',
    date_to: searchParams.get('date_to') || '',
    category_ids: searchParams.get('category_ids') || '',
    category_children_ids: searchParams.get('category_children_ids') || '',
    searchInput: searchParams.get('search') || '',
  });

  const getParams = (
    page,
    limit,
    status,
    search,
    date_from,
    date_to,
    category_ids,
    category_children_ids
  ) => {
    let params = '';
    if (page) {
      params += `page=${page}`;
    }
    if (limit) {
      params += `&limit=${limit}`;
    }
    if (status !== '' && status !== null) {
      params += `&status=${status}`;
    }
    if (search !== '') {
      params += `&search=${search}`;
    }
    if (date_from !== '') {
      params += `&date_from=${date_from}`;
    }
    if (date_to !== '') {
      params += `&date_to=${date_to}`;
    }
    if (category_ids !== '') {
      params += `&category_ids=${category_ids}`;
    }
    if (category_children_ids !== '') {
      params += `&category_children_ids=${category_children_ids}`;
    }
    return params;
  };

  useEffect(() => {
    const queryString = getParams(
      params.page,
      params.limit,
      params.status,
      params.search,
      params.date_from,
      params.date_to,
      params.category_ids,
      params.category_children_ids
    );
    dispatch(productActions.getProducts(queryString));
  }, [
    dispatch,
    params.page,
    params.limit,
    params.search,
    params.status,
    params.date_from,
    params.date_to,
    params.category_ids,
    params.category_children_ids,
  ]);

  const debounceCallBack = useCallback(
    debounce((valueSearch) => {
      setParams((prevParams) => {
        const queries = getParams(
          1,
          prevParams.limit,
          prevParams.status,
          valueSearch,
          prevParams.date_from,
          prevParams.date_to,
          prevParams.category_ids,
          prevParams.category_children_ids
        );
        // navigate(`/admin/products?${queries}`);
        return {
          ...prevParams,
          page: 1,
          search: valueSearch,
          searchInput: valueSearch,
        };
      });
    }, 500),
    []
  );

  const handleSearchProducts = (e) => {
    const value = e.target.value;
    setParams({
      ...params,
      searchInput: value,
    });
    debounceCallBack(value);
  };
  return (
    <div>
      <div className="flex content-center items-center pt-[50px] px-[100px]">
        <ArrowLeftOutlined
          className="px-5"
          style={{ fontSize: '1.3rem', cursor: 'pointer' }}
          onClick={() => props.setIsShowSearch(!props.isShowSearch)}
        />
        <Input
          placeholder="Tìm kiếm sản phẩm..."
          size="large"
          bordered={false}
          inputMode="text"
          onChange={handleSearchProducts}
          value={params.searchInput}
          style={{
            borderBottom: '1px solid #b2b2b2',
            borderRadius: '0',
          }}
          className=""
        />
      </div>
      <h2 className=" my-[20px] text-center text-[1rem] font-semibold">
        Tìm thấy{' '}
      </h2>
      {/* <div className="max-h-screen overflow-auto grid grid-cols-2 sm:grid-cols-5 gap-3 mt-2 p-[5px]"> */}
      <div className="max-h-screen sm:max-h-[63vh] overflow-auto grid grid-cols-2 sm:grid-cols-5 gap-3 mt-2 p-[5px]">
        {products?.data?.map((product) => (
          <div
            onClick={() => navigate(`/product/${product.slug}`)}
            key={product.id}
          >
            <ProductCard product={product} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default Search;
