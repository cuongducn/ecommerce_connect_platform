import React from "react";
import styled from "styled-components";
import * as errors from "../../assets/errors";

const ErrorStyles = styled.div`
  .error-page {
    display: flex;
    height: 100vh;
    margin-top: -40px;
    justify-content: center;
    align-items: center;
    &-title {
      font-size: 1.5rem;
      font-weight: bold;
    }
    &-subtitle {
      margin: 10px 0 20px 0;
    }

    .item {
      img {
        height: 260px;
      }
      .text {
        text-align: center;
      }
    }
  }
`;

const ErrorPage = ({ code = 404, children }) => (
  <ErrorStyles>
    <div className="error-page">
      <div className="item">
        <img src={errors[`Error${code}Img`]} alt="img_error" />
        <div className="text">
          <h1 className="error-page-code">{code}</h1>
          {children}
        </div>
      </div>
    </div>
  </ErrorStyles>
);

export default ErrorPage;
