import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Avatar, Loader, Modal } from 'rsuite';
import styled from 'styled-components';
import * as clinicAction from '../../actions/clinic';
import * as dependPersonAction from '../../actions/dependPerson';
import * as packetAction from '../../actions/packetMedical';
import * as doctorAction from '../../actions/doctor';
import IconCalendar from '../icon/calendar/IconCalendar';
import IconGender from '../icon/gender/IconGender';
import { combineAddress, formatNumber } from '../../utils';
import * as Types from '../../constants/actionType';
import NoImage from '../../assets/no_img.png';
import CalendarImage from '../../assets/calendar.png';
import Stethoscope from '../../assets/stethoscope.svg';
import moment from 'moment';
import IconUserPatient from '../../components/icon/user/IconUserPatient';
import { useNavigate, useSearchParams } from 'react-router-dom';
import IconUserDoctor from '../icon/user/IconUserDoctor';
import { useEffect } from 'react';
import * as calendarAction from '../../actions/calendarMedical';
import IconLocationSolid from '../icon/location/IconLocationSolid';
import { stepAppointment } from '../../utils/stepAppointment';
import { alert } from '../../utils/alerts';

const ConfirmAppointmentStyles = styled.div`
  .shadow {
    box-shadow: 0px 0px 8px 1px rgba(0, 0, 0, 0.08);
  }
`;

const ConfirmAppointment = () => {
  const dispatch = useDispatch();
  const { isLoadingAppointment } = useSelector(
    (state) => state.loadingReducers
  );
  const { badges } = useSelector((state) => state.badgeReducers);
  const { doctors } = useSelector((state) => state.doctorReducers);
  const { profile } = useSelector((state) => state.profileReducers);
  const { doctorsClinic, clinic } = useSelector(
    (state) => state.clinicReducers
  );
  const { dependPerson } = useSelector((state) => state.dependPersonReducers);
  const { packetsMedical } = useSelector(
    (state) => state.packetMedicalReducers
  );

  const navigate = useNavigate();
  const [openModalDepositEWallet, setOpenModalDepositEWallet] = useState();
  const [searchParams] = useSearchParams();
  const [appointment, setAppointment] = useState({
    clinic: '',
    packet: '',
    dependent: '',
    doctor: '',
    timeCalendar: {
      from_time: '',
      to_time: '',
    },
    type: '',
  });

  const idClinic = searchParams.get('clinicId');
  const idPacket = searchParams.get('packetId');
  const idDependent = searchParams.get('dependent');
  const idDoctor = searchParams.get('doctorId');
  const from_time = searchParams.get('from_time');
  const to_time = searchParams.get('to_time');
  const type = searchParams.get('type');
  const latitude = searchParams.get('lat');
  const longitude = searchParams.get('long');

  const handleDepositEWallet = () => {
    const data = {
      from_time: appointment.timeCalendar?.from_time,
      to_time: appointment.timeCalendar?.to_time,
      packet_medical_id: Number(idPacket),
      type: Number(appointment.type),
    };
    const isSelf = profile.id == idDependent;
    if (!isSelf) {
      data.customer_depend_id = Number(idDependent);
    }
    if (idClinic) {
      data.clinic_id = Number(idClinic);
    }
    if (idDoctor) data.user_id = Number(idDoctor);
    if (longitude && latitude) {
      data.longitude = longitude;
      data.latitude = latitude;
    }

    const moneyEWallet = badges?.e_wallet_customer?.account_balance
      ? badges?.e_wallet_customer?.account_balance
      : -1;
    let priceBookAppointment;
    if (type == Types.TYPE_MEDICAL_PRIVATE_CLINIC) {
      priceBookAppointment = appointment.packet?.info_packet_medical
        ?.price_clinic
        ? appointment.packet?.info_packet_medical?.price_clinic
        : 0;
    } else if (type == Types.TYPE_MEDICAL_AT_HOME) {
      priceBookAppointment = appointment.packet?.price_home
        ? appointment.packet?.price_home
        : 0;
    } else {
      priceBookAppointment = appointment.packet?.price_online
        ? appointment.packet?.price_online
        : 0;
    }
    const paidNeed = Number(priceBookAppointment) - Number(moneyEWallet);

    dispatch(
      calendarAction.addAppointmentMedical(data, navigate, null, () => {
        stepAppointment.removePathName();
        navigate(`/vi-khach-hang/lich-su-nap-rut/nap-vi?paid_need=${paidNeed}`);
      })
    );
  };
  const convertDepartment = (departments) => {
    if (departments.length > 0) {
      const listString = departments.reduce(
        (prevDepartments, currentDepartments, index) => {
          return (
            prevDepartments +
            `${
              index !== departments.length - 1
                ? `${currentDepartments.name}, `
                : currentDepartments.name
            }`
          );
        },
        ''
      );

      return listString;
    } else {
      return '';
    }
  };

  // Xử lý lấy gói khám
  const handlePacketList = (listPacket, idPacket, typeId) => {
    if (typeId) {
      if (listPacket?.length > 0) {
        return listPacket.filter((packet) => packet[typeId] == idPacket)
          ?.length > 0
          ? listPacket.filter((packet) => packet[typeId] == idPacket)[0]
          : {};
      }
    } else if (listPacket?.length > 0) {
      return listPacket.filter((packet) => packet.packet_medical_id == idPacket)
        ?.length > 0
        ? listPacket.filter((packet) => packet.packet_medical_id == idPacket)[0]
        : {};
    }
    return {};
  };
  // Xử lý lấy bác sĩ
  const handleDoctorList = (listDoctor, idDoctor) => {
    if (listDoctor?.length > 0 && type == Types.TYPE_MEDICAL_PRIVATE_CLINIC) {
      return listDoctor.filter((doctor) => doctor?.user_invited_id == idDoctor)
        ?.length > 0
        ? listDoctor.filter((doctor) => doctor?.user_invited_id == idDoctor)[0]
        : {};
    } else if (
      listDoctor?.length > 0 &&
      (type == Types.TYPE_MEDICAL_AT_HOME || type == Types.TYPE_MEDICAL_ONLINE)
    ) {
      return listDoctor.filter(
        (doctor) => doctor?.doctor_info?.user_id == idDoctor
      )?.length > 0
        ? listDoctor.filter(
            (doctor) => doctor?.doctor_info?.user_id == idDoctor
          )[0]
        : {};
    }
    return {};
  };

  const handleBookAppointment = () => {
    if (type == Types.TYPE_MEDICAL_AT_HOME) {
      if (
        !dependPerson.address_detail ||
        !dependPerson.wards ||
        !dependPerson.district ||
        !dependPerson.province
      ) {
        alert.error('Nhập đầy đủ địa chỉ người phụ thuộc !');
        return;
      }
    }
    const moneyEWallet = badges?.e_wallet_customer?.account_balance
      ? badges?.e_wallet_customer?.account_balance
      : -1;
    let priceBookAppointment;
    if (type == Types.TYPE_MEDICAL_PRIVATE_CLINIC) {
      priceBookAppointment = appointment.packet?.info_packet_medical
        ?.price_clinic
        ? appointment.packet?.info_packet_medical?.price_clinic
        : 0;
    } else if (type == Types.TYPE_MEDICAL_AT_HOME) {
      priceBookAppointment = appointment.packet?.price_home
        ? appointment.packet?.price_home
        : 0;
    } else {
      priceBookAppointment = appointment.packet?.price_online
        ? appointment.packet?.price_online
        : 0;
    }
    const moneyPaid = Number(moneyEWallet) - Number(priceBookAppointment);

    if (moneyPaid < 0) {
      setOpenModalDepositEWallet(true);
      return;
    }
    const data = {
      from_time: appointment.timeCalendar?.from_time,
      to_time: appointment.timeCalendar?.to_time,
      packet_medical_id: Number(idPacket),
      type: Number(appointment.type),
    };
    if (idClinic) {
      data.clinic_id = Number(idClinic);
    }
    const isSelf = profile.id == idDependent;
    if (!isSelf) {
      data.customer_depend_id = Number(idDependent);
    }
    if (idDoctor) data.user_id = Number(idDoctor);
    if (longitude && latitude) {
      data.longitude = longitude;
      data.latitude = latitude;
    }

    dispatch(
      calendarAction.addAppointmentMedical(data, navigate, () => {
        stepAppointment.removePathName();
      })
    );
  };

  // Xử lý lấy dữ liệu
  useEffect(() => {
    let newClinic = {};
    let newPacket = {};
    let newDoctor = {};
    let newDependent = {};
    let newTimeCalendar = {
      from_time: '',
      to_time: '',
    };
    let newType = '';

    if (type == Types.TYPE_MEDICAL_PRIVATE_CLINIC) {
      if (idClinic) {
        newClinic = clinic;
        newPacket = handlePacketList(clinic?.list_packet_medical, idPacket);
        if (idDoctor) {
          newDoctor = handleDoctorList(doctorsClinic?.data, idDoctor);
        }
      }
    } else if (
      type == Types.TYPE_MEDICAL_AT_HOME ||
      type == Types.TYPE_MEDICAL_ONLINE
    ) {
      if (idPacket) {
        newPacket = handlePacketList(packetsMedical?.data, idPacket, 'id');
      }
      if (idDoctor) {
        newDoctor = handleDoctorList(doctors?.data, idDoctor);
      }
    }
    if (idDependent) {
      newDependent = dependPerson;
    }
    if (from_time && to_time) {
      newTimeCalendar = {
        from_time: from_time,
        to_time: to_time,
      };
    }
    if (type) {
      newType = type;
    }
    setAppointment({
      clinic: newClinic,
      packet: newPacket,
      dependent: newDependent,
      doctor: newDoctor,
      timeCalendar: newTimeCalendar,
      type: newType,
    });
  }, [
    clinic,
    dependPerson,
    doctors?.data,
    doctorsClinic?.data,
    from_time,
    idClinic,
    idDependent,
    idDoctor,
    idPacket,
    packetsMedical?.data,
    to_time,
    type,
  ]);

  //Gọi API gói khám tại nhà, online
  useEffect(() => {
    if (
      idPacket &&
      (type == Types.TYPE_MEDICAL_AT_HOME || type == Types.TYPE_MEDICAL_ONLINE)
    ) {
      dispatch(packetAction.getPacketMedical(idPacket));
    }
  }, [dispatch, idPacket, type]);
  //Gọi API bác sĩ khám tại nhà, online
  useEffect(() => {
    if (
      idDoctor &&
      idPacket &&
      (type == Types.TYPE_MEDICAL_AT_HOME || type == Types.TYPE_MEDICAL_ONLINE)
    ) {
      dispatch(doctorAction.getDoctors(`packet_medical_id=${idPacket}`));
    }
  }, [dispatch, idClinic, idDoctor, idPacket, type]);
  //Gọi API bác sĩ trong phòng khám
  useEffect(() => {
    if (idClinic && type == Types.TYPE_MEDICAL_PRIVATE_CLINIC && idDoctor) {
      dispatch(clinicAction.getDoctorsClinic(idClinic));
    }
  }, [dispatch, idClinic, idDoctor, type]);
  // Gọi API phòng khám
  useEffect(() => {
    if (idClinic && type == Types.TYPE_MEDICAL_PRIVATE_CLINIC) {
      dispatch(clinicAction.getClinic(idClinic));
    }
  }, [dispatch, idClinic, type]);

  // Gọi API người phụ thuộc
  useEffect(() => {
    if (idDependent && Object.entries(profile).length > 0 && profile?.id) {
      const isSelf = searchParams.get('is_self');
      if (isSelf) {
        dispatch({
          type: Types.GET_DETAIL_DEPEND_PERSON,
          data: profile,
        });
        return;
      }
      dispatch(dependPersonAction.getDependPerson(idDependent));
    }
  }, [dispatch, idDependent, profile.id]);

  useEffect(() => {
    stepAppointment.setPathName();
  }, []);
  return (
    <ConfirmAppointmentStyles>
      <div>
        <div className="flex flex-col items-center mb-10">
          <h4 className="mb-4 text-2xl font-medium text-center">
            Thông tin đặt khám
          </h4>
          <div className="w-[120px] h-[2px] bg-gray-200"></div>
        </div>
        <div className="mt-5 bg-[#fff] p-5 ">
          <div>
            <div>
              <div className="p-4 mb-5 rounded-md shadow">
                <div className="flex gap-x-5">
                  <div className="w-10 h-10">
                    <img
                      className="w-full h-full"
                      src={CalendarImage}
                      alt="calendar"
                    />
                  </div>
                  <div>
                    <div className="text-xs text-gray8F">Ngày và giờ hẹn</div>
                    <div className="flex font-medium gap-x-2">
                      <span>
                        {moment(
                          moment(appointment.timeCalendar?.from_time)
                        ).format('HH:mm')}
                      </span>
                      <span>
                        {moment(
                          moment(appointment.timeCalendar?.from_time)
                        ).format('DD/MM/YYYY')}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="flex pt-5 mt-4 border-t border-t-gray-200">
                  <div className="w-[20%] flex items-center">
                    {type == Types.TYPE_MEDICAL_PRIVATE_CLINIC ? (
                      <img
                        className="w-full"
                        src={
                          appointment.packet?.info_packet_medical?.images
                            ?.length > 0
                            ? appointment.packet?.info_packet_medical.images[0]
                            : NoImage
                        }
                        alt={appointment.packet?.info_packet_medical?.name}
                      />
                    ) : (
                      <img
                        className="w-full"
                        src={
                          appointment.packet?.images?.length > 0
                            ? appointment.packet.images[0]
                            : NoImage
                        }
                        alt={appointment.packet?.name}
                      />
                    )}
                  </div>
                  <div className="p-4">
                    <div className="font-medium max-w-[250px] whitespace-nowrap overflow-hidden text-ellipsis">
                      {type == Types.TYPE_MEDICAL_PRIVATE_CLINIC
                        ? appointment.packet?.info_packet_medical?.name
                        : appointment.packet?.name}
                    </div>
                    {(appointment.packet?.list_medical_department?.length > 0 ||
                      appointment.packet?.info_packet_medical
                        ?.list_medical_department?.length > 0) && (
                      <p className="text-sm text-gray8F">
                        Khoa khám:{' '}
                        {type == Types.TYPE_MEDICAL_PRIVATE_CLINIC ? (
                          <span>
                            {convertDepartment(
                              appointment.packet.info_packet_medical
                                ?.list_medical_department
                            )}
                          </span>
                        ) : type == Types.TYPE_MEDICAL_AT_HOME ||
                          type == Types.TYPE_MEDICAL_ONLINE ? (
                          <span>
                            {convertDepartment(
                              appointment.packet?.list_medical_department
                            )}
                          </span>
                        ) : null}
                      </p>
                    )}
                    {type == Types.TYPE_MEDICAL_PRIVATE_CLINIC ? (
                      <div className="mt-2 text-sm font-medium text-main">
                        {appointment.packet?.info_packet_medical?.price_clinic
                          ? `${formatNumber(
                              appointment.packet?.info_packet_medical
                                ?.price_clinic
                            )} đ`
                          : ''}
                      </div>
                    ) : type == Types.TYPE_MEDICAL_AT_HOME ? (
                      <div className="mt-2 text-sm font-medium text-main">
                        {appointment.packet?.price_home
                          ? `${formatNumber(appointment.packet?.price_home)} đ`
                          : ''}
                      </div>
                    ) : type == Types.TYPE_MEDICAL_ONLINE ? (
                      <div className="mt-2 text-sm font-medium text-main">
                        {appointment.packet?.price_online
                          ? `${formatNumber(
                              appointment.packet?.price_online
                            )} đ`
                          : ''}
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="flex flex-col p-4 mb-5 rounded-md shadow gap-y-1">
                <div className="flex justify-between w-full text-sm">
                  <span>Số dư hiện tại</span>
                  <span>
                    {badges?.e_wallet_customer?.account_balance
                      ? `${formatNumber(
                          badges?.e_wallet_customer?.account_balance
                        )} đ`
                      : '0 đ'}
                  </span>
                </div>
                <div className="flex items-center justify-between w-full text-sm">
                  <span className="text-base font-medium">Tổng thanh toán</span>
                  {type == Types.TYPE_MEDICAL_PRIVATE_CLINIC ? (
                    <span className="text-base font-medium text-main">
                      {appointment.packet?.info_packet_medical?.price_clinic
                        ? `${formatNumber(
                            appointment.packet?.info_packet_medical
                              ?.price_clinic
                          )} đ`
                        : ''}
                    </span>
                  ) : type == Types.TYPE_MEDICAL_AT_HOME ? (
                    <span className="text-base font-medium text-main">
                      {appointment.packet?.price_home
                        ? `${formatNumber(appointment.packet?.price_home)} đ`
                        : ''}
                    </span>
                  ) : type == Types.TYPE_MEDICAL_ONLINE ? (
                    <span className="text-base font-medium text-main">
                      {appointment.packet?.price_online
                        ? `${formatNumber(appointment.packet?.price_online)} đ`
                        : ''}
                    </span>
                  ) : null}
                </div>
              </div>
              {Object.entries(appointment?.clinic)?.length > 0 && (
                <div className="p-4 mb-5 rounded-md shadow">
                  <div className="font-medium">Phòng khám</div>
                  <div className="flex pt-5 mt-4 border-t border-t-gray-200">
                    <div className="w-[20%] flex items-center">
                      <img
                        className="w-full"
                        src={
                          appointment.clinic?.images?.length > 0
                            ? appointment.clinic?.images[0]
                            : NoImage
                        }
                        alt={appointment.clinic?.clinic_name}
                      />
                    </div>
                    <div className="p-4">
                      <div className="font-medium max-w-[250px] whitespace-nowrap overflow-hidden text-ellipsis">
                        {appointment.clinic?.clinic_name}
                      </div>
                      <p className="text-sm text-gray8F">
                        {combineAddress(
                          appointment.clinic?.address_detail,
                          appointment.clinic?.wards_name,
                          appointment.clinic?.district_name,
                          appointment.clinic?.province_name
                        )}
                      </p>
                    </div>
                  </div>
                </div>
              )}
              <div className="flex p-4 mb-5 rounded-md shadow gap-x-5">
                <div className="w-12 h-12">
                  {appointment.dependent?.image_url ? (
                    <img
                      className="w-full h-full rounded-full"
                      src={appointment.dependent?.image_url}
                      alt={appointment.dependent?.name}
                    />
                  ) : (
                    <Avatar circle className="bg-[#7B1FA2] uppercase w-12 h-12">
                      {appointment.dependent?.name?.charAt(0)}
                    </Avatar>
                  )}
                </div>
                <div>
                  <div className="flex items-center text-lg text-gray8F gap-x-2">
                    <IconUserPatient className="w-5 h-5"></IconUserPatient>
                    <span>Người tới khám</span>
                  </div>
                  <div className="font-medium uppercase">
                    <span>{appointment.dependent?.name}</span>
                  </div>
                  <div className="flex items-center gap-1">
                    <IconGender className="w-3 h-3" />
                    <span className="text-sm">
                      {Number(appointment.dependent?.sex) === Types.GENDER_MALE
                        ? 'Nam'
                        : Number(appointment.dependent?.sex) ===
                          Types.GENDER_FEMALE
                        ? 'Nữ'
                        : Number(appointment.dependent?.sex) ===
                          Types.GENDER_OTHER
                        ? 'Khác'
                        : null}
                    </span>
                  </div>
                  <div className="flex items-center gap-1">
                    <IconCalendar className="w-3 h-3" />
                    <span className="text-sm">
                      {appointment.dependent?.date_of_birth
                        ? moment(
                            moment(appointment.dependent?.date_of_birth)
                          ).format('DD/MM/YYYY')
                        : ''}
                    </span>
                  </div>

                  {type == Types.TYPE_MEDICAL_AT_HOME ? (
                    <div className="flex items-center gap-1">
                      <IconLocationSolid className="w-4 h-4 text-blue-500" />
                      <span className="text-sm">
                        {combineAddress(
                          appointment.dependent?.address_detail,
                          appointment.dependent?.wards_name,
                          appointment.dependent?.district_name,
                          appointment.dependent?.province_name
                        )}
                      </span>
                    </div>
                  ) : null}
                </div>
              </div>
              {Object.entries(appointment?.doctor)?.length > 0 ? (
                <div className="flex p-4 mb-5 rounded-md shadow gap-x-5">
                  <div className="w-12 h-12">
                    {appointment.doctor?.doctor_info?.doctor_image_1 ? (
                      <img
                        className="w-full h-full rounded-full"
                        src={appointment.doctor?.doctor_info?.doctor_image_1}
                        alt={appointment.doctor?.doctor_info?.name}
                      />
                    ) : (
                      <Avatar
                        circle
                        className="bg-[#7B1FA2] uppercase w-12 h-12"
                      >
                        {appointment.doctor?.doctor_info?.first_and_last_name?.charAt(
                          0
                        )}
                      </Avatar>
                    )}
                  </div>
                  <div>
                    <div className="flex items-center text-lg text-gray8F gap-x-2 ">
                      <IconUserDoctor className="w-5 h-5"></IconUserDoctor>
                      <span>Bác sĩ khám</span>
                    </div>
                    <div className="font-medium uppercase">
                      <span>
                        {appointment.doctor?.doctor_info?.first_and_last_name}
                      </span>
                    </div>
                    <div className="flex items-center gap-1">
                      <IconGender className="w-3 h-3" />
                      <span className="text-sm">
                        {Number(appointment.doctor?.sex) === Types.GENDER_MALE
                          ? 'Nam'
                          : Number(appointment.doctor?.sex) ===
                            Types.GENDER_FEMALE
                          ? 'Nữ'
                          : Number(appointment.doctor?.sex) ===
                            Types.GENDER_OTHER
                          ? 'Khác'
                          : null}
                      </span>
                    </div>
                    {appointment.doctor?.date_of_birth ? (
                      <div className="flex items-center gap-1">
                        <IconCalendar className="w-3 h-3 " />
                        <span className="text-sm">
                          {appointment.doctor?.date_of_birth
                            ? moment(
                                moment(appointment.doctor?.date_of_birth)
                              ).format('DD/MM/YYYY')
                            : ''}
                        </span>
                      </div>
                    ) : null}
                  </div>
                </div>
              ) : null}
              {/* <div className="flex flex-col p-4 mt-4 rounded-md shadow">
                <label className="text-sm font-medium text-gray4B">
                  Triệu chứng
                </label>
                <textarea
                  rows="3"
                  className="text-sm placeholder:text-gray8F"
                  placeholder="Mô tả tình trạng bệnh..."
                />
              </div> */}
            </div>
          </div>
          <div className="flex justify-center">
            <div
              className="w-[150px] bg-main flex items-center gap-x-2 justify-center py-[10px] rounded-md mt-5 text-[#fff] cursor-pointer"
              onClick={handleBookAppointment}
            >
              {isLoadingAppointment ? (
                <Loader />
              ) : (
                <>
                  <img className="w-6 h-6" src={Stethoscope} alt="image_game" />
                  <div>Đặt khám</div>
                </>
              )}
            </div>
          </div>
        </div>
        <Modal
          size="xs"
          open={openModalDepositEWallet}
          onClose={() => setOpenModalDepositEWallet(false)}
        >
          <Modal.Header>
            <Modal.Title>
              <div className="text-xl font-medium text-center">
                Không thể đặt hẹn
              </div>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <p className="mx-auto text-center text-red-500">
                Bạn chưa đủ tiền để đặt khám. Vui lòng nạp thêm tiền để thực
                hiện giao đặt khám.
              </p>
              <div className="flex justify-end mt-5 gap-x-3">
                <button
                  className="w-[100px] py-2 px-3 text-[#fff] bg-main hover:opacity-80 transition-all rounded-lg"
                  onClick={handleDepositEWallet}
                >
                  Nạp ví
                </button>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    </ConfirmAppointmentStyles>
  );
};

export default ConfirmAppointment;
