import * as Types from "../constants/actionType";
import * as placeApi from "../data/place";
import { alert } from "../utils/alerts";

export const getProvince = () => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_ADDRESS, isLoading: true });
    try {
      const response = await placeApi.getProvince();
      dispatch({
        type: Types.GET_PLACE_PROVINCE,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_ADDRESS, isLoading: false });
    }
  };
};
export const getDistrict = (idProvince) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_ADDRESS, isLoading: true });
    try {
      const response = await placeApi.getDistrict(idProvince);
      dispatch({
        type: Types.GET_PLACE_DISTRICT,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_ADDRESS, isLoading: false });
    }
  };
};
export const getWards = (idDistrict) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_ADDRESS, isLoading: true });
    try {
      const response = await placeApi.getWards(idDistrict);
      dispatch({
        type: Types.GET_PLACE_WARDS,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_ADDRESS, isLoading: false });
    }
  };
};
