import * as Types from '../constants/actionType';
import * as categoryApi from '../data/category';
import history from '../history';
import { alert } from '../utils/alerts';

export const getCategories = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await categoryApi.categories(params);
      dispatch({
        type: Types.GET_CATEGORIES,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response?.data?.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getCategory = (idCategory) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await categoryApi.category(idCategory);
      dispatch({
        type: Types.GET_CATEGORY,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
