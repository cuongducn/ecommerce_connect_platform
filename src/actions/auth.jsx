import * as Types from '../constants/actionType';
import * as customerApi from '../data/auth';
import * as adminApi from '../data/admin/auth';
import { getLocationBeforeLogin } from '../utils';
import { actionsLocalStorage } from '../utils/actionsLocalStorage';
import { alert } from '../utils/alerts';

export const login = (form, navigate) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_AUTHENTICATION, isLoading: true });
    try {
      const response = await customerApi.login(form);
      dispatch({
        type: Types.LOGIN,
        data: response.data.data.token,
      });
      alert.success(response.data.msg);
      const token = response.data.data.token;
      actionsLocalStorage.setItem('token', token);
      // const path = getLocationBeforeLogin() || '/c/';
      const path = '/c/profile' || '/c/';
      navigate(path);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_AUTHENTICATION, isLoading: false });
    }
  };
};
export const loginSocial = (from, navigate) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_AUTHENTICATION, isLoading: true });
    try {
      const response = await customerApi.loginSocial(from);
      dispatch({
        type: Types.LOGIN_SOCIAL,
        data: response.data.data.token,
      });
      alert.success(response.data.msg);
      const token = response.data.data.token;
      actionsLocalStorage.setItem('token', token);
      // const path = getLocationBeforeLogin() || '/c/';
      const path = '/c/profile' || '/c/';
      navigate(path);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_AUTHENTICATION, isLoading: false });
    }
  };
};
export const register = (form, navigate, funcModal) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_AUTHENTICATION, isLoading: true });
    try {
      const response = await customerApi.register(form);
      dispatch({
        type: Types.REGISTER,
        data: response.data.data,
      });
      // const responseLogin = await customerApi.login(form);
      // dispatch({
      //   type: Types.LOGIN,
      //   data: responseLogin.data.data,
      // });
      // const token = responseLogin.data.data.token;
      // actionsLocalStorage.setItem("token", token);
      alert.success(response.data.msg);
      funcModal();
      // navigate("/dang-nhap");
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_AUTHENTICATION, isLoading: false });
    }
  };
};
export const checkExistAccount = (form) => {
  return async (dispatch) => {
    dispatch({
      type: Types.SHOW_LOADING_AUTHENTICATION,
      isLoading: true,
    });
    try {
      const response = await customerApi.checkExistAccount(form);
      dispatch({
        type: Types.CHECK_EXIST_ACCOUNT,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({
        type: Types.SHOW_LOADING_AUTHENTICATION,
        isLoading: false,
      });
    }
  };
};
export const checkExistAccountAdmin = (form) => {
  return async (dispatch) => {
    dispatch({
      type: Types.SHOW_LOADING_AUTHENTICATION,
      isLoading: true,
    });
    try {
      const response = await customerApi.checkExistAccountAdmin(form);
      dispatch({
        type: Types.CHECK_EXIST_ACCOUNT,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({
        type: Types.SHOW_LOADING_AUTHENTICATION,
        isLoading: false,
      });
    }
  };
};
export const resetPassword = (form) => {
  return async (dispatch) => {
    dispatch({
      type: Types.SHOW_LOADING_AUTHENTICATION,
      isLoading: true,
    });
    try {
      const response = await customerApi.resetPassword(form);
      dispatch({
        type: Types.RESET_PASSWORD,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({
        type: Types.SHOW_LOADING_AUTHENTICATION,
        isLoading: false,
      });
    }
  };
};

// admin
export const loginAdmin = (form, onSuccess) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_AUTHENTICATION, isLoading: true });
    try {
      const response = await adminApi.postLogin(form);
      dispatch({
        type: Types.LOGIN,
        data: response.data.data.token,
      });
      alert.success(response.data.msg);
      const token = response.data.data.token;
      actionsLocalStorage.setItem('admin-token', token);
      onSuccess();
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_AUTHENTICATION, isLoading: false });
    }
  };
};
