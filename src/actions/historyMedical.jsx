import * as Types from "../constants/actionType";
import * as historyMedical from "../data/historyMedical";
import { alert } from "../utils/alerts";

export const getListHistoryMedical = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await historyMedical.getListHistoryMedical(params);
      dispatch({
        type: Types.GET_LIST_MEDICAL_HISTORY,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getDetailHistoryMedical = (idApoinment) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await historyMedical.getDetailHistoryMedical(
        idApoinment
      );
      dispatch({
        type: Types.GET_MEDICAL_HISTORY,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
