import * as Types from "../constants/actionType";
import * as videoCallApi from "../data/videoCall";
import { alert } from "../utils/alerts";

export const getHistoryVideoCall = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await videoCallApi.getHistoryVideoCall(params);
      dispatch({
        type: Types.GET_HISTORY_VIDEO_CALL,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
