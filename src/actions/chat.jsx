import * as Types from "../constants/actionType";
import * as chatApi from "../data/chat";
import { alert } from "../utils/alerts";
export const getListMessages = (idDoctor, params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_CHAT, isLoading: true });
    try {
      const response = await chatApi.getListMessages(idDoctor, params);
      dispatch({
        type: Types.GET_MESSAGES,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_CHAT, isLoading: false });
    }
  };
};
export const getChatConversation = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_CHAT, isLoading: true });
    try {
      const response = await chatApi.getChatConversation(params);
      dispatch({
        type: Types.GET_CHAT_CONVERSATIONS,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_CHAT, isLoading: false });
    }
  };
};
export const sendMessage = (idDoctor, form) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_CHAT, isLoading: true });
    try {
      const response = await chatApi.sendMessage(idDoctor, form);
      dispatch({
        type: Types.SEND_MESSAGE,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_CHAT, isLoading: false });
    }
  };
};
