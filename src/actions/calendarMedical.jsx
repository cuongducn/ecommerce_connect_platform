import * as Types from '../constants/actionType';
import * as calendarMedical from '../data/calendarMedical';
import * as badgesApi from '../data/badge';
import * as badgesAction from './badge';
import { alert } from '../utils/alerts';

export const getListAppointmentsMedical = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await calendarMedical.getListAppointmentsMedical(params);
      dispatch({
        type: Types.GET_LIST_APPOINTMENTS_MEDICAL,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getAppointmentMedical = (idApoinment) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await calendarMedical.getAppointmentMedical(idApoinment);
      dispatch({
        type: Types.GET_APPOINTMENT_MEDICAL,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const addAppointmentMedical = (
  data,
  navigate,
  removePath,
  funcModal
) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_APPOINTMENT, isLoading: true });
    try {
      const response = await calendarMedical.addAppointmentMedical(data);
      dispatch(badgesAction.getBadges());
      if (navigate) {
        navigate('/lich-kham');
      }
      if (funcModal) {
        funcModal();
      } else {
        if (removePath) {
          removePath();
        }
        alert.success('Đặt lịch khám thành công!');
      }
      dispatch({
        type: Types.ADD_APPOINTMENT_MEDICAL,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_APPOINTMENT, isLoading: false });
    }
  };
};

export const cancelAppointmentMedical = (idApoinment) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const responseCancel = await calendarMedical.cancelAppointmentMedical(
        idApoinment
      );
      dispatch({
        type: Types.CANCEL_APPOINTMENT_MEDICAL,
        data: responseCancel.data.data,
      });
      dispatch(getAppointmentMedical(idApoinment));
      alert.success(responseCancel.data.msg);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const paymentAppointmentMedical = (idApoinment, params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_APPOINTMENT, isLoading: true });
    try {
      const response = await calendarMedical.paymentAppointmentMedical(
        idApoinment
      );
      window.location.reload();
      // await badgesApi.getBadges();
      // await calendarMedical.getListAppointmentsMedical(params);

      dispatch({
        type: Types.PAYMENT_APPOINTMENT_MEDICAL,
        data: response.data.data,
      });
      alert.success(response.data.msg);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_APPOINTMENT, isLoading: false });
    }
  };
};
