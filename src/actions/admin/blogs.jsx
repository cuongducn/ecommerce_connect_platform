import * as Types from "../../constants/actionType";
import * as blogsApi from "../../data/admin/blogs";
import * as uploadApi from "../../data/admin/upload";

import { notification } from "antd";

export const listBlogs = (params) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_FETCH_ALL_BLOGS,
    });
    blogsApi
      .listBlogs(params)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.FETCH_ALL_BLOGS_SUCCESS,
            payload: res.data.data,
          });
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== "undefined") {
          dispatch({
            type: Types.FETCH_ALL_BLOGS_SUCCESS,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
          notification.error({ message: error?.response?.data?.msg });
        }
      });
  };
};

export const detailBlogs = (id) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_FETCH_BLOGS,
    });
    blogsApi
      .detailBlogs(id)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.FETCH_BLOGS_SUCCESS,
            payload: res.data.data,
          });
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== "undefined") {
          dispatch({
            type: Types.FETCH_BLOGS_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
        }
      });
  };
};

export const createBlogs = (val, reset) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_CREATE_BLOGS,
    });
    blogsApi
      .createBlogs(val)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.CREATE_BLOGS_SUCCESS,
            data: res.data.data,
          });
        notification.success({ message: "Tạo thành công!" });
        listBlogs()(dispatch);
        if (reset) {
          reset();
        }
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== "undefined") {
          dispatch({
            type: Types.CREATE_BLOGS_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
          notification.error({
            message: error?.response?.data?.msg,
          });
        }
      });
  };
};
export const updateBlogs = (val, action) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_UPDATE_BLOGS,
    });
    blogsApi
      .updateBlogs(val)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.UPDATE_BLOGS_SUCCESS,
            payload: res.data.data,
          });
        notification.success({ message: "Cập nhật thành công!" });
        listBlogs()(dispatch);
        if (action) action();
      })
      .catch(function (error) {
        let content = "";
        if (typeof error.response.data.msg !== "undefined") {
          content = error?.response?.data?.msg;
        }
        dispatch({
          type: Types.UPDATE_BLOGS_FAILED,
          payload: {
            content,
          },
        });
        notification.error({
          message: content,
        });
      });
  };
};

export const deleteBlogs = (id, action) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_DELETE_BLOGS,
    });
    blogsApi
      .deleteBlogs(id)
      .then((res) => {
        dispatch({
          type: Types.DELETE_BLOGS_SUCCESS,
          data: res.data.data,
        });
        listBlogs()(dispatch);
        notification.success({ message: "Xóa  thành công!" });
        if (action) action();
      })
      .catch(function (error) {
        let content = "";
        if (typeof error.response.data.msg !== "undefined") {
          content = error?.response?.data?.msg;
        }
        dispatch({
          type: Types.DELETE_BLOGS_FAILED,
          payload: {
            content,
          },
        });
        notification.error({
          message: "Xóa thất bại: ",
        });
      });
  };
};

export const uploadImgBlogs = (imgType, file) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_UPLOAD_IMAGE,
    });
    uploadApi
      .uploadMedia(file)
      .then((res) => {
        dispatch({
          type: Types.UPLOAD_IMAGE_SUCCESS,
          payload: {
            [imgType]: res.data.link,
          },
        });
      })
      .catch(function (error) {
        dispatch({
          type: Types.UPLOAD_IMAGE_FAILED,
          payload: {
            content: error?.response?.data?.msg,
          },
        });
      });
  };
};
export const uploadVideoBlogs = (imgType, file) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_UPLOAD_VIDEO,
    });
    uploadApi
      .uploadVideo(file)
      .then((res) => {
        dispatch({
          type: Types.UPLOAD_VIDEO_SUCCESS,
          payload: {
            [imgType]: res.data.link,
          },
        });
      })
      .catch(function (error) {
        dispatch({
          type: Types.UPLOAD_VIDEO_FAILED,
          payload: {
            content: error?.response?.data?.msg,
          },
        });
      });
  };
};
