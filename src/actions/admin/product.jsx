import * as Types from "../../constants/actionType";
import * as productApi from "../../data/admin/product";
import { alert } from "../../utils/alerts";

export const getListProducts = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await productApi.getListProducts(params);
      dispatch({
        type: Types.ADMIN_GET_LIST_PRODUCT,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response?.data?.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const getOneProduct = (idProduct) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await productApi.getOneProduct(idProduct);
      dispatch({
        type: Types.ADMIN_GET_ONE_PRODUCT,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const addProduct = (data, params, onSuccess) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await productApi.addProduct(data);
      if (onSuccess) {
        onSuccess();
      }
      alert.success(response.data.msg);
      await productApi.getListProducts(params);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const updateProduct = (id, data, params, onSuccess) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await productApi.updateProduct(id, data);
      if (onSuccess) {
        onSuccess();
      }
      alert.success(response.data.msg);
      await productApi.getListProducts(params);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const deleteProduct = (data, params, onSuccess) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await productApi.deleteProduct(data);
      if (onSuccess) onSuccess();
      alert.success(response.data.msg);
      const responseGet = await productApi.getListProducts(params);
      dispatch({
        type: Types.ADMIN_GET_LIST_PRODUCT,
        data: responseGet.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const updateStatusProduct = (id, data) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await productApi.updateStatusProduct(id, data);
      dispatch({
        type: Types.ADMIN_UPDATE_STATUS_PRODUCT,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
