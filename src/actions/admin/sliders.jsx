import * as Types from "../../constants/actionType";
import * as slidersApi from "../../data/admin/sliders";
import * as uploadApi from "../../data/admin/upload";

import { notification } from "antd";

export const listSliders = (params) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_FETCH_ALL_SLIDERS,
    });
    slidersApi
      .listSliders(params)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.FETCH_ALL_SLIDERS_SUCCESS,
            payload: res.data.data,
          });
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== "undefined") {
          dispatch({
            type: Types.FETCH_ALL_SLIDERS_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
          notification.error({ message: error?.response?.data?.msg });
        }
      });
  };
};

export const detailSliders = (id) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_FETCH_SLIDERS,
    });
    slidersApi
      .detailSliders(id)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.FETCH_SLIDERS_SUCCESS,
            payload: res.data.data,
          });
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== "undefined") {
          dispatch({
            type: Types.FETCH_SLIDERS_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
        }
      });
  };
};

export const createSliders = (val, reset) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_CREATE_SLIDERS,
    });
    slidersApi
      .createSliders(val)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.CREATE_SLIDERS_SUCCESS,
            data: res.data.data,
          });
        notification.success({ message: "Tạo thành công!" });
        listSliders()(dispatch);
        if (reset) {
          reset();
        }
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== "undefined") {
          dispatch({
            type: Types.CREATE_SLIDERS_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
          notification.error({
            message: error?.response?.data?.msg,
          });
        }
      });
  };
};

export const updateSliders = (val, action) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_UPDATE_SLIDERS,
    });
    slidersApi
      .updateSliders(val)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.UPDATE_SLIDERS_SUCCESS,
            payload: res.data.data,
          });
        notification.success({ message: "Cập nhật thành công!" });
        listSliders()(dispatch);
        if (action) action();
      })
      .catch(function (error) {
        let content = "";
        if (typeof error.response.data.msg !== "undefined") {
          content = error?.response?.data?.msg;
        }
        dispatch({
          type: Types.UPDATE_SLIDERS_FAILED,
          payload: {
            content,
          },
        });
        notification.error({
          message: content,
        });
      });
  };
};

export const deleteSliders = (id, action) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_DELETE_SLIDERS,
    });
    slidersApi
      .deleteSliders(id)
      .then((res) => {
        dispatch({
          type: Types.DELETE_SLIDERS_SUCCESS,
          data: res.data.data,
        });
        listSliders()(dispatch);
        notification.success({ message: "Xóa  thành công!" });
        if (action) action();
      })
      .catch(function (error) {
        let content = "";
        if (typeof error.response.data.msg !== "undefined") {
          content = error?.response?.data?.msg;
        }
        dispatch({
          type: Types.DELETE_SLIDERS_FAILED,
          payload: {
            content,
          },
        });
        notification.error({
          message: content,
        });
      });
  };
};

export const uploadImgSliders = (imgType, file) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_UPLOAD_IMAGE,
    });
    uploadApi
      .uploadMedia(file)
      .then((res) => {
        dispatch({
          type: Types.UPLOAD_IMAGE_SUCCESS,
          payload: {
            [imgType]: res.data.link,
          },
        });
      })
      .catch(function (error) {
        dispatch({
          type: Types.UPLOAD_IMAGE_FAILED,
          payload: {
            content: error?.response?.data?.msg,
          },
        });
      });
  };
};
