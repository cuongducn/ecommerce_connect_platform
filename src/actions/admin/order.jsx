import * as Types from "../../constants/actionType";
import * as orderApi from "../../data/admin/order";
import { alert } from "../../utils/alerts";

export const getListOrders = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await orderApi.getListOrders(params);
      dispatch({
        type: Types.ADMIN_GET_LIST_ORDER,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response?.data?.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const getOneOrder = (idProduct) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await orderApi.getOneOrder(idProduct);
      dispatch({
        type: Types.ADMIN_GET_ONE_ORDER,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const updateOrder = (id, data, params, onSuccess) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await orderApi.updateOrder(id, data);
      if (onSuccess) {
        onSuccess();
      }
      alert.success(response.data.msg);
      await orderApi.getListOrders(params);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const deleteOrder = (data, params, onSuccess) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await orderApi.deleteOrder(data);
      if (onSuccess) onSuccess();
      alert.success(response.data.msg);
      const responseGet = await orderApi.getListOrders(params);

      dispatch({
        type: Types.ADMIN_GET_LIST_ORDER,
        data: responseGet.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const updateStatusOrder = (id, data) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      await orderApi.updateStatusOrder(id, data);
      const response = await orderApi.getOneOrder(id);
      dispatch({
        type: Types.ADMIN_GET_ONE_ORDER,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const exportExcel = (id, data) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      await orderApi.exportExcel(id, data);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const exportBills = (id) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      await orderApi.exportBills(id);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
