import * as Types from "../../constants/actionType";
import * as categoryChildrenApi from "../../data/admin/categoryChildren";
import * as uploadApi from "../../data/admin/upload";

import { notification } from "antd";

export const listChildren = (params) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_FETCH_ALL_CHILDREN,
    });
    categoryChildrenApi
      .listChildren(params)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.FETCH_ALL_CHILDREN_SUCCESS,
            payload: res.data.data,
          });
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== "undefined") {
          dispatch({
            type: Types.FETCH_ALL_CHILDREN_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
          notification.error({ message: error?.response?.data?.msg });
        }
      });
  };
};
export const detailChildren = (id) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_FETCH_CHILDREN,
    });
    categoryChildrenApi
      .detailChildren(id)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.FETCH_CHILDREN_SUCCESS,
            payload: res.data.data,
          });
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== "undefined") {
          dispatch({
            type: Types.FETCH_CHILDREN_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
        }
      });
  };
};

export const createChildren = (val, reset) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_CREATE_CHILDREN,
    });
    categoryChildrenApi
      .createChildren(val)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.CREATE_CHILDREN_SUCCESS,
            data: res.data.data,
          });
        notification.success({ message: "Tạo thành công!" });
        listChildren()(dispatch);
        if (reset) {
          reset();
        }
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== "undefined") {
          dispatch({
            type: Types.CREATE_CHILDREN_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
          notification.error({
            message: error?.response?.data?.msg,
          });
        }
      });
  };
};
export const updateChildren = (val, action) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_UPDATE_CHILDREN,
    });
    categoryChildrenApi
      .UpdateChildren(val)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.UPDATE_CHILDREN_SUCCESS,
            payload: res.data.data,
          });
        notification.success({ message: "Cập nhật thành công!" });
        listChildren()(dispatch);
        if (action) action();
      })
      .catch(function (error) {
        let content = "";
        if (typeof error.response.data.msg !== "undefined") {
          content = error?.response?.data?.msg;
        }
        dispatch({
          type: Types.UPDATE_CHILDREN_FAILED,
          payload: {
            content,
          },
        });
        notification.error({
          message: content,
        });
      });
  };
};

export const deleteChildren = (id, action) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_DELETE_CHILDREN,
    });
    categoryChildrenApi
      .deleteChildren(id)
      .then((res) => {
        dispatch({
          type: Types.DELETE_CHILDREN_SUCCESS,
          data: res.data.data,
        });
        listChildren()(dispatch);
        notification.success({ message: "Xóa  thành công!" });
        if (action) action();
      })
      .catch(function (error) {
        let content = "";
        if (typeof error.response.data.msg !== "undefined") {
          content = error?.response?.data?.msg;
        }
        dispatch({
          type: Types.DELETE_CHILDREN_FAILED,
          payload: {
            content,
          },
        });
        notification.error({
          message: "Xóa thất bại: ",
        });
      });
  };
};
export const uploadImgChildren = (imgType, file) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_UPLOAD_IMAGE,
    });
    uploadApi
      .uploadMedia(file)
      .then((res) => {
        dispatch({
          type: Types.UPLOAD_IMAGE_SUCCESS,
          payload: {
            [imgType]: res.data.link,
          },
        });
      })
      .catch(function (error) {
        dispatch({
          type: Types.UPLOAD_IMAGE_FAILED,
          payload: {
            content: error?.response?.data?.msg,
          },
        });
      });
  };
};
