import * as Types from '../../constants/actionType';
import * as categoryApi from '../../data/admin/category';
import * as uploadApi from '../../data/admin/upload';

import { notification } from 'antd';

export const listCategories = (params) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_FETCH_ALL_CATEGORY,
    });
    categoryApi
      .listCategory(params)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.FETCH_ALL_CATEGORY_SUCCESS,
            payload: res.data.data,
          });
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== 'undefined') {
          dispatch({
            type: Types.FETCH_ALL_CATEGORY_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
          notification.error({ message: error?.response?.data?.msg });
        }
      });
  };
};
export const detailCategory = (id) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_FETCH_CATEGORY,
    });
    categoryApi
      .detailCategory(id)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.FETCH_CATEGORY_SUCCESS,
            payload: res.data.data,
          });
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== 'undefined') {
          dispatch({
            type: Types.FETCH_CATEGORY_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
        }
      });
  };
};

export const createCategory = (val, reset) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_CREATE_CATEGORY,
    });
    categoryApi
      .createCategory(val)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.CREATE_CATEGORY_SUCCESS,
            data: res.data.data,
          });
        notification.success({ message: 'Tạo thành công!' });
        listCategories()(dispatch);
        if (reset) {
          reset();
        }
      })
      .catch(function (error) {
        if (typeof error.response.data.msg !== 'undefined') {
          dispatch({
            type: Types.CREATE_CATEGORY_FAILED,
            payload: {
              content: error?.response?.data?.msg,
            },
          });
          notification.error({
            message: error?.response?.data?.msg,
          });
        }
      });
  };
};
export const UpdateCategory = (val, action) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_UPDATE_CATEGORY,
    });
    categoryApi
      .UpdateCategory(val)
      .then((res) => {
        if (res.data.code !== 401)
          dispatch({
            type: Types.UPDATE_CATEGORY_SUCCESS,
            payload: res.data.data,
          });
        notification.success({ message: 'Cập nhật thành công!' });
        listCategories()(dispatch);
        if (action) action();
      })
      .catch(function (error) {
        let content = '';
        if (typeof error.response.data?.msg !== 'undefined') {
          content = error?.response?.data?.msg;
        }
        dispatch({
          type: Types.UPDATE_CATEGORY_FAILED,
          payload: {
            content,
          },
        });
        notification.error({
          message: content,
        });
      });
  };
};

export const deleteCategory = (id, action) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_DELETE_CATEGORY,
    });
    categoryApi
      .deleteCategory(id)
      .then((res) => {
        dispatch({
          type: Types.DELETE_CATEGORY_SUCCESS,
          data: res.data.data,
        });
        listCategories()(dispatch);
        notification.success({ message: 'Xóa  thành công!' });
        action();
      })
      .catch(function (error) {
        let content = '';
        if (typeof error.response.data.msg !== 'undefined') {
          content = error?.response?.data?.msg;
        }
        dispatch({
          type: Types.DELETE_CATEGORY_FAILED,
          payload: {
            content,
          },
        });
        notification.error({
          message: 'Xóa thất bại: ',
        });
      });
  };
};
export const uploadImgCategory = (imgType, file) => {
  return (dispatch) => {
    dispatch({
      type: Types.INIT_UPLOAD_IMAGE,
    });
    uploadApi
      .uploadMedia(file)
      .then((res) => {
        dispatch({
          type: Types.UPLOAD_IMAGE_SUCCESS,
          payload: {
            [imgType]: res.data.link,
          },
        });
      })
      .catch(function (error) {
        dispatch({
          type: Types.UPLOAD_IMAGE_FAILED,
          payload: {
            content: error?.response?.data?.msg,
          },
        });
      });
  };
};
