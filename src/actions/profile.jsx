import * as Types from '../constants/actionType';
import * as profileApi from '../data/profile';
import { actionsLocalStorage } from '../utils/actionsLocalStorage';
import { alert } from '../utils/alerts';
export const getProfile = () => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const token = actionsLocalStorage.getItem('token');
      if (token) {
        const response = await profileApi.getProfile();
        dispatch({
          type: Types.GET_PROFILE,
          data: response.data.data,
        });
      } else {
        dispatch({
          type: Types.GET_PROFILE,
          data: null,
        });
      }
    } catch (error) {
      // alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const updateProfile = (form) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING_PROFILE, isLoading: true });
    try {
      const response = await profileApi.updateProfile(form);
      dispatch({
        type: Types.UPDATE_PROFILE,
        data: response.data.data,
      });
      alert.success(response.data.msg);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING_PROFILE, isLoading: false });
    }
  };
};
