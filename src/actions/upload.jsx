import * as uploadApi from "../data/upload";
import { alert } from "../utils/alerts";

export const uploadImage = async (form) => {
  try {
    const response = await uploadApi.uploadImage(form);
    return response.data.link;
  } catch (error) {
    alert.error(error.response?.data?.message);
    return "";
  }
};
