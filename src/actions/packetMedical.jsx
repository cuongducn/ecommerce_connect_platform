import * as Types from "../constants/actionType";
import * as packetMedicalApi from "../data/packetMedical";
import { alert } from "../utils/alerts";

export const getPacketsMedical = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await packetMedicalApi.getPacketsMedical(params);
      dispatch({
        type: Types.GET_PACKET_MEDICALS,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getPacketMedical = (idPacketMedical) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await packetMedicalApi.getPacketMedical(idPacketMedical);
      dispatch({
        type: Types.GET_PACKET_MEDICAL,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
