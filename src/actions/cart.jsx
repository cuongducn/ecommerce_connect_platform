import * as Types from '../constants/actionType';
import * as clinicApi from '../data/clinic';
import history from '../history';
import { actionsLocalStorage } from '../utils/actionsLocalStorage';
import { alert } from '../utils/alerts';

export const getCart = (item) => {
  return (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const cart = JSON.parse(actionsLocalStorage.getItem('cart'));
      if (cart == null) {
        const newCart = {
          cartItems: [],
          totalQuantity: 0,
          totalMoney: 0,
          totalItem: 0,
        };

        actionsLocalStorage.setItem('cart', JSON.stringify(newCart));
      }
      dispatch({
        type: Types.GET_CART,
        payload: JSON.parse(actionsLocalStorage.getItem('cart')),
      });
    } catch (error) {
      alert.error('Lỗi giỏ hàng');
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const addItemCart = (item) => {
  return (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      dispatch({
        type: Types.ADD_ITEM_CART,
        payload: item,
      });
    } catch (error) {
      alert.error('Lỗi giỏ hàng');
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const updateItemCart = (isIncrease = true) => {
  return (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      dispatch({
        type: Types.UPDATE_ITEM_CART,
        payload: isIncrease,
      });
    } catch (error) {
      alert.error('Lỗi giỏ hàng');
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const removeItemCart = (itemId) => {
  return (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      dispatch({
        type: Types.DELETE_ITEM_CART,
        payload: itemId,
      });
    } catch (error) {
      alert.error('Lỗi giỏ hàng');
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const clearCart = () => {
  return (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      dispatch({
        type: Types.CLEAR_CART,
        payload: null,
      });
    } catch (error) {
      alert.error('Lỗi giỏ hàng');
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
