import * as Types from '../constants/actionType';
import * as productApi from '../data/product';
import history from '../history';
import { alert } from '../utils/alerts';

export const getProducts = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await productApi.getProducts(params);
      dispatch({
        type: Types.GET_PRODUCTS,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response?.data?.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getProduct = (idproduct) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await productApi.getProduct(idproduct);
      dispatch({
        type: Types.GET_PRODUCT,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
