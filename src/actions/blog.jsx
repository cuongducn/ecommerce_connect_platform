import * as Types from '../constants/actionType';
import * as blogsApi from '../data/blog';

export const getBlogs = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await blogsApi.getBlogs(params);
      dispatch({
        type: Types.GET_BLOGS,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response?.data?.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const detailBlogs = (slug) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await blogsApi.getDetailBlog(slug);
      dispatch({
        type: Types.GET_BLOG,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
