import * as Types from '../constants/actionType';
import * as orderApi from '../data/order';
import { alert } from '../utils/alerts';

export const getListOrders = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await orderApi.getListOrders(params);
      dispatch({
        type: Types.GET_LIST_ORDER,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response?.data?.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const getOrders = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await orderApi.getOrders(params);
      dispatch({
        type: Types.GET_ORDERS,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response?.data?.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getOrder = (idOrder) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await orderApi.getOrder(idOrder);
      dispatch({
        type: Types.GET_ORDER,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getDetailOrder = (idApoinment) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await orderApi.getDetailOrder(idApoinment);
      dispatch({
        type: Types.GET_DETAIL_ORDER,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const addOrder = (data, params, onSuccess) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await orderApi.addOrder(data);
      dispatch({
        type: Types.ADD_ORDER,
        data: response.data.data,
      });
      if (onSuccess) {
        localStorage.removeItem('cart');

        onSuccess();
      }
      alert.success(response.data.msg);
      // await orderApi.getOrders(params);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
