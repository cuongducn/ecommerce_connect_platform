import * as Types from "../constants/actionType";
import * as dependPersonApi from "../data/dependPerson";
import { alert } from "../utils/alerts";

export const getListDependPerson = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await dependPersonApi.getListDependPerson(params);
      dispatch({
        type: Types.GET_LIST_DEPEND_PERSON,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getDependPerson = (idDependPerson) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await dependPersonApi.getDependPerson(idDependPerson);
      dispatch({
        type: Types.GET_DETAIL_DEPEND_PERSON,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const addDependPerson = (data, onClose) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await dependPersonApi.addDependPerson(data);
      dispatch({
        type: Types.ADD_DEPEND_PERSON,
        data: response.data.data,
      });
      alert.success("Thêm người đặt hẹn thành công!");
      onClose();
    } catch (error) {
      alert.error(error.response.data.msg || error.response.data.message);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const updateDependPerson = (idDependPerson, data, onClose) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await dependPersonApi.updateDependPerson(
        idDependPerson,
        data
      );
      dispatch({
        type: Types.UPDATE_DEPEND_PERSON,
        data: response.data.data,
      });
      alert.success("Cập nhật người đặt hẹn thành công!");
      onClose();
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const deleteDependPerson = (idDependPerson, funcModal) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      await dependPersonApi.deleteDependPerson(idDependPerson);
      alert.success("Xóa người đặt hẹn thành công!");
      funcModal();
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
