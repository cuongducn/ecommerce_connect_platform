import * as Types from "../constants/actionType";
import * as badgeApi from "../data/badge";
import { alert } from "../utils/alerts";
export const getBadges = () => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_BADGES, isLoading: true });
    try {
      const response = await badgeApi.getBadges();
      dispatch({
        type: Types.GET_BADGES,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_BADGES, isLoading: false });
    }
  };
};
