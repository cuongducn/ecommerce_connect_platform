import * as Types from '../constants/actionType';
import * as clinicApi from '../data/clinic';
import history from '../history';
import { alert } from '../utils/alerts';

export const getClinics = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await clinicApi.getClinics(params);
      dispatch({
        type: Types.GET_CLINICS,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response?.data?.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getClinic = (idClinic) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await clinicApi.getClinic(idClinic);
      dispatch({
        type: Types.GET_CLINIC,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getCalendarsClinic = (idClinic, params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await clinicApi.getCalendarsClinic(idClinic, params);
      dispatch({
        type: Types.GET_CALENDARS_CLINIC,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getDoctorsClinic = (idClinic, params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await clinicApi.getDoctorsClinic(idClinic, params);
      dispatch({
        type: Types.GET_DOCTORS_CLINIC,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
