import * as Types from "../constants/actionType";
import * as eWalletApi from "../data/eWallet";
import { alert } from "../utils/alerts";

export const getEWallet = () => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await eWalletApi.getEWallet();
      dispatch({
        type: Types.GET_WALLET,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getEWalletHistory = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await eWalletApi.getEWalletHistory(params);
      dispatch({
        type: Types.GET_WALLET_HISTORIES,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getListDepositWithdrawalsHistory = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await eWalletApi.getListDepositWithdrawalsHistory(
        params
      );
      dispatch({
        type: Types.GET_LIST_DEPOSIT_WITHDRAWALS,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const getDetailDepositWithdrawalsHistory = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await eWalletApi.getDetailDepositWithdrawalsHistory(
        params
      );
      dispatch({
        type: Types.GET_DETAIL_DEPOSIT_WITHDRAWALS,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const cancelDepositWithdrawals = (idHistory, data, query, funcModal) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await eWalletApi.cancelDepositWithdrawals(
        idHistory,
        data
      );
      funcModal();
      dispatch({
        type: Types.CANCEL_DEPOSIT_WITHDRAWALS,
        data: response.data.data,
      });
      dispatch(getListDepositWithdrawalsHistory(query));
      alert.success(response.data.msg);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
export const requireCancelDepositWithdrawals = (data, navigate) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await eWalletApi.requireCancelDepositWithdrawals(data);
      navigate("/vi-khach-hang/lich-su-nap-rut");
      alert.success(response.data.msg);
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
