import * as Types from "../constants/actionType";
import * as doctorApi from "../data/doctor";
import { alert } from "../utils/alerts";

export const getDoctors = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await doctorApi.getDoctors(params);
      dispatch({
        type: Types.GET_LIST_DOCTORS,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const getCalendarsInClinic = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await doctorApi.getCalendarsInClinic(params);
      dispatch({
        type: Types.GET_CALENDAR_DOCTOR_CLINIC,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};

export const getCalendarsIndividual = (params) => {
  return async (dispatch) => {
    dispatch({ type: Types.SHOW_LOADING, isLoading: true });
    try {
      const response = await doctorApi.getCalendarsIndividual(params);
      dispatch({
        type: Types.GET_CALENDAR_DOCTOR_INDIVIDUAL,
        data: response.data.data,
      });
    } catch (error) {
      alert.error(error.response.data.msg);
    } finally {
      dispatch({ type: Types.SHOW_LOADING, isLoading: false });
    }
  };
};
